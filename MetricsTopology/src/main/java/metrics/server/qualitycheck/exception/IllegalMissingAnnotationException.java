package metrics.server.qualitycheck.exception;

/**
 * Created by ali on 8/18/14.
 */


import metrics.server.qualitycheck.ArgumentsChecked;
import metrics.server.qualitycheck.Throws;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.annotation.Annotation;

/**
 * Thrown to indicate that a method has been passed with a class that does not have a required annotation.
 */
public class IllegalMissingAnnotationException extends RuntimeException {

    /**
     * Default message to indicate that the a given class must have an annotation.
     */
    protected static final String DEFAULT_MESSAGE = "Annotation is required on the passed class.";
    /**
     * Message to indicate that the the given class must be annotated with annotation '%s'.
     */
    protected static final String MESSAGE_WITH_ANNOTATION = "Class must have annotation '%s'.";
    /**
     * Message to indicate that the the given class with <em>name</em> must be annotated with annotation '%s'.
     */
    protected static final String MESSAGE_WITH_ANNOTATION_AND_CLASS = "Class '%s' must have annotation '%s'.";
    private static final long serialVersionUID = -8428891146741574807L;
    /**
     * Annotation to search on a class
     */
    @Nullable
    private final Class<? extends Annotation> annotation;

    /**
     * Class to check for an annotation
     */
    @Nullable
    private final Class<?> clazz;

    /**
     * Constructs an {@code IllegalMissingAnnotationException} with the default message
     * {@link IllegalMissingAnnotationException#DEFAULT_MESSAGE}.
     */
    public IllegalMissingAnnotationException() {
        super(DEFAULT_MESSAGE);
        this.annotation = null;
        this.clazz = null;
    }

    /**
     * Constructs an {@code IllegalMissingAnnotationException} with the message
     * {@link IllegalMissingAnnotationException#MESSAGE_WITH_ANNOTATION} including the name of the missing annotation.
     *
     * @param annotation the required annotation
     */
    public IllegalMissingAnnotationException(@Nonnull final Class<? extends Annotation> annotation) {
        super(format(annotation));
        this.annotation = annotation;
        this.clazz = null;
    }

    /**
     * Constructs a new exception with the message {@link IllegalMissingAnnotationException#MESSAGE_WITH_ANNOTATION}
     * including the name of the missing annotation.
     *
     * @param annotation the required annotation
     * @param cause      the cause (which is saved for later retrieval by the {@link Throwable#getCause()} method). (A
     *                   {@code null} value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public IllegalMissingAnnotationException(@Nonnull final Class<? extends Annotation> annotation, @Nullable final Throwable cause) {
        super(format(annotation), cause);
        this.annotation = annotation;
        this.clazz = null;
    }

    /**
     * Constructs an {@code IllegalMissingAnnotationException} with the message
     * {@link IllegalMissingAnnotationException#MESSAGE_WITH_ANNOTATION} including the name of the missing annotation.
     *
     * @param annotation the required annotation
     * @param clazz      the name of the class which does not have the required annotation
     */
    public IllegalMissingAnnotationException(@Nonnull final Class<? extends Annotation> annotation, @Nullable final Class<?> clazz) {
        super(format(annotation, clazz));
        this.annotation = annotation;
        this.clazz = clazz;
    }

    /**
     * Constructs a new exception with the message {@link IllegalMissingAnnotationException#MESSAGE_WITH_ANNOTATION}
     * including the name of the missing annotation.
     *
     * @param annotation the required annotation
     * @param clazz      the name of the class which does not have the required annotation
     * @param cause      the cause (which is saved for later retrieval by the {@link Throwable#getCause()} method). (A
     *                   {@code null} value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public IllegalMissingAnnotationException(@Nonnull final Class<? extends Annotation> annotation, @Nullable final Class<?> clazz,
                                             @Nullable final Throwable cause) {
        super(format(annotation, clazz), cause);
        this.annotation = annotation;
        this.clazz = clazz;
    }

    /**
     * Constructs a new exception with the default message {@link IllegalMissingAnnotationException#DEFAULT_MESSAGE}.
     *
     * @param cause the cause (which is saved for later retrieval by the {@link Throwable#getCause()} method). (A
     *              {@code null} value is permitted, and indicates that the cause is nonexistent or unknown.)
     */
    public IllegalMissingAnnotationException(@Nullable final Throwable cause) {
        super(DEFAULT_MESSAGE, cause);
        this.annotation = null;
        this.clazz = null;
    }

    /**
     * Returns the formatted string {@link IllegalMissingAnnotationException#MESSAGE_WITH_ANNOTATION} with the given
     * {@code annotation}.
     *
     * @param annotation the name of the required annotation
     * @return a formatted string of message with the given argument name
     */
    @ArgumentsChecked
    @Throws(NullPointerException.class)
    private static String format(@Nonnull final Class<? extends Annotation> annotation) {
        if (annotation == null) {
            throw new NullPointerException("annotation");
        }

        return String.format(MESSAGE_WITH_ANNOTATION, annotation.getName());
    }

    /**
     * Returns the formatted string {@link IllegalMissingAnnotationException#MESSAGE_WITH_ANNOTATION_AND_CLASS} with the
     * given {@code annotation} and {@code clazz}.
     *
     * @param annotation the required annotation
     * @param clazz      the name of the class which does not have the required annotation
     * @return a formatted string of message with the given argument name
     */
    @ArgumentsChecked
    @Throws(NullPointerException.class)
    private static String format(@Nonnull final Class<? extends Annotation> annotation, @Nullable final Class<?> clazz) {
        if (annotation == null) {
            throw new NullPointerException("annotation");
        }

        if (clazz != null) {
            return String.format(MESSAGE_WITH_ANNOTATION_AND_CLASS, clazz.getName(), annotation.getName());
        } else {
            return format(annotation);
        }
    }

    /**
     * Gives access to the required annotation.
     *
     * @return the annotation which was expected on a class and was not found
     */
    public Class<? extends Annotation> getMissingAnnotation() {
        return annotation;
    }

    /**
     * Gives access to the class which does not have a required annotation.
     *
     * @return the class which caused the exception by not having a required annotation
     */
    public Class<?> getClassWithoutAnnotation() {
        return this.clazz;
    }

}
