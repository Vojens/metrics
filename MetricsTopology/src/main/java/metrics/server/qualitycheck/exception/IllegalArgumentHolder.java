package metrics.server.qualitycheck.exception;

/**
 * Created by ali on 8/18/14.
 */

/**
 * This is a marker interface indicating that an exception also holds the value which caused the exception to be thrown.
 */
public interface IllegalArgumentHolder<T> {

    /**
     * Get access to the illegal argument causing the exception.
     *
     * @return the illegal argument which caused the exception to be thrown.
     */
    T getIllegalArgument();
}
