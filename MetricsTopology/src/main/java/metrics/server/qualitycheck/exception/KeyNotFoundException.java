package metrics.server.qualitycheck.exception;

/**
 * Created by ali on 8/27/14.
 */
public class KeyNotFoundException extends RuntimeException {
    /**
     * Creates a new KeyNotFoundException datatype.
     */
    public KeyNotFoundException(){
        super();
    }

    /**
     * Creates a new KeyNotFoundException datatype.
     *
     * @param ex the cause.
     */
    public KeyNotFoundException(Throwable ex){
        super(ex);
    }

    /**
     * Creates a new KeyNotFoundException datatype.
     *
     * @param msg the detail message.
     */
    public KeyNotFoundException(String msg){
        super(msg);
    }

    /**
     * Creates a new KeyNotFoundException datatype.
     *
     * @param msg the detail message.
     * @param ex  the cause.
     */
    public KeyNotFoundException(String msg, Throwable ex){
        super(msg, ex);
    }
}
