package metrics.server.qualitycheck;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by ali on 8/19/14.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Throws {

    /**
     * @return the classes that can be thrown
     */
    Class<? extends Throwable>[] value();

}
