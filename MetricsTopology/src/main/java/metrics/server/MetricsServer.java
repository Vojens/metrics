package metrics.server;

import com.google.common.collect.Maps;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import metrics.server.configuration.Configuration;
import metrics.server.container.IManager;
import metrics.server.container.Module;
import metrics.server.io.ampq.IAmqpConnectionManager;
import metrics.server.modules.DbConnectionManagerModule;
import metrics.server.modules.RabbitMqConnectionManagerModule;
import metrics.server.modules.XmppConnectionManagerModule;
import metrics.server.spi.DbConnectionManagerImpl;
import metrics.server.spi.RabbitMqConnectionManagerImpl;
import metrics.server.spi.XmppManagerImpl;
import metrics.server.storm.trident.topology.MetricsTopology;
import metrics.server.util.Utils;
import metrics.server.util.Version;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Created by ali on 7/6/14.
 */
public class MetricsServer {
    private static transient Logger logger = Logger.getLogger(MetricsServer.class.getName());
    private static MetricsServer instance;
    Map<String, IManager> managerMap = Maps.newHashMap();
    private Version version;
    private boolean initialized = false;
    private String name;
    private Date startDate;
    private Date stopDate;
    private ClassLoader loader;
    private Configuration configuration;
    /**
     * All metrics.server.modules loaded by this server
     */
    private Map<Class, Module> modules = new LinkedHashMap<Class, Module>();
    private MetricsTopology topology = null;

    public MetricsServer() {
        // We may only have one instance of the server running on the JVM
        if (instance != null) {
            throw new IllegalStateException("Metrics Server is already running");
        }
        instance = this;
        topology = new MetricsTopology();
        start();
//        XmppManager xmppManager = ((XmppManager)this.getManager("xmpp"));
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//        MultiUserChat muc = new MultiUserChat(xmppManager.getConnection(), "devteam_aramcoproject@conference.slim2");
//        try {
//            muc.join("Metrics 2.0");
//        } catch (SmackException.NoResponseException e) {
//            logger.error(e);
//        } catch (XMPPException.XMPPErrorException e) {
//            logger.error(e);
//        } catch (SmackException.NotConnectedException e) {
//            logger.error(e);
//        }
//        String severity =
//                "           Numerical         Severity\n" +
//                "             Code\n" +
//                "\n" +
//                "              0       Emergency: system is unusable\n" +
//                "              1       Alert: action must be taken immediately\n" +
//                "              2       Critical: critical conditions\n" +
//                "              3       Error: error conditions\n" +
//                "              4       Warning: warning conditions\n" +
//                "              5       Notice: normal but significant condition\n" +
//                "              6       Informational: informational messages\n" +
//                "              7       Debug: debug-level messages";
//        try {
//            String message = "[CRIT][2] - Harddrive (FileSystem) on localhost.localdomain has reached 88%." +"\n"+
//                    "An Email has been sent to ICT & VAN."+"\n"+
//                    "Propability of next level (Alert) within 1 hour is 0.67"+"\n"+
//                    "Next Alert is in 1 hour"        +"\n"+severity;
//            logger.info(message);
//            muc.sendMessage(message);
//        } catch (XMPPException e) {
//            e.printStackTrace();
//        } catch (SmackException.NotConnectedException e) {
//            e.printStackTrace();
//        }
    }

    /**
     * Returns a singleton instance of XMPPServer.
     *
     * @return an instance.
     */
    public static MetricsServer getInstance() {
        return instance;
    }

    public void start() {
        try {
            logger.info("initializing");
            System.out.println("initializing");
            initialize();


            // First load all the metrics.server.modules so that metrics.server.modules may access other metrics.server.modules while
            // being initialized
            logger.info("loadModules");
            loadModules();
//        // Initize all the metrics.server.modules
            logger.info("initModules");
            initModules();
//        // Start all the metrics.server.modules
            logger.info("startModules");
            startModules();


            logger.info("topology.start");
            //topology.test();
            checkNotNull(topology, "Topology cannot be null").start();
            //topology.start();

            //worker();
            startDate = new Date();
            stopDate = null;
            logger.info("Server is started.");
        } catch (Exception e) {
            logger.error("startup.error", e);
            //System.out.println(("startup.error"));
            shutdownServer();
        }


    }

    public void initialize() throws IOException {
        version = new Version(2, 0, 0, Version.ReleaseStatus.Release, 1);
        loader = Thread.currentThread().getContextClassLoader();
        URL configurationURL = this.getClass().getClassLoader().getResource("configuration.json");
        try {
            checkNotNull(configurationURL, "configuration url cannot be null");
        } catch (NullPointerException e) {
            logger.error(e, e);
            throw e;
        }

        //import static com.google.common.base.Preconditions.checkNotNull;

        //System.out.println(configurationURL);


//        Configuration conf = new Configuration();
//        Database db = new Database() ;
//        CassandraProvider cp = new CassandraProvider();
//        cp.setUsername("Metrics");
//        cp.setPassword("Metrics");
//        List<String> contactLists = new LinkedList<>() ;
//        contactLists.add("localhost");
//        contactLists.add("localhost");
//        cp.setContactPointList(contactLists);
//        db.setCassandraProvider(cp);
//        conf.setDatabase(db);

        //Utils.objectMapper.writeValue(System.out,conf);


        try {
            //System.out.println("trying to deserialize configurations");
            //logger.info("trying to deserialize configurations");
            configuration = Utils.objectMapper.readValue(configurationURL, Configuration.class);
            //logger.info("trying to deserialize configurations---end");
        } catch (IOException e) {
            logger.error("Error in deserializing/reading configurations", e);
            e.printStackTrace();
            //throw e;
        } catch (Exception e) {
            logger.error("Error in deserializing/reading configurations", e);
            e.printStackTrace();
        }
        Utils.configuration = configuration;
        initialized = true;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    private void loadModules() {
        // Load this module always last since we don't want to start listening for clients
        // before the rest of the metrics.server.modules have been started
        //System.out.println(RabbitMqConnectionManagerImpl.class.getName());
        loadModule(DbConnectionManagerImpl.class.getName(), new DbConnectionManagerModule());
        loadModule(XmppManagerImpl.class.getName(), new XmppConnectionManagerModule());
        loadModule(RabbitMqConnectionManagerImpl.class.getName(), new RabbitMqConnectionManagerModule());
        // Keep a reference to the internal component manager
        //componentManager = getComponentManager();
    }

    /**
     * Loads a module.
     *
     * @param module the name of the class that implements the Module interface.
     */
    private void loadModule(String module) {
        try {
            Class modClass = loader.loadClass(module);
            Module mod = (Module) modClass.newInstance();
            this.modules.put(modClass, mod);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
        }
    }

    private void loadModule(String module, AbstractModule abstractModule) {
        try {
            Class modClass = loader.loadClass(module);
            Injector injector = Guice.createInjector(abstractModule);
            Module mod = (Module) injector.getInstance(modClass);
            this.modules.put(modClass, mod);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
        }
    }

    private void initModules() {
        for (Module module : modules.values()) {
            boolean isInitialized = false;
            try {
                module.initialize(this);
                isInitialized = true;
            } catch (Exception e) {
                e.printStackTrace();
                // Remove the failed initialized module
                this.modules.remove(module.getClass());
                if (isInitialized) {
                    module.stop();
                    module.destroy();
                }
                //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
            }
        }
    }

    /**
     * <p>Following the loading and initialization of all the metrics.server.modules
     * this method is called to iterate through the known metrics.server.modules and
     * start them.</p>
     */
    private void startModules() {
        for (Module module : modules.values()) {
            boolean started = false;
            try {
                module.start();
            } catch (Exception e) {
                if (started && module != null) {
                    module.stop();
                    module.destroy();
                }
                //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
            }
        }
    }

    public void worker() {
//        checkNotNull(configuration.getInputList(),"configuration cannot be null");
//        for(IInput input : configuration.getInputList()){
//            input.register();
//        }

        //TODO start the topology
    }

    private void shutdownServer() {
//        // Notify server listeners that the server is about to be stopped
//        for (XMPPServerListener listener : listeners) {
//            listener.serverStopping();
//        }
//        // Shutdown the task engine.
//        TaskEngine.getInstance().shutdown();
//
//        // If we don't have metrics.server.modules then the server has already been shutdown
        if (modules.isEmpty()) {
            return;
        }
        // Get all metrics.server.modules and stop and destroy them
        for (Module module : modules.values()) {
            module.stop();
            module.destroy();
        }
        modules.clear();
//        // Stop all plugins
//        if (pluginManager != null) {
//            pluginManager.shutdown();
//        }
//        // Stop the Db connection manager.
//        DbConnectionManager.destroyConnectionProvider();
//        // hack to allow safe stopping
//        Log.info("Openfire stopped");
        logger.info("Metrics Server is stopped");
    }

    public void addManager(String managerName, IManager manager) {
        managerMap.put(managerName, manager);
    }

    public IManager getManager(String managerName) {
        return managerMap.get(managerName);
    }

    public IManager removeManager(String managerName) {
        return managerMap.remove(managerName);
    }

    /**
     * Returns the <code>ConnectionManager</code> registered with this server. The
     * <code>ConnectionManager</code> was registered with the server as a module while starting up
     * the server.
     *
     * @return the <code>ConnectionManager</code> registered with this server.
     */
    public IAmqpConnectionManager getConnectionManager() {
        return (IAmqpConnectionManager) modules.get(RabbitMqConnectionManagerImpl.class);
    }
}
