package metrics.server.spi;

import com.google.inject.Guice;
import com.google.inject.Injector;
import metrics.server.MetricsServer;
import metrics.server.container.BasicModule;
import metrics.server.database.CassandraConnectionProvider;
import metrics.server.database.DbConnectionManager;
import metrics.server.database.IConnectionProvider;
import metrics.server.modules.ConnectionProviderModule;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * Created by ali on 7/7/14.
 */
public class DbConnectionManagerImpl extends BasicModule implements ShutdownListener {


    public DbConnectionManagerImpl() {
        super("Database Connection Manager");

    }

    public DbConnectionManagerImpl(String moduleName) {
        super(moduleName);
    }

    @Override
    public void shutdownCompleted(ShutdownSignalException cause) {

    }

    @Override
    public void initialize(MetricsServer server) {
        super.initialize(server);
        Injector injector = Guice.createInjector(new ConnectionProviderModule());
        IConnectionProvider connectionProvider = injector.getInstance(CassandraConnectionProvider.class);
        DbConnectionManager.setConnectionProvider(connectionProvider);
    }

    /**
     * <p>Starts the basic module.</p>
     * <p/>
     * <p>Inheriting classes that choose to override this method MUST
     * call this start() method before accessing BasicModule resources.</p>
     *
     * @throws IllegalStateException If start is called before initialize
     *                               successfully returns
     */
    @Override
    public void start() throws IllegalStateException {
        super.start();
    }

    @Override
    public void destroy() {
        super.destroy();
        DbConnectionManager.close();
    }
}
