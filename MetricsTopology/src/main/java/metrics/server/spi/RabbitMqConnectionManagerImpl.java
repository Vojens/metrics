package metrics.server.spi;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provider;
import metrics.server.MetricsServer;
import metrics.server.configuration.Configuration;
import metrics.server.container.BasicModule;
import metrics.server.io.ampq.IAmpqConnectionFactory;
import metrics.server.io.ampq.IAmqpConnectionManager;
import metrics.server.io.ampq.RabbitMq.RabbitMq;
import metrics.server.io.ampq.RabbitMq.RabbitMqConnectionFactory;
import metrics.server.io.channel.ChannelCallable;
import metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig;
import metrics.server.util.Utils;
import com.rabbitmq.client.*;

import javax.inject.Named;
import java.net.ConnectException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

@Named
public class RabbitMqConnectionManagerImpl extends BasicModule implements IAmqpConnectionManager, ShutdownListener {
    protected static transient final Logger LOGGER = Logger.getLogger(RabbitMqConnectionManagerImpl.class.getName());

    @Inject @RabbitMq
    protected Provider<IAmpqConnectionFactory> factoryProvider;

    protected final ScheduledExecutorService executor;
    protected volatile Connection connection;
    protected IAmpqConnectionFactory iAmpqConnectionFactory;
    public RabbitMqConnectionManagerImpl(){
        super("RabbitMQ Connection Manager");
        executor = Executors.newSingleThreadScheduledExecutor();
    }

//    public RabbitMqConnectionManagerImpl(final IAmqpConnectionManager factory) {
//        super("Connection Manager");
//        this.factory = factory;
//        executor = Executors.newSingleThreadScheduledExecutor();
//        connection = null;
//    }


    @Override
    public String getName() {
        return null;
    }

    @Override
    public void initialize(MetricsServer agent) {
        checkNotNull(agent,"Metrics Server cannot be null.");
        //checkNotNull(agent.getConfiguration(), "Configurations cannot be null");
        //checkNotNull(agent.getConfiguration().getDataSourceList(), "Datasources in configurations cannot be null");

        iAmpqConnectionFactory = factoryProvider.get();
        RabbitMqConnectionFactory ampqFactory = new RabbitMqConnectionFactory();
        Configuration config = agent.getConfiguration();
        //DataSource rabbitMqDataSource = config.getDataSourcesMap().get("RabbitMq");
        //checkNotNull(rabbitMqDataSource, "There exists no data source with the name of RabbitMq");
        //ampqFactory.setUri(rabbitMqDataSource.getUri());


        String rabbitmq_host = Utils.getMetricsProperty("rabbitmq.host", ConnectionFactory.DEFAULT_HOST);
        int rabbitmq_port = Utils.getIntMetricsProperty("rabbitmq.port",ConnectionFactory.DEFAULT_AMQP_PORT);
        String rabbitmq_username = Utils.getMetricsProperty("rabbitmq.username",ConnectionFactory.DEFAULT_USER);
        String rabbitmq_password = Utils.getMetricsProperty("rabbitmq.password",ConnectionFactory.DEFAULT_PASS);
        String rabbitmq_virtualhost = Utils.getMetricsProperty("rabbitmq.virtualhost",ConnectionFactory.DEFAULT_VHOST);
        int rabbitmq_heartbeat = Utils.getIntMetricsProperty("rabbitmq.heartbeat",ConnectionFactory.DEFAULT_HEARTBEAT);
        metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig connectionConfig = new ConnectionConfig(
                rabbitmq_host,
                rabbitmq_port,
                rabbitmq_username,
                rabbitmq_password,
                rabbitmq_virtualhost,
                rabbitmq_heartbeat
        );

        iAmpqConnectionFactory.setConfMap(connectionConfig.asMap());


        //Configuration.DataSource dataSource = null;
//        loop: for (Configuration.DataSource tempDataSource : config.getDataSourceList()){
//            if(tempDataSource.getName().equals("RabbitMQ")){
//                dataSource = tempDataSource;
//                break loop;
//            }
//        }
//        if(dataSource == null){
//            //TODO throw an exception
//        }
//        try{
//            MapperSingleton.getInstance().map(dataSource, ampqFactory);
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//            System.out.println(e.getStackTrace());
//        }
        //factory = ampqFactory.get();
    }
    class ConnectionFactoryModule extends AbstractModule{

        @Override
        protected void configure() {

            //bindConstant().annotatedWith(Names.named("factory")).to;
        }
    }

    public void start() {
        try {
    //        connection = factory.newConnection();

                connection = ((RabbitMqConnectionFactory)iAmpqConnectionFactory).newConnection(Executors.newFixedThreadPool(10));   //TODO make it configurable
            connection.addShutdownListener(this);
      //      LOGGER.info("Connected to " + factory.getHost() + ":" + factory.getPort());
        } catch(final ConnectException e){
            //could not connect to rabbit mq "connection refused" check rabbitmq server
            e.printStackTrace();
        } catch(final Exception e) {
        //    LOGGER.log(Level.SEVERE, "Failed to connect to " + factory.getHost() + ":" + factory.getPort(), e);
            e.printStackTrace();
            asyncWaitAndReconnect();
        }
    }

    @Override
    public void shutdownCompleted(final ShutdownSignalException cause) {
        // reconnect only on unexpected errors
        if (!cause.isInitiatedByApplication()) {
          //  LOGGER.log(Level.SEVERE, "Lost connection to " + factory.getHost() + ":" + factory.getPort(),
            //        cause);

            connection = null;
            asyncWaitAndReconnect();
        }
    }

    protected void asyncWaitAndReconnect() {
        executor.schedule(new Runnable() {
            @Override
            public void run() {
                start();
            }
        }, 15, TimeUnit.SECONDS);
    }

    public void stop() {
        executor.shutdownNow();

        if (connection == null) {
            return;
        }

        try {
            connection.close();
        } catch (final Exception e) {
            LOGGER.log(Level.SEVERE, "Failed to close connection", e);
        } finally {
            connection = null;
        }
    }


    @Override
    public void destroy() {

    }

    public Channel createChannel() {
        try {
            return connection == null ? null : connection.createChannel();
        } catch (final Exception e) {
            LOGGER.log(Level.SEVERE, "Failed to create channel", e);
            return null;
        }
    }

    public void closeChannel(final Channel channel) {
        // isOpen is not fully trustable!
        if ((channel == null) || (!channel.isOpen())) {
            return;
        }

        try {
            channel.close();
        } catch (final Exception e) {
            LOGGER.log(Level.SEVERE, "Failed to close channel: " + channel, e);
        }
    }
    @Override
    public <T> T call(final ChannelCallable<T> callable) {
        final Channel channel = createChannel();

        if (channel != null) {
            try {
                return callable.call(channel);
            } catch (final Exception e) {
                LOGGER.log(Level.SEVERE, "Failed to run: " + callable.getDescription() + " on channel: "
                        + channel, e);
            } finally {
                closeChannel(channel);
            }
        }

        return null;
    }
}
