package metrics.server.spi;

import com.google.inject.Guice;
import com.google.inject.Injector;
import metrics.server.MetricsServer;
import metrics.server.container.BasicModule;
import metrics.server.container.IManager;
import metrics.server.io.xmpp.XmppManager;
import metrics.server.modules.XmppManagerModule;
import com.rabbitmq.client.ShutdownListener;
import com.rabbitmq.client.ShutdownSignalException;

/**
 * Created by ali on 8/12/14.
 */
public class XmppManagerImpl extends BasicModule implements ShutdownListener {


    public XmppManagerImpl() {
        super("XMPP Connection Manager");

    }

    /**
     * <p>Create a basic module with the given name.</p>
     *
     * @param moduleName The name for the module or null to use the default
     */
    public XmppManagerImpl(String moduleName) {
        super(moduleName);
    }

    @Override
    public void initialize(MetricsServer server) {
        super.initialize(server);
//        Injector injector = Guice.createInjector(new XmppConnectionProviderModule());
//        XmppConnectionProvider connectionProvider = injector.getInstance(XmppConnectionProvider.class);
//        XmppManager xmppManager = new XmppManager();
//        xmppManager.setConnectionProvider(connectionProvider);
//        server.addManager("xmpp",xmppManager);

        Injector injector = Guice.createInjector(new XmppManagerModule());
        IManager manager = injector.getInstance(XmppManager.class);
        server.addManager("xmpp", manager);
    }

    @Override
    public void shutdownCompleted(ShutdownSignalException cause) {

    }
}
