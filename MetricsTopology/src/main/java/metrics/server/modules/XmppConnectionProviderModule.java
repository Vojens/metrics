package metrics.server.modules;

import com.google.inject.AbstractModule;
import metrics.server.util.Utils;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

/**
 * Created by ali on 8/12/14.
 */
public class XmppConnectionProviderModule extends AbstractModule {
    @Override
    protected void configure() {
//        String username = Utils.getMetricsProperty("io.xmpp.conf.username");
//        String password = Utils.getMetricsProperty("io.xmpp.conf.password");

        String host = Utils.getMetricsProperty("io.xmpp.conf.host");
        int port = Utils.getIntMetricsProperty("io.xmpp.conf.port", 5222);
        ConnectionConfiguration connConf = new ConnectionConfiguration(host, port, host);
        connConf.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

        bind(ConnectionConfiguration.class).toInstance(connConf);
        bind(XMPPConnection.class).toInstance(new XMPPTCPConnection(connConf));


    }
}
