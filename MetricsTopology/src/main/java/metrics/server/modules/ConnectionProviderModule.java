package metrics.server.modules;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.net.InetAddresses;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import metrics.server.util.Utils;

import java.net.InetAddress;
import java.util.Collection;

/**
 * Created by ali on 7/7/14.
 */
public class ConnectionProviderModule extends AbstractModule {

    @Override
    protected void configure() {

        bindConstant().annotatedWith(Names.named("username")).to(Utils.configuration.getDatabase().getCassandraProvider().getUsername());
        bindConstant().annotatedWith(Names.named("password")).to(Utils.configuration.getDatabase().getCassandraProvider().getPassword());
        Collection<InetAddress> contactPoints =
                Lists.newArrayList(Iterables.transform(Utils.configuration.getDatabase().getCassandraProvider().getContactPointList(),
                        new Function<String, InetAddress>() {

                            @Override
                            public InetAddress apply(final String input) {

                                try {
                                    return InetAddresses.forString(input);
                                } catch (IllegalArgumentException e) {
                                    try {
                                        return InetAddress.getByName(input);
                                    } catch (Exception ex) {
                                        e.printStackTrace();
                                    }
                                }
                                return null;

                            }
                        }));
        bind(new TypeLiteral<Collection<InetAddress>>() {
        }).toInstance(contactPoints);

    }
}
