package metrics.server.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import metrics.server.io.xmpp.IConnectionProvider;
import metrics.server.io.xmpp.XmppConnectionProvider;
import metrics.server.util.Utils;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;

/**
 * Created by ali on 8/12/14.
 */
public class XmppManagerModule extends AbstractModule {
    @Override
    protected void configure() {
        Injector injector = Guice.createInjector(new XmppConnectionProviderModule());
        XmppConnectionProvider connectionProvider = injector.getInstance(XmppConnectionProvider.class);

        bind(IConnectionProvider.class).toInstance(connectionProvider);


        String host = Utils.getMetricsProperty("io.xmpp.conf.host");
        int port = Utils.getIntMetricsProperty("io.xmpp.conf.port", 5222);
        ConnectionConfiguration connConf = new ConnectionConfiguration(host, port, host);
        connConf.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        bind(ConnectionConfiguration.class).toInstance(connConf);
        bind(XMPPConnection.class).toInstance(new XMPPTCPConnection(connConf));


        String username = Utils.getMetricsProperty("io.xmpp.conf.username");
        String password = Utils.getMetricsProperty("io.xmpp.conf.password");
        bindConstant().annotatedWith(Names.named("username")).to(username);
        bindConstant().annotatedWith(Names.named("password")).to(password);
    }
}
