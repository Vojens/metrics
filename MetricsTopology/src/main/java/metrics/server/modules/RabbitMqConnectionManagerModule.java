package metrics.server.modules;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import metrics.server.io.ampq.IAmpqConnectionFactory;
import metrics.server.io.ampq.RabbitMq.RabbitMq;
import metrics.server.io.ampq.provider.AmpqConnectionFactoryProvider;
import metrics.server.io.ampq.provider.AmpqConnectionType;

/**
 * Created by ali on 6/9/14.
 */
public class RabbitMqConnectionManagerModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(IAmpqConnectionFactory.class).
                annotatedWith(RabbitMq.class).
                toProvider(AmpqConnectionFactoryProvider.class);
        bindConstant().annotatedWith(Names.named("provider type")).to(AmpqConnectionType.RabbitMq);



    }
}
