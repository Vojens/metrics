package metrics.server.lang;

/**
 * Created by ali on 8/24/14.
 */
public interface Cloneable<T> extends java.lang.Cloneable {
    T deepCopy();
}
