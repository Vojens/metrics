package metrics.server.database;

import com.datastax.driver.core.Session;
import metrics.server.util.MetricsGlobals;
import metrics.server.util.Utils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by ali on 7/6/14.
 */
public class KeySpaceManager {
    private static final String CHECK_VERSION_OLD =
            "SELECT minorVersion FROM MetricsVersion";
    private static final String CHECK_VERSION =
            "SELECT version FROM MetricsVersion WHERE name=?";
    private static final int DATABASE_VERSION = 1;
    private Logger logger = Logger.getLogger(KeySpaceManager.class.getName());

    public KeySpaceManager() {

    }

    public boolean checkMetricsKeySpace(Session session) {
        try {
            return checkKeySpace(session, Utils.KeySpace, DATABASE_VERSION,
                    new ResourceLoader() {
                        public InputStream loadResource(String resourceName) {
                            File file = new File(MetricsGlobals.getHomeDirectory() + File.separator +
                                    "resources" + File.separator + "metrics/server/database", resourceName);
                            try {
                                return new FileInputStream(file);
                            } catch (FileNotFoundException e) {
                                return null;
                            }
                        }
                    });
        } catch (Exception e) {
            logger.error(("upgrade.metrics.server.database.failure"), e);
        }
        return false;
    }

    public boolean checkKeySpace(Session session, String keyspace, int requiredVersion, ResourceLoader resourceLoader) {
        //TODO
        return false;
    }

    private static abstract class ResourceLoader {

        public abstract InputStream loadResource(String resourceName);

    }
}
