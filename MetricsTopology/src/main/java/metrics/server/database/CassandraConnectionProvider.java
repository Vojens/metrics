package metrics.server.database;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.util.Collection;
import java.util.LinkedList;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 7/6/14.
 */
public class CassandraConnectionProvider implements IConnectionProvider {

    protected Cluster cluster = null;
    @Inject
    @Named("username")
    protected String username = null;
    @Inject
    @Named("password")
    protected String password = null;
    @Inject
    protected Collection<InetAddress> contactPoints = new LinkedList<InetAddress>();
    protected Session originalSession = null;
    private static transient final Logger logger = Logger.getLogger(CassandraConnectionProvider.class.getName());
    private Object initLock = new Object();

    public CassandraConnectionProvider() {

    }
//    public void test(){
//        Cluster cluster = Cluster.builder().withCredentials("Metrics","Metrics").addContactPoint("localhost").build();
//
//        cluster.getMetadata();
//        Collection<InetAddress> contactPoints = new LinkedList<InetAddress>();
//        try {
//            contactPoints.add(InetAddress.getByName("localhost"));
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
//        cluster.connect();
//        cluster.newSession();
//    }

    @Override
    public Session getSession() {
        return originalSession;
    }

    @Override
    public void start() {
        synchronized (initLock) {
            try {
                cluster = Cluster.builder().withCredentials(username, password).addContactPoints(contactPoints).build();
                originalSession = cluster.connect();
            } catch (Exception e) {
                logger.error(e, e);
            }
        }
    }

    public Session getNewSession() {
        checkNotNull(cluster);
        return cluster.newSession();
    }

    @Override
    public void restart() {
        close();
        start();
    }


    @Override
    public void close() {
        if (getSession() != null) {
            try {
                getSession().close();
            } catch (Exception e) {
            }
        }
        if (cluster != null) {
            try {
                cluster.close();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public Metadata getMetaData() {
        return cluster.getMetadata();
    }

}
