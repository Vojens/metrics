package metrics.server.database;

import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;

/**
 * Created by ali on 7/6/14.
 */
public interface IConnectionProvider {

    public Session getSession();

    public void start();

    public void restart();

    public void close();

    public Metadata getMetaData();
}
