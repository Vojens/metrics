package metrics.server.database;


import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import metrics.server.container.IManager;
import metrics.server.util.Utils;
import org.apache.log4j.Logger;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 7/2/14.
 */
public class DbConnectionManager implements IManager {
    private final transient static Logger logger = Logger.getLogger(DbConnectionManager.class.getName());
    private static final Object providerLock = new Object();
    @Inject
    private static IConnectionProvider connectionProvider;
    private static KeySpaceManager schemaManager = new KeySpaceManager();
    private static Map<String, Session> sessionMap = Maps.newHashMap();

    public static Session getSession() {
        if (connectionProvider == null) {
            synchronized (providerLock) {
                if (connectionProvider == null) {
                    try {
                        setConnectionProvider(new CassandraConnectionProvider());
                    } catch (Exception e) {
                        logger.error(e, e);
                    }
                }
            }
        }
        Session session = connectionProvider.getSession();

        if (session == null) {
            logger.error("WARNING: ConnectionManager.getConnection() " +
                    "failed to obtain a connection.");
        }

        return session;
    }

    public static Session getSession(String keyspace) {
        if (keyspace.equalsIgnoreCase(Utils.KeySpace)) {
            return getSession();
        } else if (connectionProvider instanceof CassandraConnectionProvider) {
            Session session = sessionMap.get(keyspace.toLowerCase());
            if (session != null) {
                session = ((CassandraConnectionProvider) connectionProvider).getNewSession();
                sessionMap.put(keyspace.toLowerCase(), session);
            }
            return session;
        }
        return null;
    }

    public static void setConnectionProvider(IConnectionProvider provider) {
        synchronized (providerLock) {
            if (connectionProvider != null) {
                connectionProvider.close();
                connectionProvider = null;
            }
            connectionProvider = provider;
            connectionProvider.start();
            // Now, get a connection to determine meta data.
            Session con = null;
            try {
                con = connectionProvider.getSession();

                setMetaData(connectionProvider.getMetaData());

                // Check to see if the metrics.server.database schema needs to be upgraded.
                schemaManager.checkMetricsKeySpace(con);
            } catch (Exception e) {
                logger.error(e, e);
            } finally {
//                try {
//                    if (con != null) {
//                        con.close();
//                    }
//                }
//                catch (Exception e) {
//                    logger.error(e);
//                }
            }
        }
        // Remember what connection provider we want to use for restarts.
        //JiveGlobals.setXMLProperty("connectionProvider.className", provider.getClass().getName());
    }

    public static void setMetaData(Metadata metaData) {
        //TODO to be implemented
        //String databaseType = metaData.
    }

    public static void close() {
        synchronized (providerLock) {
            checkNotNull(connectionProvider);
            try {
                connectionProvider.close();
            } catch (Exception e) {
                logger.error(e, e);
            }
            connectionProvider = null;
        }

    }

}
