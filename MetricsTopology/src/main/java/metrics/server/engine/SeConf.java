package metrics.server.engine;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by ali on 8/18/14.
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface SeConf {
    /**
     * Specifies whether the generated class should cache each instance's {@link Object#hashCode
     * hashCode} value in a field once it is first computed. <b>Note:</b> most classes should have
     * no need of this behavior and should omit this parameter entirely. Use only if certain of its
     * performance benefit to your application.
     * <p/>
     * <p><b>Warning:</b> while using mutable field types is strongly discouraged in general, using
     * this feature makes it <i>especially</i> dangerous.
     */
    boolean cacheHashCode() default false;
}
