package metrics.server.engine;

import com.google.common.base.Optional;
import metrics.server.mapping.scriptengine.ICalculationScriptEngineConf;
import metrics.server.qualitycheck.exception.IllegalMissingAnnotationException;
import metrics.server.util.helper.AnnotationHelper;
import metrics.server.util.singleton.TypeConverterManagerBeanSingleton;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/18/14.
 */
public class ScriptEngineConf implements Serializable {
    private static final long serialVersionUID = 1287662985839117327L;
    private static transient Logger logger = Logger.getLogger(ScriptEngineConf.class.getName());

    private String type;

    private String name;

    private Map<String, String> engineMap;

    private Map<String, String> engineOptionMap;

    private Map<String, String> engineBindingMap;

    private String script;

    public ScriptEngineConf() {

    }

    public ScriptEngineConf(ICalculationScriptEngineConf csec) {
        checkNotNull(csec, "CalculationScriptEngineConf param cannot be null");

        this.setType(csec.getType());
        this.setName(csec.getName());
        this.setEngineMap(csec.getEngineMap());
        this.setEngineOptionSet(csec.getEngineOptionMap());
        this.setEngineBindingMap(csec.getEngineBindingMap());
        this.setScript(csec.getScript());
    }

    public <T> ScriptEngineConf(T conf, Class<T> classType) {
        if (!AnnotationHelper.getClassAnnotationOptional(classType).isPresent()) {
            throw new IllegalMissingAnnotationException(SeConf.class);
        }

        Optional<Field> declaredFieldsAnnotationOptional = AnnotationHelper.getDeclaredFieldsAnnotationOptional(classType, SeParam.class);

        try {
            if (declaredFieldsAnnotationOptional.isPresent()) {
                Set<Field> fields = declaredFieldsAnnotationOptional.asSet();
                for (Field field : fields) {
                    Optional<Annotation> fieldAnnotationOptional = AnnotationHelper.getAnnotationOptional(SeParam.class, field.getDeclaredAnnotations());
                    if (fieldAnnotationOptional.isPresent()) {
                        Set<Annotation> annotations = fieldAnnotationOptional.asSet();
                        for (Annotation annotation : annotations) {
                            SeParam SeParamAnnotation = (SeParam) annotation;
                            switch (SeParamAnnotation.value()) {
                                case TYPE:
                                    this.setType(TypeConverterManagerBeanSingleton.getInstance().convertType(field.get(conf), String.class));
                                    break;
                                case NAME:
                                    this.setName(TypeConverterManagerBeanSingleton.getInstance().convertType(field.get(conf), String.class));
                                    break;
                                case ENGINE_MAP:
                                    this.setEngineMap(TypeConverterManagerBeanSingleton.getInstance().convertType(field.get(conf), Map.class));
                                    break;
                                case ENGINE_OPTION_MAP:
                                    this.setEngineOptionSet(TypeConverterManagerBeanSingleton.getInstance().convertType(field.get(conf), Map.class));
                                    break;
                                case ENGINE_BINDING_MAP:
                                    this.setEngineBindingMap(TypeConverterManagerBeanSingleton.getInstance().convertType(field.get(conf), Map.class));
                                    break;
                                case SCRIPT:
                                    this.setScript(TypeConverterManagerBeanSingleton.getInstance().convertType(field.get(conf), String.class));
                                    break;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e, e);
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getEngineMap() {
        return engineMap;
    }

    public void setEngineMap(Map<String, String> engineMap) {
        this.engineMap = engineMap;
    }

    public Map<String, String> getEngineOptionMap() {
        return engineOptionMap;
    }

    public void setEngineOptionSet(Map<String, String> engineOptionMap) {
        this.engineOptionMap = engineOptionMap;
    }

    public Map<String, String> getEngineBindingMap() {
        return engineBindingMap;
    }

    public void setEngineBindingMap(Map<String, String> engineBindingMap) {
        this.engineBindingMap = engineBindingMap;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public enum Type {
        TYPE,
        NAME,
        ENGINE_MAP,
        ENGINE_OPTION_MAP,
        ENGINE_BINDING_MAP,
        SCRIPT
    }
}
