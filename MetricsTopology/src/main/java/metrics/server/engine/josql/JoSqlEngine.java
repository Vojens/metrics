package metrics.server.engine.josql;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import metrics.server.engine.ICustomCompiledScript;
import metrics.server.engine.ICustomEngine;
import metrics.server.engine.ScriptEngineConf;
import metrics.server.util.CacheUtil;
import metrics.server.util.helper.IOHelper;
import metrics.server.util.singleton.TypeConverterManagerBeanSingleton;
import metrics.server.util.type.SerializableStringTemplateParser;
import jodd.util.StringTemplateParser;
import org.apache.log4j.Logger;
import org.josql.Query;
import org.josql.QueryResults;

import javax.annotation.Nullable;
import javax.script.*;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/19/14.
 */
public class JoSqlEngine extends AbstractScriptEngine implements Compilable, ICustomEngine<ScriptEngineConf> {
    private static final transient Logger logger = Logger.getLogger(JoSqlEngine.class.getName());
    //protected ScriptEngine engine;
    protected volatile JoSqlEngineFactory factory;
    private ScriptEngineConf conf;


    //public static final String some = "4571e871-8f63-403c-ab7e-c4da5f8bd8c3";

    @Override
    public Object eval(String script, ScriptContext context)
            throws ScriptException {
        return evalScript(script, context);

    }

    private Object evalScript(String script, ScriptContext scriptContext) {
        checkNotNull(conf, "Conf cannot be null");
        Query q = new Query();

        try {
            q.parse(script);
            Bindings engineBindings = scriptContext.getBindings(ScriptContext.ENGINE_SCOPE);
            List list = (List) engineBindings.get(LIST_NAME);

            Bindings globalBinding = scriptContext.getBindings(ScriptContext.GLOBAL_SCOPE);


            //region set variables
            final SerializableStringTemplateParser sstp = new SerializableStringTemplateParser();
            Map<String, Object> variableMap = Maps.filterKeys(globalBinding, new Predicate<String>() {
                @Override
                public boolean apply(@Nullable String input) {

                    String parsed = sstp.parse(input, new StringTemplateParser.MacroResolver() {
                        @Override
                        public String resolve(String macroName) {
                            if (macroName.startsWith("Variable_")) {
                                return macroName.replaceFirst("Variable_", "");
                            }
                            return null;
                        }
                    });

                    if (!parsed.equals(input)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            for (Map.Entry<String, Object> stringObjectEntry : variableMap.entrySet()) {
                q.setVariable(stringObjectEntry.getKey(), stringObjectEntry.getValue());
            }
            //endregion

            QueryResults queryResults = q.execute(list);

            //region check save values

            Map<String, Object> saveValueMap = Maps.filterKeys(globalBinding, new Predicate<String>() {
                @Override
                public boolean apply(@Nullable String input) {

//                    String parsed = sstp.parse(input, new StringTemplateParser.MacroResolver() {
//                        @Override
//                        public String resolve(String macroName) {
//                            if (macroName.startsWith("SaveValue_")) {
//                                return macroName.replaceFirst("Variable_", "");
//                            }
//                            return null;
//                        }
//                    });

                    if (input.startsWith("SaveValue_")) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            //endregion

            if (saveValueMap.isEmpty()) {
                //TODO get toString out of queryResult
                return queryResults;
            } else {
                if (saveValueMap.size() == 1) {
                    Set<Map.Entry<String, Object>> entries = saveValueMap.entrySet();
                    Map.Entry<String, Object> saveValueEntry = (Map.Entry<String, Object>) saveValueMap.entrySet().toArray()[0];
                    String id = saveValueEntry.getKey().replaceAll("SaveValue_", "");
                    Map saveValues = queryResults.getSaveValues();
                    Object saveValue = saveValues.get(id);
                    if (saveValue instanceof Map.Entry) {
                        saveValue = ((Map.Entry) saveValue).getValue();
                    }

                    if (saveValue == null) {
                        //TODO get default value and insert it instead
                        Object defaultValue = globalBinding.get("DefaultValue_" + id);
                        if (defaultValue == null) {
                            logger.warn("There exists no DefaultValue in the configuration map. check the database");
                        }
                        saveValue = defaultValue;
                    }
                    if (saveValueEntry.getValue() != null && !saveValueEntry.getValue().toString().isEmpty()) {
                        saveValue = TypeConverterManagerBeanSingleton.getInstance().convertType(saveValue, Class.forName(saveValueEntry.getValue().toString()));
                    }
                    return saveValue;
                } else {
                    //TODO deal with multiple returns and such things
                }
            }
        } catch (Exception e) {
            logger.error(e, e);
        }
        return null;

    }


    private Map<String, Object> getVariables(ScriptContext scriptContext) {

        Map<String, Object> variables = new HashMap<String, Object>();

        if (scriptContext.getBindings(ScriptContext.GLOBAL_SCOPE) != null) {
            variables.putAll(scriptContext
                    .getBindings(ScriptContext.GLOBAL_SCOPE));
        }

        if (scriptContext.getBindings(ScriptContext.ENGINE_SCOPE) != null) {
            variables.putAll(scriptContext
                    .getBindings(ScriptContext.ENGINE_SCOPE));
        }

        return variables;

    }


    @Override
    public Object eval(Reader reader, ScriptContext context)
            throws ScriptException {
        return eval(IOHelper.readFully(reader), context);
    }

    @Override
    public Bindings createBindings() {
        return new SimpleBindings();
    }

    @Override
    public ScriptEngineFactory getFactory() {
        if (factory == null) {
            synchronized (this) {
                if (factory == null) {
                    factory = new JoSqlEngineFactory();
                }
            }
        }
        return factory;
    }

    @Override
    public ScriptEngineConf getConf() {
        return this.conf;
    }

    @Override
    public void setConf(ScriptEngineConf conf) {
        this.conf = conf;
    }

    @Override
    public CompiledScript compile(String script) throws ScriptException {
        return new JoSqlScript(script);
    }

    @Override
    public CompiledScript compile(Reader script) throws ScriptException {
        return compile(IOHelper.readFully(script));
    }

    private class JoSqlScript extends CompiledScript implements ICustomCompiledScript {

        private String script;

        public JoSqlScript(String script) {
            String fullyQualifiedName = null;
            try {
                fullyQualifiedName = CacheUtil.getMappingClassCache().get(conf.getType()).getKey().getName();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            final Map<String, String> map = Maps.newHashMap();
            map.put("Class", fullyQualifiedName);
            SerializableStringTemplateParser sstp = new SerializableStringTemplateParser();
            this.script = sstp.parse(script, new StringTemplateParser.MacroResolver() {
                @Override
                public String resolve(String macroName) {
                    return map.get(macroName);
                }
            });
            Bindings bindings = createBindings();
            bindings.putAll(conf.getEngineBindingMap());
            setBindings(bindings, ScriptContext.GLOBAL_SCOPE);

        }

        @Override
        public Object eval(ScriptContext context) throws ScriptException {
            return evalScript(script, context);
        }

        @Override
        public ScriptEngine getEngine() {
            return JoSqlEngine.this;
        }

    }
}
