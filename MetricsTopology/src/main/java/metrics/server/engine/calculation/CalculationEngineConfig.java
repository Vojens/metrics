package metrics.server.engine.calculation;

import com.datastax.driver.core.ResultSet;
import com.google.auto.value.AutoValue;
import metrics.server.util.helper.ColumnDataType;

import java.util.List;
import java.util.Map;

/**
 * Created by ali on 8/6/14.
 */
@AutoValue
public abstract class CalculationEngineConfig {
    private Object result;

    public static CalculationEngineConfig create(final String type, final String name, final List list, final Class<?> beanClass) {
        return new AutoValue_CalculationEngineConfig(type, name, list, beanClass);
    }

    public abstract String getType();

    public abstract String getName();

    public abstract List getList();

    public abstract Class<?> getBeanClass();

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    private Map<ColumnDataType,ResultSet> tableResultSetMap;

    public Map<ColumnDataType, ResultSet> getTableResultSetMap() {
        return tableResultSetMap;
    }

    public void setTableResultSetMap(Map<ColumnDataType, ResultSet> tableResultSetMap) {
        this.tableResultSetMap = tableResultSetMap;
    }
}
