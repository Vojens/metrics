package metrics.server.engine.java;

import com.esotericsoftware.minlog.Log;
import metrics.server.engine.ICustomCompiledScript;
import metrics.server.engine.ICustomEngine;
import metrics.server.engine.ScriptEngineConf;
import metrics.server.util.helper.IOHelper;
import org.apache.log4j.Logger;

import javax.script.*;
import javax.tools.*;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by ali on 8/18/14.
 */
public class JavaEngine extends AbstractScriptEngine implements Compilable, ICustomEngine<ScriptEngineConf> {
    private static final transient Logger logger = Logger.getLogger(JavaEngine.class.getName());
    protected volatile JavaEngineFactory factory;
    protected String className;
    protected Class<?> javaScriptClass = null;
    protected Method evalMethod = null;
    private ScriptEngineConf conf;

    @Override
    public Object eval(String script, ScriptContext context) throws ScriptException {
//        Class<?> javaScriptClass = null;
//
//        try {
//            javaScriptClass = Class.forName(className);
//            evalMethod = javaScriptClass.getDeclaredMethod("eval", List.class);
//        } catch (ClassNotFoundException | NoSuchMethodException e) {
//            logger.error(e);
//            return null;
//        }
//        evalMethod
//        IJavaScript javaScript = ()
        if(evalMethod == null){
            Log.error("eval method is not initiated for class "+className);
            return null;
        }
        Bindings engineBindings = context.getBindings(ScriptContext.ENGINE_SCOPE);
        List list = (List) engineBindings.get(LIST_NAME);
        Object returnedValue = null;
        try {
            returnedValue = evalMethod.invoke(javaScriptClass.newInstance(),list);
        } catch (IllegalAccessException |InvocationTargetException | InstantiationException e) {
            logger.error(e);
        }
        return returnedValue;
    }

    @Override
    public Object eval(Reader reader, ScriptContext context) throws ScriptException {
        return eval(IOHelper.readFully(reader), context);
    }

    @Override
    public Bindings createBindings() {
        return null;
    }

    @Override
    public ScriptEngineFactory getFactory() {
        if (factory == null) {
            synchronized (this) {
                if (factory == null) {
                    factory = new JavaEngineFactory();
                }
            }
        }
        return factory;
    }

    @Override
    public CompiledScript compile(String script) throws ScriptException {

        JavaCompiledScript javaCompiledScript = new JavaCompiledScript(script);
        javaCompiledScript.compile();
        className = javaCompiledScript.getClassName();
        initInvokingMethod();
        return javaCompiledScript;
    }

    public boolean initInvokingMethod(){
        try {
            javaScriptClass = Class.forName(className);
            evalMethod = javaScriptClass.getDeclaredMethod("eval", List.class);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    @Override
    public CompiledScript compile(Reader script) throws ScriptException {
        return compile(IOHelper.readFully(script));
    }

    @Override
    public ScriptEngineConf getConf() {
        return conf;
    }

    @Override
    public void setConf(ScriptEngineConf conf) {
        this.conf = conf;
    }


    private class JavaSourceFromString extends SimpleJavaFileObject {
        final String code;

        public JavaSourceFromString(String name, String code) {
            super(URI.create("string:///" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.code = code;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return code;
        }
    }

    private class JavaCompiledScript extends CompiledScript implements ICustomCompiledScript {


        private String className;
        private String content;
        //private String method
        public JavaCompiledScript(String contents) {
            this.content = content;


            Bindings bindings = createBindings();
            bindings.putAll(conf.getEngineBindingMap());
            setBindings(bindings, ScriptContext.GLOBAL_SCOPE);
            className = conf.getEngineMap().get("class_name");
            //compile();
            // Compile source file.
//            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
//            DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
//            JavaFileObject file = new JavaSourceFromString("HelloWorld", content);
//            Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(file);
//            JavaCompiler.CompilationTask task = compiler.getTask(null, null, diagnostics, null, null, compilationUnits);
//            boolean success = task.call();
        }

        private boolean compile(){


            // for compilation diagnostic message processing on compilation WARNING/ERROR


            SimpleJavaFileObject javaFileObject = new InMemoryJavaFileObject(getClassName(),content);//TODO change
            return JavaEngine.compile(javaFileObject);
        }

        @Override
        public Object eval(ScriptContext context) throws ScriptException {

            //return evalKnowledgeBase(knowledgeBaseConf, context);

            return null;
        }

        @Override
        public ScriptEngine getEngine() {
            return JavaEngine.this;
        }

        public String getClassName() {
            return className;
        }
    }


//    public static class MyDiagnosticListener implements DiagnosticListener<JavaFileObject>
//    {
//        public void report(Diagnostic<? extends JavaFileObject> diagnostic)
//        {
//
//            System.out.println("Line Number->" + diagnostic.getLineNumber());
//            System.out.println("code->" + diagnostic.getCode());
//            System.out.println("Message->"
//                    + diagnostic.getMessage(Locale.ENGLISH));
//            System.out.println("Source->" + diagnostic.getSource());
//            System.out.println(" ");
//        }
//    }

    /** java File Object represents an in-memory java source file <br>
     * so there is no need to put the source file on hard disk  **/
    public static class InMemoryJavaFileObject extends SimpleJavaFileObject
    {
        private String contents = null;

        public InMemoryJavaFileObject(String className, String contents)
        {
            super(URI.create("string:///" + className.replace('.', '/')
                    + Kind.SOURCE.extension), Kind.SOURCE);
            this.contents = contents;
        }

        public boolean compile(){
            return JavaEngine.compile(this);
        }

        public CharSequence getCharContent(boolean ignoreEncodingErrors)
                throws IOException
        {
            return contents;
        }
    }
    /** compile your files by JavaCompiler */
    public static boolean compile(SimpleJavaFileObject javaFileObject)
    {//get system compiler:
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<JavaFileObject>();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics,
                Locale.ENGLISH,
                null);  //TODO: configurable in map
        //specify classes output folder

        Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(javaFileObject);
        JavaCompiler.CompilationTask task = compiler.getTask(null, null, diagnostics, null, null, compilationUnits);

        boolean success = task.call();
        return success;
    }



}
