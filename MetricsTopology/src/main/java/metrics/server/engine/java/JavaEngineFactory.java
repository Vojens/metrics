package metrics.server.engine.java;

import metrics.server.engine.drools.DroolsEngine;
import org.drools.core.util.Drools;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ali on 8/18/14.
 */
public class JavaEngineFactory implements ScriptEngineFactory {
    private static final String SHORT_NAME = "jdk";
    private static final String LANGUAGE_NAME = "Java JDK";
    private static final List<String> NAMES;
    private static final List<String> EXTENSIONS;
    private static final List<String> MIME_TYPES;
    static {
        List<String> n = new ArrayList(2);
        n.add(SHORT_NAME);
        n.add(LANGUAGE_NAME);
        NAMES = Collections.unmodifiableList(n);

        n = new ArrayList<String>(1);
        n.add("jdk");
        EXTENSIONS = Collections.unmodifiableList(n);

        n = new ArrayList<String>(1);
        n.add("application/x-java_jdk");
        MIME_TYPES = Collections.unmodifiableList(n);
    }
    //protected static Version version = new Version(1, 0, 0, Version.ReleaseStatus.Alpha, 0);
    protected static String version = System.getProperty("java.version");

    @Override
    public String getEngineName() {
        return "Java Engine to Act Based On Rules";
    }

    @Override
    public String getEngineVersion() {
        return version;
    }

    @Override
    public List<String> getExtensions() {
        return EXTENSIONS;
    }

    @Override
    public List<String> getMimeTypes() {
        return MIME_TYPES;
    }

    @Override
    public List<String> getNames() {
        return NAMES;
    }

    @Override
    public String getLanguageName() {
        return LANGUAGE_NAME;
    }

    @Override
    public String getLanguageVersion() {
        return Drools.getFullVersion();
    }

    @Override
    public Object getParameter(String key) {
        switch (key) {
            case ScriptEngine.ENGINE:
                return getEngineName();
            case ScriptEngine.ENGINE_VERSION:
                return getEngineVersion();
            case ScriptEngine.NAME:
                if (getNames() != null && getNames().size() > 0) {
                    return getNames().get(0);
                }
                return null;
            case ScriptEngine.LANGUAGE:
                return getLanguageVersion();
            case "THREADING":
                //TODO: follow the following to implement it
//                * A reserved key, <code><b>THREADING</b></code>, whose value describes the behavior of the engine
//                    * with respect to concurrent execution of scripts and maintenance of state is also defined.
//                    * These values for the <code><b>THREADING</b></code> key are:<br><br>
//                * <ul>
//                * <li><code>null</code> - The engine implementation is not thread safe, and cannot
//                    * be used to execute scripts concurrently on multiple threads.
//                * <li><code>&quot;MULTITHREADED&quot;</code> - The engine implementation is internally
//                * thread-safe and scripts may execute concurrently although effects of script execution
//                * on one thread may be visible to scripts on other threads.
//                    * <li><code>&quot;THREAD-ISOLATED&quot;</code> - The implementation satisfies the requirements
//                * of &quot;MULTITHREADED&quot;, and also, the engine maintains independent values
//                * for symbols in scripts executing on different threads.
//                    * <li><code>&quot;STATELESS&quot;</code> - The implementation satisfies the requirements of
//                    * <li><code>&quot;THREAD-ISOLATED&quot;</code>.  In addition, script executions do not alter the
//                    * mappings in the <code>Bindings</code> which is the engine scope of the
//                * <code>ScriptEngine</code>.  In particular, the keys in the <code>Bindings</code>
                return null;
            default:
                return null;
        }
    }

    @Override
    public String getMethodCallSyntax(String obj, String m, String... args) {
        return "If you don't know how to use jdk, go learn it.. you bloody fool. You think I would do your homework?";
    }

    @Override
    public String getOutputStatement(String toDisplay) {
        return "System.out.println(Java_JDK, " + toDisplay + ");";
    }

    @Override
    public String getProgram(String... statements) {
        //TODO: this bullthis is not important atm
        return null;
    }

    @Override
    public ScriptEngine getScriptEngine() {
        return new DroolsEngine();
    }
}
