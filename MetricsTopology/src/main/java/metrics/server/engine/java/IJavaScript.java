package metrics.server.engine.java;

import java.util.List;

/**
 * Created by ali on 11/16/14.
 */
public interface IJavaScript<T,ReturnType> {
    ReturnType eval(List<T> list);
}
