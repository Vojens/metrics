package metrics.server.engine;

/**
 * Created by ali on 8/18/14.
 */
public interface ICustomEngine<T> {
    public static final String LIST_NAME = "163c3dc0-2793-11e4-8c21-0800200c9a66";//UUID.randomUUID().toString();
    public static final String TABLE_RESULT_SET_MAP_NAME = "163c3dc0-2793-11e4-8c21-0800200c9a67";//UUID.randomUUID().toString();
    public static final String EXTRA_RESULT_MAP_NAME = "163c3dc0-2793-11e4-8c21-0800200c9a67";//UUID.randomUUID().toString();
    public static final String LOGGER_NAME = "163c3dc0-2793-11e4-8c21-0800200c9a68";//UUID.randomUUID().toString();

    public T getConf();

    public void setConf(T conf);
}
