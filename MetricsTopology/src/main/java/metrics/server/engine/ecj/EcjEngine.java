package metrics.server.engine.ecj;

import javax.script.*;
import java.io.Reader;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/17/14.
 */
public class EcjEngine extends AbstractScriptEngine {
    @Override
    public Object eval(String script, ScriptContext context) throws ScriptException {
        checkNotNull(script, "Script cannot be null");
        checkNotNull(context, "context cannot be null");
        //context.
        //TODO
        return null;
    }

    @Override
    public Object eval(Reader reader, ScriptContext context) throws ScriptException {
        ScriptContext newContext = new SimpleScriptContext();
        //TODO
        return null;
    }

    @Override
    public Bindings createBindings() {
        return null;
    }

    @Override
    public ScriptEngineFactory getFactory() {

        return new EcjEngineFactory();
    }
}
