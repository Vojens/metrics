package metrics.server.engine.drools;

import com.datastax.driver.core.ResultSet;
import metrics.server.engine.ICustomCompiledScript;
import metrics.server.engine.ICustomEngine;
import metrics.server.engine.ScriptEngineConf;
import metrics.server.engine.calculation.CalculationEngineConfig;
import metrics.server.util.CacheUtil;
import metrics.server.util.ScriptEngineGlobal;
import metrics.server.util.helper.ColumnDataType;
import metrics.server.util.helper.IOHelper;
import metrics.server.util.type.KnowledgeBaseConf;
import com.sun.corba.se.impl.io.TypeMismatchException;
import org.apache.log4j.Logger;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.io.ResourceType;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.event.KnowledgeRuntimeEventManager;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.runtime.StatelessKnowledgeSession;

import javax.script.*;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/6/14.
 */
public class DroolsEngine extends AbstractScriptEngine implements Compilable, ICustomEngine<ScriptEngineConf> {
    private static final transient Logger logger = Logger.getLogger(DroolsEngine.class.getName());
    //protected ScriptEngine engine;
    protected volatile DroolsEngineFactory factory;
    private ScriptEngineConf conf;

    @Override
    public Object eval(String script, ScriptContext context)
            throws ScriptException {
        KnowledgeBaseConf knowledgeBaseConf = parse(script);
        //Bindings bindings = context.getBindings(ScriptContext.ENGINE_SCOPE);
        return evalKnowledgeBase(knowledgeBaseConf, context);

    }

    private KnowledgeBaseConf parse(String script) {
        checkNotNull(conf, "Conf cannot be null.");

        KnowledgeBaseConf knowledgeBaseConf = null;

        try {
            String session_type = conf.getEngineMap().get("session_type");
            checkNotNull(session_type, "session_type(" + conf.getType() + ") does not exist in engine map at calculation_script_engine_conf table. Check the database.");

            KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
            builder.add(ResourceFactory.newByteArrayResource(script.getBytes()), ResourceType.DRL);
            if (builder.hasErrors()) {
                //logger.info(conf.getScript());
                throw new RuntimeException("Building Knowledge of type (" + conf.getType() + ") throws an error. " + builder.getErrors().toString());
            }
            KieBaseConfiguration configuration = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
            if (conf.getEngineOptionMap() != null) {
                for (Map.Entry<String, String> option : conf.getEngineOptionMap().entrySet()) {
                    Class enumClass = Class.forName(option.getKey());
                    if (!enumClass.isEnum()) {
                        throw new TypeMismatchException(enumClass + " in type (" + conf.getType() + ")is not of an enum type");
                    }
                    Enum<?> anEnum = Enum.valueOf(enumClass, option.getValue());
                    configuration.setOption((org.kie.api.conf.KieBaseOption) anEnum);
                }

            }
            //configuration.setOption(SequentialOption.YES);
            KnowledgeBase knowledgeBase = null;
            knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
            knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());
            KnowledgeBaseConf.KnowledgeBaseType knowledgeBaseType;
            switch (session_type.toLowerCase()) {
                case "statefulknowledgesession":
                    //session = knowledgeBase.newStatefulKnowledgeSession();
                    knowledgeBaseType = KnowledgeBaseConf.KnowledgeBaseType.STATEFUL;
                    break;
                case "statelessknowledgesession":
                    //session = knowledgeBase.newStatelessKnowledgeSession();
                    knowledgeBaseType = KnowledgeBaseConf.KnowledgeBaseType.STATELESS;
                    break;
                default:
                    //throw new TypeMismatchException("Type ("+session_type+") is not a recognized one.");
                    knowledgeBaseType = KnowledgeBaseConf.KnowledgeBaseType.TYPE_MISMATCH;
            }
            //AbstractMap.SimpleEntry knowledgeBaseEntry = new AbstractMap.SimpleEntry<KnowledgeBase,KnowledgeBaseConf.KnowledgeBaseType>(knowledgeBase, knowledgeBaseType);

            knowledgeBaseConf = KnowledgeBaseConf.create(conf.getType(), conf.getName(), knowledgeBase, knowledgeBaseType);
        } catch (Exception e) {
            logger.error(e, e);
        }
        return knowledgeBaseConf;

    }

    private Object evalKnowledgeBase(KnowledgeBaseConf knowledgeBaseConf,
                                     ScriptContext scriptContext) {
        checkNotNull(conf, "Conf cannot be null.");
        CalculationEngineConfig cec = null;
        try {
            Bindings bindings = scriptContext.getBindings(ScriptContext.ENGINE_SCOPE);
            List list = (List) bindings.get(ICustomEngine.LIST_NAME);

            Map<ColumnDataType,ResultSet> tableResultSetMap = null;
            if(bindings.containsKey(ICustomEngine.LIST_NAME)){
                tableResultSetMap = (Map<ColumnDataType, ResultSet>) bindings.get(ICustomEngine.TABLE_RESULT_SET_MAP_NAME);
            }
            //String type = String.valueOf(bindings.get("type"));
            final KnowledgeRuntimeEventManager session;
            switch (knowledgeBaseConf.getKnowledgeBaseType()) {
                case STATEFUL:
                    cec = null; //TODO change
                    session = knowledgeBaseConf.getKnowledgeBase().newStatefulKnowledgeSession();

                    break;
                case STATELESS:
                    session = knowledgeBaseConf.getKnowledgeBase().newStatelessKnowledgeSession();
                    cec = CalculationEngineConfig.create(knowledgeBaseConf.getType(), knowledgeBaseConf.getName(), list, CacheUtil.getMappingClassCache().get(conf.getType()).getKey());
                    cec.setTableResultSetMap(tableResultSetMap);
                    try{
                        ((StatelessKnowledgeSession)session).setGlobal("SeGlobal",new ScriptEngineGlobal());
                    }catch(Exception e){
                        logger.warn(e,e);
                    }

                    //Class<?> beanClass = cec.getBeanClass();
                    ((StatelessKnowledgeSession) session).execute(cec);
                    break;
                default:
                    cec = null;
                    session = null;
                    //logger.error("session can't be null");  // TODO have a better thing to do asshole
            }
        } catch (Exception e) {
            logger.error(e, e);
        }

        return cec == null?null:cec.getResult();
    }
//
//    private StandardEvaluationContext getStandardEvaluationContext(
//            ScriptContext scriptContext) {
//
//        StandardEvaluationContext standardEvaluationContext = new StandardEvaluationContext(
//                scriptContext.getAttribute(ROOT_OBJECT));
//
//        standardEvaluationContext.setVariables(getVariables(scriptContext));
//
//        return standardEvaluationContext;
//    }

    private Map<String, Object> getVariables(ScriptContext scriptContext) {

        Map<String, Object> variables = new HashMap<String, Object>();

        if (scriptContext.getBindings(ScriptContext.GLOBAL_SCOPE) != null) {
            variables.putAll(scriptContext
                    .getBindings(ScriptContext.GLOBAL_SCOPE));
        }

        if (scriptContext.getBindings(ScriptContext.ENGINE_SCOPE) != null) {
            variables.putAll(scriptContext
                    .getBindings(ScriptContext.ENGINE_SCOPE));
        }

        return variables;

    }

//    private ExpressionParser getExpressionParser() {
//        return new SpelExpressionParser();
//    }

    @Override
    public Object eval(Reader reader, ScriptContext context)
            throws ScriptException {
        return eval(IOHelper.readFully(reader), context);
    }

    @Override
    public Bindings createBindings() {
        return new SimpleBindings();
    }

    @Override
    public ScriptEngineFactory getFactory() {
        if (factory == null) {
            synchronized (this) {
                if (factory == null) {
                    factory = new DroolsEngineFactory();
                }
            }
        }
        return factory;
    }

    @Override
    public ScriptEngineConf getConf() {
        return this.conf;
    }

    @Override
    public void setConf(ScriptEngineConf conf) {
        this.conf = conf;
    }

//    @Override
//    public ScriptEngineFactory getFactory() {
//
//        return new DroolsEngineFactory();
//    }

    @Override
    public CompiledScript compile(String script) throws ScriptException {
        return new DroolsCompiledScript(parse(script));
    }

    @Override
    public CompiledScript compile(Reader script) throws ScriptException {
        return compile(IOHelper.readFully(script));
    }

    private class DroolsCompiledScript extends CompiledScript implements ICustomCompiledScript {

        private KnowledgeBaseConf knowledgeBaseConf;

        public DroolsCompiledScript(KnowledgeBaseConf knowledgeBaseConf) {
            this.knowledgeBaseConf = knowledgeBaseConf;

            Bindings bindings = createBindings();
            bindings.putAll(conf.getEngineBindingMap());
            setBindings(bindings, ScriptContext.GLOBAL_SCOPE);
        }

        @Override
        public Object eval(ScriptContext context) throws ScriptException {
            return evalKnowledgeBase(knowledgeBaseConf, context);
        }

        @Override
        public ScriptEngine getEngine() {
            return DroolsEngine.this;
        }

    }
}
