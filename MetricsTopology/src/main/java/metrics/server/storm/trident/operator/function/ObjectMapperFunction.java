package metrics.server.storm.trident.operator.function;

import backtype.storm.tuple.Fields;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import metrics.server.util.CacheUtil;
import metrics.server.util.helper.PatternInfo;
import metrics.server.util.type.converter.ConverterInfo;
import org.apache.log4j.Logger;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 7/23/14.
 */
public class ObjectMapperFunction extends BaseFunction {
    private static final transient Logger logger = Logger.getLogger(TypeConverterFunction.class.getName());

    Fields fields;
    String type;

    public ObjectMapperFunction(Fields fields, String type) {
        this.fields = fields;
        this.type = type;
    }

    public ObjectMapperFunction(String patternNameField, String groupValueMapField, String type) {
        this.fields = new Fields(patternNameField, groupValueMapField);
        this.type = type;
    }

    @Override
    @SuppressWarnings(value = {"unchecked"})
    //emits: pattern name, map of found groups
    public void execute(TridentTuple objects, TridentCollector tridentCollector) {


        try {
            checkNotNull(objects);
            checkArgument(objects.size() >= 2);

            Map<String, PatternInfo> patternInfoMap = CacheUtil.getPatternCache().get(type);
            PatternInfo patternInfo = patternInfoMap.get(objects.getString(0));
            Map<String, Map.Entry<Object, Class<?>>> valueMap = (Map<String, Map.Entry<Object, Class<?>>>) objects.get(1);


            Map.Entry<Class<?>, Map<String, ConverterInfo>> beanClassWithDetails = patternInfo.getBeanClassWithDetails();
            Class<?> beanClass = beanClassWithDetails.getKey();
            //Map<String, ConverterInfo> classProperties = beanClassWithDetails.getValue();
            Object classObject = beanClass.newInstance();

            List<Method> methodList = Arrays.asList(beanClass.getDeclaredMethods());
            for (final Map.Entry<String, Map.Entry<Object, Class<?>>> stringEntryEntry : valueMap.entrySet()) {
                Optional<Method> methodOptional = Iterables.tryFind(methodList, new com.google.common.base.Predicate<Method>() {
                    @Override
                    public boolean apply(@Nullable Method input) {
                        return input.getName().toLowerCase().contains("set" + stringEntryEntry.getKey().toLowerCase());
                    }
                });

                if (methodOptional.isPresent()) {
                    //logger.info("map "+stringEntryEntry.getValue().getKey()+" to type "+stringEntryEntry.getValue().getValue());
                    methodOptional.get().invoke(classObject, stringEntryEntry.getValue().getValue().cast(stringEntryEntry.getValue().getKey()));
                }
                //TODO: change to as the following:
                //                Object debianServer = serverClass.newInstance();
                //                Method setName = serverClass.getMethod("setName", String.class);
                //                setName.invoke(debianServer, "ali");
            }

            List<Object> outputList = Lists.newArrayList();
            outputList.add(classObject);
            tridentCollector.emit(outputList);
        } catch (Exception el) {
            logger.error(el);
        }
    }
}
