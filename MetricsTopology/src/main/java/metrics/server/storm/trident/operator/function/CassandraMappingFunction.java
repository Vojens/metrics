package metrics.server.storm.trident.operator.function;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.common.collect.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import metrics.server.database.DbConnectionManager;
import metrics.server.mapping.output.Cassandra;
import metrics.server.qualitycheck.exception.KeyNotFoundException;
import metrics.server.util.CacheUtil;
import metrics.server.util.PreparedStatementCache;
import metrics.server.util.helper.ColumnDataType;
import metrics.server.util.singleton.TypeConverterManagerBeanSingleton;
import metrics.server.util.type.SerializableStringTemplateParser;
import jodd.util.StringTemplateParser;
import org.apache.commons.collections.list.TreeList;
import org.apache.log4j.Logger;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 9/1/14.
 */
public class CassandraMappingFunction extends BaseFunction {

    private static final transient Logger logger = Logger.getLogger(CassandraMappingFunction.class.getName());
    protected final SerializableStringTemplateParser sstp = new SerializableStringTemplateParser();
    @Inject
    @Named("type")
    String type;
    @Inject
    @Named("datasetType")
    String datasetType;

    public CassandraMappingFunction(String type) {
        super();
        this.type = checkNotNull(type);
    }

    public CassandraMappingFunction(String type, String datasetType) {
        this(type);
        this.datasetType = datasetType;
    }

    @Override
    public void execute(TridentTuple objects, TridentCollector tridentCollector) {
        try {
            Map<String, Object> resultMap = (Map<String, Object>) objects.get(0);

            prettyMapPrint(resultMap);
            List<Cassandra> cassandraList = null;
            if (datasetType == null) {
                cassandraList = CacheUtil.getOutputCassandraConfCache().get(type);
            } else {
                cassandraList = CacheUtil.getOutputCassandraConfByDatasetTypeCache().get(new AbstractMap.SimpleEntry<String, String>(type, datasetType));
            }
            Multimap<String, Cassandra> keyspaceMap = ArrayListMultimap.create();
            for (Cassandra cassandra : cassandraList) {
                keyspaceMap.put(cassandra.getKeyspaceName(), cassandra);
            }
            for (String keyspaceName : keyspaceMap.keySet()) {
                Collection<Cassandra> keyspaceCollection = keyspaceMap.get(keyspaceName);
                //for each keyspace
                Session session = DbConnectionManager.getSession(keyspaceName);
                Multimap<String, Cassandra> tableMap = ArrayListMultimap.create();
                for (Cassandra cassandra : keyspaceCollection) {
                    tableMap.put(cassandra.getTableName(), cassandra);
                }
                for (String tableName : tableMap.keySet()) {
                    Collection<Cassandra> tableCollection = tableMap.get(tableName);
                    //TODO for each table insert the row
                    Multimap<String, Cassandra> columnMap = ArrayListMultimap.create();
                    for (Cassandra cassandra : tableCollection) {
                        columnMap.put(cassandra.getColumnName(), cassandra);
                    }
                    Map<String, Map.Entry<Map.Entry<Object, DataType>, Collection<Cassandra>>> valueMap = Maps.newHashMap();
                    for (String columnName : columnMap.keySet()) {
                        DataType dataType = null;
                        try {
                            dataType = CacheUtil.getColumnDataTypeCache().get(ColumnDataType.create(keyspaceName, tableName, columnName));
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                        Object value = null;
                        Serializable serializableObject = null;
                        if (dataType != null) {
                            if (dataType.asJavaClass() == List.class || dataType.asJavaClass() == TreeList.class) { //list
                                List<Object> list = Lists.newLinkedList();
                                serializableObject = Lists.newArrayList();
                                Collection<Cassandra> cassandras = columnMap.get(columnName);
                                for (Cassandra cassandra : cassandras) {
                                    try{
                                        Object result = getResult(cassandra.getSourceName(), objects);
                                        if (result != null) {
                                            list.add(result);
                                        } else {
                                            logger.warn("Value of " + cassandra.getSourceName() + " does not exist in map passed to " + CassandraMappingFunction.class.getName());
                                        }
                                    }catch(KeyNotFoundException e){
                                        logger.error("Value of " + cassandra.getSourceName() + " does not exist in map passed to " + CassandraMappingFunction.class.getName());
                                    }
                                }
                                serializableObject = (Serializable) list;
                            } else if (dataType.asJavaClass() == Set.class || dataType.asJavaClass() == TreeSet.class) {  //set
                                Set<Object> set = Sets.newHashSet();
                                Collection<Cassandra> cassandras = columnMap.get(columnName);
                                for (Cassandra cassandra : cassandras) {
                                    Object result = getResult(cassandra.getSourceName(), objects);
                                    if (result != null) {
                                        set.add(result);
                                    } else {
                                        logger.warn("Value of " + cassandra.getSourceName() + " does not exist in map passed to " + CassandraMappingFunction.class.getName());
                                    }
                                }
                                serializableObject = (Serializable) set;

                            } else if (dataType.asJavaClass() == Map.class || dataType.asJavaClass() == TreeMap.class) {  //map
                                Map<String, Object> map = Maps.newHashMap();
                                Collection<Cassandra> cassandras = columnMap.get(columnName);
                                for (Cassandra cassandra : cassandras) {
                                    try{
                                        Object result = getResult(cassandra.getSourceName(), objects);
                                        if (result != null) {
                                            String key_name = cassandra.getOptions().get("key_name");
                                            if (key_name == null) {
                                                logger.warn("row at " + cassandra.getClass().getName() + " does not have a keyname in the options map. Check the database");
                                            }
                                            map.put(key_name != null ? key_name : cassandra.getSourceName(), result);
                                        } else {
                                            logger.warn("Value of " + cassandra.getSourceName() + " does not exist in map passed to " + CassandraMappingFunction.class.getName());
                                        }
                                    }catch(KeyNotFoundException e){
                                        logger.error("Value of " + cassandra.getSourceName() + " does not exist in map passed to " + CassandraMappingFunction.class.getName());
                                    }
                                }
                                serializableObject = (Serializable) map;
                            } else {
                                Collection<Cassandra> cassandras = columnMap.get(columnName);
                                if (cassandras.isEmpty()) {
                                    logger.warn("There exists no results for " + columnName + " in the cassandra map passed");
                                } else if (cassandras.size() > 1) {
                                    logger.error("There is an error in " + tableName + ". more than 1 value is set to be stored in a column that is not one of the following (List, Set, Map)");
                                } else {
                                    try{
                                        serializableObject = getResult(((Cassandra) cassandras.toArray()[0]).getSourceName(), objects).toString();
                                    }catch(Exception e){
                                        logger.error(e,e);
                                    }
                                }
                            }
                        }
                        if (serializableObject != null) {
                            valueMap.put(columnName, new AbstractMap.SimpleEntry(new AbstractMap.SimpleEntry<Object, DataType>(serializableObject, dataType), columnMap.get(columnName)));
                        }

                    }
                    if (valueMap.size() != 0) {
                        //insert the result into the database
                        String[] names = new String[valueMap.size()];
                        Object[] values = new Object[valueMap.size()];

                        int iter = 0;
                        for (Map.Entry<String, Map.Entry<Map.Entry<Object, DataType>, Collection<Cassandra>>> stringObjectEntry : valueMap.entrySet()) {
                            names[iter] = stringObjectEntry.getKey();
                            //values[iter] = stringObjectEntry.getValue().getValue().parse(stringObjectEntry.getValue().getKey().toString());
                            values[iter] = QueryBuilder.bindMarker();
                            iter++;
                        }

                        Insert query = QueryBuilder.insertInto(keyspaceName, tableName).values(names, values);
                        PreparedStatement ps = PreparedStatementCache.get(query);//DbConnectionManager.getSession(keyspaceName).prepare(query.getQueryString());
                        BoundStatement bs = new BoundStatement(ps);

                        iter = 0;
                        for (Map.Entry<Map.Entry<Object, DataType>, Collection<Cassandra>> objectDataTypeEntry : valueMap.values()) {

//                            ASCII     (1,  String.class),
//                                    BIGINT    (2,  Long.class),
//                                    BLOB      (3,  ByteBuffer.class),
//                                    BOOLEAN   (4,  Boolean.class),
//                                    COUNTER   (5,  Long.class),
//                                    DECIMAL   (6,  BigDecimal.class),
//                                    DOUBLE    (7,  Double.class),
//                                    FLOAT     (8,  Float.class),
//                                    INET      (16, InetAddress.class),
//                                    INT       (9,  Integer.class),
//                                    TEXT      (10, String.class),
//                                    TIMESTAMP (11, Date.class),
//                                    UUID      (12, UUID.class),
//                                    VARCHAR   (13, String.class),
//                                    VARINT    (14, BigInteger.class),
//                                    TIMEUUID  (15, UUID.class),
//                                    LIST      (32, List.class),
//                                    SET       (34, Set.class),
//                                    MAP       (33, Map.class),
//                                    CUSTOM    (0,  ByteBuffer.class);

                            Object valueObject = objectDataTypeEntry.getKey().getKey();
                            Class<?> javaClassType = objectDataTypeEntry.getKey().getValue().asJavaClass();
                            Collection<Cassandra> cassandraCollection = objectDataTypeEntry.getValue();
                            //TODO move to CassandraUtils
                            if (javaClassType == String.class) {
                                bs.bind().setString(iter, objectDataTypeEntry.getKey().getKey().toString());
                            } else if (javaClassType == Long.class) {
                                Long value = TypeConverterManagerBeanSingleton.getInstance().<Long>convertType(valueObject, Long.class);
                                bs.bind().setLong(iter, value);
                            } else if (javaClassType == ByteBuffer.class) {
                                ByteBuffer value = TypeConverterManagerBeanSingleton.getInstance().<ByteBuffer>convertType(valueObject, ByteBuffer.class);
                                bs.bind().setBytes(iter, value);
                            } else if (javaClassType == Boolean.class) {
                                Boolean value = TypeConverterManagerBeanSingleton.getInstance().<Boolean>convertType(valueObject, Boolean.class);
                                bs.bind().setBool(iter, value);
                            } else if (javaClassType == BigDecimal.class) {
                                BigDecimal value = TypeConverterManagerBeanSingleton.getInstance().<BigDecimal>convertType(valueObject, BigDecimal.class);
                                bs.bind().setDecimal(iter, value);
                            } else if (javaClassType == Double.class) {
                                Double value = TypeConverterManagerBeanSingleton.getInstance().<Double>convertType(valueObject, Double.class);
                                bs.bind().setDouble(iter, value);
                            } else if (javaClassType == Float.class) {
                                Float value = TypeConverterManagerBeanSingleton.getInstance().<Float>convertType(valueObject, Float.class);
                                bs.bind().setFloat(iter, value);
                            } else if (javaClassType == InetAddress.class) {
                                InetAddress value = TypeConverterManagerBeanSingleton.getInstance().<InetAddress>convertType(valueObject, InetAddress.class);
                                bs.bind().setInet(iter, value);
                            } else if (javaClassType == Integer.class) {
                                Integer value = TypeConverterManagerBeanSingleton.getInstance().<Integer>convertType(valueObject, Integer.class);
                                bs.bind().setInt(iter, value);
                            } else if (javaClassType == Date.class) {
                                //Date value = TypeConverterManagerBeanSingleton.getInstance().<Date>convertType(valueObject,Date.class);
                                //ps.bind().setDate(iter,value);
                                org.joda.time.DateTime jodaDateTime = null;

                                if (cassandraCollection != null && !cassandraCollection.isEmpty()) {
                                    jodaDateTime = TypeConverterManagerBeanSingleton.getInstance().<org.joda.time.DateTime>convertType(valueObject, org.joda.time.DateTime.class, ((Cassandra) cassandraCollection.toArray()[0]).getOptions());

                                } else {
                                    jodaDateTime = TypeConverterManagerBeanSingleton.getInstance().<org.joda.time.DateTime>convertType(valueObject, org.joda.time.DateTime.class);
                                }
                                Date value = jodaDateTime.toDate();
                                //ps.bind().setString(iter,objectDataTypeEntry.getKey().toString());
                                bs.bind().setDate(iter, value);
                            } else if (javaClassType == UUID.class) {
                                UUID value = TypeConverterManagerBeanSingleton.getInstance().<UUID>convertType(valueObject, UUID.class);
                                bs.bind().setUUID(iter, value);
                            } else if (javaClassType == BigInteger.class) {
                                BigInteger value = TypeConverterManagerBeanSingleton.getInstance().<BigInteger>convertType(valueObject, BigInteger.class);
                                bs.bind().setVarint(iter, value);
                            } else if (javaClassType == List.class || javaClassType == TreeList.class) {
                                List value = TypeConverterManagerBeanSingleton.getInstance().<List>convertType(valueObject, List.class);
                                bs.bind().setList(iter, value);
                            } else if (javaClassType == Set.class || javaClassType == TreeSet.class) {
                                Set value = TypeConverterManagerBeanSingleton.getInstance().<Set>convertType(valueObject, Set.class);
                                bs.bind().setSet(iter, value);
                            } else if (javaClassType == Map.class || javaClassType == TreeMap.class) {
                                Map value = TypeConverterManagerBeanSingleton.getInstance().<Map>convertType(valueObject, Map.class);
                                bs.bind().setMap(iter, value);
                            }


//                            Object o = TypeConverterManagerBeanSingleton.getInstance().convertType(objectDataTypeEntry.getKey(), objectDataTypeEntry.getValue().asJavaClass());


                            iter++;
                        }
//                        for (Map.Entry<String, Map.Entry<Object, DataType>> stringObjectEntry : valueMap.entrySet()) {
//                            names[iter] = stringObjectEntry.getKey();
//                            values[iter] = QueryBuilder.bindMarker();//stringObjectEntry.getValue();
//                            iter++;
//                        }


//
//                        //TODO parse to this goddamn type..(dataType.asJavaClass()) alright?
//                        for (Map.Entry<String, Map.Entry<Object, DataType>> stringObjectEntry : valueMap.entrySet()) {
//                            //stringObjectEntry.getValue().getValue().parse()
//
//                            ps.bind().bind()
//
//
//                            iter++;
//                        }
//
//                        ps.bind().
//                        QueryBuilder.insertInto(keyspaceName,tableName).value(" ",QueryBuilder.bindMarker());


                        ResultSetFuture resultSetFuture = DbConnectionManager.getSession(keyspaceName).executeAsync(bs);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e, e);
        }
    }
    protected Object getResult(String name, final TridentTuple objects) {
        Map<String, Object> resultMap = (Map<String, Object>) objects.get(0);

        String parsed = sstp.parse(name, new StringTemplateParser.MacroResolver() {
            @Override
            public String resolve(String macroName) {
                if (macroName.startsWith("Fields_")) {
                    Object field = objects.getValueByField(macroName.replaceFirst("Fields_", ""));
                    return field.toString();
                } else if (macroName.startsWith("Static_")) {
                    return macroName.replaceFirst("Static_", "");
                }
                return null;
            }
        });

        if (!parsed.equals(name)) {
            return parsed;
        } else {
            Object o = null;
            int count = 0;
            do{
                o = resultMap.get(name);
            }while(o != null && isDelayed(1000) && count++ < 10);

            if(o == null){
                prettyMapPrint(resultMap);
                boolean b = resultMap.containsKey(name);
                Object o1 = resultMap.get(name);
                logger.error("ERROR MAPPING");
                throw new KeyNotFoundException("key ("+name+") is not found in a map passed.");
            }
            return o;
        }
//        if(parsed != null){
//            return parsed;
//        }else{
//            return resultMap.get(name);
//        }
    }

    private static boolean isDelayed(long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return true;
    }

    private static void prettyMapPrint(Map<String,Object> map){

        StringBuilder sb = new StringBuilder();
        Iterator iter = map.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry<String,Object> entry = (Map.Entry<String, Object>) iter.next();
            sb.append(entry.getKey());
            sb.append('=').append('"');
            sb.append(entry.getValue());
            sb.append('"');
            if (iter.hasNext()) {
                sb.append(',').append(' ');
            }
        }
        logger.info(sb.toString());


    }
}
