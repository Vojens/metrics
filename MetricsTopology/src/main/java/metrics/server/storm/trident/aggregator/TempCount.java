package metrics.server.storm.trident.aggregator;

import storm.trident.operation.CombinerAggregator;
import storm.trident.tuple.TridentTuple;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ali on 8/14/14.
 */
public class TempCount implements CombinerAggregator {
    @Override
    public Map init(TridentTuple tuple) {
        if (tuple.size() > 0) {
            System.out.println("tuple.size() = " + tuple.size());
        }
        return new HashMap();
    }

    @Override
    public Object combine(Object o, Object o2) {
        return o;
    }

//    @Override
//    public Map combine(Map val1, Map val2) {
//        Map tempMap = new HashMap(val1);
//        tempMap.putAll(val2);
//        return tempMap;
//    }

    @Override
    public Map zero() {
        Map map = new HashMap();
        map.put(1, 2);
        return map;
    }
}
