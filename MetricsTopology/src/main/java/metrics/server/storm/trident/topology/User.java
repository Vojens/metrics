package metrics.server.storm.trident.topology;

/**
 * Created by ali on 7/14/14.
 */
public class User {
    private String name;
    private int age;

    public User(UserBuilder userBuilder) {
        setName(userBuilder.name);
        setAge(userBuilder.age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    public static class UserBuilder {
        private String name;
        private int age;

        public UserBuilder name(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder age(int age) {
            this.age = age;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }
}
