package metrics.server.storm.trident.spout;

import backtype.storm.spout.Scheme;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import metrics.server.io.latent.storm.rabbitmq.*;
import metrics.server.io.latent.storm.rabbitmq.config.ConsumerConfig;
import metrics.server.storm.trident.StormUtils;
import metrics.server.storm.trident.streaming.model.BatchMetadata;
import metrics.server.storm.trident.streaming.model.BatchStateType;
import metrics.server.storm.trident.streaming.model.SerializedTransactionAttempt;
import metrics.server.storm.trident.topology.MetricsTopology;
import metrics.server.util.Utils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.ITridentSpout;
import storm.trident.topology.TransactionAttempt;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ali on 9/18/14.
 */
public class ComprehensiveRabbitMqBatchSpout  implements ITridentSpout {

    private static final long serialVersionUID = -7083692146780923409L;
    private static final transient Logger logger = Logger.getLogger(ComprehensiveRabbitMqBatchSpout.class);
    private static final DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis();
    SpoutOutputCollector collector;
    RabbitMqBatchCoordinator coordinator;
    RabbitMqBatchEmitter emitter = null;//new EventEmitter();
    Scheme scheme;
    Map<String, Object> mapConfig;
    private Fields additionalFields;
    private RabbitMQConsumer consumer;
    //private RabbitMQConsumer consumer;

    public ComprehensiveRabbitMqBatchSpout(Map<String, Object> mapConfig, Scheme scheme) {
        this.scheme = scheme;
        this.mapConfig = mapConfig;

        emitter = new RabbitMqBatchEmitter(scheme);
        coordinator = new RabbitMqBatchCoordinator(mapConfig,new Duration(30000), CoordinatingDateTimeType.ABSOLUTE);
        additionalFields = new Fields("deliveryTag");
    }
    @Override
    public BatchCoordinator<BatchMetadata> getCoordinator(
            String txStateId, Map conf, TopologyContext context) {
        if(!coordinator.isPrepared()){

            coordinator.open(mapConfig,context);

        }

        return coordinator;
    }

    @Override
    public Emitter<BatchMetadata> getEmitter(String txStateId, Map conf,
                                             TopologyContext context) {
        if (!emitter.isOpened()) {
            emitter.open(mapConfig, context);
        }
        return emitter;
    }

    @Override
    public Map getComponentConfiguration() {
        return null;
    }

    @Override
    public Fields getOutputFields() {
        List<String> outputList = scheme.getOutputFields().toList();
        if (additionalFields != null) {
            outputList.addAll(additionalFields.toList());
        }
        return new Fields(outputList);
    }


    class RabbitMqBatchCoordinator implements ITridentSpout.BatchCoordinator<BatchMetadata>,
            Serializable {


        //private final transient Logger logger = Logger.getLogger(RabbitMqBatchCoordinator.class);
        private static final long serialVersionUID = -7921152512216469421L;
        //private DateTime nextDateTime;
        private DateTime startCoordinatingTime;
        @Inject
        private Duration interval;
        transient private long batchNumber = 0;
        @Inject
        private CoordinatingDateTimeType coordinatingDateTimeType;

        @Inject
        private Map<String, Object> mapConfig;
        //@Inject
        //private Properties simplePersistence = null;


        private transient Client client;
        private transient WebResource webResource;
        private AtomicBoolean isPrepared = new AtomicBoolean(false);

        public RabbitMqBatchCoordinator( Map<String, Object> mapConfig, Duration interval, CoordinatingDateTimeType coordinatingDateTimeType) {
            //this.eventEmitter = eventEmitter;
            this.interval = interval;
            this.coordinatingDateTimeType = coordinatingDateTimeType;
            this.mapConfig = mapConfig;
            //simplePersistence = Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME);


            initClient();

        }
        public RabbitMqBatchCoordinator() {

        }

        private void initClient(){
            //eventEmitter.getAcknowledger().getQueueName();

            client = Client.create();
            client.addFilter(new HTTPBasicAuthFilter(mapConfig.get("rabbitmq.username").toString(),mapConfig.get("rabbitmq.password").toString()));
            String virtual_host = "/";
            virtual_host = mapConfig.get("rabbitmq.virtualhost").toString();
            if(virtual_host.trim().equals("/")){
                virtual_host = "%2f";
            }

            String url = String.format("http://%s:%d/api/queues/%s/%s",mapConfig.get("rabbitmq.host"), Utils.getIntMetricsProperty("rabbitmq.api.port", 15672), virtual_host,mapConfig.get("rabbitmq.queueName"));   //TODO handle SSL
            //URI uri = null;
//        try {
//            //uri = new URI("http",null,mapConfig.get("rabbitmq.host"),Utils.getIntMetricsProperty("rabbitmq.api.port",15672),"/"+mapConfig.get("rabbitmq.virtualhost")+"/"+);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
            //url = uri.toASCIIString();
//        try {
//            url = URLDecoder.decode(url, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
            webResource = client.resource(url);//;("http://"":15672/api/queues/%2f/all.log.witsml");
            //String s = webResource.get(String.class);
        }

        public boolean isPrepared() {
            return isPrepared.get();
        }
        public void setIsPrepared(boolean isPrepared){
            this.isPrepared.set(isPrepared);
        }

        private long getQueueMessagesReadyCount(){

            return getNodeAsLong("messages_ready");

        }
        private long getQueueMessagesCount(){
            return getNodeAsLong("messages");
        }

        public long getNodeAsLong(String nodeName){
            ClientResponse response = null;
            try{
                if(webResource == null){
                    initClient();
                }
                response = webResource.accept("application/json")
                        .get(ClientResponse.class);

                if (response.getStatus() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
                }

                String output = response.getEntity(String.class);
                //System.out.println(output);
                JsonNode rootNode = Utils.objectMapper.readTree(output);
                JsonNode messagesReadyNode = rootNode.path(nodeName);
                if(messagesReadyNode.canConvertToLong()){
                    return messagesReadyNode.asLong();
                }else{
                    return 0;
                }


            }catch (Exception e){
                logger.error(e,e);
            }finally {
                try{
                    if(response != null){
                        response.close();
                    }
                }catch (Exception e){
                    //do nothing
                }

            }
            return -1;
        }

        @Override
        public boolean isReady(long txid) {
            try{
                Long messagesCount = getQueueMessagesReadyCount();

                if(messagesCount == 0){
                    return false;
                }else// if (messagesReadyCount == -1 || messagesReadyCount > 0){
                {
                    if(txid == 1){
                        return true;
                    }else{
                        long txidInDb = Long.parseLong(Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).getOnDemand("txid"));
                        if((txid - 1) == txidInDb){
                            return true;
                        }else if(txid == txidInDb){ //when a failure occured and this needs to replay
                            return true;
                        }
                        return false;
                    }
                }
            }catch (Exception e){
                logger.error(e,e);
            }
            return false;
        }

        private Map config;
        public void open(final Map config,
                         final TopologyContext context) {
            try {
                //setAcknowledger(StormUtils.createAcknowledger(config, new Declarator.NoOp()));
                //acknowledger.open();
                isPrepared.set(true);
            } catch (Exception e) {
                logger.error(e, e);
            }

        }

        @Override
        public void close() {
            logger.info("coordinator - close");
        }

        @Override
        public BatchMetadata initializeTransaction(long l, BatchMetadata prevBatchInfo, BatchMetadata currentBatchInfo) {
            logger.info("Initializing Transaction [" + l + "]");
            //return nextDateTime.minusMinutes(1);
            SerializedTransactionAttempt ta = new SerializedTransactionAttempt(l,1);

            BatchMetadata batchInfo = new BatchMetadata("group1",ta, BatchStateType.NORMAL);

            return batchInfo;
        }

        @Override
        public void success(long txid) {
            try{
                logger.info("Successful Transaction [" + txid + "]");
                Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid", txid);
                Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid.last.success",dateTimeFormatter.print(DateTime.now()));

            String lastMessageDeliveryTag = Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).getOnDemand("txid.emit.batch.last.message.delivery.tag");
            if(!(lastMessageDeliveryTag == null || lastMessageDeliveryTag.isEmpty())){
                long deliveryTag = Long.parseLong(lastMessageDeliveryTag);
                consumer.ack(deliveryTag,true);
                //getAcknowledger().ack(deliveryTag, true);
            }

            }catch(Exception e){
                logger.error(e,e);
            }

        }
    }
    public enum CoordinatingDateTimeType {
        RELATIVE,
        ABSOLUTE
    }

    class RabbitMqBatchEmitter implements ITridentSpout.Emitter<BatchMetadata>, Serializable {
        private static final long serialVersionUID = 7676816240577814870L;
        //private final transient Logger logger = Logger.getLogger(RabbitMqBatchEmitter.class);
        protected final MessageScheme scheme;
        protected final Declarator declarator;

        private AtomicInteger successfulTransactions = new AtomicInteger(0);
        private AtomicBoolean isBatchedFinishedEmitting = new AtomicBoolean(true);
        private String txStateId;
        private Map conf;
        private TopologyContext context;
        private org.joda.time.DateTime latestDateTime = null;
        private AtomicBoolean isOpened = new AtomicBoolean(false);
        private Map config;
        //@Inject
        //private metrics.server.util.Properties simplePersistence = null;
        //private final DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis();

        public RabbitMqBatchEmitter(Scheme scheme) {
            this(MessageScheme.Builder.from(scheme), new Declarator.NoOp());
            //simplePersistence = Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME);
        }

        public RabbitMqBatchEmitter(Scheme scheme, Declarator declarator) {
            this(MessageScheme.Builder.from(scheme), declarator);
        }

        public RabbitMqBatchEmitter(MessageScheme scheme, Declarator declarator) {
            this.scheme = scheme;
            this.declarator = declarator;
        }

        @Override
        public void emitBatch(TransactionAttempt tx, BatchMetadata
                coordinatorMeta, TridentCollector collector) {
            try{
                Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid.emit.batch.start.datetime",dateTimeFormatter.print(org.joda.time.DateTime.now()));
                if(isBatchedFinishedEmitting.get()){
                    emitNextTupleBatch(collector, coordinatorMeta);
                }
            }catch (Exception e){
                logger.error(e,e);
            }
        }

        public void open(final Map config,
                         final TopologyContext context) {
            try {
                this.config = config;
//            ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(config);
//            ErrorReporter reporter = new ErrorReporter() {
//                @Override
//                public void reportError(Throwable error) {
//                    //spoutOutputCollector.reportError(error);
//                    // TODO: create one
//                }
//            };
//            setAcknowledger(loadConsumer(declarator, reporter, consumerConfig));
                setConsumer(StormUtils.createConsumer(config, new Declarator.NoOp()));

                scheme.open(config, context);
                getConsumer().open();
                //collector = spoutOutputCollector;
                isOpened.set(true);
            } catch (Exception e) {
                logger.error(e, e);
            }

        }

        public boolean isOpened() {
            return isOpened.get();
        }

        protected RabbitMQConsumer loadConsumer(Declarator declarator,
                                                ErrorReporter reporter,
                                                ConsumerConfig config) {
            return new RabbitMQConsumer(config.getConnectionConfig(),
                    config.getPrefetchCount(),
                    config.getQueueName(),
                    config.isRequeueOnFail(),
                    declarator,
                    reporter);
        }

        public void emitNextTupleBatch(TridentCollector collector, BatchMetadata dateTime) {

            System.out.println("nextTuplip");
            Message message;
            synchronized (isBatchedFinishedEmitting) {
                if (!isBatchedFinishedEmitting.get()) {
                    return;
                }
                isBatchedFinishedEmitting.set(false);
            }

            try{
                org.joda.time.DateTime startTime = org.joda.time.DateTime.now();
                //Duration duration = Duration.standardSeconds(30);

                String freqString = Utils.getMetricsProperty("Metrics.topology.batch.spout.emitter.freq.period","PT30S");
                Duration freqDuration = (new Period(freqString)).toStandardDuration();
                //int messageCount = consumer.getMessageCount();
                Message lastMessage = null;
                for (Period period = new Period(startTime, org.joda.time.DateTime.now());
                     isPeriodLessThanFreq(freqDuration, period)  && (message = getConsumer().nextMessage()) != Message.NONE;
                     period = new Period(startTime, org.joda.time.DateTime.now())) {

                    try{
                        Date messageTimestamp = ((Message.DeliveredMessage) message).getTimestamp();

                        List<Object> tuple = extractTuple(message, collector);
                        if (!tuple.isEmpty()) {
                            DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
                            org.joda.time.DateTime logTimestamp = parser.parseDateTime(tuple.get(0).toString());
                            setLatestDateTime(logTimestamp);

                            LinkedList<Object> tempList = new LinkedList<Object>();
                            tempList.add(logTimestamp);
                            tempList.add(tuple.get(1));
                            tempList.add(tuple.get(2));
                            tempList.add(((Message.DeliveredMessage) message).getDeliveryTag());
                            emit(tempList, collector);
                            this.getConsumer().ack(((Message.DeliveredMessage) message).getDeliveryTag());  //TODO remove

                        }
                    }catch (Exception e){
                        logger.error(e,e);
                        try{
                            getConsumer().fail(getDeliveryTag(message));
                        }catch (Exception ei){
                            logger.error(ei,ei);
                        }

                    }

                    lastMessage = message;
                }
                String lastMessageDeliveryTag = "";
                try{

                }catch (Exception e){
                    logger.error(e,e);
                }
                if(lastMessage != null){
                    lastMessageDeliveryTag = getDeliveryTag(lastMessage)+"";
                }
                Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid.emit.batch.last.message.delivery.tag",lastMessageDeliveryTag);
            }catch (Exception e){
                logger.error(e,e);
            }finally {
                isBatchedFinishedEmitting.set(true);
            }
        }


        private boolean isPeriodLessThanFreq(Duration duration, Period period){
            Duration duration1 = period.toStandardDuration();
            boolean shorterThan = duration1.isShorterThan(duration);
            return shorterThan;
        }
        private List<Object> extractTuple(Message message, TridentCollector collector) {
            long deliveryTag = getDeliveryTag(message);
            try {
                List<Object> tuple = scheme.deserialize(message);
                if (tuple != null && !tuple.isEmpty()) {
                    return tuple;
                }
                String errorMsg = "Deserialization error for msgId " + deliveryTag;
                logger.warn(errorMsg);
                collector.reportError(new Exception(errorMsg));
            } catch (Exception e) {
                logger.warn("Deserialization error for msgId " + deliveryTag, e);
                collector.reportError(e);
            }
            // get the malformed message out of the way by dead-lettering (if dead-lettering is configured) and move on
            getConsumer().deadLetter(deliveryTag);
            return Collections.emptyList();
        }

        protected long getDeliveryTag(Message message) {
            return ((Message.DeliveredMessage) message).getDeliveryTag();
        }


        protected void emit(List<Object> tuple,
                            TridentCollector spoutOutputCollector) {
            spoutOutputCollector.emit(tuple);

            //return spoutOutputCollector.emit(tuple, getDeliveryTag(message));
        }




        @Override
        public void success(TransactionAttempt tx) {
            successfulTransactions.incrementAndGet();


        }

        @Override
        public void close() {
            logger.info("emitter - close");
        }


        public String getTxStateId() {
            return txStateId;
        }

        public void setTxStateId(String txStateId) {
            this.txStateId = txStateId;
        }

        public Map getConf() {
            return conf;
        }

        public void setConf(Map conf) {
            this.conf = conf;
        }

        public TopologyContext getContext() {
            return context;
        }

        public void setContext(TopologyContext context) {
            this.context = context;
        }

        public synchronized org.joda.time.DateTime getLatestDateTime() {
            return latestDateTime;
        }

        public synchronized void setLatestDateTime(org.joda.time.DateTime latestDateTime) {
            this.latestDateTime = latestDateTime;
        }

        public RabbitMQConsumer getConsumer() {
            if(consumer == null){
//            ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(config);
//            ErrorReporter reporter = new ErrorReporter() {
//                @Override
//                public void reportError(Throwable error) {
//                    //spoutOutputCollector.reportError(error);
//                    // TODO: create one
//                }
//            };
//            setAcknowledger(loadConsumer(declarator, reporter, consumerConfig));
                setConsumer(StormUtils.createConsumer(config,declarator));
            }
            return consumer;
        }

        public void setConsumer(RabbitMQConsumer rabbitMQConsumer) {
            consumer = rabbitMQConsumer;

        }
    }

}
