package metrics.server.storm.trident.topology;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.spout.Scheme;
import backtype.storm.tuple.Fields;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.policies.DefaultRetryPolicy;
import com.datastax.driver.mapping.option.WriteOptions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import metrics.server.MetricsServer;
import metrics.server.database.DbConnectionManager;
import metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig;
import metrics.server.io.latent.storm.rabbitmq.config.ConsumerConfig;
import metrics.server.io.latent.storm.rabbitmq.config.ConsumerConfigBuilder;
import metrics.server.mapping.DataTypeBinding;
import metrics.server.mapping.RegexPatternSet;
import metrics.server.mapping.output.Account;
import metrics.server.mapping.output.Entity;
import metrics.server.spi.RabbitMqConnectionManagerImpl;
import metrics.server.storm.SimpleJSONScheme;
import metrics.server.storm.trident.aggregator.AccumulatingAggregator;
import metrics.server.storm.trident.operator.filter.CassandraMapping;
import metrics.server.storm.trident.operator.filter.LateComingFilter;
import metrics.server.storm.trident.operator.function.*;
import metrics.server.storm.trident.spout.AdvancedRabbitMQBatchSpout;
import metrics.server.storm.trident.state.CassandraState;
import metrics.server.util.CacheUtil;
import metrics.server.util.Utils;
import metrics.server.util.Version;
import metrics.server.util.helper.ColumnDataType;
import metrics.server.util.helper.DateTimeHelper;
import metrics.server.util.helper.RowHolderInfo;
import metrics.server.util.helper.SerializationUtil;
import metrics.server.util.singleton.MappingSessionSingleton;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MapRpcServer;
import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.core.NamingPolicy;
import net.sf.cglib.core.Predicate;
import org.apache.log4j.Logger;
import org.joda.time.Period;
import org.josql.Query;
import org.josql.QueryExecutionException;
import org.josql.QueryParseException;
import org.josql.QueryResults;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.conf.SequentialOption;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.kie.internal.runtime.StatelessKnowledgeSession;
import org.yaml.snakeyaml.util.Base64Coder;
import storm.trident.Stream;
import storm.trident.TridentTopology;
import storm.trident.fluent.IAggregatableStream;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by ali on 7/10/14.
 */
public class MetricsTopology {
    private static final transient Logger logger = Logger.getLogger(MetricsTopology.class.getName());
    private TridentTopology topology;
    private AdvancedRabbitMQBatchSpout spout;
    private Stream inputStream;
    public static final String SIMPLE_PERSISTENCE_TABLE_NAME = "simple_persistence";
    //private Properties simplePersistenceProperties;

    public MetricsTopology() {
        //don't have anything here
    }

    public static Class<?> createBeanClass(
    /* fully qualified class name */
    final String className,
    /* bean properties, name -> type */
    final Map<String, Class<?>> properties) {

        final BeanGenerator beanGenerator = new BeanGenerator();

    /* use our own hard coded class name instead of a real naming policy */
        beanGenerator.setNamingPolicy(new NamingPolicy() {
            @Override
            public String getClassName(final String prefix,
                                       final String source, final Object key, final Predicate names) {
                return className;
            }
        });
        BeanGenerator.addProperties(beanGenerator, properties);
        return (Class<?>) beanGenerator.createClass();
    }

    private static String toString(Serializable o) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(o);
        oos.close();
        return new String(Base64Coder.encode(baos.toByteArray()));
    }

    public void test() {


        final Map<String, Class<?>> properties =
                new HashMap<String, Class<?>>();
        properties.put("name", String.class);
        properties.put("age", Integer.class);

        final Class<?> beanClass =
                createBeanClass("test.User", properties);

        System.out.println(beanClass);

        Object o = null;
        try {
            o = beanClass.newInstance();


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


        for (final Method method : beanClass.getDeclaredMethods()) {
            System.out.println(method);
            System.out.println(method.getReturnType());
            if (method.getName().toLowerCase().contains("setname")) {
                try {
                    method.invoke(o, "Ali");
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            } else if (method.getName().toLowerCase().contains("setage")) {
                try {
                    method.invoke(o, 16);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    System.out.println(method.invoke(o));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }

        }
        List<Object> testList = Lists.newLinkedList();
        testList.add(o);
        testList.add(o);
        testList.add(o);


        List<User> userList = Lists.newLinkedList();
        userList.add(new User.UserBuilder().name("Ali").age(10).build());
        userList.add(new User.UserBuilder().name("Zalal").age(10).build());
        userList.add(new User.UserBuilder().name("Hubail").age(10).build());
        userList.add(new User.UserBuilder().name("Ali").age(40).build());
        userList.add(new User.UserBuilder().name("Ali").age(30).build());
        userList.add(new User.UserBuilder().name("Ali").age(10).build());

        Query query = new Query();
        String sqlQuery = "select name,age from test.User where name like \"%Ali%\" order by age EXECUTE ON RESULTS avg (:_allobjs, age) avgAge";
        try {
            query.parse(sqlQuery);
        } catch (QueryParseException e) {
            e.printStackTrace();
        }

        QueryResults execute = null;
        try {
            execute = query.execute(testList);
        } catch (QueryExecutionException e) {
            e.printStackTrace();
        }

        Map saveValues = execute.getSaveValues();
        Number avg = (Number) saveValues.get("avgAge");

//        MappingSession mappingSession = new MappingSession("analytics", DbConnectionManager.getSession());
//        mappingSession.setDoNotSync(true);
//
//
//        RegexPatternSet regexPatternSet = mappingSession.get(RegexPatternSet.class,"witsml");
        RegexPatternSet regexPatternSet = null;
        try {
            regexPatternSet = CacheUtil.getRegexPatternCache().get("witsml");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        List<DataTypeBinding> witsml = null;
        try {
            witsml = CacheUtil.getDataTypeBindingCache().get("witsml");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        WriteOptions options = new WriteOptions()
                .setTtl(300)
                .setTimestamp(42)
                .setConsistencyLevel(ConsistencyLevel.ANY)
                .setRetryPolicy(DefaultRetryPolicy.INSTANCE);

        Account account = new Account();
        account.setEmail("ali@alhubail.net");
        Entity entity = new Entity();
        entity.setEmail("ali@alhubail.net");
        entity.setName("Ali");
        //entity.setCode("");
//        ResultSet execute1 = DbConnectionManager.getSession().execute("select * from output_conf_cassandra;");
//        ColumnDefinitions columnDefinitions = execute1.getColumnDefinitions();
//        for(ColumnDefinitions.Definition cd : columnDefinitions.asList()){
//            //cd.getType()
//        }
        try {
            CacheUtil.getColumnDataTypeCache().get(ColumnDataType.create(Utils.KeySpace, "output_conf_cassandra", "default_value"));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        MappingSessionSingleton.getInstance().save(entity, options);
        String testRegexPattern = "(?<date>(?<year>\\d{2,4})-(?<month>\\d{1,2})-(?<day>\\d{1,2}) (?<hour>\\d{1,2}):(?<minute>\\d{1,2}):(?<second>\\d{1,2}).(?<milliSecond>\\d{3})), (?<opp>(.\\w*)), (?<status>[PF]), (?<returnCode>(.[^,]*)), (?<duration>(.[^,]*)),(?<objectVersion>(.[^,]*)),(?<objectType>(.[^,]*)),(?<prodNameAndVersion>(.[^,]*)), (?<lengthXmlIn>(.[^,]*)), (?<xmlOutLength>(.[^,]*)), (?<itemCount>(.[^,]*)), (?<columnCount>(.[^,]*)),(?<queryType>(.[^,]*)),(?<durationKeys>(.[^,]*)),(?<userHostAddress>(.[^,]*)),(?<identityName>(.[^,]*)),(?<capClient>(.[^,]*)),(?<requestedGUID>(.[^,]*)),(?<uidPath>(.[^,]*)),(?<objectIndices>(.*)),(?<indices>(.*)),(?<supMsgOut>(.*))";
        Pattern pattern = Pattern.compile(Utils.grokDictionary.digestExpression(testRegexPattern));
        String messageValue = "2013-06-21 00:18:44.182, ADD, P, 1, 0.639, V141, changeLog, , 389, 0, 0, 0, , ACH:0.328 , 10.32.17.12, administrator, , 699314cc-908f-4077-803b-70931fd0bf20, ,,, Add Changelog for well, uid = w-chLog";
        Matcher matcher = pattern.matcher(messageValue);

        logger.info(matcher.matches() + "");

        matcher = pattern.matcher(messageValue + System.lineSeparator());
        matcher.useTransparentBounds(true);
        matcher.useAnchoringBounds(false);

        logger.info(matcher.matches() + "");

        StatefulKnowledgeSession session = null;
        StatelessKnowledgeSession tempSession;
        try {
            KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
            builder.add(ResourceFactory.newClassPathResource("CalculationEngineRule.drl"), ResourceType.DRL);
            if (builder.hasErrors()) {
                throw new RuntimeException(builder.getErrors().toString());
            }

            KieBaseConfiguration configuration = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
            configuration.setOption(SequentialOption.YES);
            KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
            knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());

            session = knowledgeBase.newStatefulKnowledgeSession();


            Version version = new Version(3, 2, 1, Version.ReleaseStatus.Alpha, 0);
            FactHandle purchaseFact = session.insert(version);
            session.fireAllRules();

        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            if (session != null) {
                session.dispose();
            }
        }
    }
    Channel rpcChannel;
    MapRpcServer mapRpcServer;
public void test2(){

    rpcChannel = ((RabbitMqConnectionManagerImpl) MetricsServer.getInstance().getConnectionManager()).createChannel();

    try {
        rpcChannel.queueDeclare("AckRpcQueue", false, false, false, null);
        mapRpcServer = new MapRpcServer(rpcChannel, "AckRpcQueue"){
            @Override
            public Map<String, Object> handleMapCall(Map<String, Object> request){
                long deliveryTag = Long.parseLong(request.get("txid.emit.batch.last.message.delivery.tag").toString());
                Map responseMap = Maps.newHashMap();
                try{
                    //getConsumer().ack(deliveryTag,true);
                    responseMap.put("ack",true);
                    return responseMap;
                }catch (Exception e){
                    try{
                        responseMap.put("ack",false);
                        return responseMap;
                    }catch (Exception e1){

                    }
                }
                return responseMap;
            }
        };
        mapRpcServer.mainloop();
    } catch (Exception e) {
        //logger.error(e);
    }
}
    public void start() {
        //test2();

        metrics.server.util.Properties simplePersistenceProperties = new metrics.server.util.Properties(Utils.KeySpace,SIMPLE_PERSISTENCE_TABLE_NAME);
        Utils.addProperties(simplePersistenceProperties);

        //System.out.println("Starting Metrics Topology");
        logger.info("Starting Metrics Topology");

        Session session = DbConnectionManager.getSession();



        //metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig connectionConfig = new ConnectionConfig("localhost", 5672, "guest", "guest", ConnectionFactory.DEFAULT_VHOST, 10); // host, port, username, password, virtualHost, heartBeat

        String rabbitmq_host = Utils.getMetricsProperty("rabbitmq.host",ConnectionFactory.DEFAULT_HOST);
        int rabbitmq_port = Utils.getIntMetricsProperty("rabbitmq.port",ConnectionFactory.DEFAULT_AMQP_PORT);
        String rabbitmq_username = Utils.getMetricsProperty("rabbitmq.username",ConnectionFactory.DEFAULT_USER);
        String rabbitmq_password = Utils.getMetricsProperty("rabbitmq.password",ConnectionFactory.DEFAULT_PASS);
        String rabbitmq_virtualhost = Utils.getMetricsProperty("rabbitmq.virtualhost",ConnectionFactory.DEFAULT_VHOST);
        int rabbitmq_heartbeat = Utils.getIntMetricsProperty("rabbitmq.heartbeat",ConnectionFactory.DEFAULT_HEARTBEAT);
        metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig connectionConfig = new ConnectionConfig(
                rabbitmq_host,
                rabbitmq_port,
                rabbitmq_username,
                rabbitmq_password,
                rabbitmq_virtualhost,
                rabbitmq_heartbeat
        );
        ConsumerConfig spoutConfig = new ConsumerConfigBuilder().connection(connectionConfig)
                .queue("all.log.witsml")
                .prefetch(0)
                .requeueOnFail()
                .build();
        Map<String, Object> stringObjectMap = spoutConfig.asMap();
        //Scheme scheme = new RabbitMQMessageScheme(new SimpleJSONScheme(), "myMessageEnvelope", "myMessageProperties");
        Fields jsonFields = new Fields("@Timestamp", "Host", "Message");
        Scheme scheme = new SimpleJSONScheme(jsonFields);


        topology = new TridentTopology();   //TODO make it configurable to use TridentTopology or ExtendedTridentTopology

        //TridentTopologyBuilder builder = new TridentTopologyBuilder();

        spout = new AdvancedRabbitMQBatchSpout(spoutConfig.asMap(), scheme);
        Integer parallelism = 4;
        //builder.setSpout("id", "streamName", "txStateId", spout, parallelism, "batchGroup").addConfigurations(spoutConfig.asMap()).setMaxSpoutPending(200);
        //StormTopology stormTopology = builder.buildTopology();
        // builder.buildTopology()
        inputStream = topology.newStream("event", spout).parallelismHint(1);//.parallelismHint(Runtime.getRuntime().availableProcessors());
        Fields regexFields = new Fields("regexMapping");
        //inputStream.each(jsonFields,new RegexFunction(regexFields), regexFields);
        CassandraState.Options options = new CassandraState.Options();
        options.keyspace = Utils.KeySpace;
        options.columnFamily = "test";
        options.rowKey = "row";

        AbstractMap.SimpleEntry<String, String> test = new AbstractMap.SimpleEntry<String, String>("realtime", null);
        LinkedList<AbstractMap.SimpleEntry<String, String>> list = new LinkedList<AbstractMap.SimpleEntry<String, String>>();
        list.add(test);
        RowHolderInfo rowHolderInfo = new RowHolderInfo("analytics", "witsml", list);
        System.out.println(rowHolderInfo instanceof Serializable);
        System.out.println(rowHolderInfo.keyspace() instanceof Serializable);
        System.out.println(rowHolderInfo.table() instanceof Serializable);
        System.out.println(rowHolderInfo.columnValueList() instanceof Serializable);
        System.out.println(rowHolderInfo.columnValueList().get(0).getKey() instanceof Serializable);
        System.out.println(rowHolderInfo.columnValueList().get(0).getValue() instanceof Serializable);
        try {
            toString(rowHolderInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            SerializationUtil.serialize(rowHolderInfo);
        } catch (Exception e) {
            logger.error(String.valueOf(e));
        }


        try {
            SerializationUtil.serialize(list);
        } catch (Exception e) {
            logger.error(String.valueOf(e));
        }

        Fields patternFields = new Fields("patternName", "groupValueMap");
        Stream stream = inputStream.shuffle().each(jsonFields, new PatternMatcherFunction(patternFields, "witsml"), patternFields).parallelismHint(4)

                .each(patternFields, new TypeConverterFunction(patternFields, "witsml"), new Fields("convertedGroupValueMap")).parallelismHint(4)
                .each(new Fields("patternName", "convertedGroupValueMap"), new ObjectMapperFunction(new Fields("patternName", "convertedGroupValueMap"), "witsml"), new Fields("classObject")).parallelismHint(4)
                .each(new Fields("@Timestamp"), new RoundDateTimeFunction(new Period(0, 0, 1, 0), DateTimeHelper.RoundType.FLOOR), new Fields("Timestamp"))
                .partitionBy(new Fields("Host")).parallelismHint(4);
        IAggregatableStream each = stream.each(new Fields("Timestamp"), new LateComingFilter(rowHolderInfo, false))
                .each(new Fields("Host", "Timestamp"), new UniqueHostTimestamp(), new Fields("HostTimestamp"))
                //.groupBy(new Fields("HostTimestamp"))   //HOST TODO
                        //.persistentAggregate()
                        //.project()
                .groupBy(new Fields("HostTimestamp"))
                .partitionAggregate(new Fields("classObject", "Host", "Timestamp"), new AccumulatingAggregator("witsml"), new Fields("calculatedResult","Host", "Timestamp"))

                //.each(new Fields("calculatedResult"), new TestFunction(), new Fields("Test"))

                .each(new Fields("calculatedResult", "Timestamp", "Host"), new CassandraMapping("witsml", "individual"),new Fields());
//
//
//                        //.persistentAggregate(CassandraState.nonTransactional("localhost", options),new Fields("calculatedResult"),new TempCount(), new Fields("count")).newValuesStream();
//
//                .project(new Fields("calculatedResult", "Timestamp", "Host")).parallelismHint(4).each(new Fields("calculatedResult", "Timestamp", "Host"), new CassandraMapping("witsml", "individual")).parallelismHint(4);
//
//        Stream lateComingOnes = stream.each(new Fields("Timestamp"), new LateComingFilter(rowHolderInfo, true))
//
//                .groupBy(new Fields("Host", "Timestamp"))
//                        //.persistentAggregate()
//                        //.project()
//
//                .aggregate(new Fields("classObject", "Timestamp", "Host"), new LateCommingAccumulatingAggregator("witsml", "individual"), new Fields("calculatedResult"))
//
//                        //.persistentAggregate(CassandraState.nonTransactional("localhost", options),new Fields("calculatedResult"),new TempCount(), new Fields("count")).newValuesStream();
//
//                .project(new Fields("calculatedResult", "Timestamp", "Host")).each(new Fields("calculatedResult", "Timestamp", "Host"), new CassandraMapping("witsml", "individual"));
//
//        //inputStream.each(jsonFields,new PrintFilter());
//        //inputStream.each(new RegexFunction())
//
////        for(int i = 0; i < 5; i++){
////            topology.newStream("event",new AdvancedRabbitMQBatchSpout(spoutConfig.asMap(),new SimpleJSONScheme(jsonFields))).each(new Fields("event"),new PrintFilter());
////        }
//
//        //LocalCluster;
        Config conf = new Config();
//        conf.put(Config.TOPOLOGY_RECEIVER_BUFFER_SIZE,             8);
//        conf.put(Config.TOPOLOGY_TRANSFER_BUFFER_SIZE,            32);
//        conf.put(Config.TOPOLOGY_EXECUTOR_RECEIVE_BUFFER_SIZE, 16384);
//        conf.put(Config.TOPOLOGY_EXECUTOR_SEND_BUFFER_SIZE,    16384);
        conf.put(Config.TOPOLOGY_TRIDENT_BATCH_EMIT_INTERVAL_MILLIS,
                30000);
        conf.put(Config.TOPOLOGY_DEBUG, false);
        conf.setNumWorkers(Utils.getIntMetricsProperty("topology.conf.numworkers",4));
        conf.setMaxSpoutPending(1);

//        System.out.println("Submitting Metrics Topology");
//        logger.info("Submitting Metrics Topology");
//        try {
//            StormSubmitter.submitTopology("Metrics", conf, topology.build());
//            logger.info("Submitted Metrics Topology");
//            System.out.println("Submitted Metrics Topology");
//        } catch (AlreadyAliveException e) {
//            logger.error("error",e);
//            e.printStackTrace();
//        } catch (InvalidTopologyException e) {
//            logger.error("error",e);
//            e.printStackTrace();
//        }
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("ali2", conf, topology.build());



//
    }
}
