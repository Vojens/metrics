package metrics.server.storm.trident.operator.function;

import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.util.List;

/**
 * Created by ali on 9/2/14.
 */
public class UniqueHostTimestamp extends BaseFunction{
    private static final transient Logger logger = Logger.getLogger(UniqueHostTimestamp.class.getName());
    @Override
    public void execute(TridentTuple objects, TridentCollector tridentCollector) {
        try{
            String hostTimestamp = objects.getString(0)+','+objects.getString(1);
            List<Object> list = Lists.newArrayList();
            list.add(hostTimestamp);
            tridentCollector.emit(list);
        }catch(Exception e){
            logger.error(e,e);
        }

    }


}
