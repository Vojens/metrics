package metrics.server.storm.trident.operator.function;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import metrics.server.util.helper.DateTimeHelper;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Period;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/13/14.
 */
public class RoundDateTimeFunction extends BaseFunction {
    private static final transient Logger logger = Logger.getLogger(RoundDateTimeFunction.class.getName());
    @Inject
    DateTimeHelper.RoundType roundType;
    @Inject
    Period period = null;

    public RoundDateTimeFunction() {
        super();
        //this(new Period(0,0,1,0), DateTimeHelper.RoundType.FLOOR);
    }

    public RoundDateTimeFunction(Period period, DateTimeHelper.RoundType roundType) {
        super();
        this.period = period;
        this.roundType = roundType;
    }

    @Override
    public void execute(TridentTuple objects, TridentCollector tridentCollector) {
        try {
            checkArgument(checkNotNull(objects).size() >= 1);
            DateTime dt = (DateTime) objects.get(0);
            DateTime roundedDT = DateTimeHelper.getRoundDateTime(dt, period, roundType);

            List<Object> outputList = Lists.newArrayList();
            outputList.add(roundedDT.toString());
            tridentCollector.emit(outputList);
        } catch (Exception e) {
            logger.error(e, e);
        }
    }


}
