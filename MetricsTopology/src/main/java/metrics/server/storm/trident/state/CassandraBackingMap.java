package metrics.server.storm.trident.state;

import org.apache.log4j.Logger;
import storm.trident.state.map.IBackingMap;

import java.util.List;
import java.util.Map;

/**
 * Created by ali on 8/14/14.
 */
public class CassandraBackingMap implements IBackingMap<Map<String, Object>> {
    private static final transient Logger logger = Logger.getLogger(CassandraBackingMap.class.getName());

    @Override
    public List<Map<String, Object>> multiGet(List<List<Object>> lists) {
        return null;
    }

    @Override
    public void multiPut(List<List<Object>> lists, List<Map<String, Object>> maps) {
        logger.info(maps);
    }
}
