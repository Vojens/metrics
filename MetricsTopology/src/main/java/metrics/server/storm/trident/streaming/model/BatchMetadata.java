package metrics.server.storm.trident.streaming.model;

import java.io.Serializable;

/**
 * Created by ali on 9/15/14.
 */
public class BatchMetadata implements Serializable {
    public storm.trident.spout.IBatchID batchId;
    public BatchStateType state;
    public java.lang.String batchGroup;

    public BatchMetadata(java.lang.String batchGroup, storm.trident.spout.IBatchID batchId, BatchStateType state) {
        this.batchGroup = batchGroup;
        this.batchId = batchId;
        this.state = state;
    }
}
