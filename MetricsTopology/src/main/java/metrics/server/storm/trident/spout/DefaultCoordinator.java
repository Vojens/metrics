package metrics.server.storm.trident.spout;

import backtype.storm.task.TopologyContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import metrics.server.MetricsServer;
import metrics.server.io.channel.ChannelCallable;
import metrics.server.storm.trident.streaming.model.BatchMetadata;
import metrics.server.storm.trident.streaming.model.BatchStateType;
import metrics.server.storm.trident.streaming.model.SerializedTransactionAttempt;
import metrics.server.storm.trident.topology.MetricsTopology;
import metrics.server.util.Utils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.RpcClient;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import storm.trident.spout.ITridentSpout;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by ali on 6/22/14.
 */
public class DefaultCoordinator implements ITridentSpout.BatchCoordinator<BatchMetadata>,
        Serializable {


    private static final Logger logger = Logger.getLogger(DefaultCoordinator.class);
    private static final long serialVersionUID = -7921152512216469421L;
    //private DateTime nextDateTime;
    private DateTime startCoordinatingTime;
    @Inject
    private Duration interval;
    transient private long batchNumber = 0;
    @Inject
    private CoordinatingDateTimeType coordinatingDateTimeType;
    //@Inject
   // private EventEmitter<DateTime> eventEmitter;

    @Inject
    private Map<String, Object> mapConfig;
    //@Inject
    //private Properties simplePersistence = null;
    private static final DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis();

    private transient Client client;
    private transient WebResource webResource;
    private AtomicBoolean isPrepared = new AtomicBoolean(false);

    public DefaultCoordinator( Map<String, Object> mapConfig, Duration interval, CoordinatingDateTimeType coordinatingDateTimeType) {
        //this.eventEmitter = eventEmitter;
        this.interval = interval;
        this.coordinatingDateTimeType = coordinatingDateTimeType;
        this.mapConfig = mapConfig;
        //simplePersistence = Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME);


        initClient();

    }
    public DefaultCoordinator() {

    }

    private void initClient(){
        //eventEmitter.getAcknowledger().getQueueName();

        client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter(mapConfig.get("rabbitmq.username").toString(),mapConfig.get("rabbitmq.password").toString()));
        String virtual_host = "/";
        virtual_host = mapConfig.get("rabbitmq.virtualhost").toString();
        if(virtual_host.trim().equals("/")){
            virtual_host = "%2f";
        }

        String url = String.format("http://%s:%d/api/queues/%s/%s",mapConfig.get("rabbitmq.host"),Utils.getIntMetricsProperty("rabbitmq.api.port",15672), virtual_host,mapConfig.get("rabbitmq.queueName"));   //TODO handle SSL
        //URI uri = null;
//        try {
//            //uri = new URI("http",null,mapConfig.get("rabbitmq.host"),Utils.getIntMetricsProperty("rabbitmq.api.port",15672),"/"+mapConfig.get("rabbitmq.virtualhost")+"/"+);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
        //url = uri.toASCIIString();
//        try {
//            url = URLDecoder.decode(url, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        webResource = client.resource(url);//;("http://"":15672/api/queues/%2f/all.log.witsml");
        //String s = webResource.get(String.class);
    }

    public boolean isPrepared() {
        return isPrepared.get();
    }
    public void setIsPrepared(boolean isPrepared){
        this.isPrepared.set(isPrepared);
    }

    private long getQueueMessagesReadyCount(){

        return getNodeAsLong("messages_ready");

    }
    private long getQueueMessagesCount(){
        return getNodeAsLong("messages");
    }

    public long getNodeAsLong(String nodeName){
        ClientResponse response = null;
        try{
            if(webResource == null){
                initClient();
            }
            response = webResource.accept("application/json")
                    .get(ClientResponse.class);

            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }

            String output = response.getEntity(String.class);
            //System.out.println(output);
            JsonNode rootNode = Utils.objectMapper.readTree(output);
            JsonNode messagesReadyNode = rootNode.path(nodeName);
            if(messagesReadyNode.canConvertToLong()){
                return messagesReadyNode.asLong();
            }else{
                return 0;
            }


        }catch (Exception e){
            logger.error(e,e);
        }finally {
            try{
                if(response != null){
                    response.close();
                }
            }catch (Exception e){
                //do nothing
            }

        }
        return -1;
    }

//    public static DateTime getCurrentDateTime(){
//        DateTime dateTime;
//        dateTime = DateTime.now();
//        dateTime = dateTime.minusSeconds(dateTime.getSecondOfMinute());
//        dateTime = dateTime.minusMillis(dateTime.getMillisOfSecond());
//        return dateTime;
//    }
//    protected boolean isEqualTimestampUpToMinute(final DateTime dateTime1, final DateTime dateTime2){
//        LocalDateTime localDateTime1 = dateTime1.toLocalDateTime();
//        LocalDateTime localDateTime2 = dateTime2.toLocalDateTime();
//        localDateTime1 = localDateTime1.minusSeconds(localDateTime1.secondOfMinute().get());
//        localDateTime1 = localDateTime1.minusMillis(localDateTime1.millisOfSecond().get());
//        localDateTime2 = localDateTime2.minusSeconds(localDateTime2.secondOfMinute().get());
//        localDateTime2 = localDateTime2.minusMillis(localDateTime2.millisOfSecond().get());
//        //DateTime tempDateTime1 = new DateTime(localDateTime1.year(),localDateTime1.monthOfYear(),localDateTime1.getDayOfMonth(),localDateTime1.getHourOfDay(),localDateTime1.getMinuteOfHour());
//        return localDateTime1.isEqual(localDateTime2);
//    }

    @Override
    public boolean isReady(long txid) {
        try{
            Long messagesCount = getQueueMessagesReadyCount();

            if(messagesCount == 0){
                return false;
            }else// if (messagesReadyCount == -1 || messagesReadyCount > 0){
            {
                if(txid == 1){
                    return true;
                }else{
                    long txidInDb = Long.parseLong(Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).getOnDemand("txid"));
                    if((txid - 1) == txidInDb){
                        return true;
                    }else if(txid == txidInDb){ //when a failure occured and this needs to replay
                        return true;
                    }
                    return false;
                }
            }
        }catch (Exception e){
            logger.error(e,e);
        }
        return false;
//        try{
//            if(txid == 1){
//                return true;
//            }else{
//                long txidInDb = Long.parseLong(Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).getOnDemand("txid"));
//                if((txid - 1) == txidInDb){
//                    return true;
//                }
//                return false;
//            }
//        }catch (Exception e){
//            logger.error(e,e);
//        }
//        return false;




//        DateTime endCoordinatingTime = DateTime.now();
//        if (startCoordinatingTime == null) {
//            switch (coordinatingDateTimeType) {
//                case ABSOLUTE:
//                    startCoordinatingTime = endCoordinatingTime;
//                    break;
//                case RELATIVE:
//                    startCoordinatingTime = eventEmitter.getLatestDateTime();
//                    break;
//            }
//
//            return true;
//        }
//
//
//        Period period = null;
//        boolean isPassedInterval;
//        switch (coordinatingDateTimeType) {
//            case ABSOLUTE:
//                period = new Period(startCoordinatingTime, endCoordinatingTime);
//                isPassedInterval = period.toStandardDuration().compareTo(interval) >= 0;
//                if (isPassedInterval) {
//                    startCoordinatingTime = endCoordinatingTime;
//                    return true;
//                }
//                return false;
//            case RELATIVE:
//                period = new Period(startCoordinatingTime, eventEmitter.getLatestDateTime());
//                isPassedInterval = period.toStandardDuration().compareTo(interval) >= 0;
//                if (isPassedInterval) {
//                    startCoordinatingTime = eventEmitter.getLatestDateTime();
//                    return true;
//                }
//                return false;
//        }
//
//
////        if(!this.isDateTimeUsed){
////            isDateTimeUsed = true;
////            return true;
////        }
//        return true;

//        logger.debug("isReady = "+txid);
//        if(batchNumber == txid){
//            return false;
//        }else{
//            DateTime currentDateTime = getCurrentDateTime();
//            if(isEqualTimestampUpToMinute(this.nextDateTime,currentDateTime) || currentDateTime.isAfter(nextDateTime)){
//                batchNumber = txid;
//                this.nextDateTime = currentDateTime.plusMinutes(1);
//                logger.info("isReady");
//                return true;
//            }else{
//                return false;
//            }
//        }

    }

    private Map config;
    public void open(final Map config,
                     final TopologyContext context) {
        try {
            //setAcknowledger(StormUtils.createAcknowledger(config, new Declarator.NoOp()));
            //acknowledger.open();
            isPrepared.set(true);
        } catch (Exception e) {
            logger.error(e, e);
        }

    }

    @Override
    public void close() {
        logger.info("coordinator - close");
    }

    @Override
    public BatchMetadata initializeTransaction(long l, BatchMetadata prevBatchInfo, BatchMetadata currentBatchInfo) {
        logger.info("Initializing Transaction [" + l + "]");
        //return nextDateTime.minusMinutes(1);
        SerializedTransactionAttempt ta = new SerializedTransactionAttempt(l,1);

        BatchMetadata batchInfo = new BatchMetadata("group1",ta, BatchStateType.NORMAL);

        return batchInfo;
    }

    @Override
    public void success(long txid) {
        try{
            logger.info("Successful Transaction [" + txid + "]");
            Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid", txid);
            Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid.last.success",dateTimeFormatter.print(DateTime.now()));

            String lastMessageDeliveryTag = Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).getOnDemand("txid.emit.batch.last.message.delivery.tag");
            if(!(lastMessageDeliveryTag == null || lastMessageDeliveryTag.isEmpty())){
                long deliveryTag = Long.parseLong(lastMessageDeliveryTag);
                //getAcknowledger().ack(deliveryTag, true);
                final Map<String, Object> rpcMap = Maps.newHashMap();

                rpcMap.put("txid.emit.batch.last.message.delivery.tag",deliveryTag);
                Map<String, Object> ackRpcQueue = (Map<String, Object>) MetricsServer.getInstance().getConnectionManager().call(new ChannelCallable<Object>() {
                    @Override
                    public String getDescription() {
                        return "RabbitMQ Batch Coordinator";
                    }

                    @Override
                    public Object call(Channel channel) throws IOException {
                        //channel.rpc(new )
                        channel.queueDeclare("AckRpcQueue", false, false, false, null);
                        RpcClient rcpClient = new RpcClient(channel, "", "AckRpcQueue");
                        Map<String, Object> callResponse = null;
                        try {
                            callResponse = rcpClient.mapCall(rpcMap);

                        } catch (TimeoutException e) {
                            logger.error(e);
                        }
                        return callResponse;
                    }
                });
            }



        }catch(Exception e){
            logger.error(e,e);
        }

    }

//    public RabbitMQConsumer getAcknowledger() {
//        return acknowledger;
//    }


    public enum CoordinatingDateTimeType {
        RELATIVE,
        ABSOLUTE
    }


//    private TopologyContext context;
//    public void setContext(TopologyContext context){
//        this.context = context;
//
//    }
    //private RabbitMQAcknowledger acknowledger;
//    public void setAcknowledger(RabbitMQAcknowledger acknowledger){
//        this.acknowledger = acknowledger;
//    }

}
