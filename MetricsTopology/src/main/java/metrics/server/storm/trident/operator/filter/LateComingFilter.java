package metrics.server.storm.trident.operator.filter;

import com.datastax.driver.core.ResultSet;
import metrics.server.util.CacheUtil;
import metrics.server.util.helper.RowHolderInfo;
import org.apache.log4j.Logger;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.AbstractMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/24/14.
 */

public class LateComingFilter implements Filter {
    private static final transient Logger logger = Logger.getLogger(LateComingFilter.class.getName());
    //private String keyspace;
    //private String tableName;
    private RowHolderInfo rowHolderInfo;
    private boolean isInverse;  //if false then it will pass the non existing ones. otherwise, it passes the existing ones

    public LateComingFilter(RowHolderInfo rowHolderInfo, boolean isInverse) {

        //this.keyspace = checkNotNull(keyspace, "keyspace cannot be null");
        //this.tableName = checkNotNull(tableName, "tablename cannot be null");
        this.rowHolderInfo = checkNotNull(rowHolderInfo, "Row Holder info cannot be null");
        this.isInverse = isInverse;
    }

    @Override
    public boolean isKeep(TridentTuple objects) {
        ResultSet resultSet = null;
        try{
            int i = 0;
            RowHolderInfo info = rowHolderInfo.deepCopy();
            for (AbstractMap.SimpleEntry<String, String> stringObjectEntry : info.columnValueList()) {
                //DataType dataType = CacheUtil.getColumnDataTypeCache().get(ColumnDataType.create(info.keyspace(), info.table(), stringObjectEntry.getKey()));
                stringObjectEntry.setValue(objects.get(i++).toString());
            }
            resultSet = CacheUtil.getRowCache().get(info);

        }catch (ExecutionException e) {
            logger.error(e,e);
        }catch(Exception e){
            logger.error(e,e);
        }

        boolean dataExists = (resultSet != null) && (resultSet.all().size() > 0);
        return (!dataExists && !isInverse) || (dataExists && isInverse); //do a fucking truth table and k-map
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {
    }

    @Override
    public void cleanup() {

    }
}
