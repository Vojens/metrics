package metrics.server.storm.trident.streaming.model;

import storm.trident.topology.TransactionAttempt;

import java.io.Serializable;

/**
 * Created by ali on 9/15/14.
 */
public class SerializedTransactionAttempt extends TransactionAttempt implements Serializable {
    public SerializedTransactionAttempt() { super(); }

    public SerializedTransactionAttempt(java.lang.Long txid, int attemptId) {
        super(txid, attemptId);

    }

}
