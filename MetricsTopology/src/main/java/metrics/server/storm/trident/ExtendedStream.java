package metrics.server.storm.trident;

import storm.trident.Stream;
import storm.trident.planner.Node;

/**
 * Created by ali on 9/18/14.
 */
public class ExtendedStream  extends Stream{
    Node _node;
    ExtendedTridentTopology _topology;
    String _name;

    protected ExtendedStream(ExtendedTridentTopology topology, String name, Node node) {
        super(topology,name,node);
        _topology = topology;
        _node = node;
        _name = name;
    }

//    public ExtendedStream name(String name) {
//        return new ExtendedStream(_topology, name, _node);
//    }
//
//    public ExtendedStream parallelismHint(int hint) {
//        _node.parallelismHint = hint;
//        return this;
//    }
//
//    public ExtendedStream project(Fields keepFields) {
//        projectionValidation(keepFields);
//        return _topology.addSourcedNode(this, new ProcessorNode(_topology.getUniqueStreamId(), _name, keepFields, new Fields(), new ProjectedProcessor(keepFields)));
//    }
//
//    public GroupedStream groupBy(Fields fields) {
//        projectionValidation(fields);
//        return new GroupedStream(this, fields);
//    }
//
//    public ExtendedStream partitionBy(Fields fields) {
//        projectionValidation(fields);
//        return partition(Grouping.fields(fields.toList()));
//    }
//
//    public ExtendedStream partition(CustomStreamGrouping partitioner) {
//        return partition(Grouping.custom_serialized(Utils.serialize(partitioner)));
//    }
//
//    public ExtendedStream shuffle() {
//        return partition(Grouping.shuffle(new NullStruct()));
//    }
//
//    public ExtendedStream global() {
//        // use this instead of storm's built in one so that we can specify a singleemitbatchtopartition
//        // without knowledge of storm's internals
//        return partition(new GlobalGrouping());
//    }
//
//    public ExtendedStream batchGlobal() {
//        // the first field is the batch id
//        return partition(new IndexHashGrouping(0));
//    }
//
//    public ExtendedStream broadcast() {
//        return partition(Grouping.all(new NullStruct()));
//    }
//
//    public ExtendedStream identityPartition() {
//        return partition(new IdentityGrouping());
//    }

//    public ExtendedStream partition(Grouping grouping) {
//        if(_node instanceof PartitionNode) {
//            return each(new Fields(), new TrueFilter()).partition(grouping);
//        } else {
//            return _topology.addSourcedNode(this, new PartitionNode(_node.streamId, _name, getOutputFields(), grouping));
//        }
//    }

//    public ExtendedStream applyAssembly(Assembly assembly) {
//        return assembly.apply(this);
//    }

//    @Override
//    public ExtendedStream each(Fields inputFields, Function function, Fields functionFields) {
//        projectionValidation(inputFields);
//        return _topology.addSourcedNode(this,
//                new ProcessorNode(_topology.getUniqueStreamId(),
//                        _name,
//                        TridentUtils.fieldsConcat(getOutputFields(), functionFields),
//                        functionFields,
//                        new EachProcessor(inputFields, function)));
//    }
//
//    //creates brand new tuples with brand new fields
//    @Override
//    public ExtendedStream partitionAggregate(Fields inputFields, Aggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        return _topology.addSourcedNode(this,
//                new ProcessorNode(_topology.getUniqueStreamId(),
//                        _name,
//                        functionFields,
//                        functionFields,
//                        new AggregateProcessor(inputFields, agg)));
//    }

//    public ExtendedStream stateQuery(TridentState state, Fields inputFields, QueryFunction function, Fields functionFields) {
//        projectionValidation(inputFields);
//        String stateId = state._node.stateInfo.id;
//        Node n = new ProcessorNode(_topology.getUniqueStreamId(),
//                _name,
//                TridentUtils.fieldsConcat(getOutputFields(), functionFields),
//                functionFields,
//                new StateQueryProcessor(stateId, inputFields, function));
//        _topology._colocate.get(stateId).add(n);
//        return _topology.addSourcedNode(this, n);
//    }

//    public TridentState partitionPersist(StateFactory stateFactory, Fields inputFields, StateUpdater updater, Fields functionFields) {
//        return partitionPersist(new StateSpec(stateFactory), inputFields, updater, functionFields);
//    }
//
//    public TridentState partitionPersist(StateSpec stateSpec, Fields inputFields, StateUpdater updater, Fields functionFields) {
//        projectionValidation(inputFields);
//        String id = _topology.getUniqueStateId();
//        ProcessorNode n = new ProcessorNode(_topology.getUniqueStreamId(),
//                _name,
//                functionFields,
//                functionFields,
//                new PartitionPersistProcessor(id, inputFields, updater));
//        n.committer = true;
//        n.stateInfo = new NodeStateInfo(id, stateSpec);
//        return _topology.addSourcedStateNode(this, n);
//    }
//
//    public TridentState partitionPersist(StateFactory stateFactory, Fields inputFields, StateUpdater updater) {
//        return partitionPersist(stateFactory, inputFields, updater, new Fields());
//    }
//
//    public TridentState partitionPersist(StateSpec stateSpec, Fields inputFields, StateUpdater updater) {
//        return partitionPersist(stateSpec, inputFields, updater, new Fields());
//    }
//
//    public ExtendedStream each(Function function, Fields functionFields) {
//        return each(null, function, functionFields);
//    }
//
//    public ExtendedStream each(Fields inputFields, Filter filter) {
//        return each(inputFields, new FilterExecutor(filter), new Fields());
//    }
//
//    public ChainedAggregatorDeclarer chainedAgg() {
//        return new ChainedAggregatorDeclarer(this, new BatchGlobalAggScheme());
//    }
//
//    public ExtendedStream partitionAggregate(Aggregator agg, Fields functionFields) {
//        return partitionAggregate(null, agg, functionFields);
//    }

//    public ExtendedStream partitionAggregate(CombinerAggregator agg, Fields functionFields) {
//        return partitionAggregate(null, agg, functionFields);
//    }

//    public ExtendedStream partitionAggregate(Fields inputFields, CombinerAggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        return chainedAgg()
//                .partitionAggregate(inputFields, agg, functionFields)
//                .chainEnd();
//    }

//    public ExtendedStream partitionAggregate(ReducerAggregator agg, Fields functionFields) {
//        return partitionAggregate(null, agg, functionFields);
//    }

//    public ExtendedStream partitionAggregate(Fields inputFields, ReducerAggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        return chainedAgg()
//                .partitionAggregate(inputFields, agg, functionFields)
//                .chainEnd();
//    }

//    public ExtendedStream aggregate(Aggregator agg, Fields functionFields) {
//        return aggregate(null, agg, functionFields);
//    }
//
//    public ExtendedStream aggregate(Fields inputFields, Aggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        return chainedAgg()
//                .aggregate(inputFields, agg, functionFields)
//                .chainEnd();
//    }
//
//    public ExtendedStream aggregate(CombinerAggregator agg, Fields functionFields) {
//        return aggregate(null, agg, functionFields);
//    }
//
//    public ExtendedStream aggregate(Fields inputFields, CombinerAggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        return chainedAgg()
//                .aggregate(inputFields, agg, functionFields)
//                .chainEnd();
//    }
//
//    public ExtendedStream aggregate(ReducerAggregator agg, Fields functionFields) {
//        return aggregate(null, agg, functionFields);
//    }
//
//    public ExtendedStream aggregate(Fields inputFields, ReducerAggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        return chainedAgg()
//                .aggregate(inputFields, agg, functionFields)
//                .chainEnd();
//    }
//
//    public TridentState partitionPersist(StateFactory stateFactory, StateUpdater updater, Fields functionFields) {
//        return partitionPersist(new StateSpec(stateFactory), updater, functionFields);
//    }
//
//    public TridentState partitionPersist(StateSpec stateSpec, StateUpdater updater, Fields functionFields) {
//        return partitionPersist(stateSpec, null, updater, functionFields);
//    }
//
//    public TridentState partitionPersist(StateFactory stateFactory, StateUpdater updater) {
//        return partitionPersist(stateFactory, updater, new Fields());
//    }
//
//    public TridentState partitionPersist(StateSpec stateSpec, StateUpdater updater) {
//        return partitionPersist(stateSpec, updater, new Fields());
//    }
//
//    public TridentState persistentAggregate(StateFactory stateFactory, CombinerAggregator agg, Fields functionFields) {
//        return persistentAggregate(new StateSpec(stateFactory), agg, functionFields);
//    }
//
//    public TridentState persistentAggregate(StateSpec spec, CombinerAggregator agg, Fields functionFields) {
//        return persistentAggregate(spec, null, agg, functionFields);
//    }
//
//    public TridentState persistentAggregate(StateFactory stateFactory, Fields inputFields, CombinerAggregator agg, Fields functionFields) {
//        return persistentAggregate(new StateSpec(stateFactory), inputFields, agg, functionFields);
//    }
//
//    public TridentState persistentAggregate(StateSpec spec, Fields inputFields, CombinerAggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        // replaces normal aggregation here with a global grouping because it needs to be consistent across batches
//        return new ChainedAggregatorDeclarer(this, new GlobalAggScheme())
//                .aggregate(inputFields, agg, functionFields)
//                .chainEnd()
//                .partitionPersist(spec, functionFields, new CombinerAggStateUpdater(agg), functionFields);
//    }
//
//    public TridentState persistentAggregate(StateFactory stateFactory, ReducerAggregator agg, Fields functionFields) {
//        return persistentAggregate(new StateSpec(stateFactory), agg, functionFields);
//    }
//
//    public TridentState persistentAggregate(StateSpec spec, ReducerAggregator agg, Fields functionFields) {
//        return persistentAggregate(spec, null, agg, functionFields);
//    }
//
//    public TridentState persistentAggregate(StateFactory stateFactory, Fields inputFields, ReducerAggregator agg, Fields functionFields) {
//        return persistentAggregate(new StateSpec(stateFactory), inputFields, agg, functionFields);
//    }
//
//    public TridentState persistentAggregate(StateSpec spec, Fields inputFields, ReducerAggregator agg, Fields functionFields) {
//        projectionValidation(inputFields);
//        return global().partitionPersist(spec, inputFields, new ReducerAggStateUpdater(agg), functionFields);
//    }
//
//    public ExtendedStream stateQuery(TridentState state, QueryFunction function, Fields functionFields) {
//        return stateQuery(state, null, function, functionFields);
//    }
//
//    @Override
//    public ExtendedStream toStream() {
//        return this;
//    }
//
//    @Override
//    public Fields getOutputFields() {
//        return _node.allOutputFields;
//    }
//
//    static class BatchGlobalAggScheme implements GlobalAggregationScheme<ExtendedStream> {
//
//        @Override
//        public IAggregatableStream aggPartition(ExtendedStream s) {
//            return s.batchGlobal();
//        }
//
//        @Override
//        public SingleEmitAggregator.BatchToPartition singleEmitPartitioner() {
//            return new IndexHashBatchToPartition();
//        }
//
//    }
//
//    static class GlobalAggScheme implements GlobalAggregationScheme<ExtendedStream> {
//
//        @Override
//        public IAggregatableStream aggPartition(ExtendedStream s) {
//            return s.global();
//        }
//
//        @Override
//        public SingleEmitAggregator.BatchToPartition singleEmitPartitioner() {
//            return new GlobalBatchToPartition();
//        }
//
//    }
//
//    private void projectionValidation(Fields projFields) {
//        if (projFields == null) {
//            return;
//        }
//
//        Fields allFields = this.getOutputFields();
//        for (String field : projFields) {
//            if (!allFields.contains(field)) {
//                throw new IllegalArgumentException("Trying to select non-existent field: '" + field + "' from stream containing fields fields: <" + allFields + ">");
//            }
//        }
//    }
}
