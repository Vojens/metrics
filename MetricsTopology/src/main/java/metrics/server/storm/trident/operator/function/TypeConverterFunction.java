package metrics.server.storm.trident.operator.function;

import backtype.storm.tuple.Fields;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import metrics.server.util.CacheUtil;
import metrics.server.util.Utils;
import metrics.server.util.helper.PatternInfo;
import metrics.server.util.type.converter.ConverterInfo;
import org.apache.log4j.Logger;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 7/23/14.
 */
public class TypeConverterFunction extends BaseFunction {
    private static final transient Logger logger = Logger.getLogger(TypeConverterFunction.class.getName());

    Fields fields;
    String type;

    public TypeConverterFunction(Fields fields, String type) {
        this.fields = fields;
        this.type = type;
    }

    public TypeConverterFunction(String patternNameField, String groupValueMapField, String type) {
        this.fields = new Fields(patternNameField, groupValueMapField);
        this.type = type;
    }

    @Override
    @SuppressWarnings(value = {"unchecked"})
    //emits: pattern name, map of found groups
    public void execute(TridentTuple objects, TridentCollector tridentCollector) {


        try {
            checkNotNull(objects);
            checkArgument(objects.size() >= 2);

            Map<String, PatternInfo> patternInfoMap = CacheUtil.getPatternCache().get(type);
            PatternInfo patternInfo = patternInfoMap.get(objects.getString(0));
            Map<String, String> groupValueMap = (Map<String, String>) objects.get(1);


            Map.Entry<Class<?>, Map<String, ConverterInfo>> beanClassWithDetails = patternInfo.getBeanClassWithDetails();
            Class<?> beanClass = beanClassWithDetails.getKey();
            Map<String, ConverterInfo> classProperties = beanClassWithDetails.getValue();
            Object classObject = beanClass.newInstance();

            Map<String, Map.Entry<Object, Class<?>>> valueMap = Maps.newHashMap();
            for (final Map.Entry<String, ConverterInfo> classProperty : classProperties.entrySet()) {
                ConverterInfo converterInfo = classProperty.getValue();
                Object value = null;
                if (converterInfo.isConverterActivated()) {
                    value = Utils.typeConverterManagerBean.convertType(groupValueMap.get(classProperty.getKey()), converterInfo.getClassType(), converterInfo.getFormat());

                } else {
                    value = groupValueMap.get(classProperty.getKey());
                }
//                List<Method> methodList = Arrays.asList(beanClass.getDeclaredMethods());
//
//                Optional<Method> methodOptional = Iterables.tryFind(methodList, new com.google.common.base.Predicate<Method>() {
//                    @Override
//                    public boolean apply(@Nullable Method input) {
//                        return input.getName().toLowerCase().contains("set"+classProperty.getKey().toLowerCase());
//                    }
//                });
//
//                if(methodOptional.isPresent()){
//                    methodOptional.get().invoke(classObject,value);
//                }
                valueMap.put(classProperty.getKey(), new AbstractMap.SimpleEntry<Object, Class<?>>(value, converterInfo.getClassType()));
            }
            List<Object> outputList = Lists.newArrayList();
            outputList.add(valueMap);
            tridentCollector.emit(outputList);


//            Map.Entry<String, Map<String,String>> patternGroupNameValueEntry = null;
//            for_loop: for (Map.Entry<String, PatternInfo> stringPatternInfoEntry : patternInfoMap.entrySet()) {
//                PatternInfo patternInfo = stringPatternInfoEntry.getValue();
//                Matcher matcher = patternInfo.getPattern().matcher(message);
//                if(matcher.find()){
//                    groupValueMap = Maps.newHashMap();
//
//
//                    for (String groupName : patternInfo.getNamedGroupMap().keySet()) {
//
//                        groupValueMap.put(groupName,matcher.group(groupName));
//                    }
//                    patternGroupNameValueEntry = new AbstractMap.SimpleEntry<String, Map<String,String>>(stringPatternInfoEntry.getKey(),groupValueMap);
//                    break for_loop;
//                }
//            }

//            for_loop:for(PatternInfo patternInfo : patternInfoMap.values()){
//                Matcher matcher = patternInfo.getPattern().matcher(message);
//                if(matcher.find()){
//                    groupValueMap = Maps.newHashMap();
//                    Map.Entry<Class<?>, Map<String, ConverterInfo>> beanClassWithDetails = patternInfo.getBeanClassWithDetails();
//                    Class<?> beanClass = beanClassWithDetails.getKey();
//                    Map<String, ConverterInfo> classProperties = beanClassWithDetails.getValue();
//                    Object object = beanClass.newInstance();
//                    //ConvertBean convertBean = Utils.typeConverterManagerBean.getConvertBean();
//                    for (Map.Entry<String, ConverterInfo> classConverterValue : classProperties.entrySet()) {
//                        String groupName = matcher.group(classConverterValue.getKey());
//                        if(groupName != null){
//
//                            //Utils.typeConverterManagerBean.convertType().convertType(groupName, classConverterValue.getValue()..getValue());
//
//
//
//                            //convert it to the appropertiate type
//                            //add it to the class object
//                        }
//                    }
//
//                    for (String groupName : patternInfo.getNamedGroupMap().keySet()) {
//
//                        groupValueMap.put(groupName,matcher.group(groupName));
//                    }
//                    break for_loop;
//                }
//            }


//            if(patternGroupNameValueEntry != null){
//                List<Object> emittedList = Lists.newArrayList();
//                emittedList.add(patternGroupNameValueEntry.getKey());
//                emittedList.add(patternGroupNameValueEntry.getValue());
//                tridentCollector.emit(emittedList);
//            }

            //logger.debug("Tulip: "+timestamp+","+serverName+","+message);

        } catch (Exception el) {
            logger.error(el);

        }


    }


}
