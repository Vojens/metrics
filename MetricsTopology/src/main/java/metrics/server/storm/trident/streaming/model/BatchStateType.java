package metrics.server.storm.trident.streaming.model;

/**
 * Created by ali on 9/15/14.
 */
public enum BatchStateType {
    NORMAL, //prev + 1
    REPLAYED,   //prev
    HISTORICAL, // < prev
    FUTURE, // > prev + 1
    UNKNOWN,
}
