package metrics.server.storm.trident.tuple;

import clojure.lang.IPersistentVector;
import org.apache.commons.lang3.reflect.FieldUtils;
import storm.trident.tuple.TridentTupleView;
import storm.trident.tuple.ValuePointer;

import java.util.Map;

/**
 * Created by ali on 8/25/14.
 */
public class ExtendedTridentTupleView extends TridentTupleView implements IExtendedTridentTuple<Map<String, ValuePointer>>{
    public ExtendedTridentTupleView(IPersistentVector delegates, ValuePointer[] index, Map<String, ValuePointer> fieldIndex) {
        super(delegates, index, fieldIndex);
    }

    @Override
    public Map<String, ValuePointer> getFieldValuePointerMap() {

        try {
            FieldUtils.readField(this, "_fieldIndex", true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        //return TridentTupleView.;
        return null;
    }
}
