package metrics.server.storm.trident;

import metrics.server.io.latent.storm.rabbitmq.Declarator;
import metrics.server.io.latent.storm.rabbitmq.ErrorReporter;
import metrics.server.io.latent.storm.rabbitmq.RabbitMQAcknowledger;
import metrics.server.io.latent.storm.rabbitmq.RabbitMQConsumer;
import metrics.server.io.latent.storm.rabbitmq.config.ConsumerConfig;

import java.util.Map;

/**
 * Created by ali on 9/10/14.
 */
public class StormUtils {

    public static RabbitMQConsumer createConsumer(Map conf, Declarator declarator){
        ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(conf);
        ErrorReporter reporter = new ErrorReporter() {
            @Override
            public void reportError(Throwable error) {
                //spoutOutputCollector.reportError(error);
                // TODO: create one
            }
        };
        return getNewConsumer(declarator, reporter, consumerConfig);
    }

    public static RabbitMQConsumer getNewConsumer(Declarator declarator,
                                            ErrorReporter reporter,
                                            ConsumerConfig config) {
        return new RabbitMQConsumer(config.getConnectionConfig(),
                config.getPrefetchCount(),
                config.getQueueName(),
                config.isRequeueOnFail(),
                declarator,
                reporter);
    }

    public static RabbitMQAcknowledger createAcknowledger(Map conf, Declarator declarator){
        ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(conf);
        ErrorReporter reporter = new ErrorReporter() {
            @Override
            public void reportError(Throwable error) {
                //spoutOutputCollector.reportError(error);
                // TODO: create one
            }
        };
        return getNewAcknowledger(declarator, reporter, consumerConfig);
    }


    public static RabbitMQAcknowledger getNewAcknowledger(Declarator declarator,
                                                  ErrorReporter reporter,
                                                  ConsumerConfig config) {
        return new RabbitMQAcknowledger(config.getConnectionConfig(),
                config.getQueueName(),
                config.isRequeueOnFail(),
                declarator,
                reporter);
    }
}
