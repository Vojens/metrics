package metrics.server.storm.trident.aggregator;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.*;
import metrics.server.database.DbConnectionManager;
import metrics.server.engine.ICustomEngine;
import metrics.server.mapping.output.Cassandra;
import metrics.server.mapping.scriptengine.CalculationScriptEngineLateComingConf;
import metrics.server.mapping.system.SchemaColumns;
import metrics.server.qualitycheck.exception.KeyNotFoundException;
import metrics.server.util.CacheUtil;
import metrics.server.util.PreparedStatementCache;
import metrics.server.util.Utils;
import metrics.server.util.helper.CassandraUtils;
import metrics.server.util.helper.ColumnDataType;
import metrics.server.util.helper.RowHolderInfo;
import metrics.server.util.type.SerializableStringTemplateParser;
import jodd.util.StringTemplateParser;
import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.log4j.Logger;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import storm.trident.operation.BaseAggregator;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;
import storm.trident.tuple.TridentTupleView;
import storm.trident.tuple.ValuePointer;

import javax.annotation.Nullable;
import javax.script.Bindings;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import java.util.*;
import java.util.concurrent.*;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/25/14.
 */
public class LateCommingAccumulatingAggregator<T> extends BaseAggregator<AbstractMap.SimpleEntry<List<T>,Map<String,Object>>> {

    private static final transient Logger logger = Logger.getLogger(LateCommingAccumulatingAggregator.class.getName());
    private static List<StatefulKnowledgeSession> statefulKnowledgeSessionList = Lists.newLinkedList();

    final String type;
    String fullyQualifiedClassName;
    private RowHolderInfo rowHolderInfo;
    private String datasetType;
    //private AbstractMap.SimpleEntry<KnowledgeBase, KnowledgeBaseConf.KnowledgeBaseType> knowledgeBase;

    public LateCommingAccumulatingAggregator(String type, String datasetType) {
        this.type = checkNotNull(type,"Type cannot be null");
        try {
            fullyQualifiedClassName = CacheUtil.getMappingClassCache().get(type).getKey().getName();
            //knowledgeBase = CacheUtil.getCalculationKnowledgeRunTimeEventManagerSessionCache().get(type);
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        //this.rowHolderInfo = checkNotNull(rowHolderInfo,"Row Holder Info cannot be null");
        this.datasetType = checkNotNull(datasetType,"data set type cannot be null");
    }

    //TODO when shutting down applicaiton get the list and close all sessions
    public static List<StatefulKnowledgeSession> getStatefulKnowledgeSessionList() {
        return statefulKnowledgeSessionList;
    }

    @Override
    public AbstractMap.SimpleEntry<List<T>,Map<String,Object>> init(Object batchId, TridentCollector collector) {
        return new AbstractMap.SimpleEntry<List<T>,Map<String,Object>>(new ArrayList<T>(),new HashMap<String,Object>());
    }

    @Override
    public void aggregate(AbstractMap.SimpleEntry<List<T>,Map<String,Object>> pair, TridentTuple tuple, TridentCollector collector) {
        try {

            T singleTuple = (T) tuple.get(0);
            if(pair.getValue() != null && pair.getValue().isEmpty()){
                List<CalculationScriptEngineLateComingConf> calculationScriptEngineLateComingConfs = CacheUtil.getCalculationScriptEngineLateComingCache().get(type);
                if(calculationScriptEngineLateComingConfs!= null){
                    Set<String> fieldSet = Sets.newHashSet();
//                    for (CalculationScriptEngineLateComingConf calculationScriptEngineLateComingConf : calculationScriptEngineLateComingConfs) {
//                        fieldSet.add(calculationScriptEngineLateComingConf.getField());
//                    }
//                    for (int i = 1; i < tuple.size(); i++) {
//                        fieldSet.add(tuple.get(i));
//                    }

                    Map<String, ValuePointer>fieldIndex = null;

                    if(tuple instanceof TridentTupleView){
                        fieldIndex = (Map<String, ValuePointer>) FieldUtils.readField(tuple, "_fieldIndex", true);
                    }else{
                        throw new UnsupportedOperationException("This version of a class ("+tuple.getClass()+")is not supported. The only supported type is TridentTupleView");
                    }
                    Map<String, Object> fieldMap = Maps.newHashMap();
                    for (String s : fieldIndex.keySet()) {
                        Object fieldValue = tuple.getValueByField(s);
                        if(!singleTuple.equals(fieldValue)){
                            fieldMap.put(s,tuple.getValueByField(s));
                        }
                    }//                        ValuePointer valuePointer = fieldIndex.get(s);

//                    for (String s : fieldSet) {
//                        fieldMap.put(s,valuePointer.toString());
//                    }
                    pair.setValue(fieldMap);
                }else{
                    logger.warn("There exists no parameters for type ("+type+") check the database.");
                }

            }
            pair.getKey().add(singleTuple);
        } catch (Exception e) {
            logger.error(e, e);
        }

    }

//    @Override
//    public void complete(final List<T> val, final TridentCollector collector) {
//        try {
//            int nThreads = Runtime.getRuntime().availableProcessors();
//            ExecutorService executor = Executors.newFixedThreadPool(nThreads);
//
//            List<KnowledgeBaseConf> KnowledgeBaseConfList = CacheUtil.getCalculationKnowledgeRunTimeEventManagerSessionListCache().get(type);
//            final Map<String, Map.Entry<KnowledgeRuntimeEventManager,CalculationEngineConfig>> sessionMap = Maps.newHashMap();
//            for (final KnowledgeBaseConf knowledgeBaseConf : KnowledgeBaseConfList) {
////                if (knowledgeBaseConf.getKnowledgeBaseType() == STATEFUL) {
////
////                } else if (knowledgeBaseConf.getKnowledgeBaseType() == STATELESS) {
////
////                    //System.out.println(cec.getResult());
////
////                } else {
////
////                }
//
//                Future<?> kieFuture = executor.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            final KnowledgeRuntimeEventManager session;
//                            final CalculationEngineConfig cec;
//                            switch (knowledgeBaseConf.getKnowledgeBaseType()){
//                                case STATEFUL:
//                                    cec = null; //TODO change
//                                    session = knowledgeBaseConf.getKnowledgeBase().newStatefulKnowledgeSession();
//
//                                    break;
//                                case STATELESS:
//                                    session = knowledgeBaseConf.getKnowledgeBase().newStatelessKnowledgeSession();
//                                    cec = CalculationEngineConfig.create(knowledgeBaseConf.getType(),knowledgeBaseConf.getName(), val, CacheUtil.getMappingClassCache().get(type).getKey());   //TODO change null to something else
//                                    ((StatelessKnowledgeSession)session).execute(cec);
//                                    break;
//                                default:
//                                    cec = null;
//                                    session = null;
//                                    logger.error("session can't be null");  // TODO have a better thing to do asshole
//                            }
//                            sessionMap.put(knowledgeBaseConf.getName(),new AbstractMap.SimpleEntry<KnowledgeRuntimeEventManager, CalculationEngineConfig>(session,cec));
//
//                        } catch (Exception e) {
//                            logger.error(e,e);
//                        }
//
//                    }
//                });
//            }
//            executor.shutdown();
//            try {
//                executor.awaitTermination(Utils.getLongMetricsProperty("storm.trident.aggregator.accumulating.timeout",60000), TimeUnit.MILLISECONDS);
//            } catch (InterruptedException e) {
//                logger.error(e,e);
//            }
//
//            List<Object> emittedList = Lists.newArrayList();
//            Map resultMap = Maps.newHashMap(Maps.transformEntries(sessionMap,new Maps.EntryTransformer<String, Map.Entry<KnowledgeRuntimeEventManager, CalculationEngineConfig>, Object>() {
//                            @Override
//                            public Object transformEntry(@Nullable String key, @Nullable Map.Entry<KnowledgeRuntimeEventManager, CalculationEngineConfig> value) {
//                                return value.getValue().getResult();
//                            }
//                        }));
//
//            emittedList.add(resultMap);
//            collector.emit(emittedList);
//
//        } catch (Exception e){
//            logger.error(e,e);
//        } catch (Throwable t) {
//            logger.error(t,t);
//        }
//
//
//    }


    @Override
    public void complete(final AbstractMap.SimpleEntry<List<T>,Map<String,Object>> pair, final TridentCollector collector) {
        try {
            int nThreads = Runtime.getRuntime().availableProcessors();
            ExecutorService executor = Executors.newFixedThreadPool(nThreads);
            //RowHolderInfo info = rowHolderInfo.deepCopy();
            //final Map<String,Object> fieldMap = pair.getValue();
            final CaseInsensitiveMap fieldMap = new CaseInsensitiveMap(pair.getValue());
            final Map<ColumnDataType,ResultSet> tableResultSetMap = Maps.newHashMap();

            //get result set for each table in a cluster
            //List<CalculationScriptEngineLateComingConf> calculationScriptEngineLateComingConfs = CacheUtil.getCalculationScriptEngineLateComingCache().get(type);


            List<Cassandra> cassandraList = null;
            if (datasetType == null) {
                cassandraList = CacheUtil.getOutputCassandraConfCache().get(type);
            } else {
                cassandraList = CacheUtil.getOutputCassandraConfByDatasetTypeCache().get(new AbstractMap.SimpleEntry<String, String>(type, datasetType));
            }
            if(cassandraList.isEmpty()){
                logger.warn("there exists no data for type ("+type+") and datasetType ("+datasetType+") in the database. check the database.");
            }
            Multimap<String, Cassandra> keyspaceMap = ArrayListMultimap.create();
            for (Cassandra cassandra : cassandraList) {
                keyspaceMap.put(cassandra.getKeyspaceName(), cassandra);
            }
            for (String keyspaceName : keyspaceMap.keySet()) {
                Collection<Cassandra> keyspaceCollection = keyspaceMap.get(keyspaceName);
                //for each keyspace
                //Session session = DbConnectionManager.getSession(keyspaceName);
                Multimap<String, Cassandra> tableMap = ArrayListMultimap.create();
                for (Cassandra cassandra : keyspaceCollection) {
                    tableMap.put(cassandra.getTableName(), cassandra);
                }
                for (String tableName : tableMap.keySet()) {
                    Collection<Cassandra> tableCollection = tableMap.get(tableName);




                    //CacheUtil.getRowCache().get(new RowHolderInfo(keyspaceName,tableName,))

                    Multimap<String, Cassandra> columnMap = ArrayListMultimap.create();
                    for (Cassandra cassandra : tableCollection) {
                        columnMap.put(cassandra.getColumnName(), cassandra);
                    }
                    Map<String, Map.Entry<Map.Entry<Object, DataType>, Collection<Cassandra>>> valueMap = Maps.newHashMap();
                    List<SchemaColumns> schemaColumnses = null;
                    try {
                        schemaColumnses = CacheUtil.getCompoundPrimaryKeyCache().get(ColumnDataType.create(keyspaceName, tableName, null));
                    } catch (ExecutionException e1) {
                        logger.error(e1,e1);
                    }

                    if(schemaColumnses != null) {
                        Statement query = QueryBuilder.select().all().from(keyspaceName, tableName);
                        boolean isWherePresent = false;
                        int i = 0;
                        List<Map.Entry<Object, Class<?>>> bindingList = Lists.newArrayList();
                        for (final SchemaColumns schemaColumnse : schemaColumnses) {

                            Optional<Map.Entry<String, Cassandra>> columnOptional = Iterables.tryFind(columnMap.entries(), new Predicate<Map.Entry<String, Cassandra>>() {
                                @Override
                                public boolean apply(@Nullable Map.Entry<String, Cassandra> input) {
                                    return input.getKey().equalsIgnoreCase(schemaColumnse.getColumnName());
                                }
                            });
                            if(columnOptional.isPresent()){
                                Object fieldValue = getField(columnOptional.get().getValue().getSourceName(), fieldMap);
                                //parse it to the approperiate type.. fuck cassandra
                                DataType dataType = CacheUtil.getColumnDataTypeCache().get(ColumnDataType.create(keyspaceName,tableName,schemaColumnse.getColumnName()));
                                //query = CassandraUtils.bind(query, i++, fieldValue, dataType.asJavaClass());
                                bindingList.add(new AbstractMap.SimpleEntry<Object, Class<?>>(fieldValue,dataType.asJavaClass()));
                                if(!isWherePresent){
                                    query = ((Select)query).where(eq(schemaColumnse.getColumnName(),QueryBuilder.bindMarker()));
                                    isWherePresent = true;
                                }else{
                                    query = ((Select.Where)query).and(eq(schemaColumnse.getColumnName(),QueryBuilder.bindMarker()));
                                }
                            }

                        }
                        PreparedStatement prepare = PreparedStatementCache.get(query);

                        BoundStatement bs = new BoundStatement(prepare);
                        bs = CassandraUtils.bind(bs,bindingList);
                        ResultSet resultSet = DbConnectionManager.getSession(keyspaceName).execute(bs);
                        if(resultSet.all().isEmpty()){
                            logger.warn("expected data in "+bs.getKeyspace()+"."+tableName+", but resultset is empty. Binding List is "+bindingList.toString());
                        }
                        tableResultSetMap.put(ColumnDataType.create(keyspaceName,tableName,null), resultSet);

                    }
                }
            }

//TODO Enable when done and make sure it's correct
            //TODO: this is wrong. make sure to fix it
            Map<String, CompiledScript> compiledScriptMap = CacheUtil.getCalculationCompiledScriptLateComingMapCache().get(type);
            final Map<String, Object> resultMap = Maps.newHashMap();
            final List<T> list = pair.getKey();

//            ResultSet resultSet = null;
//            //if(val.size()>0){
//                for (AbstractMap.SimpleEntry<String, String> stringStringSimpleEntry : info.columnValueList()) {
//                    //ValuePointer valuePointer = val.get(0).getValue().get(stringStringSimpleEntry.getValue());
//                    //stringStringSimpleEntry.setValue(valuePointer.toString());
//                }
//                resultSet = CacheUtil.getRowCache().get(info);
//            //}

            for (final Map.Entry<String, CompiledScript> stringCompiledScriptEntry : compiledScriptMap.entrySet()) {
                Future<?> kieFuture = executor.submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            CompiledScript compiledScript = stringCompiledScriptEntry.getValue();
                            ScriptEngine engine = compiledScript.getEngine();
                            Bindings bindings = engine.createBindings();
                            bindings.put(ICustomEngine.LIST_NAME, list);
                            bindings.put(ICustomEngine.TABLE_RESULT_SET_MAP_NAME,tableResultSetMap);
                            //bindings.put(ICustomEngine.RESULTl_MAP_NAME,resultMap)
                            Object result = compiledScript.eval(bindings);
                            resultMap.put(stringCompiledScriptEntry.getKey(), result);

                        } catch (Exception e) {
                            logger.error(e, e);
                        }

                    }
                });
            }



            executor.shutdown();
            try {
                executor.awaitTermination(Utils.getLongMetricsProperty("storm.trident.aggregator.late.coming.accumulating.timeout", 60000), TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                logger.error(e, e);
            }

            List<Object> emittedList = Lists.newArrayList();
            emittedList.add(resultMap);
            collector.emit(emittedList);

        } catch (KeyNotFoundException e){
            logger.error(e+". Check the fields passed to this object",e);
        } catch (Exception e) {
            logger.error(e, e);
        } catch (Throwable t) {
            logger.error(t, t);
        }


    }

    protected final SerializableStringTemplateParser sstp = new SerializableStringTemplateParser();
    protected synchronized Object getField (String potentialField, final Map<String,Object> fieldMap){
        checkNotNull(potentialField,"Potential field cannot be null");
        String parsed = sstp.parse(potentialField, new StringTemplateParser.MacroResolver() {
            @Override
            public String resolve(String macroName) {
                if (macroName.startsWith("Fields_")) {
                    String potentialkey = macroName.replaceFirst("Fields_", "");
                    if(!fieldMap.containsKey(potentialkey)){
                        throw new KeyNotFoundException("Key ("+potentialkey+") not found in Field Map");
                    }
                    return fieldMap.get(potentialkey).toString();
                }
                return null;
            }
        });

        if (!parsed.equals(potentialField)) {
            return parsed;
        } else {
            return null;
        }
    }
}


