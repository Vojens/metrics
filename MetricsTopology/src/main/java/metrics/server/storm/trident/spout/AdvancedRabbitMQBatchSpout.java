package metrics.server.storm.trident.spout;

import backtype.storm.spout.Scheme;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import metrics.server.storm.trident.streaming.model.BatchMetadata;
import org.joda.time.Duration;
import storm.trident.spout.ITridentSpout;

import java.util.List;
import java.util.Map;

/**
 * Created by ali on 6/21/14.
 */
@SuppressWarnings({"serial", "rawtypes"})
public class AdvancedRabbitMQBatchSpout implements ITridentSpout {

    private static final long serialVersionUID = 1L;
    SpoutOutputCollector collector;
    DefaultCoordinator coordinator;
    EventEmitter emitter = null;//new EventEmitter();
    Scheme scheme;
    Map<String, Object> mapConfig;
    private Fields additionalFields;
    //private RabbitMQConsumer consumer;

    public AdvancedRabbitMQBatchSpout(Map<String, Object> mapConfig, Scheme scheme) {
        this.scheme = scheme;
        this.mapConfig = mapConfig;

        emitter = new EventEmitter(scheme);
        coordinator = new DefaultCoordinator(mapConfig,new Duration(30000), DefaultCoordinator.CoordinatingDateTimeType.ABSOLUTE);
        additionalFields = new Fields("deliveryTag");
    }

//    private RabbitMQConsumer createConsumer(Map conf, Declarator declarator){
//        ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(conf);
//        ErrorReporter reporter = new ErrorReporter() {
//            @Override
//            public void reportError(Throwable error) {
//                //spoutOutputCollector.reportError(error);
//                // TODO: create one
//            }
//        };
//        return loadConsumer(declarator, reporter, consumerConfig);
//    }
//
//    protected RabbitMQConsumer loadConsumer(Declarator declarator,
//                                            ErrorReporter reporter,
//                                            ConsumerConfig config) {
//        return new RabbitMQConsumer(config.getConnectionConfig(),
//                config.getPrefetchCount(),
//                config.getQueueName(),
//                config.isRequeueOnFail(),
//                declarator,
//                reporter);
//    }

    @Override
    public BatchCoordinator<BatchMetadata> getCoordinator(
            String txStateId, Map conf, TopologyContext context) {
        if(!coordinator.isPrepared()){
//            RabbitMQConsumer consumer = StormUtils.createAcknowledger(mapConfig, new Declarator.NoOp());
//            this.consumer = consumer;

            //consumer.open();
            //coordinator.setAcknowledger(consumer);
            //coordinator.setIsPrepared(true);
            coordinator.open(mapConfig,context);

        }

        return coordinator;
    }

    @Override
    public Emitter<BatchMetadata> getEmitter(String txStateId, Map conf,
                                        TopologyContext context) {
        if (!emitter.isOpened()) {
            //RabbitMQConsumer consumer = StormUtils.createConsumer(mapConfig,new Declarator.NoOp());
            //this.consumer = consumer;
            //emitter.setConsumer(consumer);
            emitter.open(mapConfig, context);
            //coordinator.setContext(context);
            //coordinator.setAcknowledger(consumer);
        }
//        if(emitter == null){
//            emitter = new EventEmitter(scheme);
////            emitter.setConf(conf);
////            emitter.setContext(context);
////            for (Map.Entry<String, Object> stringObjectEntry : mapConfig.entrySet()) {
////                conf.put(stringObjectEntry.getKey(),stringObjectEntry.getValue());
////            }
//
//            //conf.putAll(mapConfig);
//            emitter.open(mapConfig, context);
//        }
        return emitter;
    }

    @Override
    public Map getComponentConfiguration() {
        return null;
    }

    @Override
    public Fields getOutputFields() {
        List<String> outputList = scheme.getOutputFields().toList();
        if (additionalFields != null) {
            outputList.addAll(additionalFields.toList());
        }
        return new Fields(outputList);
    }
}
