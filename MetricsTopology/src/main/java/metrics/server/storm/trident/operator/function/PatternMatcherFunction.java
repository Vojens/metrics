package metrics.server.storm.trident.operator.function;

import backtype.storm.tuple.Fields;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import metrics.server.util.CacheUtil;
import metrics.server.util.helper.PatternInfo;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by ali on 7/23/14.
 */
public class PatternMatcherFunction extends BaseFunction {
    private static final transient Logger logger = Logger.getLogger(PatternMatcherFunction.class.getName());

    Fields fields;
    String type;

    public PatternMatcherFunction(Fields fields, String type) {
        this.fields = fields;
        this.type = type;
    }

    public PatternMatcherFunction(String patternNameField, String groupValueMapField, String type) {
        this.fields = new Fields(patternNameField, groupValueMapField);
        this.type = type;
    }

    @Override
    @SuppressWarnings(value = {"unchecked"})
    //emits: pattern name, map of found groups
    public void execute(TridentTuple objects, TridentCollector tridentCollector) {


        try {
            checkNotNull(objects);
            com.google.common.base.Preconditions.checkArgument(objects.size() >= 3);
            checkState(objects.get(0) instanceof DateTime);
            org.joda.time.DateTime timestamp = (DateTime) objects.get(0);
            String serverName = objects.getString(1);
            String message = objects.getString(2);


            Map<String, PatternInfo> patternInfoMap = CacheUtil.getPatternCache().get(type);
            Map<String, String> groupValueMap = null;
            Map.Entry<String, Map<String, String>> patternGroupNameValueEntry = null;
            for_loop:
            for (Map.Entry<String, PatternInfo> stringPatternInfoEntry : patternInfoMap.entrySet()) {
                PatternInfo patternInfo = stringPatternInfoEntry.getValue();
                Matcher matcher = patternInfo.getPattern().matcher(message);
                if (matcher.find()) {
                    groupValueMap = Maps.newHashMap();


                    for (String groupName : patternInfo.getNamedGroupMap().keySet()) {

                        groupValueMap.put(groupName, matcher.group(groupName));
                    }
                    patternGroupNameValueEntry = new AbstractMap.SimpleEntry<String, Map<String, String>>(stringPatternInfoEntry.getKey(), groupValueMap);
                    break for_loop;
                }
            }

//            for_loop:for(PatternInfo patternInfo : patternInfoMap.values()){
//                Matcher matcher = patternInfo.getPattern().matcher(message);
//                if(matcher.find()){
//                    groupValueMap = Maps.newHashMap();
//                    Map.Entry<Class<?>, Map<String, ConverterInfo>> beanClassWithDetails = patternInfo.getBeanClassWithDetails();
//                    Class<?> beanClass = beanClassWithDetails.getKey();
//                    Map<String, ConverterInfo> classProperties = beanClassWithDetails.getValue();
//                    Object object = beanClass.newInstance();
//                    //ConvertBean convertBean = Utils.typeConverterManagerBean.getConvertBean();
//                    for (Map.Entry<String, ConverterInfo> classConverterValue : classProperties.entrySet()) {
//                        String groupName = matcher.group(classConverterValue.getKey());
//                        if(groupName != null){
//
//                            //Utils.typeConverterManagerBean.convertType().convertType(groupName, classConverterValue.getValue()..getValue());
//
//
//
//                            //convert it to the appropertiate type
//                            //add it to the class object
//                        }
//                    }
//
//                    for (String groupName : patternInfo.getNamedGroupMap().keySet()) {
//
//                        groupValueMap.put(groupName,matcher.group(groupName));
//                    }
//                    break for_loop;
//                }
//            }


            if (patternGroupNameValueEntry != null) {
                List<Object> emittedList = Lists.newArrayList();
                emittedList.add(patternGroupNameValueEntry.getKey());
                emittedList.add(patternGroupNameValueEntry.getValue());
                tridentCollector.emit(emittedList);
            }

            //logger.debug("Tulip: "+timestamp+","+serverName+","+message);

        } catch (Exception el) {
            logger.error(el, el);

        }


    }


}
