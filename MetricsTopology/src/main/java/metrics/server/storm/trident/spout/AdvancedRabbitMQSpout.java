package metrics.server.storm.trident.spout;

import backtype.storm.spout.Scheme;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import metrics.server.io.latent.storm.rabbitmq.*;
import metrics.server.io.latent.storm.rabbitmq.config.ConsumerConfig;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * A simple RabbitMQ spout that emits an anchored tuple stream (on the default stream). This can be used with
 * Storm's guaranteed message processing.
 *
 * @author Ali Hubail
 */
public class AdvancedRabbitMQSpout extends BaseRichSpout {
    protected final MessageScheme scheme;
    protected final Declarator declarator;

    protected final static transient Logger logger = Logger.getLogger(AdvancedRabbitMQSpout.class.getName());
    protected transient RabbitMQConsumer consumer;
    protected transient SpoutOutputCollector collector;

    public AdvancedRabbitMQSpout(Scheme scheme) {
        this(MessageScheme.Builder.from(scheme), new Declarator.NoOp());
    }

    public AdvancedRabbitMQSpout(Scheme scheme, Declarator declarator) {
        this(MessageScheme.Builder.from(scheme), declarator);
    }

    public AdvancedRabbitMQSpout(MessageScheme scheme, Declarator declarator) {
        this.scheme = scheme;
        this.declarator = declarator;
    }

    @Override
    public void open(final Map config,
                     final TopologyContext context,
                     final SpoutOutputCollector spoutOutputCollector) {
        ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(config);
        ErrorReporter reporter = new ErrorReporter() {
            @Override
            public void reportError(Throwable error) {
                spoutOutputCollector.reportError(error);
            }
        };
        consumer = loadConsumer(declarator, reporter, consumerConfig);
        scheme.open(config, context);
        consumer.open();
        collector = spoutOutputCollector;
    }

    protected RabbitMQConsumer loadConsumer(Declarator declarator,
                                            ErrorReporter reporter,
                                            ConsumerConfig config) {
        return new RabbitMQConsumer(config.getConnectionConfig(),
                config.getPrefetchCount(),
                config.getQueueName(),
                config.isRequeueOnFail(),
                declarator,
                reporter);
    }

    @Override
    public void close() {
        consumer.close();
        scheme.close();
        super.close();
    }

    @Override
    public void nextTuple() {
        Message message;
        while ((message = consumer.nextMessage()) != Message.NONE) {
            Date messageTimestamp = ((Message.DeliveredMessage) message).getTimestamp();

            List<Object> tuple = extractTuple(message);
            if (!tuple.isEmpty()) {
                DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
                DateTime logTimestamp = parser.parseDateTime(tuple.get(0).toString());

                boolean isCurrentTimestamp = isEqualTimestampUpToMinute(logTimestamp, DateTime.now());

                //String timestamp = String.valueOf(tuple.get(scheme.getOutputFields().fieldIndex("@Timestamp")));
                //DateTime dateTime = (DateTime)tuple.get(0);
                //DateTime now = DateTime.now();
                if (isCurrentTimestamp) {
                    emit(tuple, message, collector);
                } else {
                    //return back to a new queue
                }

            }
        }
    }


    protected boolean isEqualTimestampUpToMinute(final DateTime dateTime1, final DateTime dateTime2) {
        LocalDateTime localDateTime1 = dateTime1.toLocalDateTime();
        LocalDateTime localDateTime2 = dateTime2.toLocalDateTime();
        localDateTime1 = localDateTime1.minusSeconds(localDateTime1.secondOfMinute().get());
        localDateTime1 = localDateTime1.minusMillis(localDateTime1.millisOfSecond().get());
        localDateTime2 = localDateTime2.minusSeconds(localDateTime2.secondOfMinute().get());
        localDateTime2 = localDateTime2.minusMillis(localDateTime2.millisOfSecond().get());
        //DateTime tempDateTime1 = new DateTime(localDateTime1.year(),localDateTime1.monthOfYear(),localDateTime1.getDayOfMonth(),localDateTime1.getHourOfDay(),localDateTime1.getMinuteOfHour());
        return localDateTime1.isEqual(localDateTime2);
    }

    protected List<Integer> emit(List<Object> tuple,
                                 Message message,
                                 SpoutOutputCollector spoutOutputCollector) {
        return spoutOutputCollector.emit(tuple, getDeliveryTag(message));
    }

    private List<Object> extractTuple(Message message) {
        long deliveryTag = getDeliveryTag(message);
        try {
            List<Object> tuple = scheme.deserialize(message);
            if (tuple != null && !tuple.isEmpty()) {
                return tuple;
            }
            String errorMsg = "Deserialization error for msgId " + deliveryTag;
            logger.warn(errorMsg);
            collector.reportError(new Exception(errorMsg));
        } catch (Exception e) {
            logger.warn("Deserialization error for msgId " + deliveryTag, e);
            collector.reportError(e);
        }
        // get the malformed message out of the way by dead-lettering (if dead-lettering is configured) and move on
        consumer.deadLetter(deliveryTag);
        return Collections.emptyList();
    }

    @Override
    public void ack(Object msgId) {
        if (msgId instanceof Long) consumer.ack((Long) msgId);
    }

    @Override
    public void fail(Object msgId) {
        if (msgId instanceof Long) consumer.fail((Long) msgId);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(scheme.getOutputFields());
    }

    protected long getDeliveryTag(Message message) {
        return ((Message.DeliveredMessage) message).getDeliveryTag();
    }
}
