package metrics.server.storm.trident.aggregator;

import storm.trident.operation.BaseAggregator;
import storm.trident.operation.TridentCollector;
import storm.trident.tuple.TridentTuple;

import java.util.List;

/**
 * Created by ali on 8/13/14.
 */
public class ProgressiveAggregator<T> extends BaseAggregator<List<T>> {
    @Override
    public List<T> init(Object o, TridentCollector tridentCollector) {
        return null;
    }

    @Override
    public void aggregate(List<T> ts, TridentTuple objects, TridentCollector tridentCollector) {

    }

    @Override
    public void complete(List<T> ts, TridentCollector tridentCollector) {

    }
//    private static final transient Logger logger = Logger.getLogger(MergeAggregator.class.getName());
//    private static List<StatefulKnowledgeSession> statefulKnowledgeSessionList = Lists.newLinkedList();
//    //private List<T> list;
//    //private Class<T> persistentClass;
////    public MergeAggregator(){
////        super();
////
////    }
//
////    private final TypeToken typeToken = new TypeToken(getClass()) { };
////    private final Type type = typeToken.getType(); // or getRawType() to return Class<? super T>
////
////    public Type getType() {
////        return type;
////    }
//
//    String type;
//    String fullyQualifiedClassName;
//    private AbstractMap.SimpleEntry<KnowledgeBase, KnowledgeBaseConf.KnowledgeBaseType> knowledgeBase;
//
//    public ProgressiveAggregator(String type) {
//        this.type = type;
//        try {
//            fullyQualifiedClassName = CacheUtil.getMappingClassCache().get(type).getKey().getName();
//            //knowledgeBase = CacheUtil.getCalculationKnowledgeRunTimeEventManagerSessionCache().get(type);
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//    }
//
//    //TODO when shutting down applicaiton get the list and close all sessions
//    public static List<StatefulKnowledgeSession> getStatefulKnowledgeSessionList() {
//        return statefulKnowledgeSessionList;
//    }
//
//    @Override
//    public List<T> init(Object batchId, TridentCollector collector) {
//        //list = Lists.newLinkedList();
//        return Lists.newLinkedList();
//    }
//
//    @Override
//    public void aggregate(List<T> val, TridentTuple tuple, TridentCollector collector) {
//        try {
//            T singleTuple = (T) tuple.get(0);
//            val.add(singleTuple);
//        } catch (Exception e) {
//            logger.error(e);
//        }
//
//    }
//
//    @Override
//    public void complete(List<T> val, TridentCollector collector) {
//
//        //StatefulKnowledgeSession session = null;
//
//
//        try {
////            KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
////            builder.add(ResourceFactory.newClassPathResource("CalculationEngineRule.drl"), ResourceType.DRL);
////            if (builder.hasErrors()) {
////                throw new RuntimeException(builder.getErrors().toString());
////            }
////
////            KieBaseConfiguration configuration = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
////            configuration.setOption(SequentialOption.YES);
////            KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
////            knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());
////
////            session = knowledgeBase.newStatefulKnowledgeSession();
////            Class<?> beanClass = CacheUtil.getMappingClassCache().get("witsml").getKey();
////
////            for(int i = 0; i < beanClass.getDeclaredMethods().length; i++){
////                System.out.println(beanClass.getDeclaredMethods()[i].getName());
////            }
////            System.out.println();
////
////            CalculationEngineConfig cec = CalculationEngineConfig.create("ACTIVITY_DP",val,CacheUtil.getMappingClassCache().get("witsml").getKey());
////            FactHandle purchaseFact = session.insert(cec);
////            session.fireAllRules();
////            System.out.println(cec.getResult());
//            AbstractMap.SimpleEntry<KnowledgeBase, KnowledgeBaseConf.KnowledgeBaseType> knowledgeBaseKnowledgeBaseTypeSimpleEntry = CacheUtil.getCalculationKnowledgeRunTimeEventManagerSessionCache().get(type);
//            if (knowledgeBaseKnowledgeBaseTypeSimpleEntry.getValue() == KnowledgeBaseConf.KnowledgeBaseType.STATEFUL) {
//                StatefulKnowledgeSession session = knowledgeBaseKnowledgeBaseTypeSimpleEntry.getKey().newStatefulKnowledgeSession();
//            } else if (knowledgeBaseKnowledgeBaseTypeSimpleEntry.getValue() == KnowledgeBaseConf.KnowledgeBaseType.STATELESS) {
//                StatelessKnowledgeSession session = knowledgeBaseKnowledgeBaseTypeSimpleEntry.getKey().newStatelessKnowledgeSession();
//                CalculationEngineConfig cec = CalculationEngineConfig.create("ACTIVITY_DP", val, CacheUtil.getMappingClassCache().get(type).getKey());
//                session.execute(cec);
//                System.out.println(cec.getResult());
//
//            } else {
//
//            }
//
//        } catch (Throwable t) {
//            t.printStackTrace();
//        } finally {
////            if (session != null) {
////                session.dispose();
////            }
//        }
//
//
////        Query query = new Query();
////
////        //String sqlQuery = "select duration from "+fullyQualifiedClassName+" EXECUTE ON RESULTS avg (:_allobjs, duration) avgDuration";
//////        String sqlQuery = "select indices, replace (indices ,'[','') from "+fullyQualifiedClassName+" EXECUTE ON RESULTS avg (:_allobjs, duration) avgDuration";
////        String sqlQuery = "select * from "
////        try {
////            query.parse(sqlQuery);
////        } catch (QueryParseException e) {
////            e.printStackTrace();
////        }
////
////        QueryResults execute = null;
////        try {
////            execute = query.execute(val);
////        } catch (QueryExecutionException e) {
////            e.printStackTrace();
////        }
//        List<Object> emittedList = Lists.newArrayList();
////        Map saveValues = execute.getSaveValues ();
////        Number avg = (Number) saveValues.get ("avgduration");
//
//
//        emittedList.add(val);
//        collector.emit(emittedList);
//    }
}

