package metrics.server.storm.trident.operator.filter;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by ali on 6/22/14.
 */
@SuppressWarnings({"serial", "rawtypes"})
public class PrintFilter implements Filter {
    private static final transient Logger logger = Logger.getLogger(PrintFilter.class.getName());

    @Override
    public void prepare(Map conf, TridentOperationContext context) {
        logger.debug("PrintFilter - prepare");
    }

    @Override
    public void cleanup() {

    }

    @Override
    public boolean isKeep(TridentTuple tuple) {
        try {
            checkNotNull(tuple);
            com.google.common.base.Preconditions.checkArgument(tuple.size() == 3);
            checkState(tuple.get(0) instanceof DateTime);
            org.joda.time.DateTime timestamp = (DateTime) tuple.get(0);
            String serverName = tuple.getString(1);
            String message = tuple.getString(2);

            logger.debug("Tulip: " + timestamp + "," + serverName + "," + message);
            return true;
        } catch (Exception e) {
            logger.error(e, e);
            return false;
        }
        //System.out.println("Tulip: "+tuple.getString(0)+","+tuple.getString(1)+","+tuple.getString(2));
    }


}
