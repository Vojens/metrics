package metrics.server.storm.trident.operator.function;

import storm.trident.operation.Function;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * Created by ali on 9/2/14.
 */
public class TestFunction implements Function {
    @Override
    public void execute(TridentTuple objects, TridentCollector tridentCollector) {
        System.out.println("resultsize: "+objects.size());
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
