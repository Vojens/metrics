package metrics.server.storm.trident.aggregator.combiner;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.joda.time.DateTime;
import storm.trident.operation.CombinerAggregator;
import storm.trident.tuple.TridentTuple;

/**
 * Created by ali on 9/2/14.
 */
public class AccumulatingCombinerAggregator implements CombinerAggregator<Multimap<String,Multimap<DateTime,Object>>>{

    @Override
    public Multimap<String,Multimap<DateTime,Object>> init(TridentTuple objects) {
        Multimap<String,Multimap<DateTime,Object>> multimap = LinkedListMultimap.create();
        String host = objects.getString(0);
        DateTime timestamp = (DateTime)objects.get(1);
        Object object = objects.get(2);
        LinkedListMultimap<DateTime, Object> value = LinkedListMultimap.create();
        multimap.put(host,value);
        return multimap;

    }

    @Override
    public Multimap<String, Multimap<DateTime, Object>> combine(Multimap<String, Multimap<DateTime, Object>> multimap1, Multimap<String, Multimap<DateTime, Object>> multimap2) {
        if(multimap1!= null && multimap2 != null){
            multimap1.putAll(multimap2);
            return multimap1;
        }else if(multimap1!= null && multimap2 == null){
            return multimap1;
        }else if(multimap1== null && multimap2 != null) {
            return multimap2;
        }else{
            return LinkedListMultimap.create();
        }
    }

    @Override
    public Multimap<String, Multimap<DateTime, Object>> zero() {
        return null;
    }


}
