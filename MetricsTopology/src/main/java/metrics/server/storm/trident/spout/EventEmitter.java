package metrics.server.storm.trident.spout;

import backtype.storm.spout.Scheme;
import backtype.storm.task.TopologyContext;
import com.google.common.collect.Maps;
import metrics.server.MetricsServer;
import metrics.server.io.latent.storm.rabbitmq.Declarator;
import metrics.server.io.latent.storm.rabbitmq.Message;
import metrics.server.io.latent.storm.rabbitmq.MessageScheme;
import metrics.server.io.latent.storm.rabbitmq.RabbitMQConsumer;
import metrics.server.spi.RabbitMqConnectionManagerImpl;
import metrics.server.storm.trident.StormUtils;
import metrics.server.storm.trident.streaming.model.BatchMetadata;
import metrics.server.storm.trident.topology.MetricsTopology;
import metrics.server.util.Utils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MapRpcServer;
import org.apache.log4j.Logger;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.ITridentSpout;
import storm.trident.topology.TransactionAttempt;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Created by ali on 6/22/14.
 */
public class EventEmitter implements ITridentSpout.Emitter<BatchMetadata>, Serializable {
    private static final long serialVersionUID = 7676816240577814870L;
    private static final transient Logger logger = Logger.getLogger(EventEmitter.class);
    protected final MessageScheme scheme;
    protected final Declarator declarator;
    private RabbitMQConsumer consumer;  //TODO used to be
    private AtomicInteger successfulTransactions = new AtomicInteger(0);
    private AtomicBoolean isBatchedFinishedEmitting = new AtomicBoolean(true);
    private String txStateId;
    private Map conf;
    private TopologyContext context;
    private org.joda.time.DateTime latestDateTime = null;
    private AtomicBoolean isOpened = new AtomicBoolean(false);
    private Map config;
    //@Inject
    //private metrics.server.util.Properties simplePersistence = null;
    private static final DateTimeFormatter dateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis();

    public EventEmitter(Scheme scheme) {
        this(MessageScheme.Builder.from(scheme), new Declarator.NoOp());
        //simplePersistence = Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME);
    }

    public EventEmitter(Scheme scheme, Declarator declarator) {
        this(MessageScheme.Builder.from(scheme), declarator);
    }

    public EventEmitter(MessageScheme scheme, Declarator declarator) {
        this.scheme = scheme;
        this.declarator = declarator;

    }

    @Override
    public void emitBatch(TransactionAttempt tx, BatchMetadata
            coordinatorMeta, TridentCollector collector) {

//        for (int i = 0; i < 5; i++) {
//            List<Object> events = new ArrayList<Object>();
//            double lat =
//                    new Double(-30 + (int) (Math.random() * 75));
//            double lng =
//                    new Double(-120 + (int) (Math.random() * 70));
//            long time = System.currentTimeMillis();
//            String diag = new Integer(320 +
//                    (int) (Math.random() * 7)).toString();
////            DiagnosisEvent event =
////                    new DiagnosisEvent(lat, lng, time, diag);
//            events.add(i);
//            collector.emit(events);
//        }
        try{
            Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid.emit.batch.start.datetime",dateTimeFormatter.print(org.joda.time.DateTime.now()));
            if(isBatchedFinishedEmitting.get()){
                emitNextTupleBatch(collector, coordinatorMeta);
            }
        }catch (Exception e){
            logger.error(e,e);
        }



    }

    public void open(final Map config,
                     final TopologyContext context) {
        try {
            this.config = config;
//            ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(config);
//            ErrorReporter reporter = new ErrorReporter() {
//                @Override
//                public void reportError(Throwable error) {
//                    //spoutOutputCollector.reportError(error);
//                    // TODO: create one
//                }
//            };
//            setAcknowledger(loadConsumer(declarator, reporter, consumerConfig));
            setConsumer(StormUtils.createConsumer(config, new Declarator.NoOp()));

            scheme.open(config, context);
            getConsumer().open();
            //collector = spoutOutputCollector;
            initSuccessConsumer();
            startSuccessConsumer();
            isOpened.set(true);
        } catch (Exception e) {
            logger.error(e, e);
        }

    }

    public boolean isOpened() {
        return isOpened.get();
    }

//    protected RabbitMQConsumer loadConsumer(Declarator declarator,
//                                            ErrorReporter reporter,
//                                            ConsumerConfig config) {
//        return new RabbitMQConsumer(config.getConnectionConfig(),
//                config.getPrefetchCount(),
//                config.getQueueName(),
//                config.isRequeueOnFail(),
//                declarator,
//                reporter);
//    }

    public void emitNextTupleBatch(TridentCollector collector, BatchMetadata dateTime) {

        System.out.println("nextTuplip");
        Message message;
        synchronized (isBatchedFinishedEmitting) {
            if (!isBatchedFinishedEmitting.get()) {
                return;
            }
            isBatchedFinishedEmitting.set(false);
        }

        try{
            org.joda.time.DateTime startTime = org.joda.time.DateTime.now();
            //Duration duration = Duration.standardSeconds(30);

            String freqString = Utils.getMetricsProperty("Metrics.topology.batch.spout.emitter.freq.period","PT30S");
            Duration freqDuration = (new Period(freqString)).toStandardDuration();
            //int messageCount = consumer.getMessageCount();
            Message lastMessage = null;
            for (Period period = new Period(startTime, org.joda.time.DateTime.now());
                 isPeriodLessThanFreq(freqDuration, period)  && (message = getConsumer().nextMessage()) != Message.NONE;
                 period = new Period(startTime, org.joda.time.DateTime.now())) {

                try{
                    Date messageTimestamp = ((Message.DeliveredMessage) message).getTimestamp();

                    List<Object> tuple = extractTuple(message, collector);
                    if (!tuple.isEmpty()) {
                        DateTimeFormatter parser = ISODateTimeFormat.dateTimeParser();
                        org.joda.time.DateTime logTimestamp = parser.parseDateTime(tuple.get(0).toString());
                        setLatestDateTime(logTimestamp);

                        LinkedList<Object> tempList = new LinkedList<Object>();
                        tempList.add(logTimestamp);
                        tempList.add(tuple.get(1));
                        tempList.add(tuple.get(2));
                        tempList.add(((Message.DeliveredMessage) message).getDeliveryTag());
                        emit(tempList, collector);
                        //this.getConsumer().ack(((Message.DeliveredMessage) message).getDeliveryTag());  //TODO remove

                    }
                }catch (Exception e){
                    logger.error(e,e);
                    try{
                        getConsumer().fail(getDeliveryTag(message));
                    }catch (Exception ei){
                        logger.error(ei,ei);
                    }

                }

                lastMessage = message;
            }
            String lastMessageDeliveryTag = "";
            try{

            }catch (Exception e){
                logger.error(e,e);
            }
            if(lastMessage != null){
                lastMessageDeliveryTag = getDeliveryTag(lastMessage)+"";
            }
            Utils.getProperties(MetricsTopology.SIMPLE_PERSISTENCE_TABLE_NAME).put("txid.emit.batch.last.message.delivery.tag",lastMessageDeliveryTag);
        }catch (Exception e){
            logger.error(e,e);
        }finally {
            isBatchedFinishedEmitting.set(true);
        }
    }


    private static boolean isPeriodLessThanFreq(Duration duration, Period period){
        Duration duration1 = period.toStandardDuration();
        boolean shorterThan = duration1.isShorterThan(duration);
        return shorterThan;
    }
    private List<Object> extractTuple(Message message, TridentCollector collector) {
        long deliveryTag = getDeliveryTag(message);
        try {
            List<Object> tuple = scheme.deserialize(message);
            if (tuple != null && !tuple.isEmpty()) {
                return tuple;
            }
            String errorMsg = "Deserialization error for msgId " + deliveryTag;
            logger.warn(errorMsg);
            collector.reportError(new Exception(errorMsg));
        } catch (Exception e) {
            logger.warn("Deserialization error for msgId " + deliveryTag, e);
            collector.reportError(e);
        }
        // get the malformed message out of the way by dead-lettering (if dead-lettering is configured) and move on
        getConsumer().deadLetter(deliveryTag);
        return Collections.emptyList();
    }

    protected long getDeliveryTag(Message message) {
        return ((Message.DeliveredMessage) message).getDeliveryTag();
    }


    protected void emit(List<Object> tuple,
                        TridentCollector spoutOutputCollector) {
        spoutOutputCollector.emit(tuple);

        //return spoutOutputCollector.emit(tuple, getDeliveryTag(message));
    }




    @Override
    public void success(TransactionAttempt tx) {
        successfulTransactions.incrementAndGet();


    }

    @Override
    public void close() {
        logger.info("emitter - close");
    }


    public String getTxStateId() {
        return txStateId;
    }

    public void setTxStateId(String txStateId) {
        this.txStateId = txStateId;
    }

    public Map getConf() {
        return conf;
    }

    public void setConf(Map conf) {
        this.conf = conf;
    }

    public TopologyContext getContext() {
        return context;
    }

    public void setContext(TopologyContext context) {
        this.context = context;
    }

    public synchronized org.joda.time.DateTime getLatestDateTime() {
        return latestDateTime;
    }

    public synchronized void setLatestDateTime(org.joda.time.DateTime latestDateTime) {
        this.latestDateTime = latestDateTime;
    }

    public RabbitMQConsumer getConsumer() {
        if(consumer == null){
//            ConsumerConfig consumerConfig = ConsumerConfig.getFromStormConfig(config);
//            ErrorReporter reporter = new ErrorReporter() {
//                @Override
//                public void reportError(Throwable error) {
//                    //spoutOutputCollector.reportError(error);
//                    // TODO: create one
//                }
//            };
//            setAcknowledger(loadConsumer(declarator, reporter, consumerConfig));
            setConsumer(StormUtils.createConsumer(config,declarator));

        }
        return consumer;
    }

    public void setConsumer(RabbitMQConsumer consumer) {
        this.consumer = consumer;
    }

    protected Channel rpcChannel;
    protected MapRpcServer mapRpcServer;
    ExecutorService executorService = null;

    public void startSuccessConsumer(){
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                checkNotNull(mapRpcServer,"rpc server cannot be null");
                try{
                    while(true){
                        mapRpcServer.mainloop();
                        System.out.println("test");
                    }
                }catch (Exception e){
                    logger.error(e);
                }
//                while(true){
//                    try {
//                        mapRpcServer.mainloop();
//                    } catch (IOException e) {
//                        logger.error(e);
//                    }
//                }
            }
        });



    }


    public void initSuccessConsumer(){
        rpcChannel = ((RabbitMqConnectionManagerImpl) MetricsServer.getInstance().getConnectionManager()).createChannel();
        try {
            rpcChannel.queueDeclare("AckRpcQueue", false, false, false, null);
            mapRpcServer = new MapRpcServer(rpcChannel, "AckRpcQueue"){
                @Override
                public Map<String, Object> handleMapCall(Map<String, Object> request){
                    long deliveryTag = Long.parseLong(request.get("txid.emit.batch.last.message.delivery.tag").toString());
                    Map responseMap = Maps.newHashMap();
                    try{
                        getConsumer().ack(deliveryTag,true);
                        responseMap.put("ack",true);
                        return responseMap;
                    }catch (Exception e){
                        try{
                            responseMap.put("ack",false);
                            return responseMap;
                        }catch (Exception e1){

                        }
                    }
                    return responseMap;
                }
            };
            executorService = Executors.newSingleThreadExecutor();
            //mapRpcServer.mainloop();
        } catch (Exception e) {
            logger.error(e);
        }


//        MetricsServer.getInstance().getConnectionManager().call(new ChannelCallable<Object>() {
//            @Override
//            public String getDescription() {
//                return null;
//            }
//
//            @Override
//            public Object call(Channel channel) throws IOException {
//                channel.queueDeclare("AckRpcQueue", false, false, false, null);
//                MapRpcServer server = new MapRpcServer(channel, "AckRpcQueue"){
//                    @Override
//                    public Map<String, Object> handleMapCall(Map<String, Object> request){
//                        long deliveryTag = Long.parseLong(request.get("txid.emit.batch.last.message.delivery.tag").toString());
//                        Map responseMap = Maps.newHashMap();
//                        try{
//                            getConsumer().ack(deliveryTag,true);
//                            responseMap.put("ack",true);
//                            return responseMap;
//                        }catch (Exception e){
//                            try{
//                                responseMap.put("ack",false);
//                                return responseMap;
//                            }catch (Exception e1){
//
//                            }
//                        }
//                        return responseMap;
//                    }
//                };
//                return null;
//
//            }
//        });
    }
}