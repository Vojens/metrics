//package metrics.server.storm.trident.aggregator;
//
//import com.google.common.collect.Lists;
//import com.google.common.collect.Maps;
//import metrics.server.engine.ICustomEngine;
//import metrics.server.util.CacheUtil;
//import metrics.server.util.Utils;
//import org.apache.log4j.Logger;
//import org.kie.internal.runtime.StatefulKnowledgeSession;
//import storm.trident.operation.BaseAggregator;
//import storm.trident.operation.TridentCollector;
//import storm.trident.tuple.TridentTuple;
//
//import javax.script.Bindings;
//import javax.script.CompiledScript;
//import javax.script.ScriptEngine;
//import java.util.List;
//import java.util.Map;
//import java.util.concurrent.*;
//
///**
// * Created by ali on 9/2/14.
// */
//public class AccumulatingPartitionAggregator extends BaseAggregator<List<T>> {
//    private static final transient Logger logger = Logger.getLogger(AccumulatingPartitionAggregator.class.getName());
//    private static List<StatefulKnowledgeSession> statefulKnowledgeSessionList = Lists.newLinkedList();
//
//    final String type;
//    String fullyQualifiedClassName;
//    //private AbstractMap.SimpleEntry<KnowledgeBase, KnowledgeBaseConf.KnowledgeBaseType> knowledgeBase;
//
//    public AccumulatingPartitionAggregator(String type) {
//        this.type = type;
//        try {
//            fullyQualifiedClassName = CacheUtil.getMappingClassCache().get(type).getKey().getName();
//            //knowledgeBase = CacheUtil.getCalculationKnowledgeRunTimeEventManagerSessionCache().get(type);
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//    }
//
//    //TODO when shutting down applicaiton get the list and close all sessions
//    public static List<StatefulKnowledgeSession> getStatefulKnowledgeSessionList() {
//        return statefulKnowledgeSessionList;
//    }
//
//    @Override
//    public List<T> init(Object batchId, TridentCollector collector) {
//        //list = Lists.newLinkedList();
//        return Lists.newLinkedList();
//    }
//
//    @Override
//    public void aggregate(List<T> val, TridentTuple tuple, TridentCollector collector) {
//        try {
//            T singleTuple = (T) tuple.get(0);
//            val.add(singleTuple);
//        } catch (Exception e) {
//            logger.error(e, e);
//        }
//
//    }
//
////    @Override
////    public void complete(final List<T> val, final TridentCollector collector) {
////        try {
////            int nThreads = Runtime.getRuntime().availableProcessors();
////            ExecutorService executor = Executors.newFixedThreadPool(nThreads);
////
////            List<KnowledgeBaseConf> KnowledgeBaseConfList = CacheUtil.getCalculationKnowledgeRunTimeEventManagerSessionListCache().get(type);
////            final Map<String, Map.Entry<KnowledgeRuntimeEventManager,CalculationEngineConfig>> sessionMap = Maps.newHashMap();
////            for (final KnowledgeBaseConf knowledgeBaseConf : KnowledgeBaseConfList) {
//////                if (knowledgeBaseConf.getKnowledgeBaseType() == STATEFUL) {
//////
//////                } else if (knowledgeBaseConf.getKnowledgeBaseType() == STATELESS) {
//////
//////                    //System.out.println(cec.getResult());
//////
//////                } else {
//////
//////                }
////
////                Future<?> kieFuture = executor.submit(new Runnable() {
////                    @Override
////                    public void run() {
////                        try {
////                            final KnowledgeRuntimeEventManager session;
////                            final CalculationEngineConfig cec;
////                            switch (knowledgeBaseConf.getKnowledgeBaseType()){
////                                case STATEFUL:
////                                    cec = null; //TODO change
////                                    session = knowledgeBaseConf.getKnowledgeBase().newStatefulKnowledgeSession();
////
////                                    break;
////                                case STATELESS:
////                                    session = knowledgeBaseConf.getKnowledgeBase().newStatelessKnowledgeSession();
////                                    cec = CalculationEngineConfig.create(knowledgeBaseConf.getType(),knowledgeBaseConf.getName(), val, CacheUtil.getMappingClassCache().get(type).getKey());   //TODO change null to something else
////                                    ((StatelessKnowledgeSession)session).execute(cec);
////                                    break;
////                                default:
////                                    cec = null;
////                                    session = null;
////                                    logger.error("session can't be null");  // TODO have a better thing to do asshole
////                            }
////                            sessionMap.put(knowledgeBaseConf.getName(),new AbstractMap.SimpleEntry<KnowledgeRuntimeEventManager, CalculationEngineConfig>(session,cec));
////
////                        } catch (Exception e) {
////                            logger.error(e,e);
////                        }
////
////                    }
////                });
////            }
////            executor.shutdown();
////            try {
////                executor.awaitTermination(Utils.getLongMetricsProperty("storm.trident.aggregator.accumulating.timeout",60000), TimeUnit.MILLISECONDS);
////            } catch (InterruptedException e) {
////                logger.error(e,e);
////            }
////
////            List<Object> emittedList = Lists.newArrayList();
////            Map resultMap = Maps.newHashMap(Maps.transformEntries(sessionMap,new Maps.EntryTransformer<String, Map.Entry<KnowledgeRuntimeEventManager, CalculationEngineConfig>, Object>() {
////                            @Override
////                            public Object transformEntry(@Nullable String key, @Nullable Map.Entry<KnowledgeRuntimeEventManager, CalculationEngineConfig> value) {
////                                return value.getValue().getResult();
////                            }
////                        }));
////
////            emittedList.add(resultMap);
////            collector.emit(emittedList);
////
////        } catch (Exception e){
////            logger.error(e,e);
////        } catch (Throwable t) {
////            logger.error(t,t);
////        }
////
////
////    }
//
//
//    @Override
//    public void complete(final List<T> val, final TridentCollector collector) {
//        try {
//
//            int nThreads = Runtime.getRuntime().availableProcessors();
//            ExecutorService executor = Executors.newFixedThreadPool(nThreads);
//
//            Map<String, CompiledScript> compiledScriptMap = CacheUtil.getCalculationCompiledScriptMapCache().get(type);
//            final Map<String, Object> resultMap = Maps.newHashMap();
//            for (final Map.Entry<String, CompiledScript> stringCompiledScriptEntry : compiledScriptMap.entrySet()) {
//                Future<?> kieFuture = executor.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            CompiledScript compiledScript = stringCompiledScriptEntry.getValue();
//                            ScriptEngine engine = compiledScript.getEngine();
//                            Bindings bindings = engine.createBindings();
//                            bindings.put(ICustomEngine.LIST_NAME, val);
//                            Object result = compiledScript.eval(bindings);
//                            resultMap.put(stringCompiledScriptEntry.getKey(), result);
//
//                        } catch (Exception e) {
//                            logger.error(e, e);
//                        }
//
//                    }
//                });
//            }
//
//            executor.shutdown();
//
//
//
//
//            try {
//                executor.awaitTermination(Utils.getLongMetricsProperty("storm.trident.aggregator.accumulating.timeout", 60000), TimeUnit.MILLISECONDS);
//            } catch (InterruptedException e) {
//                logger.error(e, e);
//            }catch (Exception e){
//                logger.error(e,e);
//            }
//
//            List<Object> emittedList = Lists.newArrayList();
//            emittedList.add(resultMap);
//            collector.emit(emittedList);
//
//        } catch (Exception e) {
//            logger.error(e, e);
//        } catch (Throwable t) {
//            logger.error(t, t);
//        }
//
//
//    }
//}