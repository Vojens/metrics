package metrics.server.storm.trident;

import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.planner.Node;

/**
 * Created by ali on 9/18/14.
 */
public class ExtendedTridentState extends TridentState {
    TridentTopology topology;
    Node node;
    protected ExtendedTridentState(TridentTopology topology, Node node) {
        super(topology, node);
        this.topology = topology;
        this.node = node;
    }
}
