package metrics.server.storm.trident.tuple;

/**
 * Created by ali on 8/25/14.
 */
public interface IExtendedTridentTuple<T> {
    public T getFieldValuePointerMap();
}
