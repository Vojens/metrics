package metrics.server.storm.trident.operator.filter;
import storm.trident.operation.Filter;
import storm.trident.operation.TridentOperationContext;
import storm.trident.tuple.TridentTuple;

import java.util.Map;

/**
 * Created by ali on 9/2/14.
 */
public class TestFilter implements Filter {
    @Override
    public boolean isKeep(TridentTuple objects) {
        return false;
    }

    @Override
    public void prepare(Map map, TridentOperationContext tridentOperationContext) {

    }

    @Override
    public void cleanup() {

    }
}
