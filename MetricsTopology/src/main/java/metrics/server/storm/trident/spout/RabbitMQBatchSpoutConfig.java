package metrics.server.storm.trident.spout;


import com.google.auto.value.AutoValue;
import storm.trident.TridentTopology;

import java.io.Serializable;

/**
 * Created by ali on 8/6/14.
 */
@AutoValue
public abstract class RabbitMQBatchSpoutConfig implements Serializable {
    private static final long serialVersionUID = 6205755594839583570L;

    public static RabbitMQBatchSpoutConfig create(final TridentTopology topology, String type) {
        return new AutoValue_RabbitMQBatchSpoutConfig(topology, type);
    }

    public abstract TridentTopology getTopology();

    public abstract String getType();
}
