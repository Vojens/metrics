package metrics.server.storm;

import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import com.google.common.collect.Lists;
import org.json.simple.JSONValue;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

/**
 * Created by ali on 6/17/14.
 */
public class SimpleJSONScheme implements backtype.storm.spout.Scheme {
    protected Fields fields;

    public SimpleJSONScheme(Fields fields) {
        this.fields = fields;
    }

    @Override
    public List<Object> deserialize(byte[] bytes) {
        String json = null;
        Charset charset = Charset.forName("UTF-8");
        json = new String(bytes, charset);


        Map<String, Object> map = (Map<String, Object>)
                JSONValue.parse(json);
        Values values = new Values();
        List<Object> outputList = Lists.newLinkedList();
        for (int i = 0; i < this.fields.size(); i++) {
            //values.add(map.get(this.fields.get(i)));
            outputList.add(map.get(this.fields.get(i)));
        }


//        System.out.println(str);
//        Map<String, Object> map = (Map<String, Object>)
//                JSONValue.parse(str);
//        //map.get("@Timestamp");
//        List<Object> outputList = Lists.newLinkedList();
//        outputList.add(str);
        return outputList;
    }

    @Override
    public Fields getOutputFields() {
        return fields;
    }
}
