package metrics.server.io.ampq.RabbitMq;


import metrics.server.io.ampq.IAmpqConnectionFactory;
import metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Map;


/**
 * Created by ali on 6/4/14.
 */
public class RabbitMqConnectionFactory extends ConnectionFactory implements IAmpqConnectionFactory {
    //private ConnectionFactory connectionFactory;

    private Map<String,Object> conf;
    public RabbitMqConnectionFactory(){
        super();
    }
    public RabbitMqConnectionFactory(Map<String,Object> conf){
        super();
        setConfMap(conf);
    }

//    public ConnectionFactory get(){
//        try {
//            connectionFactory.setUri(uri);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//        return this.connectionFactory;
//    }


    @Override
    public void setConfMap(Map<String,Object> conf) {
        this.conf = conf;
    }

    protected void pushConf(Map<String,Object> conf){
//        String rabbitmq_host = conf.get("rabbitmq.host").toString();
//        int rabbitmq_port = Integer.parseInt(conf.get("rabbitmq.port").toString());
//        String rabbitmq_username = conf.get("rabbitmq.username").toString();
//        String rabbitmq_password = conf.get("rabbitmq.password").toString();
//        String rabbitmq_virtualhost = conf.get("rabbitmq.virtualhost").toString();
//        int rabbitmq_heartbeat = Integer.parseInt(conf.get("rabbitmq.heartbeat").toString());


        ConnectionConfig connectionConfig = ConnectionConfig.getFromStormConfig(this.conf);


        super.setHost(connectionConfig.getHost());
        super.setPort(connectionConfig.getPort());
        super.setUsername(connectionConfig.getUsername());
        super.setPassword(connectionConfig.getPassword());
        super.setVirtualHost(connectionConfig.getVirtualHost());
        super.setRequestedHeartbeat(connectionConfig.getHeartBeat());


    }
}
