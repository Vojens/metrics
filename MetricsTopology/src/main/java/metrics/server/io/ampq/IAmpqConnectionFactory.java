package metrics.server.io.ampq;

import com.google.inject.name.Named;

import java.util.Map;

/**
 * Created by ali on 6/5/14.
 */
public interface IAmpqConnectionFactory {
    @Named("conf_map")
    public void setConfMap(Map<String,Object> uri);
    //public void close();
}
