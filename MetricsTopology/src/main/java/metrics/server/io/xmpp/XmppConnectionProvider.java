package metrics.server.io.xmpp;

import com.google.inject.Inject;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;

/**
 * Created by ali on 8/12/14.
 */
public class XmppConnectionProvider implements IConnectionProvider {
    private Logger logger = Logger.getLogger(XmppConnectionProvider.class.getName());

    @Inject
    private ConnectionConfiguration connectionConf;
    @Inject
    private XMPPConnection xmppConnection;

    public XmppConnectionProvider() {

    }

    @Override
    public void start() {
        //xmppConnection = new XMPPTCPConnection(getConnectionConf());
    }

    @Override
    public void restart() {
        close();
        start();
    }


    @Override
    public void close() {
        if (xmppConnection != null) {
            try {
                xmppConnection.disconnect();
            } catch (Exception e) {
            }
        }
    }

    @Override
    public XMPPConnection getConnection() {
        return xmppConnection;
    }

    @Override
    public ConnectionConfiguration getConnectionConf() {
        return connectionConf;
    }
}
