package metrics.server.io.xmpp;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import metrics.server.container.IManager;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/12/14.
 */
public class XmppManager implements IManager {
    private final transient static Logger logger = Logger.getLogger(XmppManager.class.getName());
    private static final Object connectionLock = new Object();
    //@Inject
    private XMPPConnection connection;
    //@Inject
    private ConnectionConfiguration connConf;
    @Inject
    @Named("username")
    private String username;
    @Inject
    @Named("password")
    private String password;

    @Inject
    public void setConnectionProvider(IConnectionProvider provider) {
        this.connConf = provider.getConnectionConf();
        setConnection(provider.getConnection());
    }

    protected void login(String username, String password) throws XMPPException {
        if (connection != null && connection.isConnected()) {
            try {
                connection.login(username, password);
            } catch (SmackException e) {
                logger.error(e, e);
            } catch (IOException e) {
                logger.error(e, e);
            }
        }
    }

    public void close() {
        synchronized (connectionLock) {
            checkNotNull(connection);
            try {
                connection.disconnect();
            } catch (Exception e) {
                logger.error(e, e);
            }
            connection = null;
        }

    }

    public XMPPConnection getConnection() {
        if (connection != null && connection.isConnected()) {
            //connection.disconnect();
            return connection;
        }
        return null;
    }

    public void setConnection(XMPPConnection provider) {
        synchronized (connectionLock) {
            if (connection != null) {
                try {
                    if (connection.isConnected()) {
                        connection.disconnect();
                    }
                } catch (SmackException.NotConnectedException e) {
                    logger.error(e, e);
                }
                connection = null;
            }
            connection = provider;
            try {
                connection.addConnectionListener(new ConnectionListener());
                connection.addPacketSendingListener(new PacketListener() {
                    @Override
                    public void processPacket(Packet packet) throws SmackException.NotConnectedException {
                        logger.info(packet.getXmlns());
                    }
                }, null);
                //SASLAuthentication.supportSASLMechanism("PLAIN",0);
                //connection.connect();
                SASLAuthentication.supportSASLMechanism("PLAIN", 0);
                //GSSAPI,CRAM-MD5,DIGEST-MD5,PLAIN,EXTERNAL
                //SASLAuthentication.supportSASLMechanism("PLAIN", 0);
                //SASLAuthentication.supportSASLMechanism("CRAM-MD5", 0);
                //SASLAuthentication.supportSASLMechanism("DIGEST-MD5", 0);
                //SASLAuthentication.supportSASLMechanism("GSSAPI", 0);
                //SASLAuthentication.supportSASLMechanism("EXTERNAL", 0);
                login(username, password);
           }
// catch (SmackException e) {
//                logger.error(e, e);
//            } catch (IOException e) {
//                logger.error(e, e);
//            }
            catch (XMPPException e) {
                logger.error(e, e);
            } catch (Exception e) {
                logger.error(e, e);
            }
        }

    }

    public void setStatus(boolean available, String status) {

        Presence.Type type = available ? Presence.Type.available : Presence.Type.unavailable;
        Presence presence = new Presence(type);
        presence.setMode(Presence.Mode.available);
        presence.setStatus(status);
        try {
            connection.sendPacket(presence);
        } catch (SmackException.NotConnectedException e) {
            logger.error(e, e);
        }

    }

    private class ConnectionListener implements org.jivesoftware.smack.ConnectionListener {
        //        XmppManager xmppManager;
//
//        public ConnectionListener(XmppManager xmppManager){
//            this.xmppManager = xmppManager;
//        }
        @Override
        public void connected(XMPPConnection xmppConnection) {

        }

        @Override
        public void authenticated(XMPPConnection xmppConnection) {
            logger.info("connection to xmpp is authenticated");
        }

        @Override
        public void connectionClosed() {

        }

        @Override
        public void connectionClosedOnError(Exception e) {

        }

        @Override
        public void reconnectingIn(int i) {

        }

        @Override
        public void reconnectionSuccessful() {

        }

        @Override
        public void reconnectionFailed(Exception e) {

        }
    }

}
