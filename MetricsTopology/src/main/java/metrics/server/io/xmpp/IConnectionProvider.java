package metrics.server.io.xmpp;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;

/**
 * Created by ali on 8/12/14.
 */
public interface IConnectionProvider {
    public void start();

    public void restart();

    public void close();

    public XMPPConnection getConnection();

    public ConnectionConfiguration getConnectionConf();
}
