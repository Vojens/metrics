package metrics.server.io.channel.handler;

import com.google.common.eventbus.EventBus;
import metrics.server.io.channel.IChannel;
import metrics.server.io.channel.RabbitMq;
import metrics.server.io.channel.Stdout;


/**
 * Created by ali on 6/10/14.
 */
public class OutputHandlerFactory {
    private IChannel output;
    private EventBus eventBus;

    public OutputHandlerFactory(final IChannel output, final EventBus eventBus) {
        this.output = output;
        this.eventBus = eventBus;
    }

    public IChannelHandler get() {
        if (output instanceof RabbitMq) {
            return new RabbitMqHandler((RabbitMq) output, eventBus);
        } else if (output instanceof Stdout) {
            return new StdoutHandler((Stdout) output, eventBus);
        }
        //add more if necessary
        return null;
    }
}
