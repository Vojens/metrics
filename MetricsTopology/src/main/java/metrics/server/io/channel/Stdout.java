package metrics.server.io.channel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import metrics.server.io.ISerialization;

import javax.annotation.Generated;

/**
 * Created by ali on 6/12/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})
public class Stdout extends BaseChannel {
    public Stdout() {
        super();
    }

    public Stdout(Stdout stdout) {
        super(stdout);
    }

    @Override
    public ISerialization deepCopy() {
        return new Stdout(this);
    }
}
