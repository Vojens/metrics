package metrics.server.io.channel;

/**
 * Created by ali on 6/9/14.
 */

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import metrics.server.io.ISerialization;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RabbitMq.class),
        @JsonSubTypes.Type(value = Stdout.class),
})
public interface IChannel extends ISerialization {

}
