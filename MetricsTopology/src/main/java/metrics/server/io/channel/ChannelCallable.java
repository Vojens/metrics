package metrics.server.io.channel; /**
 * Created by ali on 6/4/14.
 */

import com.rabbitmq.client.Channel;

import java.io.IOException;

public interface ChannelCallable<T> {
    String getDescription();

    T call(Channel channel) throws IOException;
}
