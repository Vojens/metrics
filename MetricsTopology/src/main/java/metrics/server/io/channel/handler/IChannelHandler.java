package metrics.server.io.channel.handler;

import com.google.common.eventbus.Subscribe;
import metrics.server.io.ISerialization;
import metrics.server.io.event.IEvent;


/**
 * Created by ali on 6/10/14.
 */
public interface IChannelHandler extends ISerialization {
    @Subscribe
    void submit(final IEvent event);
}
