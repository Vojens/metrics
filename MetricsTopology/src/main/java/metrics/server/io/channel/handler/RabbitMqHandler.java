package metrics.server.io.channel.handler;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.common.io.CharStreams;
import metrics.server.io.channel.RabbitMq;
import metrics.server.io.codec.CodecFactory;
import metrics.server.io.codec.ICodec;
import metrics.server.io.event.Event;
import metrics.server.io.event.IEvent;
import metrics.server.io.event.ReaderEvent;
import metrics.server.io.event.StreamEvent;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.charset.Charset;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 6/10/14.
 */
public class RabbitMqHandler extends RabbitMq implements IChannelHandler {
    private static Logger logger = Logger.getLogger(RabbitMqHandler.class.getName());
    protected boolean initialized = false;

    protected RabbitMqHandler(RabbitMq rabbit, EventBus eventBus) {
        super(rabbit);
        checkNotNull(eventBus, "Event Bus cannot be null").register(this);
    }

    private static byte[] serialize(Object obj) {

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        ObjectOutputStream o = null;
        try {
            o = new ObjectOutputStream(b);
            o.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return b.toByteArray();
    }

    @Override
    @Subscribe
    public void submit(final IEvent event) {
        //System.out.println("sumbit is executed "+System.currentTimeMillis());
        //event.getMessage()
        String message = null;

        if (event instanceof Event) {

        } else if (event instanceof StreamEvent) {

        } else if (event instanceof ReaderEvent) {
            try {
                message = CharStreams.toString(((ReaderEvent) event).getReader());

                //System.out.println(message);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    ((ReaderEvent) event).getReader().close();
                } catch (IOException e) {
                    logger.error("error in closing a stream", e);
                }
            }

        }

        //System.out.println(message);
//        final byte[] messageBytes = message.getBytes();
        final String exchange = super.getExchange();
        final String exchangeType = super.getExchangeType();
        final String bindingKey = super.getRountingKey();
        int workers = super.getWorkers();


        ICodec codec = CodecFactory.get(super.getCodec());
        codec.setMessage(message);

        final byte[] messageBytes = codec.toString().getBytes(Charset.forName("UTF-8"));
        //TODO enable later when needed
//        String messagId = ShipperAgent.getInstance().getConnectionManager().call(new ChannelCallable<String>() {
//            @Override
//            public String getDescription() {
//                return "submit to main";
//            }
//
//            @Override
//            public String call(Channel channel) throws IOException {
//                //System.out.println("call");
//                Map<String,Object> headers = Maps.newHashMap();
//                if(getCc()!=null){
//                    headers.put("CC",getCc());
//                }
//                if(getBcc() != null){
//                    headers.put("BCC",getBcc());
//                }
//
//
//
//                AMQP.BasicProperties.Builder propBuilder = new AMQP.BasicProperties.Builder();
//                final AMQP.BasicProperties basicProperties = propBuilder
//                        .appId("Metrics")
//                        .deliveryMode(2)
//                        .messageId(UUID.randomUUID().toString())
//                        .priority(0)
//                        .expiration("60000")
//                        .contentType("application/vnd.apache.avro+json")
//                        .contentEncoding("UTF-8")
//                        .timestamp(DateTime.now().toDate())
//                        .type("WITSML")
//                        .headers(headers).build();
//
//                //return null;
//                if(!initialized){
//                    AMQP.Exchange.DeclareOk declareOk = null;
//                    try{
//                        declareOk = channel.exchangeDeclare(exchange, exchangeType,true,false,null);
//                        initialized = true;
//                    }catch(Exception e){
//                        logger.error("Error in declaring an exchange",e);
//                    }
//
//                    if(declareOk != null){
//                        logger.info("exchange is declared");
//                    }
//                }
//                //System.out.println(messageBytes);
//
//                channel.basicPublish(exchange,bindingKey,basicProperties,messageBytes);
//
//
//                //channel.basicPublish(super.);
//                return basicProperties.getMessageId();
//            }
//        });


    }
}
