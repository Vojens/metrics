package metrics.server.io.channel.handler;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import metrics.server.io.channel.Stdout;
import metrics.server.io.codec.CodecFactory;
import metrics.server.io.codec.ICodec;
import metrics.server.io.event.Event;
import metrics.server.io.event.IEvent;
import metrics.server.io.event.ReaderEvent;
import metrics.server.io.event.StreamEvent;

import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 6/12/14.
 */
public class StdoutHandler extends Stdout implements IChannelHandler {
    public StdoutHandler(Stdout stdout, EventBus eventBus) {
        super(stdout);
        checkNotNull(eventBus, "Event Bus cannot be null").register(this);
    }

    @Override
    @Subscribe
    public void submit(IEvent event) {
        Object message = null;
        if (event instanceof Event) {
            message = ((Event) event).getMessage();
        } else if (event instanceof StreamEvent) {
            try {
                message = ByteStreams.toByteArray(((StreamEvent) event).getStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (event instanceof ReaderEvent) {
            try {
                message = CharStreams.toString(((ReaderEvent) event).getReader());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        ICodec codec = CodecFactory.get(super.getCodec());
        codec.setMessage(message);
        System.out.println(codec.toString());
    }
}
