package metrics.server.io.channel;

import com.fasterxml.jackson.annotation.JsonProperty;
import metrics.server.io.BaseSerialization;
import metrics.server.io.ISerialization;

/**
 * Created by ali on 6/9/14.
 */

public abstract class BaseChannel extends BaseSerialization implements ISerialization, IChannel {
    private String codec = "Plain";

    protected BaseChannel() {

    }

    protected BaseChannel(BaseChannel io) {
        super(io);
        this.setCodec(io.getCodec());
    }

    @JsonProperty("Codec")
    public String getCodec() {
        return codec;
    }

    @JsonProperty("Codec")
    public void setCodec(String codec) {
        this.codec = codec;
    }
}
