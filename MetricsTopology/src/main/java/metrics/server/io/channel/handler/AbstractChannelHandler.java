package metrics.server.io.channel.handler;

import com.google.common.eventbus.EventBus;
import metrics.server.io.ISerialization;
import metrics.server.io.channel.BaseChannel;

/**
 * Created by ali on 6/10/14.
 */
public abstract class AbstractChannelHandler extends BaseChannel implements IChannelHandler {
    protected AbstractChannelHandler(BaseChannel io, EventBus eventbus) {
        super(io);
        //super(metrics.server.io);
        //this.setCodec(metrics.server.io.getCodec());
    }

    @Override
    public ISerialization deepCopy() {
        return null;
    }


}
