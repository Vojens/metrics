package metrics.server.io;

import com.google.common.collect.Maps;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by ali on 6/17/14.
 */
public interface ISerialization extends Serializable {
    Map<String, Object> additionalProperties = Maps.newHashMap();

    Map<String, Object> getAdditionalProperties();

    void setAdditionalProperty(String name, Object value);

    String getName();

    void setName(String name);

    ISerialization deepCopy();
}
