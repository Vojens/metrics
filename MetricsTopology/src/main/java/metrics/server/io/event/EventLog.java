package metrics.server.io.event;

import com.google.auto.value.AutoValue;
import metrics.server.util.Version;

//import metrics.server.storm.model.event.AutoValue_EventLog;
//import metrics.server.storm.model.event.IEvent;

/**
 * Created by ali on 6/23/14.
 */
@AutoValue
public abstract class EventLog implements IEvent {
//    private String host;
//    private String message;
//    private DateTime dateTime;
//    private String type;
//    private Version version;

    public static EventLog create(final org.joda.time.DateTime timestamp, final String serverName, final String message) {

        return new AutoValue_EventLog(EventLog.class, timestamp, serverName, message, null, null);
    }

    public static EventLog create(final org.joda.time.DateTime timestamp, final String serverName, final String message, final String type, final Version version) {

        return new AutoValue_EventLog(EventLog.class, timestamp, serverName, message, type, version);
    }

    public abstract org.joda.time.DateTime getTimestamp();

    public abstract String getServerName();

    public abstract String getMessage();

    public abstract String getType();

    public abstract Version getVersion();

}
