package metrics.server.io.event;

/**
 * Created by ali on 6/11/14.
 */
public interface IEvent {
    public Class<? extends IEvent> getEventType();
}
