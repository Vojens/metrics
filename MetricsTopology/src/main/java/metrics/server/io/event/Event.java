package metrics.server.io.event;

/**
 * Created by ali on 6/10/14.
 */
public class Event implements IEvent {
    private String message;

    public Event(String message) {
        setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public Class<? extends IEvent> getEventType() {
        return getClass();
    }
}
