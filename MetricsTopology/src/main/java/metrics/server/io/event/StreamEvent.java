package metrics.server.io.event;


import java.io.InputStream;

/**
 * Created by ali on 6/11/14.
 */
public class StreamEvent implements IEvent {
    private InputStream stream;

    public StreamEvent(InputStream stream) {
        this.stream = stream;
    }

    public InputStream getStream() {
        return stream;
    }

    @Override
    public Class<? extends IEvent> getEventType() {
        return StreamEvent.class;
    }
}
