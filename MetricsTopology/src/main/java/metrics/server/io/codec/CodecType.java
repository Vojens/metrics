package metrics.server.io.codec;

/**
 * Created by ali on 8/11/14.
 */
public enum CodecType {
    PLAIN {
        public String toString() {
            return "plain";
        }
    },
    JSON {
        public String toString() {
            return "json";
        }
    }
}
