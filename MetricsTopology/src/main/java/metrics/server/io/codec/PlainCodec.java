package metrics.server.io.codec;

/**
 * Created by ali on 6/12/14.
 */
public class PlainCodec extends AbstractCodec {
    private Object message;

    public Object getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(Object message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message.toString();
    }

}
