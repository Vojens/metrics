package metrics.server.io.latent.storm.rabbitmq;

import metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig;
import com.rabbitmq.client.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Serializable;

/**
 * An abstraction on RabbitMQ client API to encapsulate interaction with RabbitMQ and de-couple Storm API from RabbitMQ API.
 *
 * @author peter@latent.metrics.server.io
 */
public class RabbitMQConsumer implements Serializable {
    public static final long MS_WAIT_FOR_MESSAGE = 1L;
    private static final long serialVersionUID = 4704553594508796454L;

    protected final ConnectionFactory connectionFactory;
    protected final int prefetchCount;
    protected final String queueName;
    protected final boolean requeueOnFail;
    protected final Declarator declarator;
    protected final ErrorReporter reporter;
    protected static final transient Logger logger = Logger.getLogger(RabbitMQConsumer.class.getName());

    protected Connection connection;
    protected Channel channel;
    protected QueueingConsumer consumer;
    protected String consumerTag;

    public RabbitMQConsumer(ConnectionConfig connectionConfig,
                            int prefetchCount,
                            String queueName,
                            boolean requeueOnFail,
                            Declarator declarator,
                            ErrorReporter errorReporter) {
        this.connectionFactory = connectionConfig.asConnectionFactory();
        this.prefetchCount = prefetchCount;
        this.queueName = queueName;
        this.requeueOnFail = requeueOnFail;
        this.declarator = declarator;

        this.reporter = errorReporter;
    }

    public Message nextMessage() {
        reinitIfNecessary();
        if (consumerTag == null || consumer == null) return Message.NONE;
        try {
            return Message.forDelivery(consumer.nextDelivery(MS_WAIT_FOR_MESSAGE));
        } catch (ShutdownSignalException sse) {
            reset();
            logger.error("shutdown signal received while attempting to get next message", sse);
            reporter.reportError(sse);
            return Message.NONE;
        } catch (InterruptedException ie) {
      /* nothing to do. timed out waiting for message */
            logger.debug("interruepted while waiting for message", ie);
            return Message.NONE;
        } catch (ConsumerCancelledException cce) {
      /* if the queue on the broker was deleted or node in the cluster containing the queue failed */
            reset();
            logger.error("consumer got cancelled while attempting to get next message", cce);
            reporter.reportError(cce);
            return Message.NONE;
        }
    }

    public void ack(Long msgId) {
        ack(msgId,false);
    }
    public void ack(Long msgId, boolean multiple){
        reinitIfNecessary();
        try {
            channel.basicAck(msgId, multiple);
        } catch (ShutdownSignalException sse) {
            reset();
            logger.error("shutdown signal received while attempting to ack message", sse);
            reporter.reportError(sse);
        } catch (Exception e) {
            logger.error("could not ack for msgId: " + msgId, e);
            reporter.reportError(e);
        }
    }

    public void fail(Long msgId) {
        if (requeueOnFail)
            failWithRedelivery(msgId);
        else
            deadLetter(msgId);
    }

    public void failWithRedelivery(Long msgId) {
        reinitIfNecessary();
        try {
            channel.basicReject(msgId, true);
        } catch (ShutdownSignalException sse) {
            reset();
            logger.error("shutdown signal received while attempting to fail with redelivery", sse);
            reporter.reportError(sse);
        } catch (Exception e) {
            logger.error("could not fail with redelivery for msgId: " + msgId, e);
            reporter.reportError(e);
        }
    }

    public void deadLetter(Long msgId) {
        reinitIfNecessary();
        try {
            channel.basicReject(msgId, false);
        } catch (ShutdownSignalException sse) {
            reset();
            logger.error("shutdown signal received while attempting to fail with no redelivery", sse);
            reporter.reportError(sse);
        } catch (Exception e) {
            logger.error("could not fail with dead-lettering (when configured) for msgId: " + msgId, e);
            reporter.reportError(e);
        }
    }

    public void open() {
        try {
            connection = createConnection();
            channel = connection.createChannel();
            if (prefetchCount > 0) {
                logger.info("setting basic.qos / prefetch count to " + prefetchCount + " for " + getQueueName());
                channel.basicQos(prefetchCount);
            }
            // run any declaration prior to queue consumption
            declarator.execute(channel);

            consumer = new QueueingConsumer(channel);
            consumerTag = channel.basicConsume(getQueueName(), isAutoAcking(), consumer);
        } catch (Exception e) {
            reset();
            logger.error("could not open listener on queue " + getQueueName());
            reporter.reportError(e);
        }
    }


//    public int getMessageCount(){
//        try {
//            AMQP.Queue.DeclareOk declareOk = channel.queueDeclarePassive(queueName);
//            return declareOk.getMessageCount();
//        } catch (IOException e) {
//            logger.error(e,e);
//        }
//        return -1;
//    }
    protected boolean isAutoAcking() {
        return false;
    }

    public void close() {
        try {
            if (channel != null && channel.isOpen()) {
                if (consumerTag != null) channel.basicCancel(consumerTag);
                channel.close();
            }
        } catch (Exception e) {
            logger.debug("error closing channel and/or cancelling consumer", e);
        }
        try {
            logger.info("closing connection to rabbitmq: " + connection);
            connection.close();
        } catch (Exception e) {
            logger.debug("error closing connection", e);
        }
        consumer = null;
        consumerTag = null;
        channel = null;
        connection = null;
    }

    protected void reset() {
        consumerTag = null;
    }

    private void reinitIfNecessary() {
        if (consumerTag == null || consumer == null) {
            close();
            open();
        }
    }

    protected Connection createConnection() throws IOException {
        Connection connection = connectionFactory.newConnection();
        connection.addShutdownListener(new ShutdownListener() {
            @Override
            public void shutdownCompleted(ShutdownSignalException cause) {
                logger.error("shutdown signal received", cause);
                reporter.reportError(cause);
                reset();
            }
        });
        logger.info("connected to rabbitmq: " + connection + " for " + getQueueName());
        return connection;
    }

    public String getQueueName() {
        return queueName;
    }
}
