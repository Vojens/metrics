package metrics.server.io.latent.storm.rabbitmq.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static metrics.server.io.latent.storm.rabbitmq.config.ConfigUtils.*;

public class ConsumerConfig implements Serializable {
    private static final long serialVersionUID = -7831201298410660913L;
    private final ConnectionConfig connectionConfig;
    private final int prefetchCount;
    private final String queueName;
    private final boolean requeueOnFail;

    public ConsumerConfig(ConnectionConfig connectionConfig,
                          int prefetchCount,
                          String queueName,
                          boolean requeueOnFail) {
        if (connectionConfig == null || prefetchCount < 0) {
            throw new IllegalArgumentException("Invalid configuration."+(prefetchCount < 0 ? " prefetch count < 0":""));
        }

        this.connectionConfig = connectionConfig;
        this.prefetchCount = prefetchCount;
        this.queueName = queueName;
        this.requeueOnFail = requeueOnFail;
    }

    public static ConsumerConfig getFromStormConfig(Map<String, Object> stormConfig) {
        ConnectionConfig connectionConfig = ConnectionConfig.getFromStormConfig(stormConfig);
        return new ConsumerConfig(connectionConfig,
                getFromMapAsInt("rabbitmq.prefetchCount", stormConfig),
                getFromMap("rabbitmq.queueName", stormConfig),
                getFromMapAsBoolean("rabbitmq.requeueOnFail", stormConfig));
    }

    public ConnectionConfig getConnectionConfig() {
        return connectionConfig;
    }

    public int getPrefetchCount() {
        return prefetchCount;
    }

    public String getQueueName() {
        return queueName;
    }

    public boolean isRequeueOnFail() {
        return requeueOnFail;
    }

    public Map<String, Object> asMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.putAll(connectionConfig.asMap());
        addToMap("rabbitmq.prefetchCount", map, prefetchCount);
        addToMap("rabbitmq.queueName", map, queueName);
        addToMap("rabbitmq.requeueOnFail", map, requeueOnFail);
        return map;
    }
}

