package metrics.server.io.latent.storm.rabbitmq;

import metrics.server.io.latent.storm.rabbitmq.config.ConnectionConfig;

import java.io.Serializable;

/**
 * An abstraction on RabbitMQ client API to encapsulate interaction with RabbitMQ and de-couple Storm API from RabbitMQ API.
 *
 * @author peter@latent.metrics.server.io
 */
public class RabbitMQAcknowledger extends RabbitMQConsumer implements Serializable {
    private static final long serialVersionUID = -4580261825988879567L;


    public RabbitMQAcknowledger(ConnectionConfig connectionConfig,
                            String queueName,
                            boolean requeueOnFail,
                            Declarator declarator,
                            ErrorReporter errorReporter) {
        super(connectionConfig,
                0,
        queueName,
        requeueOnFail,
        declarator,
        errorReporter);
    }



    public void open(){
        try {
            connection = createConnection();
            channel = connection.createChannel();
            // run any declaration prior to queue consumption
            declarator.execute(channel);

            //consumer = new QueueingConsumer(channel);
            //consumerTag = channel.basicConsume(getQueueName(), isAutoAcking(), consumer);
        } catch (Exception e) {
            reset();
            logger.error("could not open listener on queue " + getQueueName());
            reporter.reportError(e);
        }
    }


}

