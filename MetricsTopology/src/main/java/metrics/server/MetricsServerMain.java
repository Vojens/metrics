package metrics.server;

import com.google.common.util.concurrent.Monitor;

/**
 * Created by ali on 7/7/14.
 */
public class MetricsServerMain {
    static final ClassLoader loader = MetricsServerMain.class.getClassLoader();
    static final boolean isShutDownRequested = false;

    public static void main(String[] args) {


        MetricsServer shipperAgent = new MetricsServer();

        Monitor monitor = new Monitor();

        Monitor.Guard isShutDownGuard = new Monitor.Guard(monitor) {
            @Override
            public boolean isSatisfied() {
                return isShutDownRequested;
            }
        };
        try {
            monitor.enterWhen(isShutDownGuard);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
