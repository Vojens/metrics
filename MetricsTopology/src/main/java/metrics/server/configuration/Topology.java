package metrics.server.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ali on 6/16/14.
 */
public class Topology {
    private Declarer declarer;

    @JsonProperty("Declarer")
    public Declarer getDeclarer() {
        return declarer;
    }

    @JsonProperty("Declarer")
    public void setDeclarer(Declarer declarer) {
        this.declarer = declarer;
    }
}
