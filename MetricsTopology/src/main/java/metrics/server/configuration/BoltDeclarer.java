package metrics.server.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.Lists;
import metrics.server.configuration.grouping.BaseGrouping;
import metrics.server.configuration.grouping.IGrouping;
import metrics.server.io.BaseSerialization;
import metrics.server.io.ISerialization;

import java.util.List;

/**
 * Created by ali on 6/17/14.
 */
public class BoltDeclarer extends BaseSerialization {
    private List<IGrouping> groupingList = Lists.newLinkedList();
    private String boltName;
    private int parallelism;

    public BoltDeclarer() {
        super();
    }

    public BoltDeclarer(BoltDeclarer boltDeclarer) {
        super(boltDeclarer);
        this.setBoltName(boltDeclarer.getBoltName());
        this.setGroupingList(boltDeclarer.getGroupingList());
        this.setParallelism(boltDeclarer.getParallelism());
    }

    @Override
    public ISerialization deepCopy() {
        return new BoltDeclarer(this);
    }

    @JsonProperty("BoltName")
    public String getBoltName() {
        return boltName;
    }

    @JsonProperty("BoltName")
    public void setBoltName(String boltName) {
        this.boltName = boltName;
    }

    @JsonProperty("Parallelism")
    public int getParallelism() {
        return parallelism;
    }

    @JsonProperty("Parallelism")
    public void setParallelism(int parallelism) {
        this.parallelism = parallelism;
    }

    @JsonProperty("Groupings")
    @JsonDeserialize(contentAs = BaseGrouping.class)
    public List<IGrouping> getGroupingList() {
        return groupingList;
    }

    @JsonProperty("Groupings")
    @JsonDeserialize(contentAs = BaseGrouping.class)
    public void setGroupingList(List<IGrouping> groupingList) {
        this.groupingList = groupingList;
    }
}
