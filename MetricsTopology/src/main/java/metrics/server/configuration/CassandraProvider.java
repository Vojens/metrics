package metrics.server.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import metrics.server.io.BaseSerialization;
import metrics.server.io.ISerialization;

import java.util.List;

/**
 * Created by ali on 7/6/14.
 */
public class CassandraProvider extends BaseSerialization {
    private String username;
    private String password;
    private List<String> contactPointList = Lists.newLinkedList();


    public CassandraProvider() {

    }

    public CassandraProvider(CassandraProvider database) {
        //TODO: implements
    }

    @JsonProperty("Username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("Username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("Password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("Password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("ContactPoints")
    public List<String> getContactPointList() {
        return contactPointList;
    }

    @JsonProperty("ContactPoints")
    public void setContactPointList(List<String> contactPointList) {
        this.contactPointList = contactPointList;
    }

    @Override
    public ISerialization deepCopy() {
        return new CassandraProvider(this);
    }
}
