package metrics.server.configuration.grouping;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import javax.annotation.Generated;

/**
 * Created by ali on 6/17/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})
public abstract class BasicGrouping extends BaseGrouping {
    private String componentId;
    private String streamId;

    protected BasicGrouping(BasicGrouping grouping) {
        super(grouping);
        this.setComponentId(grouping.getComponentId());
        this.setStreamId(grouping.getStreamId());
    }

    protected BasicGrouping() {
        super();
    }

    @JsonProperty("ComponentId")
    public String getComponentId() {
        return componentId;
    }

    @JsonProperty("ComponentId")
    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    @JsonProperty("StreamId")
    public String getStreamId() {
        return streamId;
    }

    @JsonProperty("StreamId")
    public void setStreamId(String streamId) {
        this.streamId = streamId;
    }
}
