package metrics.server.configuration.grouping.spi;

import backtype.storm.tuple.Fields;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import metrics.server.configuration.grouping.BasicGrouping;
import metrics.server.io.ISerialization;

import javax.annotation.Generated;

/**
 * Created by ali on 6/17/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})
public class FieldsGrouping extends BasicGrouping {

    private Fields fields;

    public FieldsGrouping() {
        super();
    }

    public FieldsGrouping(FieldsGrouping fieldsGrouping) {
        super(fieldsGrouping);
        this.setFields(fieldsGrouping.getFields());
    }

    @Override
    public ISerialization deepCopy() {
        return new FieldsGrouping(this);
    }

    @JsonProperty("Fields")
    public Fields getFields() {
        return fields;
    }

    @JsonProperty("Fields")
    public void setFields(Fields fields) {
        this.fields = fields;
    }
}
