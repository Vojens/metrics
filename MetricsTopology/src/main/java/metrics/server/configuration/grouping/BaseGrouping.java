package metrics.server.configuration.grouping;

import metrics.server.io.BaseSerialization;

/**
 * Created by ali on 6/17/14.
 */
public abstract class BaseGrouping extends BaseSerialization implements IGrouping {
    protected BaseGrouping(BaseGrouping grouping) {
        super(grouping);
    }

    protected BaseGrouping() {
        super();
    }
}
