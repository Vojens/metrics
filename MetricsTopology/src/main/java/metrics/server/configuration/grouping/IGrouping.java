package metrics.server.configuration.grouping;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import metrics.server.configuration.grouping.spi.*;
import metrics.server.io.ISerialization;

/**
 * Created by ali on 6/17/14.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AllGrouping.class),
        @JsonSubTypes.Type(value = DirectGrouping.class),
        @JsonSubTypes.Type(value = FieldsGrouping.class),
        @JsonSubTypes.Type(value = GlobalGrouping.class),
        @JsonSubTypes.Type(value = LocalOrShuffleGrouping.class),
        @JsonSubTypes.Type(value = NoneGrouping.class),
        @JsonSubTypes.Type(value = ShuffleGrouping.class),
})
public interface IGrouping extends ISerialization {
}
