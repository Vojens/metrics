package metrics.server.configuration.grouping.spi;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import metrics.server.configuration.grouping.BasicGrouping;
import metrics.server.io.ISerialization;

import javax.annotation.Generated;

/**
 * Created by ali on 6/17/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})
public class ShuffleGrouping extends BasicGrouping {
    public ShuffleGrouping() {
        super();
    }

    public ShuffleGrouping(ShuffleGrouping shuffleGrouping) {
        super(shuffleGrouping);
    }

    @Override
    public ISerialization deepCopy() {
        return new ShuffleGrouping(this);
    }
}
