package metrics.server.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import metrics.server.io.BaseSerialization;
import metrics.server.io.ISerialization;

import java.util.List;

/**
 * Created by ali on 6/17/14.
 */
public class Declarer extends BaseSerialization {
    private List<SpoutDeclarer> spoutDeclarerList = Lists.newLinkedList();
    private List<BoltDeclarer> boltDeclarerList = Lists.newLinkedList();

    public Declarer() {
        super();
    }

    public Declarer(Declarer declarer) {
        super(declarer);
        this.setSpoutDeclarerList(declarer.getSpoutDeclarerList());
        this.setBoltDeclarerList(declarer.getBoltDeclarerList());
    }


    @JsonProperty("SpoutDeclarers")
    public List<SpoutDeclarer> getSpoutDeclarerList() {
        return spoutDeclarerList;
    }

    @JsonProperty("SpoutDeclarers")
    public void setSpoutDeclarerList(List<SpoutDeclarer> spoutDeclarerList) {
        this.spoutDeclarerList = spoutDeclarerList;
    }

    @JsonProperty("BoltDeclarers")
    public List<BoltDeclarer> getBoltDeclarerList() {
        return boltDeclarerList;
    }

    @JsonProperty("BoltDeclarers")
    public void setBoltDeclarerList(List<BoltDeclarer> boltDeclarerList) {
        this.boltDeclarerList = boltDeclarerList;
    }

    @Override
    public ISerialization deepCopy() {
        return new Declarer(this);
    }
}
