package metrics.server.configuration;

import backtype.storm.Config;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ali on 6/16/14.
 */
public class Cluster {
    @JsonIgnore
    private Config config;

    @JsonProperty("Config")
    public Config getConfig() {
        return config;
    }

    @JsonProperty("Config")
    public void setConfig(Config config) {
        this.config = config;
    }
}
