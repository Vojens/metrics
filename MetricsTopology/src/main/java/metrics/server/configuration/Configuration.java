package metrics.server.configuration;

import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.topology.base.BaseRichSpout;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by ali on 6/16/14.
 */
public class Configuration {
    private List<Topology> topologyList = Lists.newLinkedList();
    private List<Cluster> clusterList = Lists.newLinkedList();

    private List<BaseRichSpout> spoutList = Lists.newArrayList();
    private List<BaseRichBolt> boltList = Lists.newArrayList();

    private Database database = new Database();

    private boolean isSetup;

    public Configuration() {

    }

    @JsonProperty("Database")
    public Database getDatabase() {
        return database;
    }

    @JsonProperty("Database")
    public void setDatabase(Database database) {
        this.database = database;
    }


    @JsonProperty("Setup")
    public boolean isSetup() {
        return isSetup;
    }

    @JsonProperty("Setup")
    public void setSetup(boolean isSetup) {
        this.isSetup = isSetup;
    }
}
