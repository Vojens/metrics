package metrics.server.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import metrics.server.io.BaseSerialization;
import metrics.server.io.ISerialization;

import javax.annotation.Generated;

/**
 * Created by ali on 7/6/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({
})
public class Database extends BaseSerialization {
    private CassandraProvider cassandraProvider;

    public Database() {

    }

    public Database(Database database) {
        //TODO: implements
    }


    @JsonProperty("CassandraProvider")
    public CassandraProvider getCassandraProvider() {
        return cassandraProvider;
    }

    @JsonProperty("CassandraProvider")
    public void setCassandraProvider(CassandraProvider cassandraProvider) {
        this.cassandraProvider = cassandraProvider;
    }

    @Override
    public ISerialization deepCopy() {
        return new Database(this);
    }
}
