package metrics.server.util;

import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.google.common.base.Ticker;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import metrics.server.database.DbConnectionManager;

import java.util.AbstractMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by ali on 8/27/14.
 */
public class PreparedStatementCache {
//    private final static LoadingCache<Statement, PreparedStatement> preparedStatementCache = CacheBuilder.newBuilder()
//            .maximumSize(Utils.getLongMetricsProperty("prepared.statement.cache.maximum.size", 5000L))
//            //.maximumWeight(Utils.getLongMetricsProperty("prepared.statement.cache.maximum.weight", 5L*1024*1024)) //5mb
////            .weigher(new Weigher<Statement, PreparedStatement>() {
////
////                @Override
////                public int weigh(Statement key, PreparedStatement value) {
////                    return 0;
////                }
////            })
////            .removalListener(new TradeAccountRemovalListener())1024
//            .ticker(Ticker.systemTicker())
//            .build(new CacheLoader<Statement, PreparedStatement>() {
//                @Override
//                public PreparedStatement load(Statement statement) throws Exception {
//                    Session session;
//                    if(statement.getKeyspace() == null){
//                        session = DbConnectionManager.getSession();
//                    }else{
//                        session = DbConnectionManager.getSession(statement.getKeyspace());
//                    }
//                    String query = statement.toString();
//                    PreparedStatement preparedStatement = session.prepare(query.toString());
//                    return preparedStatement;
//
//                }
//            });

    private final static LoadingCache<Map.Entry<String,String>, PreparedStatement> preparedStatementCache = CacheBuilder.newBuilder()
            .maximumSize(Utils.getLongMetricsProperty("prepared.statement.cache.maximum.size", 5000L))
                    //.maximumWeight(Utils.getLongMetricsProperty("prepared.statement.cache.maximum.weight", 5L*1024*1024)) //5mb
//            .weigher(new Weigher<Statement, PreparedStatement>() {
//
//                @Override
//                public int weigh(Statement key, PreparedStatement value) {
//                    return 0;
//                }
//            })
//            .removalListener(new TradeAccountRemovalListener())1024
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<Map.Entry<String,String>, PreparedStatement>() {
                @Override
                public PreparedStatement load(Map.Entry<String,String> keyspaceQueryPair) throws Exception {
                    Session session;
                    if(keyspaceQueryPair.getKey() == null){
                        session = DbConnectionManager.getSession();
                    }else{
                        session = DbConnectionManager.getSession(keyspaceQueryPair.getKey());
                    }
                    String query = keyspaceQueryPair.getValue();
                    PreparedStatement preparedStatement = session.prepare(query);
                    return preparedStatement;

                }
            });
    public static PreparedStatement get(Statement statement) throws ExecutionException {
        //return preparedStatementCache.get(statement);
        return get(statement.getKeyspace(),statement.toString());
    }
    public static PreparedStatement get(String keyspace, String query) throws ExecutionException {
        return preparedStatementCache.get(new AbstractMap.SimpleEntry<String, String>(keyspace,query));
    }

}
