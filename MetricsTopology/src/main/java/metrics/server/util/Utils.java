package metrics.server.util;

import com.datastax.driver.mapping.MappingSession;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import metrics.server.configuration.Configuration;
import metrics.server.database.DbConnectionManager;
import metrics.server.util.singleton.GrokDictionarySingleton;
import metrics.server.util.singleton.ObjectMapperSingleton;
import metrics.server.util.singleton.TypeConverterManagerBeanSingleton;
import metrics.server.util.type.converter.ExtendedTypeConverterManagerBean;
import org.aicer.grok.dictionary.GrokDictionary;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 7/6/14.
 */
public final class Utils {
    //public final static Mapper mapper = MapperSingleton.getInstance();
    public final static ObjectMapper objectMapper = ObjectMapperSingleton.getInstance();
    public final static GrokDictionary grokDictionary = GrokDictionarySingleton.getInstance();
    public final static ExtendedTypeConverterManagerBean typeConverterManagerBean = TypeConverterManagerBeanSingleton.getInstance();
    private final static Map<String, Properties> propertiesList = Maps.newHashMap();
    public static Configuration configuration;
    public static Properties MetricsProperties = null;//new Properties("MetricsProperties");
    public static MappingSession mappingSession = null;
    public static String KeySpace = "analytics";


    static {
        //propertiesList.put(MetricsProperties.getTableName(),MetricsProperties);
    }


    public static boolean isSetupMode() {
        if (configuration == null) {
            //return; TODO throw an error
            return false;
        }

        if (configuration.isSetup()) {
            return false;
        }
        if (DbConnectionManager.getSession() == null) {
            return true;
        }
        return false;
    }

    private static Properties getMetricsPropertiesInstance() {
        return new Properties(Utils.KeySpace,"Metrics_properties");
    }

    public static Properties getProperties(String propertiesName) {
        checkNotNull("properties name cannot be null");
        return propertiesList.get(propertiesName);
    }

    public static void addProperties(Properties properties) {
        checkNotNull("properties name cannot be null");
        addProperties(properties.getTableName(), properties);
    }

    public static void addProperties(String propertiesName, Properties properties) {
        checkNotNull("properties name cannot be null");
        checkNotNull(properties, "Properties cannot be null");
        propertiesList.put(propertiesName, properties);
    }


    public static void deleteProperty(String name) {
        if (MetricsProperties == null) {
            if (isSetupMode()) {
                return;
            }
        }
        deleteProperty(MetricsProperties, name);
    }

    public static void deleteProperty(String propertiesName, String name) {
        deleteProperty(getProperties(propertiesName), name);
    }

    public static void deleteProperty(Properties properties, String name) {
        checkNotNull(properties, "Properties cannot be null");
        checkNotNull(name, "property name cannot be null");
        properties.remove(name);
    }

    public static String getMetricsProperty(String name) {
        if (MetricsProperties == null) {
            if (isSetupMode()) {
                return null;
            }
            MetricsProperties = getMetricsPropertiesInstance();
        }
        return getProperty(MetricsProperties, name);
    }

    public static String getProperty(String propertiesName, String name) {
        return getProperty(getProperties(propertiesName), name);
    }

    public static String getProperty(Properties properties, String name) {
        checkNotNull(properties, "Properties cannot be null");
        checkNotNull(name, "property name cannot be null");
        return properties.get(name);
    }

    public static String getMetricsProperty(String name, String defaultValue) {
        if (MetricsProperties == null) {
            if (isSetupMode()) {
                return null;
            }
            MetricsProperties = getMetricsPropertiesInstance();
        }
        return getProperty(MetricsProperties, name, defaultValue);
    }

    public static String getProperty(String propertiesName, String name, String defaultValue) {
        return getProperty(getProperties(propertiesName), name, defaultValue);
    }

    public static String getProperty(Properties properties, String name, String defaultValue) {
        checkNotNull(properties, "Properties cannot be null");
        checkNotNull(name, "property name cannot be null");
        String value = properties.get(name);

        return value != null ? value : defaultValue;
    }

    public static int getIntMetricsProperty(String name, int defaultValue) {
        String value = getMetricsProperty(name);
        if (value != null) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException nfe) {

            }
        }
        return defaultValue;
    }

    public static int getIntProperty(String propertiesName, String name, int defaultValue) {
        return getIntProperty(getProperties(propertiesName), name, defaultValue);
    }

    public static int getIntProperty(Properties properties, String name, int defaultValue) {
        checkNotNull(properties, "Properties cannot be null");
        checkNotNull(name, "property name cannot be null");
        String value = properties.get(name);

        if (value != null) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException nfe) {

            }
        }
        return defaultValue;
    }


    public static long getLongMetricsProperty(String name, long defaultValue) {
        String value = getMetricsProperty(name);
        if (value != null) {
            try {
                return Long.parseLong(value);
            } catch (NumberFormatException nfe) {

            }
        }
        return defaultValue;
    }

    public static long getIntProperty(String propertiesName, String name, long defaultValue) {
        return getIntProperty(getProperties(propertiesName), name, defaultValue);
    }

    public static long getIntProperty(Properties properties, String name, long defaultValue) {
        checkNotNull(properties, "Properties cannot be null");
        checkNotNull(name, "property name cannot be null");
        String value = properties.get(name);

        if (value != null) {
            try {
                return Long.parseLong(value);
            } catch (NumberFormatException nfe) {

            }
        }
        return defaultValue;
    }

    public static boolean getBooleanMetricsProperty(String name, boolean defaultValue) {
        String value = getMetricsProperty(name);
        if (value != null) {
            try {
                return Boolean.valueOf(value);
            } catch (NumberFormatException nfe) {

            }
        }
        return defaultValue;
    }

    public static boolean getBooleanProperty(String propertiesName, String name, boolean defaultValue) {
        return getBooleanProperty(getProperties(propertiesName), name, defaultValue);
    }

    public static boolean getBooleanProperty(Properties properties, String name, boolean defaultValue) {
        checkNotNull(properties, "Properties cannot be null");
        checkNotNull(name, "property name cannot be null");
        String value = properties.get(name);

        if (value != null) {
            try {
                return Boolean.valueOf(value);
            } catch (NumberFormatException nfe) {

            }
        }
        return defaultValue;
    }


}
