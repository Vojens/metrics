package metrics.server.util.helper;

import com.datastax.driver.core.DataType;
import com.datastax.driver.mapping.EntityTypeMetadata;
import com.datastax.driver.mapping.annotation.TableProperties;
import com.datastax.driver.mapping.annotation.TableProperty;
import com.google.common.collect.Maps;

import javax.persistence.Column;
import javax.persistence.Index;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ali on 7/16/14.
 */
public class Cql3Type {
    private static final Map<Class<?>, EntityTypeMetadata> entityData = new HashMap<Class<?>, EntityTypeMetadata>();
    private static Map<DataType.Name, Class<?>> dataTypeToJavaType = Maps.newHashMap();
    static {

        dataTypeToJavaType.put(DataType.Name.BLOB, DataType.Name.BLOB.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.BOOLEAN, DataType.Name.BOOLEAN.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.TEXT, DataType.Name.TEXT.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.TIMESTAMP, DataType.Name.TIMESTAMP.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.UUID, DataType.Name.UUID.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.INT, DataType.Name.INT.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.DOUBLE, DataType.Name.DOUBLE.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.FLOAT, DataType.Name.FLOAT.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.BIGINT, DataType.Name.BIGINT.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.DECIMAL, DataType.Name.DECIMAL.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.VARINT, DataType.Name.VARINT.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.MAP, DataType.Name.MAP.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.LIST, DataType.Name.LIST.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.SET, DataType.Name.SET.asJavaClass());
        dataTypeToJavaType.put(DataType.Name.BOOLEAN, boolean.class);
        dataTypeToJavaType.put(DataType.Name.INT, int.class);
        dataTypeToJavaType.put(DataType.Name.BIGINT, long.class);
        dataTypeToJavaType.put(DataType.Name.DOUBLE, double.class);
        dataTypeToJavaType.put(DataType.Name.FLOAT, float.class);
        dataTypeToJavaType.put(DataType.Name.VARCHAR, Enum.class);
    }
    //private static Map<Class<?>, DataType.Name> javaTypeToDataType = new HashMap<Class<?>, DataType.Name>();


//    private static Map<String, DataType> validatorToDataType = new CaseInsensitiveMap();
//
//    static {
//        CQL3Type cql3Type = AsciiType.instance.asCQL3Type();
//        validatorToDataType.put("org.apache.cassandra.db.marshal.UTF8Type", new DataType(DataType.Name.ASCII, TypeCodecs));
//    }


    /**
     * to override default java to datastax type mapping
     *
     * @param mapping
     */
    public static void setJavaTypeMapping(Map<DataType.Name, Class<?>> mapping) {
        dataTypeToJavaType = mapping;
    }

    /**
     * Override individual entry for java type to datastax type
     *
     * @param clazz the class of a java data type
     * @param type  the datastax DataType.Name
     */
    public static void overrideDataTypeMapping(DataType.Name type, Class<?> clazz) {
        dataTypeToJavaType.put(type, clazz);
    }

    /**
     * Remove entity metadata from the cache.
     */
    public static <T> void remove(Class<T> clazz) {
        entityData.remove(clazz);
    }


    /**
     * Returns List<FieldData> - all the fields which can be persisted for the given Entity type
     * the field to be persisted must have getter/setter and not be annotated as @Transient
     */
//    public static <T> EntityTypeMetadata getEntityMetadata(Class<T> clazz) {
//        EntityTypeMetadata edata = entityData.get(clazz);
//        if (edata == null) {
//            edata = parseEntityClass(clazz);
//            entityData.put(clazz, edata);
//        }
//        return edata;
//    }

//    /** use reflection to iterate entity properties and collect fields to be persisted */
//    private static <T> EntityTypeMetadata parseEntityClass(Class<T> clazz) {
//        EntityTypeMetadata result = parseEntityLevelMetadata(clazz);
//        parsePropertyLevelMetadata(result.getEntityClass(), result, null, false);
//        return result;
//    }

    /**
     * Parses class level annotations and initializes EntityMetadata object for given entity class
     *
     * @param clazz
     * @return EntityMetadata
     */
    private static <T> EntityTypeMetadata parseEntityLevelMetadata(Class<T> clazz) {
        EntityTypeMetadata result = null;
        Annotation annotation = clazz.getAnnotation(Table.class);
        if (annotation instanceof Table) {
            Table tableAntn = (Table) annotation;
            String tableName = tableAntn.name();
            if (tableName != null && tableName.length() > 0) {
                result = new EntityTypeMetadata(clazz, tableName);
            } else {
                result = new EntityTypeMetadata(clazz);
            }

            Index[] indexes = tableAntn.indexes();
            if (indexes != null && indexes.length > 0) {
                for (Index index : indexes) {
                    result.addindex(index.name(), index.columnList());
                }
            }
        } else {
            result = new EntityTypeMetadata(clazz);
        }

        // parse properties
        annotation = clazz.getAnnotation(TableProperties.class);
        if (annotation instanceof TableProperties) {
            TableProperty[] props = ((TableProperties) annotation).values();
            for (TableProperty prop : props) {
                result.addProperty(prop.value());
            }
        }
        return result;
    }

//    private static EntityTypeMetadata parsePropertyLevelMetadata(Class<?> clazz, EntityTypeMetadata result, PrimaryKeyMetadata pkmeta, boolean isPartitionKey) {
//        Field[] fields = clazz.getDeclaredFields();
//        Method[] methods = clazz.getDeclaredMethods();
//
//        for (Field f: fields) {
//            boolean isOwnField = false;
//            PrimaryKeyMetadata pkm = null;
//            // for embedded key go recursive
//            if (f.getAnnotation(EmbeddedId.class) != null || f.getAnnotation(Id.class) != null) {
//                isOwnField = true;
//                pkm = new PrimaryKeyMetadata();
//                pkm.setPartition(isPartitionKey);
//                if (isPartitionKey) {
//                    pkmeta.setPartitionKey(pkm);
//                } else {
//                    result.setPrimaryKeyMetadata(pkm);
//                }
//                parsePropertyLevelMetadata(f.getType(), result,  pkm, true);
//            }
//
//            if ((f.getAnnotation(Transient.class) == null && javaTypeToDataType.get(f.getType()) != null) || isOwnField || f.getType().isEnum()){
//                Method getter = null;
//                Method setter = null;
//                for (Method m: methods) {
//
//                    // before add a field we need to make sure both getter and setter are defined
//                    if (isGetterFor(m, f.getName())) {
//                        getter = m;
//                    } else if (isSetterFor(m, f.getName())) {
//                        setter = m;
//                    }
//                    if (setter!=null && getter != null) {
//                        String columnName = getColumnName(f);
//                        DataType.Name dataType = getColumnDataType(f);
//                        EntityFieldMetaData fd = new EntityFieldMetaData(f, dataType, getter, setter, columnName);
//
//                        if (pkmeta != null && !isOwnField) {
//                            fd.setPartition(pkmeta.isPartition());
//                            fd.setPrimary(true);
//                            pkmeta.addField(fd);
//                        } else if (isOwnField) {
//                            pkm.setOwnField(fd);
//                        }
//
//                        if (f.getAnnotation(EmbeddedId.class) != null) {
//                            break;
//                        }
//
//                        if (f.getAnnotation(Version.class) != null) {
//                            result.setVersionField(fd);
//                        }
//
//                        setCollections(f, fd);
//                        result.addField(fd);
//                        break; // exit inner loop on filed's methods and go to the next field
//                    }
//                }
//            }
//        }
//        return result;
//    }

//    private static void setCollections(Field f, EntityFieldMetaData fd) {
//        if (isList(f.getType())) {
//            fd.setGenericDef(genericsOfList(f));
//        } else if(isSet(f.getType())) {
//            fd.setGenericDef(genericsOfSet(f));
//        } else if(isMap(f.getType())) {
//            fd.setGenericDef(genericsOfMap(f));
//        }
//
//        Annotation annotation = f.getAnnotation(CollectionType.class);
//        if(annotation instanceof CollectionType){
//            fd.setCollectionType(((CollectionType) annotation).value());
//        }
//    }

    /**
     * by default the field name is the column name.
     *
     * @Column annotation will override defaults
     */
    private static String getColumnName(Field f) {
        String columnName = null;
        Annotation columnA = f.getAnnotation(Column.class);
        if (columnA instanceof Column) {
            columnName = ((Column) columnA).name();
        }
        if (columnName == null || columnName.length() < 1) {
            columnName = f.getName();
        }
        return columnName;
    }

//    /**
//     * by default data type retrieved from javaTypeToDataType.
//     * @Column columnDefinition may override datatype.
//     */
//    private static DataType.Name getColumnDataType(Field f) {
//        Class<?> t = f.getType();
//        DataType.Name dataType = javaTypeToDataType.get(t);
//
//        if (t.isEnum()) { // enum is a special type.
//            dataType = javaTypeToDataType.get(Enum.class);
//        }
//
//        Annotation columnA = f.getAnnotation(Column.class);
//        if (columnA instanceof Column) {
//            String typedef = ((Column) columnA).columnDefinition();
//            if (typedef != null && typedef.length()>0) {
//                DataType.Name dt = DataType.Name.valueOf(typedef.toUpperCase());
//                if (dt != null) {
//                    dataType = dt;
//                }
//            }
//        }
//        return dataType;
//    }

    public static Class<?> getJavaDataType(String dataType) {
        return getJavaDataType(DataType.Name.valueOf(dataType));
    }

    public static Class<?> getJavaDataType(DataType.Name dataType) {
        Class<?> aClass = dataTypeToJavaType.get(dataType);
        //TODO: implement the rest
        return aClass;
    }

//    private static String genericsOfList(Field f) {
//        Type[] fieldGenerics = getGenericTypes(f);
//        if (fieldGenerics != null) {
//            return String.format("list<%s>", javaTypeToDataType.get(fieldGenerics[0]));
//        } else {
//            return "list<text>";
//        }
//    }
//
//    private static String genericsOfSet(Field f) {
//        Type[] fieldGenerics = getGenericTypes(f);
//        if (fieldGenerics != null) {
//            return String.format("set<%s>", javaTypeToDataType.get(fieldGenerics[0]));
//        } else {
//            return "set<text>";
//        }
//    }
//
//    private static String genericsOfMap(Field f) {
//        Type[] fieldGenerics = getGenericTypes(f);
//        if (fieldGenerics != null) {
//            return String.format("map<%s, %s>", javaTypeToDataType.get(fieldGenerics[0]), javaTypeToDataType.get(fieldGenerics[1]));
//        } else {
//            return "map<text, text>";
//        }
//    }

    private static Type[] getGenericTypes(Field f) {
        Type genType = f.getGenericType();
        if (genType instanceof ParameterizedType) {
            ParameterizedType aType = (ParameterizedType) genType;
            Type[] fieldGenerics = aType.getActualTypeArguments();
            return fieldGenerics;
        }
        return null;
    }

    private static <T> boolean isMap(Class<T> clazz) {
        return clazz.equals(Map.class);
    }

    private static <T> boolean isList(Class<T> clazz) {
        return clazz.equals(List.class);
    }

    private static <T> boolean isSet(Class<T> clazz) {
        return clazz.equals(Set.class);
    }

    /**
     * check if the method is getter method for the property
     */
    private static boolean isGetterFor(Method method, String property) {
        String name = method.getName().toLowerCase();
        if (!(name.startsWith("get" + property.toLowerCase()) || name.startsWith("is" + property.toLowerCase())))
            return false;
        if (method.getParameterTypes().length != 0) return false;
        if (void.class.equals(method.getReturnType())) return false;
        return true;
    }

    /**
     * check if the method is setter method for the property
     */
    private static boolean isSetterFor(Method method, String property) {
        if (!method.getName().toLowerCase().startsWith("set" + property.toLowerCase())) return false;
        if (method.getParameterTypes().length != 1) return false;
//		 if(!void.class.equals(method.getReturnType())) return false;
        return true;
    }

//    public static String mappingToString() {
//        StringBuilder b = new StringBuilder();
//        for (Class<?> c: javaTypeToDataType.keySet()) {
//            b.append(c.getName());
//            b.append("|");
//            b.append(javaTypeToDataType.get(c));
//            b.append("\n");
//        }
//        return b.toString();
//    }
}
