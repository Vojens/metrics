package metrics.server.util.helper;

import org.joda.time.DateTime;
import org.joda.time.Period;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 8/13/14.
 */
public class DateTimeHelper {
    public static org.joda.time.DateTime getRoundDateTime(final org.joda.time.DateTime dt, final Period p, RoundType rt) {
        checkNotNull(rt, "RountType cannot be null");
        switch (rt) {
            case FLOOR:
                return getDateTimeFloor(dt, p);
        }
        return null;
    }

    private static org.joda.time.DateTime getDateTimeFloor(final org.joda.time.DateTime dt, final Period p) {
        checkNotNull(dt, "DateTime cannot be null");
        checkNotNull(p, "Period cannot be null");
        Period tempPeriod = null;
        if (p.getYears() != 0) {
            tempPeriod = p.minusYears(p.getYears());
            return getDateTimeFloor(dt.yearOfEra().roundFloorCopy().minusYears(dt.getYearOfEra() % p.getYears()), tempPeriod);
        } else if (p.getMonths() != 0) {
            tempPeriod = p.minusMonths(p.getMonths());
            return getDateTimeFloor(dt.monthOfYear().roundFloorCopy().minusMonths((dt.getMonthOfYear() - 1) % p.getMonths()), tempPeriod);
        } else if (p.getWeeks() != 0) {
            tempPeriod = p.minusWeeks(p.getWeeks());
            return getDateTimeFloor(dt.weekOfWeekyear().roundFloorCopy().minusWeeks((dt.getWeekOfWeekyear() - 1) % p.getWeeks()), tempPeriod);
        } else if (p.getDays() != 0) {
            tempPeriod = p.minusDays(p.getDays());
            return getDateTimeFloor(dt.dayOfMonth().roundFloorCopy().minusDays((dt.getDayOfMonth() - 1) % p.getDays()), tempPeriod);
        } else if (p.getHours() != 0) {
            tempPeriod = p.minusHours(p.getHours());
            return getDateTimeFloor(dt.hourOfDay().roundFloorCopy().minusHours(dt.getHourOfDay() % p.getHours()), tempPeriod);
        } else if (p.getMinutes() != 0) {
            tempPeriod = p.minusMinutes(p.getMinutes());
            return getDateTimeFloor(dt.minuteOfHour().roundFloorCopy().minusMinutes(dt.getMinuteOfDay() % p.getMinutes()), tempPeriod);
        } else if (p.getSeconds() != 0) {
            tempPeriod = p.minusSeconds(p.getSeconds());
            return getDateTimeFloor(dt.secondOfMinute().roundFloorCopy().minusSeconds(dt.getSecondOfMinute() % p.getSeconds()), tempPeriod);
        } else if (p.getMillis() != 0) {
            tempPeriod = p.minusMillis(p.getMillis());
            return getDateTimeFloor(dt.millisOfSecond().roundFloorCopy().minusSeconds(dt.getMillisOfSecond() % p.getMillis()), tempPeriod);
        }
        return dt;
    }

    private static long getRandomTime() {
        return getRandomTime(0, System.currentTimeMillis());
    }

    private static long getRandomTime(long startTime, long endTime) {
        long diff = endTime - startTime + 1;
        return startTime + (long) (Math.random() * diff);
    }

    private static DateTime getGeneratedRandomDateTime() {
        DateTime randomDate = new DateTime(getRandomTime());
        return randomDate;
    }

    public static DateTime getRandomDateTime() {
//        Random random = new Random();
//
//        DateTime startTime = new DateTime(random.nextLong()).withMillisOfSecond(0);
//
//        Minutes minimumPeriod = Minutes.TWO;
//        int minimumPeriodInSeconds = minimumPeriod.toStandardSeconds().getSeconds();
//        int maximumPeriodInSeconds = Hours.ONE.toStandardSeconds().getSeconds();
//
//        Seconds randomPeriod = Seconds.seconds(random.nextInt(maximumPeriodInSeconds - minimumPeriodInSeconds));
//        DateTime endTime = startTime.plus(minimumPeriod).plus(randomPeriod);
//        return endTime;
        return getGeneratedRandomDateTime();
    }


    public enum RoundType {
        FLOOR,
        CELLING,
    }
}
