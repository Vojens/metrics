package metrics.server.util.helper;

import com.google.common.base.Optional;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkState;

/**
 * Created by ali on 8/19/14.
 */
public class AnnotationHelper {

    public static Optional<Annotation> getAnnotationOptional(List<Annotation> annotationList, final Class<? extends Annotation> classType) {
        Optional<Annotation> annotationOptional = Iterables.tryFind(annotationList, new com.google.common.base.Predicate<Annotation>() {
            @Override
            public boolean apply(@Nullable Annotation input) {
                return classType.isInstance(input);
            }
        });
        return annotationOptional;
    }

    public static Optional<Annotation> getClassAnnotationOptional(final Class<?> classType) {

        List<Annotation> annotationList = Lists.newArrayList(classType.getAnnotations());
        Optional<Annotation> annotationOptional = Iterables.tryFind(annotationList, new com.google.common.base.Predicate<Annotation>() {
            @Override
            public boolean apply(@Nullable Annotation input) {
                return classType.isInstance(input);
            }
        });
        return annotationOptional;
    }

    public static Optional<Annotation> getAnnotationOptional(final Class<?> classType, Annotation[] annotations) {

        List<Annotation> annotationList = Lists.newArrayList(annotations);
        Optional<Annotation> annotationOptional = Iterables.tryFind(annotationList, new com.google.common.base.Predicate<Annotation>() {
            @Override
            public boolean apply(@Nullable Annotation input) {
                return classType.isInstance(input);
            }
        });
        return annotationOptional;
    }


    public static Optional<Field> getDeclaredFieldsAnnotationOptional(final Class<?> classType, final Class<? extends Annotation> annotationType) {

        List<Field> declaredFieldsList = Lists.newArrayList(classType.getDeclaredFields());

        Optional<Field> annotationOptional = Iterables.tryFind(declaredFieldsList, new com.google.common.base.Predicate<Field>() {
            @Override
            public boolean apply(@Nullable Field input) {
                Optional<Annotation> fieldAnnotationOptional = getAnnotationOptional(annotationType, input.getDeclaredAnnotations());
                Set<Annotation> fieldAnnotationSet = fieldAnnotationOptional.asSet();
                if (fieldAnnotationSet.isEmpty()) {
                    return false;
                }
                checkState(fieldAnnotationSet.size() == 1, "The field " + input.getName() + " has more than 1 annotation of type " + annotationType.getName());   //TODO reevaluate.. sometimes you want to have the same field attached to 2 different params.
                return true;
            }
        });
        return annotationOptional;
    }

}
