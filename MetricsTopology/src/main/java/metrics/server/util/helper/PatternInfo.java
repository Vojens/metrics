package metrics.server.util.helper;

import com.google.auto.value.AutoValue;
import metrics.server.util.type.converter.ConverterInfo;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by ali on 7/17/14.
 */
@AutoValue
public abstract class PatternInfo {
    public static PatternInfo create(final Pattern pattern, final Map<String, Integer> namedGroupMap, Map.Entry<Class<?>, Map<String, ConverterInfo>> beanClassWithDetails) {

        return new AutoValue_PatternInfo(pattern, namedGroupMap, beanClassWithDetails);
    }

    public abstract Pattern getPattern();

    public abstract Map<String, Integer> getNamedGroupMap();

    public abstract Map.Entry<Class<?>, Map<String, ConverterInfo>> getBeanClassWithDetails();

//    protected static Class<?> createBeanClass(
//    /* fully qualified class name */
//    final String className,
//    /* bean properties, name -> type */
//    final Map<String, Class<?>> properties){
//
//        final BeanGenerator beanGenerator = new BeanGenerator();
//
//    /* use our own hard coded class name instead of a real naming policy */
//        beanGenerator.setNamingPolicy(new NamingPolicy(){
//            @Override public String getClassName(final String prefix,
//                                                 final String source, final Object key, final Predicate names){
//                return className;
//            }});
//        BeanGenerator.addProperties(beanGenerator, properties);
//        return (Class<?>) beanGenerator.createClass();
//    }
}
