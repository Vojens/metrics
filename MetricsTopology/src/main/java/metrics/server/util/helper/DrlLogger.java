package metrics.server.util.helper;

import com.sun.javafx.binding.StringFormatter;
import org.apache.log4j.Logger;
import org.kie.api.runtime.rule.RuleContext;

/**
 * Created by ali on 8/6/14.
 */

/**
 * Example of Usage
 * import function metrics.server.util.helper.DrlLogger
 * <p/>
 * rule "myrule"
 * when
 * $fact: Fact()
 * then
 * info(drools, "Fact:{}", $fact);
 * end
 */
public class DrlLogger {

    public DrlLogger() {

    }

    protected static Logger getLogger(final RuleContext drools) {
        return Logger.getLogger(drools.getRule().getPackageName() + "." + drools.getRule().getName());
    }

    public static void info(final RuleContext drools, final String message) {
        info(drools, message, null);
    }

    public static void info(final RuleContext drools, final String message, final Object... parameters) {
        final Logger logger = getLogger(drools);
        logger.info(StringFormatter.format(message, parameters));
    }

    public static void debug(final RuleContext drools, final String message) {
        debug(drools, message, null);
    }

    public static void debug(final RuleContext drools, final String message, final Object... parameters) {
        final Logger logger = getLogger(drools);
        logger.debug(StringFormatter.format(message, parameters));
    }

    public static void error(final RuleContext drools, final String message) {
        error(drools, message, null);
    }

    public static void error(final RuleContext drools, final String message, final Object... parameters) {
        final Logger logger = getLogger(drools);
        logger.error(StringFormatter.format(message, parameters));
    }
}
