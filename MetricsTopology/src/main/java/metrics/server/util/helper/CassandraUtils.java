package metrics.server.util.helper;

import com.datastax.driver.core.BoundStatement;
import metrics.server.util.singleton.TypeConverterManagerBeanSingleton;
import org.apache.commons.collections.list.TreeList;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Created by ali on 8/24/14.
 */
public class CassandraUtils {

    public static BoundStatement bind(final BoundStatement bs, int iter, Object valueObject, Class<?> javaClassType){
        if (javaClassType == String.class) {
            bs.bind().setString(iter, valueObject.toString());
        } else if (javaClassType == Long.class) {
            Long value = TypeConverterManagerBeanSingleton.getInstance().<Long>convertType(valueObject, Long.class);
            bs.bind().setLong(iter, value);
        } else if (javaClassType == ByteBuffer.class) {
            ByteBuffer value = TypeConverterManagerBeanSingleton.getInstance().<ByteBuffer>convertType(valueObject, ByteBuffer.class);
            bs.bind().setBytes(iter, value);
        } else if (javaClassType == Boolean.class) {
            Boolean value = TypeConverterManagerBeanSingleton.getInstance().<Boolean>convertType(valueObject, Boolean.class);
            bs.bind().setBool(iter, value);
        } else if (javaClassType == BigDecimal.class) {
            BigDecimal value = TypeConverterManagerBeanSingleton.getInstance().<BigDecimal>convertType(valueObject, BigDecimal.class);
            bs.bind().setDecimal(iter, value);
        } else if (javaClassType == Double.class) {
            Double value = TypeConverterManagerBeanSingleton.getInstance().<Double>convertType(valueObject, Double.class);
            bs.bind().setDouble(iter, value);
        } else if (javaClassType == Float.class) {
            Float value = TypeConverterManagerBeanSingleton.getInstance().<Float>convertType(valueObject, Float.class);
            bs.bind().setFloat(iter, value);
        } else if (javaClassType == InetAddress.class) {
            InetAddress value = TypeConverterManagerBeanSingleton.getInstance().<InetAddress>convertType(valueObject, InetAddress.class);
            bs.bind().setInet(iter, value);
        } else if (javaClassType == Integer.class) {
            Integer value = TypeConverterManagerBeanSingleton.getInstance().<Integer>convertType(valueObject, Integer.class);
            bs.bind().setInt(iter, value);
        } else if (javaClassType == Date.class) {
            org.joda.time.DateTime jodaDateTime = null;
            jodaDateTime = TypeConverterManagerBeanSingleton.getInstance().<org.joda.time.DateTime>convertType(valueObject, org.joda.time.DateTime.class);
            Date value = jodaDateTime.toDate();
            bs.bind().setDate(iter, value);
        } else if (javaClassType == UUID.class) {
            UUID value = TypeConverterManagerBeanSingleton.getInstance().<UUID>convertType(valueObject, UUID.class);
            bs.bind().setUUID(iter, value);
        } else if (javaClassType == BigInteger.class) {
            BigInteger value = TypeConverterManagerBeanSingleton.getInstance().<BigInteger>convertType(valueObject, BigInteger.class);
            bs.bind().setVarint(iter, value);
        } else if (javaClassType == List.class || javaClassType == TreeList.class) {
            List value = TypeConverterManagerBeanSingleton.getInstance().<List>convertType(valueObject, List.class);
            bs.bind().setList(iter, value);
        } else if (javaClassType == Set.class || javaClassType == TreeSet.class) {
            Set value = TypeConverterManagerBeanSingleton.getInstance().<Set>convertType(valueObject, Set.class);
            bs.bind().setSet(iter, value);
        } else if (javaClassType == Map.class || javaClassType == TreeMap.class) {
            Map value = TypeConverterManagerBeanSingleton.getInstance().<Map>convertType(valueObject, Map.class);
            bs.bind().setMap(iter, value);
        }
        return bs;
    }
    public static BoundStatement bind(final BoundStatement bs, List<Map.Entry<Object, Class<?>>> bindingList) {

        int i = 0;
        BoundStatement boundStatement = bs;
        for (Map.Entry<Object, Class<?>> objectClassSimpleEntry : bindingList) {
            boundStatement = bind(bs,i++,objectClassSimpleEntry.getKey(),objectClassSimpleEntry.getValue());
        }
        return boundStatement;
    }
}
