package metrics.server.util.helper;

import com.google.auto.value.AutoValue;

/**
 * Created by ali on 7/17/14.
 */
//@AutoValue
public class ColumnDataType {
    public static ColumnDataType create(final String keyspace, final String table, final String column) {

        return new ColumnDataType(keyspace, table, column);
    }

//    public abstract String keyspace();
//
//    public abstract String table();
//
//    public abstract String column();


    private final String keyspace;
    private final String table;
    private final String column;

    protected ColumnDataType(
            String keyspace,
            String table,
            String column) {
        if (keyspace == null) {
            throw new NullPointerException("Null keyspace");
        }
        this.keyspace = keyspace;
//        if (table == null) {
//            throw new NullPointerException("Null table");
//        }
        this.table = table;
//        if (column == null) {
//            throw new NullPointerException("Null column");
//        }
        this.column = column;
    }



    public String keyspace() {
        return keyspace;
    }


    public String table() {
        return table;
    }


    public String column() {
        return column;
    }

    @Override
    public String toString() {
        return "ColumnDataType{"
                + "keyspace=" + keyspace
                + ", table=" + table
                + ", column=" + column
                + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ColumnDataType)) return false;

        ColumnDataType that = (ColumnDataType) o;

        if (column != null ? !column.equals(that.column) : that.column != null) return false;
        if (!keyspace.equals(that.keyspace)) return false;
        if (table != null ? !table.equals(that.table) : that.table != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = keyspace.trim().toLowerCase().hashCode();
        result = 31 * result + (table != null ? table.trim().toLowerCase().hashCode() : 0);
        result = 31 * result + (column != null ? column.trim().toLowerCase().hashCode() : 0);
        return result;
    }
}
