package metrics.server.util.helper;

import com.google.common.collect.Lists;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.LinkedList;

/**
 * Created by ali on 8/24/14.
 */
//@AutoValue
//public abstract class RowHolderInfo implements Serializable {
//    public static RowHolderInfo create (final String keyspace, final String table, final LinkedList<AbstractMap.SimpleEntry<String, String>> columnValueList){
//
//        return new AutoValue_RowHolderInfo(keyspace, table, columnValueList);
//    }
//
//    public abstract String keyspace();
//    public abstract String table();
//    public abstract LinkedList<AbstractMap.SimpleEntry<String,String>> columnValueList();
//}

public class RowHolderInfo implements Serializable, metrics.server.lang.Cloneable<RowHolderInfo>{
    private static final long serialVersionUID = -599399423927370911L;
    private final String keyspace;
    private final String table;
    private final LinkedList<AbstractMap.SimpleEntry<String, String>> columnValueList;

    protected RowHolderInfo(RowHolderInfo rowHolderInfo){
        this.keyspace = rowHolderInfo.keyspace();
        this.table = rowHolderInfo.table();
        LinkedList<AbstractMap.SimpleEntry<String, String>> tempList = Lists.newLinkedList();
        for (AbstractMap.SimpleEntry<String, String> stringStringSimpleEntry : rowHolderInfo.columnValueList()) {
            tempList.add(new AbstractMap.SimpleEntry<String, String>(stringStringSimpleEntry.getKey(),stringStringSimpleEntry.getValue()));
        }
        this.columnValueList = tempList;
    }


    public RowHolderInfo(
            String keyspace,
            String table,
            LinkedList<AbstractMap.SimpleEntry<String, String>> columnValueList) {
        if (keyspace == null) {
            throw new NullPointerException("Null keyspace");

        }
        this.keyspace = keyspace;
        if (table == null) {
            throw new NullPointerException("Null table");
        }
        this.table = table;
        if (columnValueList == null) {
            throw new NullPointerException("Null columnValueList");
        }
        this.columnValueList = columnValueList;
    }

    public String keyspace() {
        return keyspace;
    }

    public String table() {
        return table;
    }

    public LinkedList<AbstractMap.SimpleEntry<String, String>> columnValueList() {
        return columnValueList;
    }

    public String toString() {
        return "RowHolderInfo{"
                + "keyspace=" + keyspace
                + ", table=" + table
                + ", columnValueList=" + columnValueList
                + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof RowHolderInfo) {
            RowHolderInfo that = (RowHolderInfo) o;
            return (this.keyspace.equals(that.keyspace()))
                    && (this.table.equals(that.table()))
                    && (this.columnValueList.equals(that.columnValueList()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int h = 1;
        h *= 1000003;
        h ^= keyspace.hashCode();
        h *= 1000003;
        h ^= table.hashCode();
        h *= 1000003;
        h ^= columnValueList.hashCode();
        return h;
    }

    @Override
    public RowHolderInfo deepCopy() {

        return new RowHolderInfo(this);
    }
}