package metrics.server.util.singleton;

import metrics.server.util.type.converter.ExtendedTypeConverterManagerBean;
import metrics.server.util.type.converter.impl.ByteBufferConverter;
import metrics.server.util.type.converter.impl.DateTimeConverter;
import metrics.server.util.type.converter.impl.InetAddressConverter;
import metrics.server.util.type.converter.impl.UuidConverter;

import java.io.Serializable;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Created by ali on 7/21/14.
 */
public class TypeConverterManagerBeanSingleton implements Serializable {

    private static final long serialVersionUID = 206327043528897811L;
    private ExtendedTypeConverterManagerBean typeConverterManagerBean;

    private TypeConverterManagerBeanSingleton() {
        // private constructor
        typeConverterManagerBean = new ExtendedTypeConverterManagerBean();
        registerDefaults(typeConverterManagerBean);
    }

    protected static void registerDefaults(ExtendedTypeConverterManagerBean typeConverterManagerBean) {
        typeConverterManagerBean.register(InetAddress.class, new InetAddressConverter());
        typeConverterManagerBean.register(org.joda.time.DateTime.class, new DateTimeConverter());
        typeConverterManagerBean.register(ByteBuffer.class, new ByteBufferConverter());
        typeConverterManagerBean.register(UUID.class, new UuidConverter());

    }

    public static ExtendedTypeConverterManagerBean getInstance() {
        return TypeConverterManagerBeanSingletonHolder.INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }

    private static class TypeConverterManagerBeanSingletonHolder {
        public static final ExtendedTypeConverterManagerBean INSTANCE = new TypeConverterManagerBeanSingleton().typeConverterManagerBean;
    }
}
