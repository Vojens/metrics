package metrics.server.util.singleton;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

/**
 * Created by ali on 6/4/14.
 */

public class ObjectMapperSingleton implements Serializable {

    private static final long serialVersionUID = -3151306742775984878L;
    private ObjectMapper objectMapper;

    private ObjectMapperSingleton() {
        // private constructor
        objectMapper = new ObjectMapper();
    }

    public static ObjectMapper getInstance() {
        return MapperSingletonHolder.INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }

    private static class MapperSingletonHolder {
        public static final ObjectMapper INSTANCE = new ObjectMapperSingleton().objectMapper;
    }
}