package metrics.server.util.singleton;

import com.datastax.driver.mapping.MappingSession;
import metrics.server.database.DbConnectionManager;
import metrics.server.util.Utils;

import java.io.Serializable;

/**
 * Created by ali on 7/14/14.
 */

public class MappingSessionSingleton implements Serializable {

    private static final long serialVersionUID = 5377450743082736895L;
    private MappingSession mappingSession;

    private MappingSessionSingleton() {
        // private constructor
        mappingSession = new MappingSession(Utils.KeySpace, DbConnectionManager.getSession());
        mappingSession.setDoNotSync(true);
    }

    public static MappingSession getInstance() {
        return MappingSessionSingletonHolder.INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }

    private static class MappingSessionSingletonHolder {
        public static final MappingSession INSTANCE = new MappingSessionSingleton().mappingSession;
    }
}