package metrics.server.util.singleton;

import metrics.server.engine.drools.DroolsEngineFactory;
import metrics.server.engine.josql.JoSqlEngineFactory;

import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

/**
 * Created by ali on 8/6/14.
 */
public class ScriptEngineManagerSingleton {
    private static final long serialVersionUID = 1L;
    private ScriptEngineManager scriptEngineManager;

    private ScriptEngineManagerSingleton() {
        // private constructor
        scriptEngineManager = new ScriptEngineManager();
        registerDefaults(scriptEngineManager);
    }

    protected static void registerDefaults(ScriptEngineManager scriptEngineManager) {
        registerScriptEngineFactory(scriptEngineManager, new DroolsEngineFactory());
        registerScriptEngineFactory(scriptEngineManager, new JoSqlEngineFactory());

        //scriptEngineManager.registerEngineName("ecj", new EcjEngineFactory());
    }

    private static void registerScriptEngineFactory(ScriptEngineManager scriptEngineManager, ScriptEngineFactory scriptEngineFactory) {
        scriptEngineManager.registerEngineName(scriptEngineFactory.getLanguageName(), scriptEngineFactory);
    }

    public static ScriptEngineManager getInstance() {
        return ScriptEngineManagerSingletonHolder.INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }

    private static class ScriptEngineManagerSingletonHolder {
        public static final ScriptEngineManager INSTANCE = new ScriptEngineManagerSingleton().scriptEngineManager;
    }
}
