package metrics.server.util;

import org.apache.log4j.Logger;

/**
 * Created by ali on 8/28/14.
 */
public final class ScriptEngineGlobal {
    public static final Utils UTILS = new Utils();
    public static final CacheUtil CACHE_UTIL = new CacheUtil();
    public static final PreparedStatementCache PREPARED_STATEMENT_CACHE = new PreparedStatementCache();
    public static final transient Logger LOGGER = Logger.getLogger(ScriptEngineGlobal.class.getName());
}
