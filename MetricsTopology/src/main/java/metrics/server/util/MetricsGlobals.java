package metrics.server.util;

import java.io.File;

/**
 * Created by ali on 7/2/14.
 */
public class MetricsGlobals {
    private static String Metrics_CONFIG_FILENAME = "conf" + File.separator + "configuration.json";

    /**
     * Returns the name of the local config file name.
     *
     * @return the name of the config file.
     */
    static String getConfigName() {
        return Metrics_CONFIG_FILENAME;
    }

    /**
     * Allows the name of the local config file name to be changed. The
     * default is "configuration.json".
     *
     * @param configName the name of the config file.
     */
    public static void setConfigName(String configName) {
        Metrics_CONFIG_FILENAME = configName;
    }

    /**
     * Returns the location of the <code>home</code> directory.
     *
     * @return the location of the home dir.
     */
    public static String getHomeDirectory() {
        return null;
    }







}
