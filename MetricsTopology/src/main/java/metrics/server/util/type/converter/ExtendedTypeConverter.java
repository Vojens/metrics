package metrics.server.util.type.converter;

import jodd.typeconverter.TypeConverter;

import java.io.IOException;
import java.util.Map;

/**
 * Created by ali on 7/22/14.
 */
public interface ExtendedTypeConverter<T> extends TypeConverter<T> {
    T convert(java.lang.Object o, Map<String, String> format) throws IOException;
}
