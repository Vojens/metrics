package metrics.server.util.type.converter.impl;

import metrics.server.util.type.converter.ExtendedTypeConverter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Locale;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 7/22/14.
 */
public class DateTimeConverter implements ExtendedTypeConverter<DateTime> {
    @Override
    public DateTime convert(Object value) {
        return ISODateTimeFormat.dateTime().parseDateTime(value.toString());
    }

    @Override
    public DateTime convert(Object value, Map<String, String> format) {
        if (value == null) {
            return null;
        }

        try {
            checkNotNull(format);
        } catch (Exception e) {
            return convert(value);
        }
        String formatterValue = format.get("formatter");
        DateTimeFormatter formatter = null;
        if (formatterValue != null) {
            if (formatterValue.trim().toLowerCase().equals("ISODateTimeFormat".toLowerCase())) {
                formatter = ISODateTimeFormat.dateTime();
            } else if (formatterValue.trim().toLowerCase().equals("DateTimeFormat".toLowerCase())) {
                String pattern = format.get("pattern");
                checkNotNull(pattern, "There exists no 'Pattern' value to be used for DateTimeConversing");

                formatter = DateTimeFormat.forPattern(pattern);
                String language = format.get("language");
                String country = format.get("country");
                String variant = format.get("variant");
                if (language != null && country != null && variant != null) {
                    formatter = formatter.withLocale(new Locale(language, country, variant));
                } else if (language != null && country != null) {
                    formatter = formatter.withLocale(new Locale(language, country));
                } else if (language != null) {
                    formatter = formatter.withLocale(new Locale(language));
                }

            }
            return formatter.parseDateTime(value.toString());

        } else {
            return convert(value);
        }
    }
}
