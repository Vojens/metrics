package metrics.server.util.type;

import jodd.util.StringTemplateParser;

import java.io.Serializable;

/**
 * Created by ali on 8/17/14.
 */
public class SerializableStringTemplateParser extends StringTemplateParser implements Serializable {
    private static final long serialVersionUID = -8013232616202705757L;

    public SerializableStringTemplateParser() {
        super();
    }
}
