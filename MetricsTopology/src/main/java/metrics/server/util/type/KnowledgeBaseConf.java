package metrics.server.util.type;

import com.google.auto.value.AutoValue;
import org.kie.internal.KnowledgeBase;

/**
 * Created by ali on 8/13/14.
 */
@AutoValue
public abstract class KnowledgeBaseConf {
    /**
     * Created by ali on 8/10/14.
     */
    public static KnowledgeBaseConf create(final String type, final String name, final KnowledgeBase knowledgeBase, final KnowledgeBaseConf.KnowledgeBaseType knowledgeBaseType) {
        return new AutoValue_KnowledgeBaseConf(type, name, knowledgeBase, knowledgeBaseType);
    }

    public abstract String getType();

    public abstract String getName();

    public abstract KnowledgeBase getKnowledgeBase();

    public abstract KnowledgeBaseConf.KnowledgeBaseType getKnowledgeBaseType();


    public static enum KnowledgeBaseType {
        STATEFUL,
        STATELESS,
        TYPE_MISMATCH
    }
}
