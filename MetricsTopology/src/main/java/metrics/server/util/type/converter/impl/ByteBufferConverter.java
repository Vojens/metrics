package metrics.server.util.type.converter.impl;

import metrics.server.util.helper.BytesHelper;
import metrics.server.util.type.converter.ExtendedTypeConverter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 * Created by ali on 8/17/14.
 */
public class ByteBufferConverter implements ExtendedTypeConverter<ByteBuffer> {
    private final transient Logger logger = Logger.getLogger(ByteBufferConverter.class.getName());

    @Override
    public ByteBuffer convert(Object o, Map<String, String> format) {
        return convert(o);
    }

    @Override
    public ByteBuffer convert(Object value) {

        byte[] bytes = new byte[0];
        try {
            bytes = BytesHelper.toByteArray(value);
            return ByteBuffer.wrap(bytes);
        } catch (IOException e) {
            logger.error(e, e);
        }
        return null;
    }


}
