package metrics.server.util.type;

import java.io.Serializable;

/**
 * Created by ali on 8/24/14.
 */
public class SerializedObject extends Object implements Serializable {
    private static final long serialVersionUID = 1860758045366611437L;
    Serializable serializable = null;

    public SerializedObject(Object object) {
        if (object instanceof SerializedObject) {
            serializable = (SerializedObject) object;
        } else if (object instanceof Serializable) {
            serializable = (Serializable) object;
        } else {

        }
    }
}
