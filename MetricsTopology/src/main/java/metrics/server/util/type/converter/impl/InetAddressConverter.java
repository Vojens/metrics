package metrics.server.util.type.converter.impl;

import com.google.common.net.InetAddresses;
import metrics.server.util.type.converter.ExtendedTypeConverter;
import jodd.typeconverter.TypeConversionException;

import java.net.InetAddress;
import java.util.Map;

/**
 * Created by ali on 7/21/14.
 */
public class InetAddressConverter implements ExtendedTypeConverter<InetAddress> {
    @Override
    public InetAddress convert(Object value) {
        if (value == null) {
            return null;
        }

        if (value instanceof InetAddress) {
            return (InetAddress) value;
        }

        try {
            return InetAddresses.forString(value.toString());
        } catch (Exception e) {
            throw new TypeConversionException(value, e);
        }
    }

    @Override
    public InetAddress convert(Object o, Map<String, String> format) {
        return convert(o);
    }
}
