package metrics.server.util.type.converter;

import com.google.auto.value.AutoValue;

import java.util.Map;

/**
 * Created by ali on 7/22/14.
 */
@AutoValue
public abstract class ConverterInfo {
    public static ConverterInfo create(final Class<?> classType, Map<String, String> format, boolean isConverterActivated) {
        return new AutoValue_ConverterInfo(classType, format, isConverterActivated);
    }

    public abstract Class<?> getClassType();

    public abstract Map<String, String> getFormat();

    public abstract boolean isConverterActivated();
}