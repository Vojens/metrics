package metrics.server.util.type;

import java.io.Serializable;

/**
 * Created by ali on 8/5/14.
 */
public abstract class AbstractSerialization implements Serializable {
    private static final long serialVersionUID = -7766628181176764340L;

    public AbstractSerialization() {
        super();
    }
}
