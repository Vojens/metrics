package metrics.server.util.type.converter.impl;

import metrics.server.util.type.converter.ExtendedTypeConverter;

import java.util.Map;
import java.util.UUID;

/**
 * Created by ali on 8/17/14.
 */
public class UuidConverter implements ExtendedTypeConverter<UUID> {
    @Override
    public UUID convert(Object o, Map<String, String> format) {
        String from = format.get("from");
        if (from == null) {
            return convert(o);
        }
        switch (from.toLowerCase().trim()) {
            case "string":
                return convert(o);
            case "bytes":
                return UUID.nameUUIDFromBytes((byte[]) o);
            default:
                return null;
        }
    }

    @Override
    public UUID convert(Object value) {
        return UUID.fromString(value.toString());

    }
}
