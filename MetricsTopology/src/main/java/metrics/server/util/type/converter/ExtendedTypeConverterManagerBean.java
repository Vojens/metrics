package metrics.server.util.type.converter;

import jodd.typeconverter.TypeConversionException;
import jodd.typeconverter.TypeConverter;
import jodd.typeconverter.TypeConverterManagerBean;
import jodd.typeconverter.impl.ArrayConverter;
import jodd.typeconverter.impl.CollectionConverter;
import jodd.util.ReflectUtil;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Created by ali on 7/22/14.
 */
public class ExtendedTypeConverterManagerBean extends TypeConverterManagerBean {

    public ExtendedTypeConverterManagerBean() {
        super();
    }

    /**
     * Converts an object to destination type. If type is registered, it's
     * {@link jodd.typeconverter.TypeConverter} will be used. If not, it scans of destination is
     * an array or enum, as those two cases are handled in a special way.
     * <p/>
     * If destination type is one of common types, consider using {@link jodd.typeconverter.Convert}
     * instead for somewhat faster approach (no lookup).
     */
    @SuppressWarnings({"unchecked"})
    public <T> T convertType(Object value, Class<T> destinationType, Map<String, String> format) throws IOException {
        if (destinationType == Object.class) {
            // no conversion :)
            return (T) value;
        }
        TypeConverter converter = lookup(destinationType);

        if (converter != null) {
            if (ReflectUtil.isInstanceOf(converter, ExtendedTypeConverter.class)) {
                return (T) ((ExtendedTypeConverter) converter).convert(value, format);
            } else {
                return (T) converter.convert(value);
            }

        }

        // no converter

        if (value == null) {
            return null;
        }

        // handle destination arrays
        if (destinationType.isArray()) {
            ArrayConverter<T> arrayConverter = new ArrayConverter(this, destinationType.getComponentType());

            return (T) arrayConverter.convert(value);
        }

        // handle enums
        if (destinationType.isEnum()) {
            Object[] enums = destinationType.getEnumConstants();
            String valStr = value.toString();
            for (Object e : enums) {
                if (e.toString().equals(valStr)) {
                    return (T) e;
                }
            }
        }

        // check same instances
        if (ReflectUtil.isInstanceOf(value, destinationType) == true) {
            return (T) value;
        }

        // collection
        if (ReflectUtil.isInterfaceImpl(destinationType, Collection.class)) {
            // component type is unknown because of Java's type-erasure
            CollectionConverter<T> collectionConverter =
                    new CollectionConverter(this, destinationType, Object.class);

            return (T) collectionConverter.convert(value);
        }

        // fail
        throw new TypeConversionException("Conversion failed: " + destinationType.getName());
    }

    @SuppressWarnings({"unchecked"})
    public <T> T convertType(Object value, Class<T> destinationType, Map<String, String> format, Object defaultValue) {
        try {
            return convertType(value, destinationType, format);
        } catch (Exception e) {
            return (T) defaultValue;
        }
    }


}
