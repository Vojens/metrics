package metrics.server.util;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.datastax.driver.core.querybuilder.Select;
import com.datastax.driver.mapping.EntityFieldMetaData;
import com.datastax.driver.mapping.EntityTypeMetadata;
import com.datastax.driver.mapping.EntityTypeParser;
import com.google.common.base.Ticker;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import metrics.server.database.DbConnectionManager;
import metrics.server.engine.ICustomEngine;
import metrics.server.engine.ScriptEngineConf;
import metrics.server.mapping.DataTypeBinding;
import metrics.server.mapping.RegexPatternSet;
import metrics.server.mapping.output.Cassandra;
import metrics.server.mapping.scriptengine.CalculationScriptEngineConf;
import metrics.server.mapping.scriptengine.CalculationScriptEngineLateComingConf;
import metrics.server.mapping.system.SchemaColumns;
import metrics.server.util.helper.*;
import metrics.server.util.singleton.MappingSessionSingleton;
import metrics.server.util.singleton.ScriptEngineManagerSingleton;
import metrics.server.util.type.AbstractSerialization;
import metrics.server.util.type.KnowledgeBaseConf;
import metrics.server.util.type.converter.ConverterInfo;
import com.sun.corba.se.impl.io.TypeMismatchException;
import net.sf.cglib.beans.BeanGenerator;
import net.sf.cglib.core.NamingPolicy;
import net.sf.cglib.core.Predicate;
import org.apache.log4j.Logger;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.io.ResourceType;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Created by ali on 7/15/14.
 */
public final class CacheUtil {
    private static final Logger logger = Logger.getLogger(CacheUtil.class.getName());
    private final static LoadingCache<String, RegexPatternSet> regexPatternCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("regex.pattern.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("regex.pattern.cache.maximum.size", 5000L))
            .recordStats()
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, RegexPatternSet>() {
                @Override
                public RegexPatternSet load(String key) throws Exception {
                    RegexPatternSet regexPatternSet = MappingSessionSingleton.getInstance().get(RegexPatternSet.class, key);
                    return regexPatternSet;
                }
            });
    private final static LoadingCache<String, Map<String, PatternInfo>> patternCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("pattern.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("pattern.cache.maximum.size", 5000L))
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, Map<String, PatternInfo>>() {
                @Override
                public Map<String, PatternInfo> load(String key) throws Exception {
                    RegexPatternSet regexPatternSet = getRegexPatternCache().get(key);
                    checkNotNull(regexPatternSet, "Regex Pattern is null. Could not find Regex of type (" + key + "). Check the database.");
                    Map<String, String> regexPatternMap = getRegexPatternCache().get(key).getPatterns();
                    Map<String, PatternInfo> patternInfoMap = Maps.newHashMap();
                    Iterator<Map.Entry<String, String>> iterator = regexPatternMap.entrySet().iterator();

                    while (iterator.hasNext()) {
                        Map.Entry pair = (Map.Entry) iterator.next();
                        Pattern pattern = Pattern.compile(Utils.grokDictionary.digestExpression((String) pair.getValue()));
                        Map<String, Integer> namedGroups = RegexHelper.getNamedGroups(pattern);
                        PatternInfo patternInfo = PatternInfo.create(pattern, namedGroups, getMappingClassCache().get(key));
                        patternInfoMap.put((String) pair.getKey(), patternInfo);
                    }
                    return patternInfoMap;
                }
            });
    private final static LoadingCache<ColumnDataType, DataType> columnDataTypeCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("column.data.type.cache.expire.after.write", 60L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("column.data.type.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<ColumnDataType, DataType>() {
                @Override
                public DataType load(final ColumnDataType key) throws Exception {
                    try{
                        Session session = DbConnectionManager.getSession(key.keyspace());
                        ResultSet execute = session.execute(QueryBuilder.select().column(key.column()).from(key.keyspace(), key.table()).limit(1));
    //                    Optional<ColumnDefinitions.Definition> definitionOptional = Iterables.tryFind(execute.getColumnDefinitions(), new com.google.common.base.Predicate<ColumnDefinitions.Definition>() {
    //                        @Override
    //                        public boolean apply(@Nullable ColumnDefinitions.Definition input) {
    //                            return input.getName().equals(key.column()) ? true : false;
    //                        }
    //                    });
    //                    if (definitionOptional.isPresent()) {
    //                        return definitionOptional.get().getType();
    //                    }
                            DataType type = execute.getColumnDefinitions().getType(key.column());
                            return type;

                            //else if there exists no rows in the table, use the other method

                            //String query = "Select * from " + key.keyspace() + '.' + key.table() + " limit 1;";
    //                    Statement statement = QueryBuilder.select().all().from("system", "schema_columns").where(eq("keyspace_name", QueryBuilder.bindMarker()))
    //                            .and(eq("columnfamily_name", QueryBuilder.bindMarker()))
    //                            .and(eq("column_name", QueryBuilder.bindMarker()));
    //                    PreparedStatement ps = PreparedStatementCache.get(statement);
    //                    BoundStatement bs = new BoundStatement(ps);
    //                    bs.setString(0,key.keyspace());
    //                    bs.setString(1,key.table());
    //                    bs.setString(2,key.column());
    //                    ResultSet resultSet = DbConnectionManager.getSession(key.keyspace()).execute(bs);
    //                    Row one = resultSet.one();
    //                    if(one == null){
    //                        return null;
    //                    }
    //                    //one.getString()
    //                    session.getCluster().register()


                            //return null;
                    }catch(Exception e){
                        logger.error(e,e);
                    }
                    return null;

                }
            });
    private final static LoadingCache<String, List<DataTypeBinding>> dataTypeBindingCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("data.type.binding.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("data.type.binding.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, List<DataTypeBinding>>() {
                @Override
                public List<DataTypeBinding> load(String key) throws Exception {
                    EntityTypeMetadata emeta = EntityTypeParser.getEntityMetadata(DataTypeBinding.class);
                    EntityFieldMetaData fmeta = emeta.getFieldMetadata("type");
                    Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), QueryBuilder.bindMarker()));
                    PreparedStatement preparedStatement = PreparedStatementCache.get(query);
                    BoundStatement bs = new BoundStatement(preparedStatement);
                    bs.setString(0,key);
                    List<DataTypeBinding> dtbList = MappingSessionSingleton.getInstance().getByQuery(DataTypeBinding.class, bs);
                    return dtbList;
                }
            });
    private final static LoadingCache<String, Map.Entry<Class<?>, Map<String, ConverterInfo>>> mappingClassCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("data.type.binding.cache.expire.after.write", 60L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("data.type.binding.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, Map.Entry<Class<?>, Map<String, ConverterInfo>>>() {
                @Override
                public Map.Entry<Class<?>, Map<String, ConverterInfo>> load(String key) throws Exception {

                    String fullyQualifiedPackageName = Utils.getMetricsProperty("mapping.class.package", "metrics.server.mapping.cache");
                    String fullyQualifiedClassName = "";
                    List<DataTypeBinding> dataTypeBindingList = getDataTypeBindingCache().get(key);

                    try {
                        checkState(!dataTypeBindingList.isEmpty(), "There is no data binding from the cache with the key " + key);
                        final Map<String, ConverterInfo> converterInfoMap = Maps.newHashMap();
                        final Map<String, Class<?>> properties = Maps.newHashMap();
                        for (DataTypeBinding dataTypeBinding : dataTypeBindingList) {
                            Class<?> parsingClassType = Class.forName(dataTypeBinding.getParsingType());
                            ConverterInfo converterInfo = ConverterInfo.create(parsingClassType, dataTypeBinding.getFormat(), !dataTypeBinding.getIsTypeConverterDeactivated());
//                                if(dataTypeBinding.isTypeConverterActivated()){
//                                    properties.put(dataTypeBinding.getName(), parsingClassType);
//                                }else{
//                                    properties.put(dataTypeBinding.getName(), String.class);
//                                }
                            converterInfoMap.put(dataTypeBinding.getName(), converterInfo);
                            properties.put(dataTypeBinding.getName(), parsingClassType);
                            fullyQualifiedClassName = fullyQualifiedPackageName + '.' + dataTypeBinding.getType();
                        }

                        Class<?> beanClass = createBeanClass(fullyQualifiedClassName, properties);
                        return new AbstractMap.SimpleEntry(beanClass, converterInfoMap);
                    } catch (IllegalStateException e) {
                        logger.error(e, e);
                        return null;
                    } catch (Exception e) {
                        logger.error(e, e);
                        return null;
                    }
                }
            });
    private final static LoadingCache<String, List<CalculationScriptEngineConf>> calculationScriptEngineCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("calculation.script.engine.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("calculation.script.engine.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, List<CalculationScriptEngineConf>>() {
                @Override
                public List<CalculationScriptEngineConf> load(String key) throws Exception {
//                    CalculationScriptEngineConf calculationScriptEngineConf =  MappingSessionSingleton.getInstance().get(CalculationScriptEngineConf.class, key);
//                    return calculationScriptEngineConf;
                    EntityTypeMetadata emeta = EntityTypeParser.getEntityMetadata(CalculationScriptEngineConf.class);
                    //EntityTypeMetadata semeta = EntityTypeParser.getEntityMetadata(CalculationScriptEngineConf.class.getSuperclass());
                    EntityFieldMetaData fmeta = emeta.getFieldMetadata("type");
                    //Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), key));
                    Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), QueryBuilder.bindMarker()));
                    PreparedStatement preparedStatement = PreparedStatementCache.get(query);
                    BoundStatement bs = new BoundStatement(preparedStatement);
                    bs.setString(0,key);
                    List<CalculationScriptEngineConf> csecList = MappingSessionSingleton.getInstance().getByQuery(CalculationScriptEngineConf.class, bs);
                    return csecList;
                }
            });
    private final static LoadingCache<String, List<CalculationScriptEngineLateComingConf>> calculationScriptEngineLateComingCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("calculation.script.engine.late.coming.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("calculation.script.engine.late.coming.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, List<CalculationScriptEngineLateComingConf>>() {
                @Override
                public List<CalculationScriptEngineLateComingConf> load(String key) throws Exception {
//                    CalculationScriptEngineConf calculationScriptEngineConf =  MappingSessionSingleton.getInstance().get(CalculationScriptEngineConf.class, key);
//                    return calculationScriptEngineConf;
                    EntityTypeMetadata emeta = EntityTypeParser.getEntityMetadata(CalculationScriptEngineLateComingConf.class);
                    //EntityTypeMetadata semeta = EntityTypeParser.getEntityMetadata(CalculationScriptEngineLateComingConf.class.getSuperclass());
                    EntityFieldMetaData fmeta = emeta.getFieldMetadata("type");
                    //Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), key));
                    Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), QueryBuilder.bindMarker()));
                    PreparedStatement preparedStatement = PreparedStatementCache.get(query);
                    BoundStatement bs = new BoundStatement(preparedStatement);
                    bs.setString(0,key);
                    List<CalculationScriptEngineLateComingConf> csecList = MappingSessionSingleton.getInstance().getByQuery(CalculationScriptEngineLateComingConf.class, bs);
                    return csecList;
                }
            });
    private final static LoadingCache<String, Map<String, CompiledScript>> calculationCompiledScriptMapCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("calculation.compiled.script.list.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("calculation.compiled.script.list.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, Map<String, CompiledScript>>() {
                @Override
                public Map<String, CompiledScript> load(String key) throws Exception {
                    Map<String, CompiledScript> compiledScriptMap = Maps.newLinkedHashMap();
                    try {
                        List<CalculationScriptEngineConf> calculationScriptEngineConfList = calculationScriptEngineCache.get(key);
                        checkNotNull(calculationScriptEngineConfList, "Calculation Script Engine Conf List is null. Could not find any CalculationScriptEngineConf of type (" + key + "). Check the database.");
                        //KnowledgeRuntimeEventManager session = null;

                        for (CalculationScriptEngineConf calculationScriptEngineConf : calculationScriptEngineConfList) {

                            String engineName = calculationScriptEngineConf.getEngineMap().get("name");
                            checkNotNull(engineName, "engine name (" + key + ") does not exist in engine map at calculation_script_engine_conf table. Check the database.");

                            ScriptEngine scriptEngine = ScriptEngineManagerSingleton.getInstance().getEngineByName(engineName);
                            if (scriptEngine instanceof ICustomEngine) {
                                ((ICustomEngine) scriptEngine).setConf(new ScriptEngineConf(calculationScriptEngineConf));
                            }
                            Compilable compilingEngine = (Compilable) scriptEngine;
                            CompiledScript compiledScript = compilingEngine.compile(calculationScriptEngineConf.getScript());

                            compiledScriptMap.put(calculationScriptEngineConf.getName(), compiledScript);
                        }
                    } catch (Throwable t) {
                        logger.error(t);
                    }
                    return compiledScriptMap;
                }
            });
    private final static LoadingCache<String, Map<String, CompiledScript>> calculationCompiledScriptLateComingMapCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("calculation.compiled.script.late.coming.map.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("calculation.compiled.script.late.coming.map.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, Map<String, CompiledScript>>() {
                @Override
                public Map<String, CompiledScript> load(String key) throws Exception {
                    Map<String, CompiledScript> compiledScriptMap = Maps.newLinkedHashMap();
                    try {
                        List<CalculationScriptEngineLateComingConf> calculationScriptEngineLateComingConfList = getCalculationScriptEngineLateComingCache().get(key);
                        checkNotNull(calculationScriptEngineLateComingConfList, "Calculation Script Engine Conf List is null. Could not find any CalculationScriptEngineLateComingConf of type (" + key + "). Check the database.");
                        //KnowledgeRuntimeEventManager session = null;

                        for (CalculationScriptEngineLateComingConf calculationScriptEngineLateComingConf : calculationScriptEngineLateComingConfList) {

                            String engineName = calculationScriptEngineLateComingConf.getEngineMap().get("name");
                            checkNotNull(engineName, "engine name (" + key + ") does not exist in engine map at calculation_script_engine_late_coming_conf table. Check the database.");

                            ScriptEngine scriptEngine = ScriptEngineManagerSingleton.getInstance().getEngineByName(engineName);
                            if (scriptEngine instanceof ICustomEngine) {
                                ((ICustomEngine) scriptEngine).setConf(new ScriptEngineConf(calculationScriptEngineLateComingConf));
                            }
                            Compilable compilingEngine = (Compilable) scriptEngine;
                            CompiledScript compiledScript = compilingEngine.compile(calculationScriptEngineLateComingConf.getScript());

                            compiledScriptMap.put(calculationScriptEngineLateComingConf.getName(), compiledScript);
                        }
                    } catch (Throwable t) {
                        logger.error(t);
                    }
                    return compiledScriptMap;
                }
            });
    private final static LoadingCache<String, List<KnowledgeBaseConf>> calculationKnowledgeRunTimeEventManagerSessionListCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("calculation.knowledge.runtime.event.manager.session.list.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("calculation.knowledge.runtime.event.manager.session.list.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, List<KnowledgeBaseConf>>() {
                @Override
                public List<KnowledgeBaseConf> load(String key) throws Exception {
                    List<KnowledgeBaseConf> KnowledgeBaseConfList = null;
                    KnowledgeBaseConfList = Lists.newArrayList();
                    try {
                        List<CalculationScriptEngineConf> calculationScriptEngineConfList = calculationScriptEngineCache.get(key);
                        checkNotNull(calculationScriptEngineConfList, "Calculation Script Engine Conf List is null. Could not find any CalculationScriptEngineConf of type (" + key + "). Check the database.");
                        //KnowledgeRuntimeEventManager session = null;

                        for (CalculationScriptEngineConf calculationScriptEngineConf : calculationScriptEngineConfList) {

                            String session_type = calculationScriptEngineConf.getEngineMap().get("session_type");
                            checkNotNull(session_type, "session_type(" + key + ") does not exist in engine map at calculation_script_engine_conf table. Check the database.");


                            KnowledgeBuilder builder = KnowledgeBuilderFactory.newKnowledgeBuilder();
                            builder.add(ResourceFactory.newByteArrayResource(calculationScriptEngineConf.getScript().getBytes()), ResourceType.DRL);
                            if (builder.hasErrors()) {
                                logger.info(calculationScriptEngineConf.getScript());
                                throw new RuntimeException("Building Knowledge of type (" + key + ") throws an error. " + builder.getErrors().toString());
                            }

                            KieBaseConfiguration configuration = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
                            if (calculationScriptEngineConf.getEngineOptionMap() != null) {
                                for (Map.Entry<String, String> option : calculationScriptEngineConf.getEngineOptionMap().entrySet()) {
                                    Class enumClass = Class.forName(option.getKey());
                                    if (!enumClass.isEnum()) {
                                        throw new TypeMismatchException(enumClass + " in type (" + key + ")is not of an enum type");
                                    }
                                    Enum<?> anEnum = Enum.valueOf(enumClass, option.getValue());
                                    configuration.setOption((org.kie.api.conf.KieBaseOption) anEnum);
                                }

                            }
                            //configuration.setOption(SequentialOption.YES);
                            KnowledgeBase knowledgeBase = KnowledgeBaseFactory.newKnowledgeBase();
                            knowledgeBase.addKnowledgePackages(builder.getKnowledgePackages());
                            KnowledgeBaseConf.KnowledgeBaseType knowledgeBaseType;
                            switch (session_type.toLowerCase()) {
                                case "statefulknowledgesession":
                                    //session = knowledgeBase.newStatefulKnowledgeSession();
                                    knowledgeBaseType = KnowledgeBaseConf.KnowledgeBaseType.STATEFUL;
                                    break;
                                case "statelessknowledgesession":
                                    //session = knowledgeBase.newStatelessKnowledgeSession();
                                    knowledgeBaseType = KnowledgeBaseConf.KnowledgeBaseType.STATELESS;
                                    break;
                                default:
                                    //throw new TypeMismatchException("Type ("+session_type+") is not a recognized one.");
                                    knowledgeBaseType = KnowledgeBaseConf.KnowledgeBaseType.TYPE_MISMATCH;
                            }
                            //AbstractMap.SimpleEntry knowledgeBaseEntry = new AbstractMap.SimpleEntry<KnowledgeBase,KnowledgeBaseConf.KnowledgeBaseType>(knowledgeBase, knowledgeBaseType);

                            KnowledgeBaseConf knowledgeBaseConf = KnowledgeBaseConf.create(key, calculationScriptEngineConf.getName(), knowledgeBase, knowledgeBaseType);
                            KnowledgeBaseConfList.add(knowledgeBaseConf);

                        }


                    } catch (Throwable t) {
                        logger.error(t);
                    }
                    return KnowledgeBaseConfList;
                }
            });
    private final static LoadingCache<String, List<Cassandra>> outputCassandraConfCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("output.cassandra.conf.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("output.cassandra.conf.cache.maximum.size", 60000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<String, List<Cassandra>>() {
                @Override
                public List<Cassandra> load(String key) throws Exception {
                    List<Cassandra> outputCassandraConfList = null;
                    try {
                        EntityTypeMetadata emeta = EntityTypeParser.getEntityMetadata(Cassandra.class);
                        EntityFieldMetaData fmeta = emeta.getFieldMetadata("type");
                        //Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), key));
                        Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), QueryBuilder.bindMarker()));
                        PreparedStatement preparedStatement = PreparedStatementCache.get(query);
                        BoundStatement bs = new BoundStatement(preparedStatement);
                        bs.setString(0,key);
                        outputCassandraConfList = MappingSessionSingleton.getInstance().getByQuery(Cassandra.class, bs);
                    } catch (Throwable t) {
                        logger.error(t);
                    }
                    return outputCassandraConfList;
                }
            });
    private final static LoadingCache<Map.Entry<String, String>, List<Cassandra>> outputCassandraConfByDatasetTypeCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("output.cassandra.conf.dataset.cache.expire.after.write", 5L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("output.cassandra.conf.dataset.cache.maximum.size", 60000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<Map.Entry<String, String>, List<Cassandra>>() {
                @Override
                public List<Cassandra> load(Map.Entry<String, String> input) throws Exception {
                    List<Cassandra> outputCassandraConfList = null;
                    try {
                        EntityTypeMetadata emeta = EntityTypeParser.getEntityMetadata(Cassandra.class);
                        EntityFieldMetaData fmeta = emeta.getFieldMetadata("type");
                        //Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), input.getKey())).and(eq(emeta.getFieldMetadata("datasetType").getColumnName(), input.getValue()));
                        Statement query = QueryBuilder.select().all().from(Utils.KeySpace, emeta.getTableName()).where(eq(fmeta.getColumnName(), QueryBuilder.bindMarker())).and(eq(emeta.getFieldMetadata("datasetType").getColumnName(), QueryBuilder.bindMarker()));
                        PreparedStatement preparedStatement = PreparedStatementCache.get(query);
                        BoundStatement bs = new BoundStatement(preparedStatement);
                        bs.setString(0,input.getKey());
                        bs.setString(1,input.getValue());

                        outputCassandraConfList = MappingSessionSingleton.getInstance().getByQuery(Cassandra.class, bs);
                    } catch (Throwable t) {
                        logger.error(t);
                    }
                    return outputCassandraConfList;
                }
            });
    private final static LoadingCache<RowHolderInfo, ResultSet> rowCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("row.cache.expire.after.write", 2000L), TimeUnit.MILLISECONDS)
            .maximumSize(Utils.getLongMetricsProperty("row.cache.maximum.size", 500L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<RowHolderInfo, ResultSet>() {
                @Override
                public ResultSet load(final RowHolderInfo rowHolderInfo) throws Exception {
                    Session session = DbConnectionManager.getSession(rowHolderInfo.keyspace());
                    checkNotNull(session,"There is no keyspace with the following name: "+rowHolderInfo.keyspace());
                    RegularStatement query = QueryBuilder.select().all().from(rowHolderInfo.keyspace(), rowHolderInfo.table());
                    BoundStatement bs = null;
                    if (rowHolderInfo.columnValueList() != null) {
                        //List<Statement> whereStatementList = Lists.newLinkedList();
                        //boolean isFirst = true;
                        RegularStatement statement = query;
                        if (rowHolderInfo.columnValueList().size() > 0) {
                            statement = ((Select) query).where(eq(rowHolderInfo.columnValueList().get(0).getKey(), QueryBuilder.bindMarker()));

                            //rowHolderInfo.columnValueList().get(0).getValue();
                            for (int i = 1; i < rowHolderInfo.columnValueList().size(); i++) {
                                statement = ((Select.Where) statement).and(eq(rowHolderInfo.columnValueList().get(i).getKey(), QueryBuilder.bindMarker()));
                            }

                        }

                        PreparedStatement ps = PreparedStatementCache.get(statement);
                        bs = new BoundStatement(ps);
                        int i = 0;
                        for (AbstractMap.SimpleEntry<String, String> stringStringSimpleEntry : rowHolderInfo.columnValueList()) {
                            DataType dataType = CacheUtil.getColumnDataTypeCache().get(ColumnDataType.create(rowHolderInfo.keyspace(), rowHolderInfo.table(), stringStringSimpleEntry.getKey()));
                            bs = CassandraUtils.bind(bs,i++,stringStringSimpleEntry.getValue(),dataType.asJavaClass());
                        }
                    }
                    return bs == null?null:session.execute(bs);
                }
            });


    private final static LoadingCache<ColumnDataType, TreeSet<SchemaColumns>> clusteringKeysCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("clustering.keys.cache.expire.after.write", 60L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("clustering.keys.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<ColumnDataType, TreeSet<SchemaColumns>>() {
                @Override
                public TreeSet<SchemaColumns> load(final ColumnDataType columnDataType) throws Exception {

                    checkNotNull(columnDataType, "columnDataType cannot be null");
                    checkNotNull(columnDataType.keyspace(), "keyspace cannot be null");

                    Session session = DbConnectionManager.getSession("system");
                    EntityTypeMetadata emeta = EntityTypeParser.getEntityMetadata(SchemaColumns.class);
                    EntityFieldMetaData keyspaceMeta = emeta.getFieldMetadata("keyspaceName");
                    Select.Where query = QueryBuilder.select().all().from("system", "schema_columns").where(eq(keyspaceMeta.getColumnName(), columnDataType.keyspace()));
                    if(columnDataType.table() != null){
                        EntityFieldMetaData columnfamilyMeta = emeta.getFieldMetadata("columnfamilyName");
                        query = query.and(eq(columnfamilyMeta.getColumnName(),columnDataType.table()));
                        if(columnDataType.column() != null){
                            EntityFieldMetaData columnMeta = emeta.getFieldMetadata("columnName");
                            query = query.and(eq(columnMeta.getColumnName(), columnDataType.column()));
                        }
                    }
                    List<SchemaColumns> schemaColumnsList = MappingSessionSingleton.getInstance().getByQuery(SchemaColumns.class, query);
                    TreeSet<SchemaColumns> schemaColumnsTreeSet = new TreeSet<SchemaColumns>(SchemaColumns.TypeComponentIndexComparator);
                    for (SchemaColumns schemaColumns : schemaColumnsList) {
                        if(schemaColumns.getType().equalsIgnoreCase("clustering_key")){
                            schemaColumnsTreeSet.add(schemaColumns);
                        }
                    }
                    return schemaColumnsTreeSet;
                }
            });

    private final static LoadingCache<ColumnDataType, List<SchemaColumns>>   compoundPrimaryKeyCache = CacheBuilder.newBuilder()
            .expireAfterWrite(Utils.getLongMetricsProperty("compound.primary.key.cache.expire.after.write", 60L), TimeUnit.MINUTES)
            .maximumSize(Utils.getLongMetricsProperty("compound.primary.key.cache.maximum.size", 5000L))
//            .removalListener(new TradeAccountRemovalListener())
            .ticker(Ticker.systemTicker())
            .build(new CacheLoader<ColumnDataType, List<SchemaColumns>>() {
                @Override
                public List<SchemaColumns> load(final ColumnDataType columnDataType) throws Exception {

                    checkNotNull(columnDataType, "columnDataType cannot be null");
                    checkNotNull(columnDataType.keyspace(), "keyspace cannot be null");

                    Session session = DbConnectionManager.getSession("system");
                    EntityTypeMetadata emeta = EntityTypeParser.getEntityMetadata(SchemaColumns.class);
                    EntityFieldMetaData keyspaceMeta = emeta.getFieldMetadata("keyspaceName");
                    Select.Where query = QueryBuilder.select().all().from("system", "schema_columns").where(eq(keyspaceMeta.getColumnName(), columnDataType.keyspace()));
                    if(columnDataType.table() != null){
                        EntityFieldMetaData columnfamilyMeta = emeta.getFieldMetadata("columnfamilyName");
                        query = query.and(eq(columnfamilyMeta.getColumnName(),columnDataType.table()));
                        if(columnDataType.column() != null){
                            EntityFieldMetaData columnMeta = emeta.getFieldMetadata("columnName");
                            query = query.and(eq(columnMeta.getColumnName(), columnDataType.column()));
                        }
                    }
                    List<SchemaColumns> schemaColumnsList = MappingSessionSingleton.getInstance().getByQuery(SchemaColumns.class, query);
                    List<SchemaColumns> schemaColumnsTreeSet = Lists.newLinkedList();//new TreeSet<SchemaColumns>(SchemaColumns.ComponentIndexComparator);
                    for (SchemaColumns schemaColumns : schemaColumnsList) {
                        if(schemaColumns.getType().equalsIgnoreCase("clustering_key") || schemaColumns.getType().equalsIgnoreCase("partition_key")){
                            schemaColumnsTreeSet.add(schemaColumns);
                        }
                    }
                    Collections.sort(schemaColumnsTreeSet,SchemaColumns.TypeComponentIndexComparator);
                    return schemaColumnsTreeSet;
                }
            });


//    private final static LoadingCache<String, Map.Entry<Class<?>,Map<String, ConverterInfo>>> uniqueMappingClassCache = CacheBuilder.newBuilder()
//            .expireAfterWrite(Utils.getLongMetricsProperty("data.type.binding.cache.expire.after.write", 60L), TimeUnit.MINUTES)
//            .maximumSize(Utils.getLongMetricsProperty("data.type.binding.cache.maximum.size", 5000L))
////            .removalListener(new TradeAccountRemovalListener())
//            .ticker(Ticker.systemTicker())
//            .build(new CacheLoader<String, Map.Entry<Class<?>,Map<String, ConverterInfo>>>() {
//                @Override
//                public Map.Entry<Class<?>,Map<String, ConverterInfo>> load(String key) throws Exception {
//
//                    String fullyQualifiedPackageName = Utils.getMetricsProperty("mapping.class.package", "metrics.server.mapping.cache")+"."+key;
//                    String fullyQualifiedClassName = "";
//                    List<DataTypeBinding> dataTypeBindingList = getDataTypeBindingCache().get(key);
//
//                    try{
//                        checkState(!dataTypeBindingList.isEmpty(), "There is no data binding from the cache with the key "+key);
//                        final Map<String, ConverterInfo> converterInfoMap = Maps.newHashMap();
//                        final Map<String, Class<?>> properties = Maps.newHashMap();
//                        for (DataTypeBinding dataTypeBinding : dataTypeBindingList) {
//                            Class<?> parsingClassType = Class.forName(dataTypeBinding.getParsingType());
//                            ConverterInfo converterInfo = ConverterInfo.create(parsingClassType, dataTypeBinding.getFormat(), !dataTypeBinding.getIsTypeConverterDeactivated());
////                                if(dataTypeBinding.isTypeConverterActivated()){
////                                    properties.put(dataTypeBinding.getName(), parsingClassType);
////                                }else{
////                                    properties.put(dataTypeBinding.getName(), String.class);
////                                }
//                            converterInfoMap.put(dataTypeBinding.getName(),converterInfo);
//                            properties.put(dataTypeBinding.getName(), parsingClassType);
//                            fullyQualifiedClassName = fullyQualifiedPackageName + '.' + dataTypeBinding.getType();
//                        }
//
//                        Class<?> beanClass = createBeanClass(fullyQualifiedClassName, properties);
//                        return new AbstractMap.SimpleEntry(beanClass,converterInfoMap);
//                    }catch (IllegalStateException e){
//                        logger.error(e);
//                        return null;
//                    }catch (Exception e){
//                        logger.error(e);
//                        return null;
//                    }
//                }
//            });

    public static LoadingCache<String, RegexPatternSet> getRegexPatternCache() {
        return regexPatternCache;
    }

    public static LoadingCache<String, Map<String, PatternInfo>> getPatternCache() {
        return patternCache;
    }

    public static LoadingCache<ColumnDataType, DataType> getColumnDataTypeCache() {
        return columnDataTypeCache;
    }

    public static LoadingCache<String, List<DataTypeBinding>> getDataTypeBindingCache() {
        return dataTypeBindingCache;
    }

    protected static Class<?> createBeanClass(
    /* fully qualified class name */
    final String className,
    /* bean properties, name -> type */
    final Map<String, Class<?>> properties) {

        final BeanGenerator beanGenerator = new BeanGenerator();

    /* use our own hard coded class name instead of a real naming policy */
        beanGenerator.setNamingPolicy(new NamingPolicy() {
            @Override
            public String getClassName(final String prefix,
                                       final String source, final Object key, final Predicate names) {
                return className;
            }
        });
        beanGenerator.setSuperclass(AbstractSerialization.class);
        BeanGenerator.addProperties(beanGenerator, properties);
        return (Class<?>) beanGenerator.createClass();
    }

    public static LoadingCache<String, Map.Entry<Class<?>, Map<String, ConverterInfo>>> getMappingClassCache() {
        return mappingClassCache;
    }

    public static LoadingCache<String, Map<String, CompiledScript>> getCalculationCompiledScriptMapCache() {
        return calculationCompiledScriptMapCache;
    }

    //TODO: add the calculation engine as well and return theml
    public static LoadingCache<String, List<KnowledgeBaseConf>> getCalculationKnowledgeRunTimeEventManagerSessionListCache() {
        return calculationKnowledgeRunTimeEventManagerSessionListCache;
    }

    public static LoadingCache<String, List<Cassandra>> getOutputCassandraConfCache() {
        return outputCassandraConfCache;
    }

    public static LoadingCache<Map.Entry<String, String>, List<Cassandra>> getOutputCassandraConfByDatasetTypeCache() {
        return outputCassandraConfByDatasetTypeCache;
    }

    public static LoadingCache<RowHolderInfo, ResultSet> getRowCache() {
        return rowCache;
    }

    public static LoadingCache<String, List<CalculationScriptEngineLateComingConf>> getCalculationScriptEngineLateComingCache() {
        return calculationScriptEngineLateComingCache;
    }


    public static LoadingCache<ColumnDataType, TreeSet<SchemaColumns>> getClusteringKeysCache() {
        return clusteringKeysCache;
    }

    public static LoadingCache<ColumnDataType, List<SchemaColumns>> getCompoundPrimaryKeyCache() {
        return compoundPrimaryKeyCache;
    }

    public static LoadingCache<String, Map<String, CompiledScript>> getCalculationCompiledScriptLateComingMapCache() {
        return calculationCompiledScriptLateComingMapCache;
    }
}
