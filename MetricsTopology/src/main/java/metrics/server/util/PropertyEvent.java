package metrics.server.util;

import java.util.Map;

/**
 * Created by ali on 7/10/14.
 */
public class PropertyEvent {

    private String property;
    private EventType eventType;
    private Map<String, Object> params;

    public PropertyEvent(String property, EventType eventType, Map<String, Object> params) {
        this.setProperty(property);
        this.setEventType(eventType);
        this.setParams(params);
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }


    /**
     * Represents valid event types.
     */
    public enum EventType {

        /**
         * A property was set.
         */
        property_set,

        /**
         * A property was deleted.
         */
        property_deleted,

        /**
         * A JSON property was set.
         */
        json_property_set,

        /**
         * A JSON property was deleted.
         */
        json_property_deleted;
    }
}
