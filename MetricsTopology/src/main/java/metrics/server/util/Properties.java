package metrics.server.util;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.google.common.collect.Maps;
import com.google.common.eventbus.AsyncEventBus;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import metrics.server.database.DbConnectionManager;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.set;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 7/9/14.
 */
public class Properties implements Map<String, String>{
    private static final Logger logger = Logger.getLogger(Properties.class.getName());

    @Inject @Named("table_name")private String tableName;
    private Statement LOAD_PROPERTIES;// = QueryBuilder.select("name", "value").from(getTableName());//.select()"SELECT name, value FROM " + getTableName();
    private Statement INSERT_PROPERTY;// = QueryBuilder.insertInto(getTableName()).value("name",QueryBuilder.bindMarker()).value("value", QueryBuilder.bindMarker());//"INSERT INTO " + getTableName() + "(name, value) VALUES(?,?)";
    private Statement UPDATE_PROPERTY;// = QueryBuilder.update(getTableName()).with(set("value",QueryBuilder.bindMarker())).where(eq("name", QueryBuilder.bindMarker()));//"UPDATE" + getTableName() + " SET value=? WHERE name=?";
    private Statement DELETE_PROPERTY;// = QueryBuilder.delete().from(getTableName()).where(eq("name", QueryBuilder.bindMarker()));//"DELETE FROM " + getTableName() + " WHERE name=?";

    private Statement GET_ON_DEMAND_PROPERTY;

    private Map<String, String> properties;

    private AsyncEventBus eventBus;
    private final String keyspace;


//    private static class PropertyHolder {
//        private static final Properties instance = new Properties();
//        static {
//            instance.init();
//        }
//    }
//
//
//    public static Properties getInstance(){
//        return PropertyHolder.instance;
//    }

    public Properties(String keyspace, String tableName) {
        super();
        if(keyspace == null || keyspace.isEmpty()){
            this.keyspace = Utils.KeySpace;
        }else{
            this.keyspace = keyspace;
        }
        checkNotNull(tableName);
        this.tableName = tableName;
        initStatements();
        init();
    }

    public synchronized void initStatements(){
        LOAD_PROPERTIES = QueryBuilder.select("name", "value").from(keyspace,getTableName());//.select()"SELECT name, value FROM " + getTableName();
        INSERT_PROPERTY = QueryBuilder.insertInto(keyspace,getTableName()).value("name",QueryBuilder.bindMarker()).value("value", QueryBuilder.bindMarker());//"INSERT INTO " + getTableName() + "(name, value) VALUES(?,?)";
        UPDATE_PROPERTY = QueryBuilder.update(keyspace,getTableName()).with(set("value",QueryBuilder.bindMarker())).where(eq("name", QueryBuilder.bindMarker()));//"UPDATE" + getTableName() + " SET value=? WHERE name=?";
        DELETE_PROPERTY = QueryBuilder.delete().from(keyspace,getTableName()).where(eq("name", QueryBuilder.bindMarker()));//"DELETE FROM " + getTableName() + " WHERE name=?";
        GET_ON_DEMAND_PROPERTY = QueryBuilder.select().column("name").column("value").from(keyspace, getTableName()).where(eq("name",QueryBuilder.bindMarker()));
    }

    public void init() {
        if (properties == null) {
            properties = Maps.newConcurrentMap();
        } else {
            properties.clear();
        }
        ExecutorService executorService = Executors.newFixedThreadPool(5);  //TODO move this one under loadProperties() and make it configurable with default value
        eventBus = new AsyncEventBus(executorService);

        loadProperties();
    }


    public void registerListener(Object object) {
        eventBus.register(object);
    }

    public void unregisterListener(Object object) {
        eventBus.unregister(object);
    }

    protected void dispatchEvent(String property, PropertyEvent.EventType eventType, Map<String, Object> params) {
        eventBus.post(new PropertyEvent(property, eventType, params));
    }

    @Override
    public int size() {
        return properties.size();
    }

    @Override
    public boolean isEmpty() {
        return properties.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return properties.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return properties.containsValue(value);
    }

    @Override
    public String get(Object key) {
        return properties.get(key);
    }

    public synchronized String getOnDemand(Object key){
        Session session = null;
        ResultSet rs = null;
        try {
            session = DbConnectionManager.getSession();

            PreparedStatement preparedStatement = PreparedStatementCache.get(GET_ON_DEMAND_PROPERTY);
            BoundStatement bs = new BoundStatement(preparedStatement);
            String name = key.toString();
            bs.setString(0,name);
            rs = session.execute(bs);
            Row row = rs.one();
            if(row != null){
                String value = row.getString(1);
                put(name, value);
                return value;
            }
        } catch (Exception e) {
            logger.error(e, e);
        }
        return null;
    }

    @Override
    public String put(String key, String value) {
        if (value == null) {
            // This is the same as deleting, so remove it.
            return remove(key);
        }
        if (key == null) {
            throw new NullPointerException("Key cannot be null. Key=" +
                    key + ", value=" + value);
        }
        if (key.endsWith(".")) {
            key = key.substring(0, key.length() - 1);
        }
        key = key.trim();
        String result;
        synchronized (this) {
            if (properties.containsKey(key)) {
                if (!properties.get(key).equals(value)) {
                    updateProperty(key, value);
                }
            } else {
                insertProperty(key, value);
            }

            result = properties.put(key, value);
        }

        // Generate event.
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("value", value);
        dispatchEvent(key, PropertyEvent.EventType.property_set, params);


        return result;
    }
    public String put(String key, Object value){
        return put(key,value.toString());
    }

    @Override
    public String remove(Object key) {
        String value;
        synchronized (this) {
            value = properties.remove(key);
            // Also remove any children.
            Collection<String> propNames = getPropertyNames();
            for (String name : propNames) {
                if (name.startsWith((String) key)) {
                    properties.remove(name);
                }
            }
            deleteProperty((String) key);
        }

        // Generate event.
        Map<String, Object> params = Collections.emptyMap();
        dispatchEvent((String) key, PropertyEvent.EventType.property_deleted, params);


        return value;
    }

    @Override
    public void putAll(Map<? extends String, ? extends String> m) {
        for (Map.Entry<? extends String, ? extends String> entry : m.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<String> keySet() {
        return Collections.unmodifiableSet(properties.keySet());
    }

    @Override
    public Collection<String> values() {
        return Collections.unmodifiableCollection(properties.values());
    }

    @Override
    public Set<Entry<String, String>> entrySet() {
        return Collections.unmodifiableSet(properties.entrySet());
    }

    public Collection<String> getPropertyNames() {
        return properties.keySet();
    }

    private void insertProperty(String name, String value) {
        Session session = null;
        PreparedStatement pstmt = null;
        try {
            session = DbConnectionManager.getSession();
            //Statement INSERT_PROPERTY = QueryBuilder.insertInto(getTableName()).value("name",QueryBuilder.bindMarker()).value("value", QueryBuilder.bindMarker());//"INSERT INTO " + getTableName() + "(name,
            pstmt = PreparedStatementCache.get(INSERT_PROPERTY);////session.prepare(INSERT_PROPERTY);
            BoundStatement bs = new BoundStatement(pstmt);
            bs.bind(name, value);
            session.execute(bs);
        } catch (Exception e) {
            logger.error(e, e);
        } finally {
        }
    }

    private void updateProperty(String name, String value) {
        Session session = null;
        PreparedStatement pstmt = null;
        try {
            checkNotNull(name,"name cannot be null");
            session = DbConnectionManager.getSession();
            //Statement UPDATE_PROPERTY = QueryBuilder.update(getTableName()).with(set("value",QueryBuilder.bindMarker())).where(eq("name", QueryBuilder.bindMarker()));//"UPDATE" + getTableName() + " SET value=?
            pstmt = PreparedStatementCache.get(UPDATE_PROPERTY);
            BoundStatement bs = new BoundStatement(pstmt);
            bs.setString(0,value);
            bs.setString(1,name);
            session.execute(bs);
        } catch (Exception e) {
            logger.error(e, e);
        }
    }

    private void deleteProperty(String name) {
        Session session = null;
        PreparedStatement pstmt = null;
        try {
            session = DbConnectionManager.getSession();
            pstmt = PreparedStatementCache.get(DELETE_PROPERTY);//.session.prepare(DELETE_PROPERTY);
            BoundStatement bs = new BoundStatement(pstmt);
            bs.bind(name, name);
            session.execute(bs);
        } catch (Exception e) {
            logger.error(e, e);
        }

    }

    private void loadProperties() {

        Session session = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            session = DbConnectionManager.getSession();

//            pstmt = session.prepare(LOAD_PROPERTIES);
//            BoundStatement bs = new BoundStatement(pstmt);
            rs = session.execute(LOAD_PROPERTIES);
            for (Row row : rs) {
                String name = row.getString(0);
                String value = row.getString(1);
                properties.put(name, value);
            }
        } catch (Exception e) {
            logger.error(e, e);
        }


    }

    public String getTableName() {
        return tableName;
    }
}
