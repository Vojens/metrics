package metrics.server.mapping.output;

/**
 * Created by ali on 7/15/14.
 */

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.UUID;


@Table(name = "account", indexes = {@Index(name = "account_email_idx", columnList = "email")})
public class Account {
    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "email")
    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
