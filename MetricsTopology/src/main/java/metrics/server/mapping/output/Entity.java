package metrics.server.mapping.output;

/**
 * Created by ali on 7/15/14.
 */

import com.datastax.driver.core.utils.UUIDs;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.UUID;

@Table(name = "mytable",
        indexes = {
                @Index(name = "entity_email_idx", columnList = "email"),
                @Index(name = "entity_name_idx", columnList = "myname")
        })
public class Entity {

    @Id
    UUID code = UUIDs.timeBased();

    @Column(name = "myname")
    private String name;
    private String email;

    public UUID getCode() {
        return code;
    }

    public void setCode(UUID code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    // public getters/setters ...
}
