package metrics.server.mapping.scriptengine;

import java.util.Map;

/**
 * Created by ali on 8/25/14.
 */
public interface ICalculationScriptEngineConf {
    String getType();

    void setType(String type);

    String getName();

    void setName(String name);

    Map<String, String> getEngineMap();

    void setEngineMap(Map<String, String> engineMap);

    Map<String, String> getEngineOptionMap();

    void setEngineOptionSet(Map<String, String> engineOptionMap);

    Map<String, String> getEngineBindingMap();

    void setEngineBindingMap(Map<String, String> engineBindingMap);

    String getScript();

    void setScript(String script);
}
