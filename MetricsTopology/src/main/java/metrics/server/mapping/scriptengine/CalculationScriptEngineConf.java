package metrics.server.mapping.scriptengine;

import com.datastax.driver.mapping.annotation.CollectionType;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ali on 8/6/14.
 */
@Table(name = "calculation_script_engine_conf")
public class CalculationScriptEngineConf implements ICalculationScriptEngineConf{

    @Id
    private String type;
    private String name;
    @CollectionType(TreeMap.class)
    @Column(name = "engine")
    private Map<String, String> engineMap;
    @CollectionType(TreeMap.class)
    @Column(name = "engine_options")
    private Map<String, String> engineOptionMap;
    @CollectionType(TreeMap.class)
    @Column(name = "engine_bindings")
    private Map<String, String> engineBindingMap;
    private String script;

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Map<String, String> getEngineMap() {
        return engineMap;
    }

    @Override
    public void setEngineMap(Map<String, String> engineMap) {
        this.engineMap = engineMap;
    }

    @Override
    public Map<String, String> getEngineOptionMap() {
        return engineOptionMap;
    }

    @Override
    public void setEngineOptionSet(Map<String, String> engineOptionMap) {
        this.engineOptionMap = engineOptionMap;
    }

    @Override
    public Map<String, String> getEngineBindingMap() {
        return engineBindingMap;
    }

    @Override
    public void setEngineBindingMap(Map<String, String> engineBindingMap) {
        this.engineBindingMap = engineBindingMap;
    }

    @Override
    public String getScript() {
        return script;
    }

    @Override
    public void setScript(String script) {
        this.script = script;
    }
}
