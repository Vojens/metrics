package metrics.server.mapping;

import com.datastax.driver.mapping.annotation.CollectionType;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ali on 7/14/14.
 */
@Table(name = "regex_pattern_set")
public class RegexPatternSet {


//    @Id
//    private java.util.UUID id;

    @Column(name = "type")
    @Id
    private String type;

//    @Column(name = "name")
//    private String name;
//
//    @Column(name = "data_type_binding")
//    private String dataTypeBinding;
    //    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getDataTypeBinding() {
//        return dataTypeBinding;
//    }
//
//    public void setDataTypeBinding(String dataTypeBinding) {
//        this.dataTypeBinding = dataTypeBinding;
//    }
    @CollectionType(TreeMap.class)
    private Map<String, String> patterns;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

//    public UUID getId() {
//        return id;
//    }
//
//    public void setId(UUID id) {
//        this.id = id;
//    }

    public Map<String, String> getPatterns() {
        return patterns;
    }

    public void setPatterns(Map<String, String> patterns) {
        this.patterns = patterns;
    }
}
