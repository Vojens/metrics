package metrics.server.mapping.system;

import org.apache.commons.collections.map.CaseInsensitiveMap;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Comparator;
import java.util.Map;

/**
 * Created by ali on 8/26/14.
 */
@Table(name = "schema_columns")
public class SchemaColumns implements Comparable<SchemaColumns>{

    @Id
    @Column(name = "keyspace_name")
    private String keyspaceName;

    @Id
    @Column(name = "columnfamily_name")
    private String columnfamilyName;

    @Id
    @Column(name = "column_name")
    private String columnName;


    @Column(name = "component_index")
    private int componentIndex;


    @Column(name = "index_name")
    private String indexName;


    @Column(name = "index_options")
    private String indexOptions;


    @Column(name = "index_type")
    private String indexType;

    private String type;

    private String validator;

    public String getKeyspaceName() {
        return keyspaceName;
    }

    public void setKeyspaceName(String keyspaceName) {
        this.keyspaceName = keyspaceName;
    }

    public String getColumnfamilyName() {
        return columnfamilyName;
    }

    public void setColumnfamilyName(String columnfamilyName) {
        this.columnfamilyName = columnfamilyName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getComponentIndex() {
        return componentIndex;
    }

    public void setComponentIndex(int componentIndex) {
        this.componentIndex = componentIndex;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexOptions() {
        return indexOptions;
    }

    public void setIndexOptions(String indexOptions) {
        this.indexOptions = indexOptions;
    }

    public String getIndexType() {
        return indexType;
    }

    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValidator() {
        return validator;
    }

    public void setValidator(String validator) {
        this.validator = validator;
    }

    @Override
    public int compareTo(SchemaColumns o) {
        return TypeComponentIndexComparator.compare(this,o);
    }

    public static Comparator ComponentIndexComparator = new Comparator() {
        public int compare(Object schemaColumns1, Object schemaColumns2) {
            int componentIndex1 = ((SchemaColumns) schemaColumns1).getComponentIndex();
            int componentIndex2 = ((SchemaColumns) schemaColumns2).getComponentIndex();

            return Integer.compare(componentIndex1,componentIndex2);
        }
    };

    public static Comparator<SchemaColumns> TypeComponentIndexComparator = new Comparator<SchemaColumns>() {
        public int compare(SchemaColumns schemaColumns1, SchemaColumns schemaColumns2) {

            String type1 = schemaColumns1.getType();
            String type2 =  schemaColumns2.getType();
            if(type1.equals(type2)){
                int componentIndex1 = schemaColumns1.getComponentIndex();
                int componentIndex2 = schemaColumns2.getComponentIndex();

                return Integer.compare(componentIndex1,componentIndex2);
            }else{
                return TypeComponentIndex.compare(schemaColumns1, schemaColumns2);
            }
        }
    };
    private static final Map<String,Integer> typeMap;
static {
    typeMap = new CaseInsensitiveMap();
    typeMap.put("partition_key",1);
    typeMap.put("clustering_key",2);
    typeMap.put("regular",3);

}
    public static Comparator<SchemaColumns> TypeComponentIndex = new Comparator<SchemaColumns>() {




        @Override
        public int compare(SchemaColumns o1, SchemaColumns o2) {
            String type1 = o1.type;
            String type2 = o2.type;

            Integer typePriority1 = typeMap.get(type1);
            Integer typePriority2 = typeMap.get(type2);
            if(typePriority1 == null && typePriority2 == null){
                return 0;
            }else if(typePriority1 == null){
                return -1;
            }else if(typePriority2 == null){
                return 1;
            }

            return typePriority2-typePriority1;

        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SchemaColumns)) return false;

        SchemaColumns that = (SchemaColumns) o;

        if (componentIndex != that.componentIndex) return false;
        if (!columnName.equals(that.columnName)) return false;
        if (!columnfamilyName.equals(that.columnfamilyName)) return false;
        if (indexName != null ? !indexName.equals(that.indexName) : that.indexName != null) return false;
        if (indexOptions != null ? !indexOptions.equals(that.indexOptions) : that.indexOptions != null) return false;
        if (indexType != null ? !indexType.equals(that.indexType) : that.indexType != null) return false;
        if (!keyspaceName.equals(that.keyspaceName)) return false;
        if (!type.equals(that.type)) return false;
        if (!validator.equals(that.validator)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = keyspaceName.hashCode();
        result = 31 * result + columnfamilyName.hashCode();
        result = 31 * result + columnName.hashCode();
        result = 31 * result + componentIndex;
        result = 31 * result + (indexName != null ? indexName.hashCode() : 0);
        result = 31 * result + (indexOptions != null ? indexOptions.hashCode() : 0);
        result = 31 * result + (indexType != null ? indexType.hashCode() : 0);
        result = 31 * result + type.hashCode();
        result = 31 * result + validator.hashCode();
        return result;
    }
}
