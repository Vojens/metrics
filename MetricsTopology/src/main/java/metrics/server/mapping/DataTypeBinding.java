package metrics.server.mapping;

import com.datastax.driver.mapping.annotation.CollectionType;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by ali on 7/15/14.
 */
@Table(name = "data_type_binding")
public class DataTypeBinding {

    @Id
    private String type;

    private String name;

    @Column(name = "parsing_type")
    private String parsingType;


    @CollectionType(TreeMap.class)
    private Map<String, String> format;

    @Column(name = "conversion_deactivated")
    private boolean isTypeConverterDeactivated;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParsingType() {
        return parsingType;
    }

    public void setParsingType(String parsingType) {
        this.parsingType = parsingType;
    }

    public Map<String, String> getFormat() {
        return format;
    }

    public void setFormat(Map<String, String> format) {
        this.format = format;
    }

    public boolean getIsTypeConverterDeactivated() {
        return isTypeConverterDeactivated;
    }

    public void setIsTypeConverterDeactivated(boolean isTypeConverterDeactivated) {
        this.isTypeConverterDeactivated = isTypeConverterDeactivated;
    }

//    public boolean isTypeConverterActivated() {
//        return !isTypeConverterDeactivated;
//    }
//
//    public void setTypeConverterActivated(boolean isTypeConverterActivated) {
//        this.isTypeConverterDeactivated = !isTypeConverterActivated;
//    }
}
