package metrics.shipper;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import metrics.shipper.nio.IO.Binding;
import metrics.shipper.nio.IO.DataSource;
import metrics.shipper.nio.IO.filter.AbstractFilter;
import metrics.shipper.nio.IO.filter.IFilter;
import metrics.shipper.nio.IO.input.AbstractInput;
import metrics.shipper.nio.IO.input.IInput;
import metrics.shipper.nio.IO.output.AbstractOutput;
import metrics.shipper.nio.IO.output.IOutput;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by ali on 6/3/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({
        "DataSource",
        "Input",
        "Output",

})

public class Configuration {
    private List<DataSource> dataSourceList = new LinkedList<DataSource>();

    private List<IInput> inputList = Lists.newLinkedList();

    private List<IOutput> outputList = Lists.newLinkedList();

    private List<IFilter> filterList = Lists.newLinkedList();

    private List<Binding> bindingList = Lists.newLinkedList();



    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("DataSources")
    public List<DataSource> getDataSourceList() {
        return dataSourceList;
    }

    @JsonProperty("DataSources")
    public void setDataSourceList(List<DataSource> dataSourceList) {
        this.dataSourceList = dataSourceList;
    }


    @JsonProperty("Inputs")
    @JsonDeserialize(contentAs=AbstractInput.class)
    public List<IInput> getInputList() {
        return inputList;
    }

    @JsonProperty("Inputs")
    @JsonDeserialize(contentAs=AbstractInput.class)
    public void setInputList(List<IInput> inputList) {
        this.inputList = inputList;
    }



    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("Outputs")
    @JsonDeserialize(contentAs=AbstractOutput.class)
    public List<IOutput> getOutputList() {
        return outputList;
    }

    @JsonProperty("Outputs")
    @JsonDeserialize(contentAs=AbstractOutput.class)
    public void setOutputList(List<IOutput> outputList) {
        this.outputList = outputList;
    }

    @JsonProperty("Bindings")
    public List<Binding> getBindingList() {
        return bindingList;
    }

    @JsonProperty("Bindings")
    public void setBindingList(List<Binding> bindingList) {
        this.bindingList = bindingList;
    }


    @JsonIgnore
    public Map<String, DataSource> getDataSourcesMap(){
        Map<String,DataSource>dataSourceMap = Maps.uniqueIndex(getDataSourceList().iterator(),new Function<DataSource, String>(){
            @Override
            public String apply( DataSource input) {
                return input.getName();
            }
        });
        return dataSourceMap;
    }

    @JsonProperty("Filters")
    @JsonDeserialize(contentAs=AbstractFilter.class)
    public List<IFilter> getFilterList() {
        return filterList;
    }

    @JsonProperty("Filters")
    @JsonDeserialize(contentAs=AbstractFilter.class)
    public void setFilterList(List<IFilter> filterList) {
        this.filterList = filterList;
    }

//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @Generated("metrics")
//    @JsonPropertyOrder({
//            "DirectoryPath",
//            "FileRegexPattern"
//    })
//    public static class DataSource{
//        private String name;
//        private String uri;
//
//        @Mapping("uri")
//        @JsonProperty("DirectoryPath")
//        public String getUri() {
//            return uri;
//        }
//
//        @JsonProperty("DirectoryPath")
//        public void setUri(String uri) {
//            this.uri = uri;
//        }
//
//        @JsonProperty("Name")
//        public String getName() {
//            return name;
//        }
//
//        @JsonProperty("Name")
//        public void setName(String name) {
//            this.name = name;
//        }
//    }
//
//    @JsonInclude(JsonInclude.Include.NON_NULL)
//    @Generated("metrics")
//    @JsonPropertyOrder({
//            "DirectoryPath",
//            "FileRegexPattern"
//    })
//    public static class FileSource{
//        private String directoryPath;
//        private String fileRegexPattern;
//        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
//
//        @JsonProperty("DirectoryPath")
//        public String getDirectoryPath() {
//            return directoryPath;
//        }
//
//        @JsonProperty("DirectoryPath")
//        public void setDirectoryPath(String directoryPath) {
//            this.directoryPath = directoryPath;
//        }
//
//        @JsonProperty("FileRegexPattern")
//        public String getFileRegexPattern() {
//            return fileRegexPattern;
//        }
//
//        @JsonProperty("FileRegexPattern")
//        public void setFileRegexPattern(String fileRegexPattern) {
//            this.fileRegexPattern = fileRegexPattern;
//        }
//
//        @JsonAnyGetter
//        public Map<String, Object> getAdditionalProperties() {
//            return this.additionalProperties;
//        }
//
//        @JsonAnySetter
//        public void setAdditionalProperty(String name, Object value) {
//            this.additionalProperties.put(name, value);
//        }
//    }
}
