package metrics.shipper;

/**
 * $RCSfile$
 * $Revision: 1583 $
 * $Date: 2005-07-03 17:55:39 -0300 (Sun, 03 Jul 2005) $
 *
 * Copyright (C) 2004 Jive Software. All rights reserved.
 *
 * This software is published under the terms of the GNU Public License (GPL),
 * a copy of which is included in this distribution.
 */

import metrics.shipper.ampq.IAmqpConnectionManager;
import metrics.shipper.util.Version;

import java.util.Date;


/**
 * Implements the server info for a basic server. Optimization opportunities
 * in reusing this object the data is relatively static.
 *
 * @author Iain Shigeoka
 */
public class ShipperAgentInfoImpl implements ShipperAgentInfo {

    private Date startDate;
    private Date stopDate;
    private String name;
    private Version ver;
    private IAmqpConnectionManager connectionManager;

    /**
     * Simple constructor
     *
     * @param serverName        the server's serverName (e.g. example.org).
     * @param version           the server's version number.
     * @param startDate         the server's last start time (can be null indicating
     *                          it hasn't been started).
     * @param stopDate          the server's last stop time (can be null indicating it
     *                          is running or hasn't been started).
     * @param connectionManager the object that keeps track of the active ports.
     */
    public ShipperAgentInfoImpl(String serverName, Version version, Date startDate, Date stopDate,
                                IAmqpConnectionManager connectionManager) {
        this.name = serverName;
        this.ver = version;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.connectionManager = connectionManager;
    }

    public Version getVersion() {
        return ver;
    }

    /*
     * Change this because we want to get the name from configuration.
     */
    public String getName() {
        return name;
    }

    /*public String getName() {
        if("".equals(name) || name == null){
            name = JiveGlobals.GetServerName();
        }

        Log.debug("XMPPServerInfo Server Name: " + name);
        return name;
    }*/

    /*
     * Comment this since we don't want to put server name in metrics.server.database.
     */
    public void setName(String serverName) {
//        name = serverName;
//        if (serverName == null) {
//            JiveGlobals.deleteProperty("xmpp.domain");
//        }
//        else {
//            JiveGlobals.setProperty("xmpp.domain", serverName);
//        }
    }

    public Date getLastStarted() {
        return startDate;
    }

    public Date getLastStopped() {
        return stopDate;
    }

//    public Collection<ServerPort> getServerPorts() {
//        if (connectionManager == null) {
//            return Collections.emptyList();
//        }
//        else {
//            return connectionManager.getPorts();
//        }
//    }
}