package metrics.shipper.modules;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import metrics.shipper.ampq.IAmpqConnectionFactory;
import metrics.shipper.ampq.RabbitMq.RabbitMq;
import metrics.shipper.ampq.provider.AmpqConnectionFactoryProvider;
import metrics.shipper.ampq.provider.AmpqConnectionType;

/**
 * Created by ali on 6/9/14.
 */
public class RabbitMqConnectionManagerModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(IAmpqConnectionFactory.class).
                annotatedWith(RabbitMq.class).
                toProvider(AmpqConnectionFactoryProvider.class);
        bindConstant().annotatedWith(Names.named("provider type")).to(AmpqConnectionType.RabbitMq);



    }
}
