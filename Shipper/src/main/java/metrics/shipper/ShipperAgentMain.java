package metrics.shipper;

import com.google.common.util.concurrent.Monitor;
import metrics.shipper.nio.files.event.PathEventContext;
import metrics.shipper.nio.files.event.PathEventSubscriber;
import com.google.common.eventbus.Subscribe;
import metrics.shipper.util.MapperSingleton;
import org.dozer.Mapper;

import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 6/3/14.
 */
public class ShipperAgentMain {
    static final ClassLoader loader = ShipperAgentMain.class.getClassLoader();
    static final Mapper mapper = MapperSingleton.getInstance();
    static final boolean isShutDownRequested = false;
    public static void main(String[] args){




        ShipperAgent shipperAgent = new ShipperAgent();

//
//        DirectoryEventWatcherImpl dirWatcher;
//        CountDownLatch doneSignal;
//        TestSubscriber subscriber;
//        //eventBus = new EventBus();
//        Path basePath = Paths.get("/home/ali/Desktop/test");
//        Injector injector = Guice.createInjector(new DirectoryEventModule(basePath.toString()));
//        DirectoryEventWatcher directoryEventWatcher;
//        directoryEventWatcher = injector.getInstance(DirectoryEventWatcher.class);
//
//        EventBus eventBus = injector.getInstance(EventBus.class);
//        subscriber = new TestSubscriber();
//        eventBus.register(subscriber);
//        try {
//            directoryEventWatcher.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        try {
//            Thread.sleep(60000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Monitor monitor = new Monitor();

        Monitor.Guard isShutDownGuard = new Monitor.Guard(monitor) {
            @Override
            public boolean isSatisfied() {
                return isShutDownRequested;
            }
        };
        try {
            monitor.enterWhen(isShutDownGuard);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //producerServer.start();

        URL resource = loader.getResource("configuration.json");
        Path configPath = Paths.get(resource.getPath());
//        ObjectMapper json = new ObjectMapper();
        java.io.File file = configPath.toFile();
        Path path = Paths.get(file.getPath());
        System.out.println(file.toPath());
//        Configuration config = new Configuration();
//        String content = "{\"DataSources\":[{\"Name\":\"ds Name\",\"Uri\":\"ampq\"},{\"Name\":\"ds Name\",\"Uri\":\"ampq\"}],\"Inputs\":[{\"Type\":\"File\",\"Name\":\"name\",\"StartPosition\":\"end\",\"FileRegexPattern\":\".*\",\"Paths\":[\"path1\",\"path2\"]},{\"Type\":\"File\",\"Name\":\"name\",\"StartPosition\":\"end\",\"FileRegexPattern\":\".*\",\"Paths\":[\"path1\",\"path2\"]}],\"Outputs\":[{\"Type\":\"RabbitMq\",\"Name\":\"name\",\"IsDurable\":false,\"Exchange\":\"exchange\",\"Workers\":0,\"DataSource\":\"ds\"}]}";
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.setPropertyNamingStrategy(
//                new CamelCaseNamingStrategy());
//        try {
//            config = mapper.readValue(file, Configuration.class);
//        } catch (IOException e) {
//            e.printStackTrace();
//            System.out.println(e.getMessage());
//        }


//        try {
//            Utils.objectMapper.writeValue(System.out,config);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


//        List<DataSource> dataSourceList = Lists.newLinkedList();
//        DataSource ds = new DataSource();
//        ds.setName("ds Name");
//        ds.setUri("ampq");
//        dataSourceList.add(ds);
//        dataSourceList.add(ds);
//        List<IInput> inputList = Lists.newLinkedList();
//        File input = new File();
//        input.setName("name");
//        List<String> paths = Lists.newLinkedList();
//        paths.add("path1");
//        paths.add("path2");
//        input.setPathListInString(paths);
//        inputList.add(input);
//        inputList.add(input);
//        List<IOutput> outputList = Lists.newLinkedList();
//        RabbitMq rabbitmq = new RabbitMq();
//        rabbitmq.setName("name");
//        rabbitmq.setDataSource("ds");
//        rabbitmq.setExchange("exchange");
//        outputList.add(rabbitmq);
//
//        config.setDataSourceList(dataSourceList);
//        config.setInputList(inputList);
//        config.setOutputList(outputList);
//        //ObjectMapper mapper = new ObjectMapper();
//
////        mapper.setPropertyNamingStrategy(
////                new CamelCaseNamingStrategy());
//        try {
//            mapper.writeValue(System.out,config);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        List<Configuration.FileSource> fileSourceList = config.getInputList();
//        config.setInputList(fileSourceList);
//        Configuration.FileSource fileSource;
//        fileSource = new Configuration.FileSource();
//        fileSource.setDirectoryPath("Directory");
//        fileSource.setFileRegexPattern("Path Regex");
//        fileSourceList.add(fileSource);
//        fileSourceList.add(fileSource);
//        List<Configuration.DataSource> dataSourceList = config.getDataSourceList();
//        Configuration.DataSource dataSource = new Configuration.DataSource();
//        dataSource.setUri("amqp://guest:guest@localhost:5672");
//        dataSourceList.add(dataSource);
//        AmqpConnectionFactory factory = new AmqpConnectionFactory();
//
//        try{
//            mapper.map(dataSource,factory);
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//            System.out.println(e.getStackTrace());
//        }

        //RabbitMqConnectionManagerImpl rabbitMqManager = new RabbitMqConnectionManagerImpl(factory.get());
        //rabbitMqManager.start();

//        try {
//            //json.writeValue(System.out,config);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        WatchService watcher = null;
        try {
            watcher = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path dir = null;
//        try {
////            WatchKey key = dir.register(watcher,
////                    ENTRY_CREATE,
////                    ENTRY_DELETE,
////                    ENTRY_MODIFY);
//        } catch (IOException x) {
//            System.err.println(x);
//        }

        //EventBus eventBus;
//        DirectoryEventWatcherImpl dirWatcher;
//        CountDownLatch doneSignal;
//        TestSubscriber subscriber;
//        //eventBus = new EventBus();
//        Path basePath = Paths.get("/home/ali/Desktop");
//        Injector injector = Guice.createInjector(new DirectoryEventModule(basePath.toString()));
//        DirectoryEventWatcher directoryEventWatcher;
//        directoryEventWatcher = injector.getInstance(DirectoryEventWatcher.class);
//
//        EventBus eventBus = injector.getInstance(EventBus.class);
//        subscriber = new TestSubscriber();
//        eventBus.register(subscriber);
//        try {
//            directoryEventWatcher.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }
    public static class TestSubscriber implements PathEventSubscriber {
        List<PathEventContext> pathEvents = new ArrayList<PathEventContext>();

        @Override
        @Subscribe
        public void handlePathEvents(PathEventContext pathEventContext) {
            pathEvents.add(pathEventContext);
            System.out.println(pathEventContext.getWatchedDirectory().getFileName());
            //doneSignal.countDown();
        }

    }
}
