package metrics.shipper.ampq.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import metrics.shipper.ampq.IAmpqConnectionFactory;
import metrics.shipper.ampq.RabbitMq.RabbitMqConnectionFactory;

/**
 * Created by ali on 6/9/14.
 */
public class AmpqConnectionFactoryProvider implements Provider<IAmpqConnectionFactory> {
    @Inject
    @Named("provider type")
    AmpqConnectionType type;
    public AmpqConnectionFactoryProvider(){

    }
    public AmpqConnectionFactoryProvider(AmpqConnectionType type){
        this.type = type;
    }
    @Override
    public IAmpqConnectionFactory get() {
        switch (type){
            case RabbitMq:
                return new RabbitMqConnectionFactory();
        }
        return null;
    }
}
