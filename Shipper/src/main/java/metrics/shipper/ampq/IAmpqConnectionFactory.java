package metrics.shipper.ampq;

import com.google.inject.name.Named;

/**
 * Created by ali on 6/5/14.
 */
public interface IAmpqConnectionFactory {
    @Named("uri")
    public void setUri(String uri);
    //public void close();
}
