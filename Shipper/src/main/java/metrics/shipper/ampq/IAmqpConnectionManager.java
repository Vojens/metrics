package metrics.shipper.ampq;

import metrics.shipper.ChannelCallable;

/**
 * Created by ali on 6/4/14.
 */
public interface IAmqpConnectionManager {
    public void start();
    public void stop();
    <T> T call(final ChannelCallable<T> callable);
}
