package metrics.shipper.ampq.RabbitMq;

import metrics.shipper.ampq.IAmpqConnectionFactory;
import com.rabbitmq.client.ConnectionFactory;
import org.dozer.Mapping;

/**
 * Created by ali on 6/4/14.
 */
public class RabbitMqConnectionFactory extends ConnectionFactory implements IAmpqConnectionFactory {
    //private ConnectionFactory connectionFactory;

    @Mapping("uri")
    private String uri;
    public RabbitMqConnectionFactory(){
        super();
    }
    public RabbitMqConnectionFactory(String uri){
        super();
        setUri(uri);
    }

//    public ConnectionFactory get(){
//        try {
//            connectionFactory.setUri(uri);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (KeyManagementException e) {
//            e.printStackTrace();
//        }
//        return this.connectionFactory;
//    }

    @Mapping("uri")
    @Override
    public void setUri(String uri) {
        this.uri = uri;
    }


}
