package metrics.shipper.nio.IO.input;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import metrics.shipper.nio.IO.IIO;

/**
 * Created by ali on 6/9/14.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = File.class),
        @JsonSubTypes.Type(value = FileSize.class),
})
public interface IInput extends IIO {
    void register();

}
