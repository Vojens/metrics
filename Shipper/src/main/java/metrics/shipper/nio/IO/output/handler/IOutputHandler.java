package metrics.shipper.nio.IO.output.handler;

import com.google.common.eventbus.Subscribe;
import metrics.shipper.nio.IO.IIO;
import metrics.shipper.nio.IO.event.IEvent;

/**
 * Created by ali on 6/10/14.
 */
public interface IOutputHandler extends IIO {
    @Subscribe
    void submit(final IEvent event);
}
