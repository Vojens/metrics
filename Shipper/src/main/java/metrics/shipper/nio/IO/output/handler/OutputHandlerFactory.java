package metrics.shipper.nio.IO.output.handler;

import com.google.common.eventbus.EventBus;
import metrics.shipper.nio.IO.output.IOutput;
import metrics.shipper.nio.IO.output.RabbitMq;
import metrics.shipper.nio.IO.output.Stdout;

/**
 * Created by ali on 6/10/14.
 */
public class OutputHandlerFactory {
    private IOutput output;
    private EventBus eventBus;
    public OutputHandlerFactory(final IOutput output, final EventBus eventBus){
        this.output = output;
        this.eventBus = eventBus;
    }
    public IOutputHandler get(){
        if(output instanceof RabbitMq){
            return new RabbitMqHandler((RabbitMq) output,eventBus);
        }else if(output instanceof Stdout){
            return new StdoutHandler((Stdout)output,eventBus);
        }
        //add more if necessary
        return null;
    }
}
