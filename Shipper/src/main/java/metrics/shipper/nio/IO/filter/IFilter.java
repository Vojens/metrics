package metrics.shipper.nio.IO.filter;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import metrics.shipper.nio.IO.IIO;

/**
 * Created by ali on 6/11/14.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = DateFilter.class),
})
public interface IFilter extends IIO{
}
