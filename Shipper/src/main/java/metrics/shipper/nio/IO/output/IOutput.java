package metrics.shipper.nio.IO.output;

/**
 * Created by ali on 6/9/14.
 */

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import metrics.shipper.nio.IO.IIO;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = RabbitMq.class),
        @JsonSubTypes.Type(value = Stdout.class),
})
public interface IOutput extends IIO {

}
