package metrics.shipper.nio.IO.event;


import java.io.Reader;

/**
 * Created by ali on 6/11/14.
 */
public class ReaderEvent implements IEvent{
    private Reader reader;
    public ReaderEvent(Reader reader){
        this.reader = reader;
    }
    public Reader getReader(){
        return reader;
    }
}
