package metrics.shipper.nio.IO;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by ali on 6/9/14.
 */
public abstract class AbstractIO implements IIO{
    protected String name;

    protected AbstractIO(AbstractIO io){
        this.setName(io.getName());
        for (Map.Entry<String, Object> entry : io.getAdditionalProperties().entrySet())
        {
            this.setAdditionalProperty(entry.getKey(),entry.getValue());
        }
    }

    protected AbstractIO() {
        super();
    }

    @JsonAnyGetter
    @Override
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    @Override
    public void setAdditionalProperty(String name, Object value) {
        additionalProperties.put(name, value);
    }

    @JsonProperty("Name")
    @Override
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    @Override
    public void setName(String name) {
        this.name = name;
    }
}
