package metrics.shipper.nio.IO.input.handler;

import com.google.common.collect.Maps;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.common.io.ByteSource;
import com.google.common.io.Files;
import metrics.shipper.nio.IO.event.IEvent;
import metrics.shipper.nio.IO.event.ReaderEvent;
import metrics.shipper.nio.files.directory.event.PathEvent;
import metrics.shipper.nio.files.event.PathEventContext;
import metrics.shipper.nio.files.event.PathEventSubscriber;
import metrics.shipper.util.Utils;
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.Map;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 6/10/14.
 */
public class FileHandler implements IInputHandler,PathEventSubscriber {
    private static Logger logger = Logger.getLogger(FileHandler.class);
    private EventBus eventBus;
    private Map<String, Long> fileMap = Maps.newConcurrentMap();
    private Path dirPath;
    private Pattern regexPattern;
    private static DateTimeFormatter formatter = ISODateTimeFormat.dateTime();
    public FileHandler(EventBus eventBus, Path dirPath, String fileRegexPattern){   //TODO add regex

        this.eventBus= eventBus;
        this.dirPath = dirPath;
        this.regexPattern = Pattern.compile(Utils.grokDictionary.digestExpression(fileRegexPattern));
        initFileMap();
    }


    public void initFileMap(){ //TODO add regex
        checkNotNull(dirPath, "Path cannot be null");
        String files;
        File folder = dirPath.toFile();
        File[] fileList = folder.listFiles();
        for (int i = 0; i < fileList.length; i++)
        {

            if (fileList[i].isFile() && regexPattern.matcher(fileList[i].getName()).matches())
            {

                fileMap.put(fileList[i].getAbsolutePath(),fileList[i].length());
            }
        }
    }

    @Override
    @Subscribe
    public void handlePathEvents(PathEventContext pathEventContext) {
        if(!pathEventContext.isValid()){
            return;
        }
        //event.getEventTarget()
        for (PathEvent event : pathEventContext.getEvents()){

            Path path = event.getEventTarget();
            if(!regexPattern.matcher(path.getFileName().toString()).matches()){
                continue;
            }
            //System.out.println(path.getFileName());
//            if(!path.getFileName().toString().equals("test2.log")){
//                continue;
//            }
            File file = new File(pathEventContext.getWatchedDirectory().toString(),path.getFileName().toString());

            switch (event.getType().name()) {
                case "ENTRY_CREATE":
                    //fileMap.containsKey(event.getEventTarget().toString());
                    fileMap.put(event.getEventTarget().toAbsolutePath().toString(), 0L);
                    break;
                case "ENTRY_MODIFY":
                    //file.length();
                    //CharSource charSource = Files.asCharSource(file, Charset.defaultCharset());
                    //charSource.
                    String absolutePath = new File(dirPath.toAbsolutePath().toString(),event.getEventTarget().getFileName().toString()).toString();
                    long fileLength = file.length();
                    if(!fileMap.containsKey(absolutePath)){
                        fileMap.put(absolutePath,0L);
                    }
                    long startPos = fileMap.get(absolutePath);

                    if(fileLength-startPos == 0){
                        continue;
                    }
                    if(fileLength < startPos){
                        startPos = 0;
                    }
                    fileMap.put(absolutePath,fileLength);
                    Files.asByteSource(file).slice(startPos, file.length() - startPos);
                    ByteSource modifiedByteSource = Files.asByteSource(file).slice(startPos, file.length() - startPos);

                    InputStream inputStream = null;
                    int count = 0;
                    while(inputStream == null && count < 3)
                    {
                        try {
                            inputStream = modifiedByteSource.openStream();
                        } catch (IOException e) {
                            //e.printStackTrace();
//                            logger.error("Error in opening a stream to a file",e);
                        }
                        try {
                            Thread.sleep(0,(int)(Math.random()*100+1));
                        } catch (InterruptedException e) {
                        }
                        count++;
                    }
                    if(inputStream == null){
                        logger.error("Error in opening a stream to a file");
                    }
                    IEvent readerEvent = new ReaderEvent(new InputStreamReader(inputStream));
                    //StreamEvent streamEvent = new StreamEvent(inputStream);

                    eventBus.post(readerEvent);
                    break;
            }
        }


        //System.out.println(new Event("File Handler is interrupted"));
        //eventBus.post(new Event("Hello there. from File Handler"));
    }
}
