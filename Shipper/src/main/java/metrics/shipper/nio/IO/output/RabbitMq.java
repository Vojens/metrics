package metrics.shipper.nio.IO.output;

import com.fasterxml.jackson.annotation.*;
import metrics.shipper.nio.IO.IIO;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by ali on 6/9/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})
public class RabbitMq extends AbstractOutput{
    @JsonIgnore
    private boolean durable;
    @JsonIgnore
    private String exchange;
    @JsonIgnore
    private String exchangeType;
    @JsonIgnore
    private String rountingKey;
    @JsonIgnore
    private int workers;
    @JsonIgnore
    private String dataSource;
    @JsonIgnore
    private List<String> cc;
    @JsonIgnore
    private List<String> bcc;

    protected RabbitMq(RabbitMq rabbit) {
        super(rabbit);
        this.setDurable(rabbit.isDurable());
        this.setExchange(rabbit.getExchange());
        this.setExchangeType(rabbit.getExchangeType());
        this.setRountingKey(rabbit.getRountingKey());
        this.setWorkers(rabbit.getWorkers());
        this.setDataSource(rabbit.getDataSource());
        this.setCc(rabbit.getCc());
        this.setBcc(rabbit.getBcc());
    }

    public RabbitMq(){
        super();
        //super();
    }

    @JsonProperty("IsDurable")
    public boolean isDurable() {
        return durable;
    }

    @JsonProperty("IsDurable")
    public void setDurable(boolean durable) {
        this.durable = durable;
    }

    @JsonProperty("Exchange")
    public String getExchange() {
        return exchange;
    }

    @JsonProperty("Exchange")
    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    @JsonProperty("ExchangeType")
    public String getExchangeType() {
        return exchangeType;
    }

    @JsonProperty("ExchangeType")
    public void setExchangeType(String exchangeType) {
        this.exchangeType = exchangeType;
    }

    @JsonProperty("RoutingKey")
    public String getRountingKey() {
        return rountingKey;
    }

    @JsonProperty("RoutingKey")
    public void setRountingKey(String rountingKey) {
        this.rountingKey = rountingKey;
    }

    @JsonProperty("Workers")
    public int getWorkers() {
        return workers;
    }

    @JsonProperty("Workers")
    public void setWorkers(int workers) {
        this.workers = workers;
    }

    @JsonProperty("DataSource")
    public String getDataSource() {
        return dataSource;
    }

    @JsonProperty("DataSource")
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public IIO deepCopy() {
        return new RabbitMq(this);
    }

    @JsonProperty("CC")
    public List<String> getCc() {
        return cc;
    }

    @JsonProperty("CC")
    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    @JsonProperty("BCC")
    public List<String> getBcc() {
        return bcc;
    }

    @JsonProperty("BCC")
    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }
}
