package metrics.shipper.nio.IO.input;

import com.fasterxml.jackson.annotation.*;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.eventbus.EventBus;
import com.google.inject.Guice;
import com.google.inject.Injector;
import metrics.shipper.nio.IO.IIO;
import metrics.shipper.nio.IO.handler.BindingHandler;
import metrics.shipper.nio.IO.input.handler.FileSizeHandler;
import metrics.shipper.nio.files.directory.IDirectoryWatcher;
import metrics.shipper.nio.files.directory.interval.DirectoryIntervalModule;
import metrics.shipper.util.Utils;

import javax.annotation.Generated;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 * Created by ali on 8/21/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})

public class FileSize extends AbstractInput{
    @JsonIgnore
    protected BindingHandler bindingHandler;
    @JsonIgnore
    protected List<Path> pathList = Lists.newLinkedList();
    @JsonIgnore
    protected List<Path> excludeList = Lists.newLinkedList();
    @JsonIgnore
    protected String fileRegexPattern;
    @JsonIgnore
    private Long interval;

    @JsonIgnore
    private static Function<Path,String> PATH_TO_STRING_FUNCTION = new Function<Path,String>(){

        @Override
        public String apply(Path path) {
            return path.getFileName().toString();
        }
    };

    @JsonIgnore
    private static Function<String,Path> STRING_TO_PATH_FUNCTION = new Function<String, Path>() {
        @Override
        public Path apply(String s) {
            return Paths.get(s);
        }
    };

    public FileSize(){
        super();
        init();
    }

    protected FileSize(FileSize file){
        super(file);
//        this.setName(file.getName());
        this.setExcludeList(file.getExcludeList());
        this.setFileRegexPattern(file.getFileRegexPattern());
        this.setPathList(file.getPathList());
        this.setInterval(file.getInterval());
//        for (Map.Entry<String, Object> entry : file.getAdditionalProperties().entrySet())
//        {
//            this.setAdditionalProperty(entry.getKey(),entry.getValue());
//        }
    }
    @JsonIgnore
    public void init(){
        fileRegexPattern = ".*";
    }

    @JsonIgnore
    public List<Path> getPathList() {
        return pathList;
    }

    @JsonIgnore
    public void setPathList(List<Path> pathList) {
        this.pathList = pathList;
    }

    @JsonIgnore
    public List<Path> getExcludeList() {
        return excludeList;
    }

    @JsonIgnore
    public void setExcludeList(List<Path> excludeList) {
        this.excludeList = excludeList;
    }

    @JsonProperty("Paths")
    public List<String> getPathListInString(){

        List<String> transformed =
                Lists.newArrayList(Iterables.transform(getPathList(), this.PATH_TO_STRING_FUNCTION));
        return transformed;
    }

    @JsonProperty("Paths")
    public void setPathListInString(List<String> list){
        this.setPathList(Lists.newArrayList(Iterables.transform(list, STRING_TO_PATH_FUNCTION)));
    }

    @JsonProperty("FileRegexPattern")
    public String getFileRegexPattern() {
        return fileRegexPattern;
    }

    @JsonProperty("FileRegexPattern")
    public void setFileRegexPattern(String fileRegexPattern) {
        this.fileRegexPattern = fileRegexPattern;
    }

    @JsonProperty("Interval")
    public Long getInterval() {
        return interval;
    }

    @JsonProperty("Interval")
    public void setInterval(Long interval) {
        this.interval = interval;
    }


    //    @JsonIgnore
//    private List<DirectoryEventWatcher> directoryEventWatcher = Lists.newLinkedList();
    @JsonIgnore
    private Map<Path,PathInfo> pathMap = Maps.newLinkedHashMap();
//    @JsonIgnore
//    private EventBus eventBus;

    @Override
    @JsonIgnore
    public void register() {

        for(Path path : this.getPathList()){
            BindingHandler bindingHandler = new BindingHandler(Utils.configuration,this);
            FileSizeHandler fileSizeHandler;
            //eventBus = new EventBus();

            Injector injector = Guice.createInjector(new DirectoryIntervalModule(path.toString(),getInterval()));

            IDirectoryWatcher directoryIntervalWatcher = injector.getInstance(IDirectoryWatcher.class);



            EventBus eventBus = injector.getInstance(EventBus.class);
            fileSizeHandler = new FileSizeHandler(bindingHandler.getEventBus(),path, this.getFileRegexPattern());

            eventBus.register(fileSizeHandler);
            try {
                directoryIntervalWatcher.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
            PathInfo pathInfo = new PathInfo();
            pathInfo.bindingHandler = bindingHandler;
            pathInfo.interval = interval;
            pathInfo.eventBus = eventBus;
            pathInfo.fileSizeHandler = fileSizeHandler;
            pathMap.put(path,pathInfo);
        }
    }

    @Override
    public FileSize clone(){
        try {
            return (FileSize) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public IIO deepCopy() {
        return new FileSize(this);
    }

    @JsonIgnoreType
    private class PathInfo{
        public FileSizeHandler fileSizeHandler;
        public BindingHandler bindingHandler;
        public Long interval;
        public EventBus eventBus;
    }
    //TODO: implement setExcludeListInString and getExcludeListInString


}
