package metrics.shipper.nio.IO.handler;

import metrics.shipper.nio.IO.AbstractIO;

/**
 * Created by ali on 6/10/14.
 */
public abstract class AbstractIOHandler implements IIOHandler{


    protected AbstractIOHandler(AbstractIO io){

    }

    protected String name;
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
