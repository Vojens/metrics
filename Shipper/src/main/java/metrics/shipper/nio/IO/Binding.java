package metrics.shipper.nio.IO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by ali on 6/9/14.
 */
public class Binding {
    private String inputName;
    private String outputName;
    private List<String> filterNameList = Lists.newLinkedList();

    @JsonProperty("InputName")
    public String getInputName() {
        return inputName;
    }

    @JsonProperty("InputName")
    public void setInputName(String inputName) {
        this.inputName = inputName;
    }

    @JsonProperty("OutputName")
    public String getOutputName() {
        return outputName;
    }

    @JsonProperty("OutputName")
    public void setOutputName(String outputName) {
        this.outputName = outputName;
    }

    @JsonProperty("FilterNames")
    public List<String> getFilterNameList() {
        return filterNameList;
    }

    @JsonProperty("FilterNames")
    public void setFilterNameList(List<String> filterNameList) {
        this.filterNameList = filterNameList;
    }

//    @JsonProperty("FilterName")
//    public String getFilterName() {
//        return filterName;
//    }
//
//    @JsonProperty("FilterName")
//    public void setFilterName(String filterName) {
//        this.filterName = filterName;
//    }
}
