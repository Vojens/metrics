package metrics.shipper.nio.IO.handler;

import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;
import com.google.common.eventbus.AsyncEventBus;
import metrics.shipper.Configuration;
import metrics.shipper.nio.IO.Binding;
import metrics.shipper.nio.IO.input.IInput;
import metrics.shipper.nio.IO.output.IOutput;
import metrics.shipper.nio.IO.event.Event;
import metrics.shipper.nio.IO.output.handler.IOutputHandler;
import metrics.shipper.nio.IO.output.handler.OutputHandlerFactory;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 6/10/14.
 */
public class BindingHandler implements IBindingHandler{
    protected IInput input;
    protected List<IOutputHandler> outputHandlerList = Lists.newLinkedList();
    private AsyncEventBus eventBus;
    public BindingHandler(final Configuration config, final IInput input){
        checkNotNull(config,"Configuration cannot be null");
        checkNotNull(input,"Input cannot be null");
        this.input = input;

//        Collection<Binding> bindingCollection = Collections2.filter(config.getBindingList(), new Predicate<Binding>() {
//            @Override
//            public boolean apply(Binding input) {
//                if (input.getInputName().equals(input.getInputName())) {
//                    return true;
//                }
//                return false;
//            }
//        });
        final List<Binding> bindingList = FluentIterable.from(config.getBindingList()).filter(new Predicate<Binding>() {
            @Override
            public boolean apply(Binding input) {
                if (input.getInputName().equals(input.getInputName())) {
                    return true;
                }
                return false;
            }
        }).toList();
        List<IOutput> tempOutputList = FluentIterable.from(config.getOutputList()).filter(new Predicate<IOutput>() {
            @Override
            public boolean apply(IOutput output) {
                for(int i = 0; i < bindingList.size(); i++){
                    if(bindingList.get(i).getOutputName().equals(output.getName())){
                        return true;
                    }
                }
                return false;
            }
        }).toList();

        ExecutorService executorService = Executors.newFixedThreadPool(tempOutputList.size() < 1 ? 1 : tempOutputList.size() );

        eventBus = new AsyncEventBus(executorService);
        for(IOutput output : tempOutputList){
            //outputHandlerList.add((IOutput) output.deepCopy());
            IOutputHandler iOutputHandler = new OutputHandlerFactory(output, getEventBus()).get();
            eventBus.register(iOutputHandler);
            eventBus.post(new Event("Hello"));
            outputHandlerList.add(iOutputHandler);
        }

    }

    public AsyncEventBus getEventBus() {
        return eventBus;
    }
}
