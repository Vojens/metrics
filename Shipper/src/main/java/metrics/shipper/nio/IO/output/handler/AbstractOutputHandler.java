package metrics.shipper.nio.IO.output.handler;

import com.google.common.eventbus.EventBus;

import metrics.shipper.nio.IO.IIO;
import metrics.shipper.nio.IO.output.AbstractOutput;

/**
 * Created by ali on 6/10/14.
 */
public abstract class AbstractOutputHandler extends AbstractOutput implements IOutputHandler{
    protected AbstractOutputHandler(AbstractOutput io, EventBus eventbus) {
        super(io);
        //super(metrics.server.io);
        //this.setCodec(metrics.server.io.getCodec());
    }

    @Override
    public IIO deepCopy() {
        return null;
    }


}
