package metrics.shipper.nio.IO.codec;

/**
 * Created by ali on 6/12/14.
 */


public interface ICodec {
    void setMessage(Object message);
    //void setType(Object type);
    String toString();
}
