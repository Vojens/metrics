package metrics.shipper.nio.IO.codec;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * Created by ali on 6/12/14.
 */
public class PlainCodec extends AbstractCodec {
    private Object message;

    @Override
    public void setMessage(Object message) {
        this.message = message;
    }

    public Object getMessage() {
        return this.message;
    }

    @Override
    public String toString(){
        return message.toString();
    }

}
