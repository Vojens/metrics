package metrics.shipper.nio.IO.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import metrics.shipper.nio.IO.IIO;

import javax.annotation.Generated;

/**
 * Created by ali on 6/12/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})
public class Stdout extends AbstractOutput{
    public Stdout(){
        super();
    }
    public Stdout(Stdout stdout){
        super(stdout);
    }
    @Override
    public IIO deepCopy() {
        return new Stdout(this);
    }
}
