package metrics.shipper.nio.IO.output.handler;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.common.io.ByteStreams;
import com.google.common.io.CharStreams;
import metrics.shipper.nio.IO.codec.CodecFactory;
import metrics.shipper.nio.IO.codec.ICodec;
import metrics.shipper.nio.IO.event.Event;
import metrics.shipper.nio.IO.event.IEvent;
import metrics.shipper.nio.IO.event.ReaderEvent;
import metrics.shipper.nio.IO.event.StreamEvent;
import metrics.shipper.nio.IO.output.Stdout;

import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 6/12/14.
 */
public class StdoutHandler extends Stdout implements IOutputHandler {
    public StdoutHandler(Stdout stdout, EventBus eventBus){
        super(stdout);
        checkNotNull(eventBus,"Event Bus cannot be null").register(this);
    }
    @Override
    @Subscribe
    public void submit(IEvent event) {
        Object message = null;
        if(event instanceof Event){
            message = ((Event) event).getMessage();
        }else if(event instanceof StreamEvent){
            try {
                message = ByteStreams.toByteArray(((StreamEvent) event).getStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(event instanceof ReaderEvent){
            try {
                message = CharStreams.toString(((ReaderEvent) event).getReader());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        ICodec codec = CodecFactory.get(super.getCodec());
        codec.setMessage(message);
        System.out.println(codec.toString());
    }
}
