package metrics.shipper.nio.IO.codec;

/**
 * Created by ali on 6/12/14.
 */
public class CodecFactory {
    public static ICodec get(String codeName){
        switch (codeName.toLowerCase()){
            case "json":
                return new JsonCodec();
            case "plain":
                return new PlainCodec();
            default:
                return new PlainCodec();
        }
    }
}
