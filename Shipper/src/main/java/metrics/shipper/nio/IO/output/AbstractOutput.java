package metrics.shipper.nio.IO.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import metrics.shipper.nio.IO.AbstractIO;
import metrics.shipper.nio.IO.IIO;

/**
 * Created by ali on 6/9/14.
 */

public abstract class AbstractOutput extends AbstractIO implements IIO, IOutput {
    private String codec = "Plain";

    protected AbstractOutput(){

    }
    protected AbstractOutput(AbstractOutput io) {
        super(io);
        this.setCodec(io.getCodec());
    }

    @JsonProperty("Codec")
    public String getCodec() {
        return codec;
    }

    @JsonProperty("Codec")
    public void setCodec(String codec) {
        this.codec = codec;
    }
}
