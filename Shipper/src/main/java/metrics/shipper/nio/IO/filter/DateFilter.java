package metrics.shipper.nio.IO.filter;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.google.common.collect.Lists;
import metrics.shipper.nio.IO.IIO;
import org.joda.time.format.DateTimeFormat;

import javax.annotation.Generated;
import java.util.List;

/**
 * Created by ali on 6/11/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({

})
public class DateFilter extends AbstractFilter {
    @JsonIgnore
    private Match match;
    @JsonIgnore
    private String target = "@Timestamp";

    public DateFilter(){
        super();
    }

    @JsonProperty("Match")
    public Match getMatch() {
        return match;
    }

    @JsonProperty("Match")
    public void setMatch(Match match) {
        this.match = match;
    }

    @JsonProperty("Target")
    public String getTarget() {
        return target;
    }

    @JsonProperty("Target")
    public void setTarget(String target) {
        this.target = target;
    }

    @Override
    public IIO deepCopy() {
        return null;
        //TODO: implment later by adding DateFilter(DateFilter dateFilter)
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Generated("metrics")
    @JsonPropertyOrder({

    })
    public static class Match{
        @JsonIgnore
        private String field;
        @JsonIgnore
        private List<String> formats = Lists.newLinkedList();;

        public Match(){

        }

        @JsonProperty("Field")
        public String getField() {
            return field;
        }
        @JsonProperty("Field")
        public void setField(String field) {
            this.field = field;
        }

        @JsonProperty("Formats")
        public List<String> getFormats() {
            return formats;
        }

        @JsonProperty("Formats")
        public void setFormats(List<String> formats) {
            this.formats = formats;
        }
    }
}
