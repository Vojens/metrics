package metrics.shipper.nio.IO;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by ali on 6/9/14.
 */
public interface IIO{
    Map<String, Object> additionalProperties = Maps.newHashMap();
    Map<String, Object> getAdditionalProperties();
    void setAdditionalProperty(String name, Object value);

    void setName(String name);
    String getName();

    IIO deepCopy();
}
