package metrics.shipper.nio.IO.codec;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Maps;
import metrics.shipper.util.Utils;
import metrics.shipper.util.Version;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import javax.annotation.Generated;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * Created by ali on 6/12/14.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("metrics")
@JsonPropertyOrder({


})
public class JsonCodec extends AbstractCodec{
    @JsonIgnore
    private Map<String,Object>  fieldMap= Maps.newHashMap();
    @JsonIgnore
    public JsonCodec(){
        initFieldMap();
    }
    @JsonIgnore
    protected void initFieldMap(){
        getFieldMap().put("Message", null);
        DateTimeFormatter formatter = ISODateTimeFormat.dateTime();
        getFieldMap().put("@Timestamp", formatter.print(DateTime.now()));
        getFieldMap().put("@Version", new Version(1,0,0, Version.ReleaseStatus.Alpha,0).getVersionString());
        String hostName = "";
        try {
            hostName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        hostName = Math.random() >= .5? "eccviewer101":"eccviewer102";
        getFieldMap().put("Host", hostName);

        //getFieldMap().put("LoadBalancer", )
    }

    @JsonProperty
    public Map<String, Object> getFieldMap() {
        return fieldMap;
    }

    @JsonProperty
    public void setFieldMap(Map<String, Object> fieldMap) {
        this.fieldMap = fieldMap;
    }

    @Override
    public void setMessage(Object message) {
        fieldMap.put("Message",message);
    }

    public void setType(Object type){fieldMap.put("Type",type);}

    @Override
    public String toString(){
        try {
            return Utils.objectMapper.writeValueAsString(fieldMap);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
