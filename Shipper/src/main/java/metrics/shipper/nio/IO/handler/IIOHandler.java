package metrics.shipper.nio.IO.handler;

/**
 * Created by ali on 6/10/14.
 */
public interface IIOHandler {
    void setName(String name);
    String getName();
}
