package metrics.shipper.nio.IO.input;

import metrics.shipper.nio.IO.AbstractIO;
import metrics.shipper.nio.IO.IIO;

/**
 * Created by ali on 6/9/14.
 */

public abstract class AbstractInput extends AbstractIO implements IIO,  IInput{

    protected AbstractInput(AbstractInput io) {
        super(io);
    }

    protected AbstractInput() {
        super();
    }
}
