package metrics.shipper.nio.files.directory.interval;

import com.google.common.eventbus.EventBus;
import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import metrics.shipper.nio.files.directory.IDirectoryWatcher;

import java.nio.file.Paths;

/**
 * Created by ali on 8/21/14.
 */
public class DirectoryIntervalWatcherProvider implements Provider<IDirectoryWatcher> {
    private EventBus eventBus;
    private String startPath;
    private Long interval;


    @Inject
    public DirectoryIntervalWatcherProvider(EventBus eventBus, @Named("START_PATH") String startPath, @Named("INTERVAL") Long interval) {
        this.eventBus = eventBus;
        this.startPath = startPath;
        this.interval = interval;
    }

    @Override
    public IDirectoryWatcher get() {
        return new DirectoryIntervalWatcherImpl(eventBus,Paths.get(startPath),interval);
    }
}
