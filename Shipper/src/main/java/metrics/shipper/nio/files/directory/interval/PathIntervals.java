package metrics.shipper.nio.files.directory.interval;

import com.google.common.collect.Lists;
import metrics.shipper.nio.files.interval.PathIntervalContext;

import java.nio.file.Path;
import java.util.List;

/**
 * Created by ali on 8/21/14.
 */
public class PathIntervals implements PathIntervalContext{

    private final List<PathInterval> pathIntervals = Lists.newArrayList();
    private final Path watchedDirectory;
    private final boolean isValid;

    public PathIntervals(boolean valid, Path watchedDirectory) {
        isValid = valid;
        this.watchedDirectory = watchedDirectory;
    }

    @Override
    public boolean isValid(){
        return isValid;
    }

    @Override
    public Path getWatchedDirectory(){
        return watchedDirectory;
    }

    @Override
    public List<PathInterval> getIntervals() {
        return pathIntervals;
    }

    public void add(PathInterval pathInterval) {
        pathIntervals.add(pathInterval);
    }
}
