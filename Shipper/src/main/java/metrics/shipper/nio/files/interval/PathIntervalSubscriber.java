package metrics.shipper.nio.files.interval;

/**
 * Created by ali on 8/21/14.
 */
public interface PathIntervalSubscriber {
    void handlePathEvents(PathIntervalContext pathIntervalContext);
}
