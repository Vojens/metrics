package metrics.shipper.nio.files.directory.interval;

import org.joda.time.DateTime;

import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Created by ali on 8/21/14.
 */
public class PathInterval {
    private final Path eventTarget;
    private final BasicFileAttributes fileAttributes;
    private final DateTime attributesDateTime;

    public PathInterval(Path eventTarget, BasicFileAttributes fileAttributes, DateTime attributesDateTime) {
        this.eventTarget = eventTarget;
        this.fileAttributes = fileAttributes;
        this.attributesDateTime = attributesDateTime;
    }

    public Path getEventTarget() {
        return eventTarget;
    }

    public BasicFileAttributes getFileAttributes() {
        return fileAttributes;
    }

    public DateTime getAttributesDateTime() {
        return attributesDateTime;
    }
}
