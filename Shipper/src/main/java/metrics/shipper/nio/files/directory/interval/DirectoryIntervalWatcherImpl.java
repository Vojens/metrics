package metrics.shipper.nio.files.directory.interval;

import com.google.common.collect.Lists;
import com.google.common.eventbus.EventBus;
import metrics.shipper.nio.files.directory.IDirectoryWatcher;
import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Created by ali on 8/21/14.
 */
public class DirectoryIntervalWatcherImpl implements IDirectoryWatcher {

    private FutureTask<Integer> watchTask;
    private EventBus eventBus;
    //private WatchService watchService;
    private volatile boolean keepWatching = true;
    private Path startPath;
    private Duration duration;


    DirectoryIntervalWatcherImpl(EventBus eventBus, Path startPath, Long interval) {
        this.eventBus = Objects.requireNonNull(eventBus);
        this.startPath = Objects.requireNonNull(startPath);
        this.duration = new Duration(interval);
    }

    @Override
    public void start() throws IOException {
        createWatchTask();
        startWatching();
    }

    @Override
    public boolean isRunning() {
        return watchTask != null && !watchTask.isDone();
    }

    @Override
    public void stop() {
        keepWatching = false;
    }

    //Used for testing purposes
    Integer getEventCount() {
        try {
            return watchTask.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }


    private void createWatchTask() {
        watchTask = new FutureTask<>(new Callable<Integer>() {
            private int totalEventCount;

            @Override
            public Integer call() throws Exception {
                while (keepWatching) {
                    try{
                        Thread.sleep(duration.getMillis());
                    }catch(InterruptedException e){
                        System.out.println(e);
                    }

                    PathVisitor pathVisitor = new PathVisitor();
                    boolean isValid = false;
                    try{
                        Files.walkFileTree(startPath, pathVisitor);
                        isValid = true;
                    }catch(Exception e){
                        System.out.println(e);
                    }
                    DateTime attributesDateTime = DateTime.now();
                    PathIntervals pathIntervals = new PathIntervals(isValid,startPath);

                    for (Path path : pathVisitor.getPathList()) {
                        pathIntervals.add(new PathInterval(path,Files.readAttributes(path, BasicFileAttributes.class),attributesDateTime));
                    }
                    eventBus.post(pathIntervals);
                }
                return totalEventCount;
            }
        });
    }

    private void startWatching() {
        new Thread(watchTask).start();
    }

    private class PathVisitor extends SimpleFileVisitor<Path> {
        private List<Path> pathList;
        public PathVisitor(){
            pathList = Lists.newLinkedList();
        }
//        @Override
//        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
//            pathList.add(dir);
//            return FileVisitResult.CONTINUE;
//        }

        @Override
        public FileVisitResult visitFile(Path dir, BasicFileAttributes attrs)
                throws IOException
        {
            Objects.requireNonNull(dir);
            Objects.requireNonNull(attrs);
            pathList.add(dir);
            return FileVisitResult.CONTINUE;
        }


        public List<Path> getPathList() {
            return pathList;
        }
    }
}
