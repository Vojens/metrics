package metrics.shipper.nio.files.interval;

import metrics.shipper.nio.files.directory.interval.PathInterval;

import java.nio.file.Path;
import java.util.List;

/**
 * Created by ali on 8/21/14.
 */
public interface PathIntervalContext {
    boolean isValid();

    Path getWatchedDirectory();

    List<PathInterval> getIntervals();
}
