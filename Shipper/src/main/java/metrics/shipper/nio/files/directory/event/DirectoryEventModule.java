package metrics.shipper.nio.files.directory.event;

import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import metrics.shipper.nio.files.directory.IDirectoryWatcher;

/**
 * Created by IntelliJ IDEA.
 * User: bbejeck
 * Date: 2/25/12
 * Time: 9:30 PM
 */

public class DirectoryEventModule extends AbstractModule {
    private String path;
    public DirectoryEventModule(String path){
        this.path = path;
    }
    @Override
    protected void configure() {
        //bind(EventBus.class).in(Singleton.class);
        bind(EventBus.class).in(Singleton.class);
        bind(String.class).annotatedWith(Names.named("START_PATH")).toInstance(path);
        bind(IDirectoryWatcher.class).toProvider(DirectoryEventWatcherProvider.class);
    }
}
