package metrics.shipper.nio.files.directory.interval;

import com.google.common.eventbus.EventBus;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import metrics.shipper.nio.files.directory.IDirectoryWatcher;

/**
 * Created by ali on 8/21/14.
 */
public class DirectoryIntervalModule extends AbstractModule {
    private String path;
    private Long interval;
    public DirectoryIntervalModule(String path, Long interval){
        this.path = path;
        this.interval = interval;
    }
    @Override
    protected void configure() {
        bind(EventBus.class).in(Singleton.class);
        bind(String.class).annotatedWith(Names.named("START_PATH")).toInstance(path);
        bind(Long.class).annotatedWith(Names.named("INTERVAL")).toInstance(interval);
        bind(IDirectoryWatcher.class).toProvider(DirectoryIntervalWatcherProvider.class);
    }
}
