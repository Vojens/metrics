package metrics.shipper.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import metrics.shipper.Configuration;
import org.aicer.grok.dictionary.GrokDictionary;
import org.dozer.Mapper;

/**
 * Created by ali on 6/4/14.
 */
public class Utils {
    public final static Mapper mapper = MapperSingleton.getInstance();
    public final static ObjectMapper objectMapper = ObjectMapperSingleton.getInstance();
    public static Configuration configuration;
    public final static GrokDictionary grokDictionary = GrokDictionarySingleton.getInstance();
}
