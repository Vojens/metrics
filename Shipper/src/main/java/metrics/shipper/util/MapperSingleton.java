package metrics.shipper.util;

import org.dozer.DozerBeanMapper;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ali on 6/4/14.
 */

public class MapperSingleton implements Serializable {
    private static final long serialVersionUID = 1L;
    private Mapper mapper;
    private MapperSingleton() {
        // private constructor
        mapper = DozerBeanMapperSingletonWrapper.getInstance();
        List mappingFiles = new ArrayList();
        mappingFiles.add("dozerBeanMapping.xml");
        ((DozerBeanMapper)mapper).setMappingFiles(mappingFiles);
    }

    private static class MapperSingletonHolder {
        public static final Mapper INSTANCE = new MapperSingleton().mapper;
    }

    public static Mapper getInstance() {
        return MapperSingletonHolder.INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }
}