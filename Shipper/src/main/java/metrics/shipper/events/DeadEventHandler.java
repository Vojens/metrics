package metrics.shipper.events;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * Created by ali on 6/10/14.
 */
public class DeadEventHandler {
    //private static final Logger logger = Logger.getLogger(DeadEventHandler.class);

    public DeadEventHandler(EventBus eventBus) {
        eventBus.register(this);
    }

    @Subscribe
    public void handleUnsubscribedEvent(DeadEvent deadEvent){
        System.out.println("No subscribers for "+deadEvent.getEvent());
        //logger.warn("No subscribers for "+deadEvent.getEvent());
    }
}
