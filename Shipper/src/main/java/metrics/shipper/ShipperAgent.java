package metrics.shipper;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import metrics.shipper.ampq.IAmqpConnectionManager;
import metrics.shipper.container.Module;
import metrics.shipper.modules.RabbitMqConnectionManagerModule;
import metrics.shipper.nio.IO.input.IInput;
import metrics.shipper.util.Utils;
import metrics.shipper.util.Version;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by ali on 6/4/14.
 */
public class ShipperAgent {
    private Logger logger = Logger.getLogger(ShipperAgent.class.getName());
    private Version version;
    private boolean initialized = false;
    private String name;
    private Date startDate;
    private Date stopDate;
    private ClassLoader loader;
    private Configuration configuration;
    private static ShipperAgent instance;
    /**
     * All metrics.server.modules loaded by this server
     */
    private Map<Class, Module> modules = new LinkedHashMap<Class, Module>();

    /**
     * Returns a singleton instance of XMPPServer.
     *
     * @return an instance.
     */
    public static ShipperAgent getInstance() {
        return instance;
    }

    public ShipperAgent() {
        // We may only have one instance of the server running on the JVM
        if (instance != null) {
            throw new IllegalStateException("A agent is already running");
        }
        instance = this;
        start();
    }

    public void start() {
        try {
            initialize();


            // First load all the metrics.server.modules so that metrics.server.modules may access other metrics.server.modules while
            // being initialized
        loadModules();
//        // Initize all the metrics.server.modules
        initModules();
//        // Start all the metrics.server.modules
        startModules();

            worker();
            startDate = new Date();
            stopDate = null;
            logger.info("Shipper Agent is started.");
        } catch (Exception e) {
            logger.error("startup.error",e);
            //System.out.println(("startup.error"));
            shutdownServer();
        }


    }

    public Configuration getConfiguration(){
        return configuration;
    }
    private void loadModules() {
        // Load this module always last since we don't want to start listening for clients
        // before the rest of the metrics.server.modules have been started
        //System.out.println(RabbitMqConnectionManagerImpl.class.getName());
        loadModule(RabbitMqConnectionManagerImpl.class.getName(), new RabbitMqConnectionManagerModule());
        // Keep a reference to the internal component manager
        //componentManager = getComponentManager();
    }

    /**
     * Loads a module.
     *
     * @param module the name of the class that implements the Module interface.
     */
    private void loadModule(String module) {
        try {
            Class modClass = loader.loadClass(module);
            Module mod = (Module) modClass.newInstance();
            this.modules.put(modClass, mod);
        }
        catch (Exception e) {
            e.printStackTrace();
            //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
        }
    }

    private void loadModule(String module, AbstractModule abstractModule){
        try {
            Class modClass = loader.loadClass(module);
            Injector injector = Guice.createInjector(abstractModule);
            Module mod = (Module) injector.getInstance(modClass);
            this.modules.put(modClass, mod);
        }
        catch (Exception e) {
            e.printStackTrace();
            //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
        }
    }


    private void initModules() {
        for (Module module : modules.values()) {
            boolean isInitialized = false;
            try {
                module.initialize(this);
                isInitialized = true;
            }
            catch (Exception e) {
                e.printStackTrace();
                // Remove the failed initialized module
                this.modules.remove(module.getClass());
                if (isInitialized) {
                    module.stop();
                    module.destroy();
                }
                //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
            }
        }
    }

    /**
     * <p>Following the loading and initialization of all the metrics.server.modules
     * this method is called to iterate through the known metrics.server.modules and
     * start them.</p>
     */
    private void startModules() {
        for (Module module : modules.values()) {
            boolean started = false;
            try {
                module.start();
            }
            catch (Exception e) {
                if (started && module != null) {
                    module.stop();
                    module.destroy();
                }
                //Log.error(LocaleUtils.getLocalizedString("admin.error"), e);
            }
        }
    }
    public void worker(){
        checkNotNull(configuration.getInputList(),"configuration cannot be null");
        for(IInput input : configuration.getInputList()){
            input.register();
        }
    }
    private void shutdownServer() {
//        // Notify server listeners that the server is about to be stopped
//        for (XMPPServerListener listener : listeners) {
//            listener.serverStopping();
//        }
//        // Shutdown the task engine.
//        TaskEngine.getInstance().shutdown();
//
//        // If we don't have metrics.server.modules then the server has already been shutdown
        if (modules.isEmpty()) {
            return;
        }
        // Get all metrics.server.modules and stop and destroy them
        for (Module module : modules.values()) {
            module.stop();
            module.destroy();
        }
        modules.clear();
//        // Stop all plugins
//        if (pluginManager != null) {
//            pluginManager.shutdown();
//        }
//        // Stop the Db connection manager.
//        DbConnectionManager.destroyConnectionProvider();
//        // hack to allow safe stopping
//        Log.info("Openfire stopped");
        logger.info("Shipper Agent is stopped");
    }

    public void stop() {
        shutdownServer();
        stopDate = new Date();
        Thread shutdownThread = new ShutdownThread();
        shutdownThread.setDaemon(true);
        shutdownThread.start();
    }

    public void initialize() throws IOException {
        version = new Version(2, 0, 0, Version.ReleaseStatus.Release, 1);
        loader = Thread.currentThread().getContextClassLoader();

        URL configurationURL = this.getClass().getClassLoader().getResource("configuration.json");
        try {
            configuration = Utils.objectMapper.readValue(configurationURL,Configuration.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        Utils.configuration = configuration;
        initialized = true;
    }

    public ShipperAgentInfo getAgentInfo() {
        if (!initialized) {
            throw new IllegalStateException("Not initialized yet");
        }
        return new ShipperAgentInfoImpl(name, version, startDate, stopDate, getConnectionManager());
    }

    private class ShutdownThread extends Thread {

        /**
         * <p>Shuts down the JVM after a 5 second delay.</p>
         */
        public void run() {
            try {
                Thread.sleep(5000);
                // No matter what, we make sure it's dead
                System.exit(0);
            } catch (InterruptedException e) {
                // Ignore.
            }

        }
    }


    /**
     * Returns the <code>ConnectionManager</code> registered with this server. The
     * <code>ConnectionManager</code> was registered with the server as a module while starting up
     * the server.
     *
     * @return the <code>ConnectionManager</code> registered with this server.
     */
    public IAmqpConnectionManager getConnectionManager() {
        return (IAmqpConnectionManager) modules.get(RabbitMqConnectionManagerImpl.class);
    }

}
