package metrics.logreplayer.io;

import com.google.common.util.concurrent.Monitor;

/**
 * Created by ali on 6/22/14.
 */

public class LogReplayerMain {
    static final ClassLoader loader = LogReplayerMain.class.getClassLoader();
    static final boolean isShutDownRequested = false;
    public static void main(String[] args){
        LogReplayer logReplayer = new LogReplayer();
        Monitor monitor = new Monitor();

        Monitor.Guard isShutDownGuard = new Monitor.Guard(monitor) {
            @Override
            public boolean isSatisfied() {
                return isShutDownRequested;
            }
        };
        try {
            monitor.enterWhen(isShutDownGuard);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
