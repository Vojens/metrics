package metrics.logreplayer.io;

import com.fasterxml.jackson.annotation.JsonProperty;
import metrics.logreplayer.io.io.AbstractSerialization;

/**
 * Created by ali on 6/22/14.
 */
public class Log extends AbstractSerialization{
    private String sourcePath;
    private String targetPath;
    private String contentPattern;
    private Long randomSleep;

    public Log(){

    }

    public Log(Log log){
        this.setSourcePath(log.getSourcePath());
        this.setTargetPath(log.getTargetPath());
        this.setContentPattern(log.getContentPattern());
        this.setRandomSleep(log.getRandomSleep());
    }

    @Override
    public ISerialization deepCopy() {
        return null;
    }

    @JsonProperty("SourcePath")
    public String getSourcePath() {
        return sourcePath;
    }

    @JsonProperty("SourcePath")
    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;

    }

    @JsonProperty("TargetPath")
    public String getTargetPath() {
        return targetPath;
    }

    @JsonProperty("TargetPath")
    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    @JsonProperty("ContentPattern")
    public String getContentPattern() {
        return contentPattern;
    }

    @JsonProperty("ContentPattern")
    public void setContentPattern(String contentPattern) {
        this.contentPattern = contentPattern;
    }

    @JsonProperty("RandomSleep")
    public Long getRandomSleep() {
        return randomSleep;
    }

    @JsonProperty("RandomSleep")
    public void setRandomSleep(Long randomSleep) {
        this.randomSleep = randomSleep;
    }
}
