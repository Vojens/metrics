package metrics.logreplayer.io;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * Created by ali on 6/17/14.
 */
public interface ISerialization {
    Map<String, Object> additionalProperties = Maps.newHashMap();
    Map<String, Object> getAdditionalProperties();
    void setAdditionalProperty(String name, Object value);

    void setName(String name);
    String getName();

    ISerialization deepCopy();
}
