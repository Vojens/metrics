package metrics.logreplayer.io;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Lists;
import metrics.logreplayer.io.io.AbstractSerialization;

import java.util.List;

/**
 * Created by ali on 6/22/14.
 */
public class Configuration extends AbstractSerialization{
    @JsonIgnore
    private List<Log> logList = Lists.newLinkedList();

    public Configuration(){

    }

    public Configuration(Configuration configuration){
        this.setLogList(configuration.getLogList());
    }


    @Override
    @JsonIgnore
    public ISerialization deepCopy() {
        return new Configuration(this);
    }

    @JsonProperty("Logs")
    public List<Log> getLogList() {
        return logList;
    }

    @JsonProperty("Logs")
    public void setLogList(List<Log> logList) {
        this.logList = logList;
    }
}
