package metrics.logreplayer.io;

import metrics.logreplayer.io.util.Utils;
import metrics.logreplayer.io.util.Version;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ali on 6/22/14.
 */
public class LogReplayer {
    private Logger logger = org.slf4j.LoggerFactory.getLogger(LogReplayer.class);
    private Version version;
    private boolean initialized = false;
    private String name;
    private Date startDate;
    private Date stopDate;
    private ClassLoader loader;
    private Configuration configuration;
    private ExecutorService executorService;
    public LogReplayer(){
        start();
    }
    public void start(){
        try {
            initialize();
        } catch (IOException e) {
            e.printStackTrace();
        }
        worker();
    }

    public void worker(){
        for(Log log : configuration.getLogList()){
            Callable logHandler = new LogHandler(log);
            executorService.submit(logHandler);
        }
    }

    public void initialize() throws IOException {
        version = new Version(2, 0, 0, Version.ReleaseStatus.Release, 1);
        loader = Thread.currentThread().getContextClassLoader();

        URL configurationURL = this.getClass().getClassLoader().getResource("configuration.json");
        try {
            configuration = Utils.objectMapper.readValue(configurationURL,Configuration.class);
            this.executorService = Executors.newFixedThreadPool(configuration.getLogList().size());
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
        Utils.configuration = configuration;
        initialized = true;
    }
}
