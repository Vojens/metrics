package metrics.logreplayer.io;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by ali on 6/17/14.
 */
public abstract class AbstractSerialization implements ISerialization{
    protected String name;

    protected AbstractSerialization(AbstractSerialization io){
        this.setName(io.getName());
        for (Map.Entry<String, Object> entry : io.getAdditionalProperties().entrySet())
        {
            this.setAdditionalProperty(entry.getKey(),entry.getValue());
        }
    }

    protected AbstractSerialization() {
        super();
    }

    @JsonAnyGetter
    @Override
    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    @JsonAnySetter
    @Override
    public void setAdditionalProperty(String name, Object value) {
        additionalProperties.put(name, value);
    }

    @JsonProperty("Name")
    @Override
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    @Override
    public void setName(String name) {
        this.name = name;
    }
}