package metrics.logreplayer.io;

import metrics.logreplayer.io.util.Utils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.*;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ali on 6/22/14.
 */
public class LogHandler extends Log implements Runnable, Callable{

    private Pattern regexPattern;

    public LogHandler(Log log) {
        super(log);
        this.regexPattern = Pattern.compile(Utils.grokDictionary.digestExpression(getContentPattern()));
    }

    @Override
    public void run() {
       worker();

    }

    private void worker(){
        BufferedReader br = null;
        BufferedWriter bw = null;

        String line;
        try {
            br = new BufferedReader(new FileReader(getSourcePath()));
            bw = new BufferedWriter(new FileWriter(getTargetPath()));
            while ((line = br.readLine()) != null) {

                try{
                    Matcher matcher = regexPattern.matcher(line);
                    //boolean matches = matcher.matches();
                    String outStr = null;
                    if(matcher.matches()){
                        DateTimeFormatter formatter = ISODateTimeFormat.dateTime();
                        outStr = matcher.replaceFirst(formatter.print(DateTime.now())+", ");
                    }else{
                        outStr = line;
                    }
                    bw.write(outStr);
                    bw.flush();
//                    while(matcher.find()) {
//                        System.out.println("found: " + matcher.group(1));
//                    }
//                    String replaced = matcher.replaceFirst("Hello");
//
//                    //String matchergroup = matcher.group();
//                    String date = matcher.group("date");
//                    if(date != null && !date.isEmpty()){
//
//                    }
                    Thread.sleep((long)(Math.random()*getRandomSleep()));
                }catch(Exception e){
                    e.printStackTrace();
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(br != null){
                    br.close();
                }
                if(bw != null){
                    bw.close();
                }
            } catch (IOException e) {
            }
        }
    }

    @Override
    public Object call() throws Exception {
        worker();
        return null;
    }
}
