package metrics.logreplayer.io.util;

import org.aicer.grok.dictionary.GrokDictionary;

import java.io.Serializable;

/**
 * Created by ali on 6/12/14.
 */
public class GrokDictionarySingleton implements Serializable {
    private static final long serialVersionUID = 1L;
    private GrokDictionary grokDictionary;
    private GrokDictionarySingleton() {
        // private constructor
        grokDictionary = new GrokDictionary();
        // Load the built-in dictionaries
        grokDictionary.addBuiltInDictionaries();

        //grokDictionary.addDictionary();   //TODO add the config dictionary file
        // Resolve all expressions loaded
        grokDictionary.bind();
    }

    private static class GrokDictionarySingletonHolder {
        public static final GrokDictionary INSTANCE = new GrokDictionarySingleton().grokDictionary;
    }

    public static GrokDictionary getInstance() {
        return GrokDictionarySingletonHolder.INSTANCE;
    }

    protected Object readResolve() {
        return getInstance();
    }
}
