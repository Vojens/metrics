package metrics.logreplayer.io.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import metrics.logreplayer.io.Configuration;
import org.aicer.grok.dictionary.GrokDictionary;

/**
 * Created by ali on 6/4/14.
 */
public class Utils {
    public final static ObjectMapper objectMapper = ObjectMapperSingleton.getInstance();
    public static Configuration configuration;
    public final static GrokDictionary grokDictionary = GrokDictionarySingleton.getInstance();
}
